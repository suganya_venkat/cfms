class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class NoDataReadError(Error):
    """Use for any process that requires populated data. If the consuming process can handle empty input then this exception is not
       needed."""
    def __init__(self, user_message):
        
        self.title = "No Data to Read Error!! "  
        self.message = user_message
        
class NoDataWriteError(Error):
    """Use for any process that requires populated data. If the consuming process can handle empty input then this exception is not
       needed."""
    def __init__(self, user_message):
        
        self.title = "No Data to Write Error!! "  
        self.message = user_message
        
class UnexpectedInputError(Error):
  
    def __init__(self, user_message):
        
        self.title = "Unexpected Input Data or Type!! "  
        self.message = user_message
        
class AutomatedTestError(Error):
  
   def __init__(self, user_message):
      
        self.title = "Automated Unit Test Error!! "
        self.message = user_message
        
class EnvironmentValidationError(Error):
  
   def __init__(self, user_message):
      
        self.title = "Environment is Not Validated!! "
        self.message = user_message