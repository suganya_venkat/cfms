import pandas as pd
import numpy as np
import sqlite3
import utils
import sql_queries
import pdb

config_data_dict = utils.load_app_config_dict()


def combined_tables():
    cfms_db_path = config_data_dict['CFMS_DB_PATH']
    cfms_connection, cursor = utils.connect2sqlite3(cfms_db_path)
    
    driver_db_path = config_data_dict['DRIVER_DB_PATH']
    driver_connection, cursor = utils.connect2sqlite3(driver_db_path)

    # Get RC Buckets
    rc_bucket_df = pd.read_sql(sql_queries.sql_rc_buckets, cfms_connection)
    rc_bucket_df = rc_bucket_df[rc_bucket_df['IdField'] == 'ID-1'].copy()
    
    # Raw Data
    raw_df = pd.read_sql(sql_queries.sql_fetch_all_claims, driver_connection)
    
    # Investigation / Asisgnments
    investigation_feedback_df = pd.read_sql(sql_queries.sql_investigation_feedback, cfms_connection)
    
    # Weighted Scores
    weighted_scores_db_path = config_data_dict['CFMS_VIEWS_DB_PATH']
    cfms_views_connection, _ = utils.connect2sqlite3(weighted_scores_db_path)
    weighted_scores_df = pd.read_sql(sql_queries.sql_weighted_scores, cfms_views_connection)
    
    # Combine
    investigation_feedback_df['investigated'] = np.where(investigation_feedback_df['InvestigationStatusId']=='I0', 'Y', 'N')
    raw_df.rename(columns={'CLAIMREPORTDATE': 'claim_report_date', 
                          'CLAIM_AMT_USD': 'claim_amt_usd',
                          }, inplace=True)    
    weighted_scores_df.rename(columns={'Decile': 'severity'}, inplace=True)
    investigation_feedback_df.rename(columns={'UserName': 'assigned_resource'}, inplace=True)
    
    all_combined_df = raw_df.merge(rc_bucket_df, left_on=['claim_number'], right_on=['IdFieldValue'], how='left')\
                  .merge(investigation_feedback_df, left_on=['IdFieldValue'], right_on=['IdValue'], how='left')\
                  .merge(weighted_scores_df, left_on=['IdFieldValue'], right_on=['IDFieldValue'], how='left')
    
    all_combined_df['investigated'] = np.where(all_combined_df['investigated'].isna(), "N", all_combined_df['investigated'])
    
    
    return all_combined_df


def create_alert_page_data():
    
    all_combined_df = combined_tables()
    
    pdb.set_trace()
    
    combined_df = all_combined_df[['claim_number', 'investigated', 'claim_report_date', 'claim_amt_usd', 'severity', 
                              'assigned_resource', 'rc_bucket']].copy()
    
    alert_screen_json = dict()
    rcs = combined_df['rc_bucket'].unique()
    for rc in rcs:
        chunk_df = combined_df[combined_df['rc_bucket'] == rc].copy()
        alert_screen_json[rc] = utils.create_table_json(chunk_df)
        
    req_assigned_user_info = all_combined_df[['claim_number', 'severity', 'investigated', 'claim_report_date', 'claim_amt_usd', 
                                             'CLAIM_STATUS', 'assigned_resource']].copy()
    req_assigned_user_info.rename(columns={'severity': 'score_grp',
                                           'CLAIM_STATUS': 'claim_status', 
                                           'assigned_resource': 'assigned_to' }, inplace=True)
    user_info_df = req_assigned_user_info.groupby(['assigned_to', 'claim_status']).count().reset_index()
    user_info_json = utils.create_table_json(user_info_df)
    
    response_dict = {}
    for k in alert_screen_json.keys():
        response_dict[k] = eval(alert_screen_json[k])['data']
    
    response_dict['resources_claim_count'] = eval(user_info_json)['data']
    
    return response_dict


def create_claim_level_data(claim_num):
    driver_db_path = config_data_dict['DRIVER_DB_PATH']
    driver_connection, cursor = utils.connect2sqlite3(driver_db_path)
    
    cfms_db_path = config_data_dict['CFMS_DB_PATH']
    cfms_connection, cursor = utils.connect2sqlite3(cfms_db_path)
        
    claim_df = pd.read_sql(sql_queries.sql_claim_query.format(claim_id=claim_num) , driver_connection)
    
    req_cols = ['claim_number', 'CLAIMREPORTDATE', 'DATEOFLOSS', 'CLAIMTYPEDESC', 'address', 'pin_code']
    data = list(claim_df.loc[0, req_cols].values)
    
    comments = pd.read_sql(sql_queries.sql_comments.format(claim_id=claim_num), cfms_connection)
    assigned_users = pd.read_sql(sql_queries.sql_assigned_users.format(claim_id=claim_num), cfms_connection)
    assigned_users.drop_duplicates(subset=['IdField', 'IdValue'], inplace=True)
    
    user_comments = comments.merge(assigned_users, 
                                   left_on=['UserId', 'IdField', 'IdValue'], 
                                   right_on=['UserId', 'IdField', 'IdValue'], 
                                   how='left')
    
    user_comments = user_comments[['UserName', 'Comments', 'insert_ts']].rename(columns={'UserName': 'user_name',
                                                                                        'Comments': 'comment'})    
    claim_details_json = dict()
    claim_details_json['claim specific details'] = dict(zip(req_cols, [str(s) for s in data]))
    claim_details_json['comments'] = eval(utils.create_table_json(comments))['data']
        
    return claim_details_json


# use this method to add comments specific to claims
def add_comments_to_claims(u_id, claim_number, txt):
    chunk_df = pd.DataFrame({"IdField": ['ID-1'],
                            "IdValue": [claim_number],
                             "UserId": [u_id],
                             "Comments": [txt],
                             "RID": [datetime.today()]
                            })
        
    try:
        cfms_con, _ = utils.connect2sqlite3(config_data_dict['CFMS_DB_PATH'])
        chunk_df.to_sql(name='Comments', con=cfms_con, if_exists='append', index=False)
        return {"response": "SUCCESS"}
    except:
        return {"response": "FAILURE"}
    

def get_model_outputs():
    # Model Indicators
    cfms_db_path = config_data_dict['CFMS_DB_PATH']
    cfms_connection, cursor = utils.connect2sqlite3(cfms_db_path)
    model_indicators_df = pd.read_sql(sql_queries.sql_model_indicators, cfms_connection)

    m_ind_feat_id_val_df = model_indicators_df.groupby(['IdField', 'IdFieldValue', 'ModelId', 'RunId'])[
['ModelFeatureName', 'ModelFeatureValue'] ].apply(lambda x: x.set_index('ModelFeatureName').T ).reset_index()
    m_ind_feat_imp_df = model_indicators_df.groupby(['IdField', 'IdFieldValue', 'ModelId', 'RunId'])[
    ['ModelFeatureName', 'ModelFeatureImportance'] ].apply(lambda x: x.set_index('ModelFeatureName').T ).reset_index()

    m_ind_df = pd.merge(m_ind_feat_id_val_df, m_ind_feat_imp_df, 
    on=['IdField', 'IdFieldValue', 'ModelId', 'RunId'], suffixes=('', '_FeatImp'), 
    how='left')
    
    pdb.set_trace()
    


if __name__=="__main__":
    result = create_alert_page_data()
#     result = create_claim_level_data(claim_num='V4301451')
#     result = get_model_outputs()
    print(result)