import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)

import matplotlib.pyplot as plt
from my_auto_claims_code import lift, plot_lift_chart
import numpy as np
import pandas as pd
import os, json
# import scikitplot as skplt
import scikitplot as skplt
from sqlite_db import create_db_connection, write_to_table
from my_auto_claims_code import create_table_json

from IPython.display import display, HTML
from pyod.models.iforest import IForest
from sklearn.metrics import roc_auc_score, accuracy_score, precision_score, recall_score
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
import pandas as pd


# data_impute = pd.read_excel(r"D:\CFMS_updated_code\cfms\backend\python_code\spreadsheet_files\auto_anomaly.xlsx")
connection, cursor = create_db_connection('db_files/input_data.db')
data_impute = pd.read_sql('SELECT * FROM input_claims_anomaly_data', connection)
connection.close()


def create_lift_chart(y_test, y_pred, show_plot=False):
    model_metrics = lift(y_test, y_pred, 10)
    lift_df = plot_lift_chart(model_metrics, show_plot)
    return lift_df


def iso_forest(model_params, scale_params, in_data, contamination, show_graph=False, feat_imp=False):
    data = in_data.copy()

    # data[scale_params] = preprocessing.normalize(data[scale_params])
    # data[scale_params] = preprocessing.scale(data[scale_params])

    X_train, X_test = train_test_split(data, test_size=.2, random_state=42)

    scaler = preprocessing.StandardScaler()
    scaler.fit(X_train[scale_params])
    X_train[scale_params] = scaler.transform(X_train[scale_params])
    X_test[scale_params] = scaler.transform(X_test[scale_params])

    model1 = IForest(verbose=0, contamination=contamination, bootstrap=True, random_state=42)

    model1.fit(X_train[model_params])

    # outlier scores

    x_train_scores = model1.decision_scores_
    x_train_scores = pd.Series(x_train_scores)

    threshold = model1.threshold_

    # Predict anomaly scores

    x_test_scores = model1.decision_function(X_test[model_params])
    x_test_scores = pd.Series(x_test_scores)

    probs_test = model1.predict_proba(X_test[model_params])
    probs_test = pd.DataFrame(probs_test)

    probs_train = model1.predict_proba(X_train[model_params])
    probs_train = pd.DataFrame(probs_train)

    X_train[scale_params] - scaler.inverse_transform(X_train[scale_params])
    X_test[scale_params] - scaler.inverse_transform(X_test[scale_params])

    df_test = X_test.copy().reset_index()
    df_test['score'] = x_test_scores
    df_test.loc[:, 'probability'] = pd.Series(probs_test.iloc[:, 1])
    df_test['cluster'] = np.where(df_test['score'] < threshold, False, True)

    print('ROC AUC: ', roc_auc_score(df_test['fraud'], df_test['score']))
    print('Accuracy: ', accuracy_score(df_test['fraud'], df_test['cluster']))
    print('Precision: ', precision_score(df_test['fraud'], df_test['cluster']))
    print('Recall: ', recall_score(df_test['fraud'], df_test['cluster']))

    lift_df = create_lift_chart(df_test['fraud'], probs_test.values)

    if show_graph:
        fig = plt.figure(figsize=(15, 10))
        ax1 = plt.subplot(1, 2, 1)
        skplt.metrics.plot_confusion_matrix(df_test['fraud'], df_test['cluster'], ax=ax1)
        ax2 = plt.subplot(1, 2, 2)
        skplt.metrics.plot_roc(df_test['fraud'], probs_test, ax=ax2)
        plt.savefig('prod_model_artifacts/anomaly/roc_auc.jpg')
        plt.show()

        fig = plt.figure(figsize=(15, 10))
        ax1 = plt.subplot(1, 2, 1)
        skplt.metrics.plot_lift_curve(df_test['fraud'], probs_test, ax=ax1)
        plt.title('Lift Curve for Test Data')
        ax2 = plt.subplot(1, 2, 2)
        skplt.metrics.plot_lift_curve(X_train['fraud'], model1.predict_proba(X_train[model_params]), ax=ax2)
        plt.title('Lift Curve for Train Data')
        plt.show()

    probs_anom = pd.concat([probs_train, probs_test]).values[:, 0]
    clm_num = pd.concat([X_train.claim_number, X_test.claim_number])

    data = {'claim_id': clm_num, 'proba_score': probs_anom}
    score_df = pd.DataFrame(data)

    feat_imp_df = RFE_FeatImp(X_train[model_params], X_train['fraud'], X_test[model_params],
                              X_test['fraud'], model1, cont=0)
    save_to_json(feat_imp_df, lift_df, contamination)
    return score_df


def save_to_json(shap_df, lift_df, cnt_val):
    model_params = dict()
    model_params['name'] = 'Isolation Forest'
    model_params['contamination'] = cnt_val
    if not os.path.isdir('prod_model_artifacts'):
        os.mkdir('prod_model_artifacts')
    model_params['Shap -feature importance'] = create_table_json(shap_df)
    model_params['Lift Chart'] = create_table_json(lift_df)
    with open('prod_model_artifacts/anomaly/model_params.json', 'w') as outfile:
        json.dump(model_params, outfile)