import pandas as pd
import networkx as nx
import json
import sql_queries
import utils
import re
import EntityFraudRate
import pdb

class GraphNetwork:
    
    def __init__(self, ):
        self.app_config_dict = utils.load_app_config_dict()
        self.driver_con, self.driver_cursor = utils.connect2sqlite3(self.app_config_dict['DRIVER_DB_PATH'])
        self.G = None
    
    def load_full_graph(self, ):
        try:
            print("Loaded the Graph DB pickle")
            self.G = nx.read_gpickle(self.app_config_dict['GRAPH_DB_PICKLE_PATH'])
            return True
        except:
            print("Loading failed, trying building the graph")
            try:
                self.G = self.build_graph()
                print("Graph built")
            except:
                print("Error building Graph")
                return False
    
    def build_graph(self, claim_number):
        parti_df = pd.read_sql(sql_queries.sql_fetch_all_related_participants.format(claim_number=claim_number), 
                               self.driver_con)
        
        parti_frd_rate = EntityFraudRate.compute_entity_fraud_rates()
        parti_df['FraudRate'] = parti_df['ParticipantId'].map(parti_frd_rate) 
        
        # Nodes-Edge List
        G = nx.Graph()
        for i, row in parti_df.iterrows():
            name = str(row['Name'])
            telephone = str(row['Telephone'])
            zipcode = str(row['Zipcode'])
            email = str(row['Email'])
            parti_id = str(row['ParticipantId'])
            clm_number = str(row['claim_number'])
            role = str(row['Role'])
            frd_rate = str(row['FraudRate'])
            
            frd_flag = parti_frd_rate[clm_number]
            
            # Nodes
            G.add_node(clm_number, info_email=email, info_telephone=telephone, label='claim_number', info_frd_flag=frd_flag)
            G.add_node(parti_id, label=role, info_email=email, info_name=name, info_zipcode=zipcode, info_telephone=telephone,
                      info_frd_rate=frd_rate)
            G.add_edge(clm_number, parti_id, label=role)
            
        self.G = G
        print("Graph Built using Participants data")

    def add_data_to_graph(self, ):
        pass

    
    
class Node:
    def __init__(self, node_id, name, value, link=None, children=None, info_name=None, info_frd_rate=None, info_frd_flag=None, root_node=False):
        self.node_id = node_id
        self.name = name
        self.value = value
        self.root_node = root_node
        if link is None:
            self.link = []
        else:
            self.link = link
        if children is None:
            self.children = []
        else:
            self.children = children
        if info_name is not None:
            self.info_name = info_name
        else:
            self.info_name = ""
        if info_frd_rate is not None:
            self.info_frd_rate = info_frd_rate
        else:
            self.info_frd_rate = 0
            
        if info_frd_flag is not None:
            self.info_frd_flag = info_frd_flag
        else:
            self.info_frd_flag = 0
        
        self.visited = []
        
        
class CreateNode:
    def __init__(self, G):
        self.visited = []
        self.G = G
        
    def compute_subgraph(self, n):
        # Connected components based on given node n
        self.all_connected_nodes = list(nx.node_connected_component(self.G, n))
        self.subgraph = self.G.subgraph(self.all_connected_nodes)
        self.subgraph_nodes_data = dict(eval(self.subgraph.nodes.data().__str__()))
        
    def _name(self, n):
        return self.subgraph_nodes_data[n]['label']
    
    def _info_name(self, n):
        if "info_name" in self.subgraph_nodes_data[n]:
            return self.subgraph_nodes_data[n]['info_name']
        else:
            return ""
    
    def _info_frd_rate(self, n):
        if "info_frd_rate" in self.subgraph_nodes_data[n]:
            return self.subgraph_nodes_data[n]['info_frd_rate']
        else:
            return 0
    
    def _info_frd_flag(self, n):
        if "info_frd_flag" in self.subgraph_nodes_data[n]:
            return self.subgraph_nodes_data[n]['info_frd_flag']
        else:
            return 0
    
    def _id(self, n):
        return n
    
    def _value(self, n):
        if self._name(n) != 'claim_number':
            return "1"
        else:
            return "10"
    
    def isleaf(self, n):
        if self.G.degree(n) == 1:
            return True
        else:
            return False
                    
    def run(self, node_obj):
        neighbors_list = self.G.neighbors(node_obj.node_id)
        self.visited.append(node_obj.node_id)
        for neighbor in neighbors_list:
            if neighbor not in self.visited:
                self.visited.append(neighbor)
                neighbor_node_obj = Node(node_id=self._id(neighbor), 
                                         name=self._name(neighbor), 
                                         value=self._value(neighbor),
                                        info_name=self._info_name(neighbor),
                                         info_frd_rate=self._info_frd_rate(neighbor),
                                         info_frd_flag=self._info_frd_flag(neighbor)
                                        )
                if self.isleaf(neighbor):
                    node_obj.children.extend([neighbor_node_obj])
                else:
                    node_obj.children.extend([neighbor_node_obj])
                    self.run(neighbor_node_obj)
            else:
                node_obj.link.extend([neighbor])
                
def node_frd_color(node):
    if node.root_node:
        return "blue"
    if node.name == "claim_number":
        if node.info_frd_flag:
            return "pink"
        return "cyan"
    if float(node.info_frd_rate.replace("%", "")) > 0:
        return "pink"
    else:
        return "lime"
                
def node_json(node):
    if len(node.children) == 0:
        req_json = {"id": node.node_id, "name": node.name, "value": node.value, "link": node.link, "collapsed": True}
        if len(node.link) == 0:
            req_json.pop("link")
        
        if node.info_name:
            req_json['info_name'] = node.info_name
            
        req_json['info_frd_rate'] = node.info_frd_rate
        req_json['info_color'] = node_frd_color(node)
        
        if node_frd_color(node) == "pink" and node.name == 'claim_number':
            req_json['info_frd_rate'] = "Fraud"
            
        if node.root_node:
            req_json['info_frd_rate'] = ""
            
        
        return req_json
    else:
        req_json = {"id": node.node_id, "name": node.name, "value": node.value, "link": node.link, "collapsed": True,
                    "children": [node_json(c) for c in node.children] }
        if len(node.link) == 0:
            req_json.pop("link")
            
        if node.info_name:
            req_json['info_name'] = node.info_name
            
        req_json['info_frd_rate'] = node.info_frd_rate
        req_json['info_color'] = node_frd_color(node)
        
        if node_frd_color(node) == "pink" and node.name == 'claim_number':
            req_json['info_frd_rate'] = "Fraud"
            
        if node.root_node:
            req_json['info_frd_rate'] = ""
            
        return req_json
    
    
def create_graph_json(claim_number):
    
    # Load Subgraph relevant to given claim number
    graph_obj = GraphNetwork()
    graph_obj.build_graph(claim_number=claim_number)
        
    # Node Obj
    node_create_obj = CreateNode(G=graph_obj.G)
    node_create_obj.compute_subgraph(n=claim_number)
    
    root_node = Node(node_id=f"{claim_number}", 
                    name="claim_number",
                    value="10",
                    children=None,
                    root_node=True)
    
    node_create_obj.run(root_node)
    
    result = node_json(root_node)
    
    return result


if __name__=="__main__":
    
#     claim_number = "V4302239"
    """
    Sample Claims 'V4292279', 'V4308167', 'V4305077', 'V4304256', 'V4302505', 'V4297952', 'V4303956', 'V4306301', 'V4299741', 'V4302665', 'V4297942', 'V4307035', 'V4299283', 'V4303435', 'V4303428', 'V4306299', 'V4304368', 'V4305533', 'V4259670', 'V4302928', 'V4271508', 'V4302693', 'V4299787', 'V4302504', 'V4303402', 'V4299831', 'V4298698', 'V4302245', 'V4301516', 'V4303629', 'V4304530'
    """
    claim_number =  "V4302488" #"V4304256"
    result = create_graph_json(claim_number)
    print(json.dumps(result))
    