import numpy as np
import pandas as pd
import json
import sqlite3
import utils
import pdb
import os

config_data_dict = utils.load_app_config_dict()

def create_alert_page_data():
    claims_data_db_path = config_data_dict['CLAIMS_DATA_DB_PATH']
    connection, cursor = utils.connect2sqlite3(claims_data_db_path)
    rc_query = 'SELECT DISTINCT rc_bucket FROM predictions_reason_codes'
    alert_screen_json = dict()
    rcs = pd.read_sql(rc_query, connection).rc_bucket.values
    for rc in rcs:
        query = "SELECT p.claim_number, p.investigated, p.claim_report_date, p.claim_amt_usd, " \
                "ws.score_grp as severity, tu.user_name as assigned_resource, rc.rc_bucket " \
                "FROM predictions p LEFT JOIN predictions_reason_codes rc ON p.claim_number = rc.claim_number " \
                "LEFT JOIN tool_users tu ON p.assigned_to = tu.user_id " \
                "LEFT JOIN claims_weighted_score ws ON p.claim_number = ws.claim_id " \
                "WHERE rc.rc_bucket = '%s' ORDER BY p.score_grp DESC" % rc
        df = pd.read_sql(query, connection)
        alert_screen_json[rc] = utils.create_table_json(df)
        
    # populating users info
    count_query = 'SELECT user_name, claim_status, COUNT(*) AS count FROM predictions ' \
                  'LEFT JOIN tool_users ON assigned_to = user_id ' \
                  'GROUP BY assigned_to, claim_status '
    user_info_df = pd.read_sql(count_query, connection)
    user_info_json = utils.create_table_json(user_info_df)
    
    response_dict = {}
    for k in alert_screen_json.keys():
        response_dict[k] = eval(alert_screen_json[k])['data']
    
    response_dict['resources_claim_count'] = eval(user_info_json)['data']
    
    return response_dict


def create_claim_level_data(claim_num):
    input_data_db_path = config_data_dict['INP_DATA_DB_PATH']
    connection, cursor = utils.connect2sqlite3(input_data_db_path)
    
    claim_query = "SELECT * FROM input_claims_data WHERE claim_number = '%s' " % claim_num
    claim_df = pd.read_sql(claim_query, connection)

    req_cols = ['claim_number', 'CLAIMREPORTDATE', 'DATEOFLOSS', 'CLAIMTYPEDESC', 'address',
                'pin_code']
    data = list(claim_df.loc[0, req_cols].values)
    
    conn, cur = utils.connect2sqlite3(config_data_dict['CLAIMS_DATA_DB_PATH'])
    comment_query = "SELECT user_name, comment, insert_ts FROM user_comments " \
                    "LEFT JOIN tool_users ON user_comments.user_id = tool_users.user_id " \
                    "WHERE claim_number = '%s' " % claim_num
    comments = pd.read_sql(comment_query, conn)
    
    claim_details_json = dict()
    claim_details_json['claim specific details'] = dict(zip(req_cols, [str(s) for s in data]))
    claim_details_json['comments'] = eval(utils.create_table_json(comments))['data']
    
    return claim_details_json


# use this method to add comments specific to claims
def add_comments_to_claims(u_id, claim_number, txt):
    try:
        connection, cursor = utils.connect2sqlite3(config_data_dict['CLAIMS_DATA_DB_PATH'])
        insert_query = 'INSERT INTO user_comments (user_id, claim_number, comment) VALUES (?, ?, ?)'
        cursor.execute(insert_query, (u_id, claim_number, txt))
        connection.commit()
        return {"response": "SUCCESS"}
    except:
        return {"response": "FAILURE"}


def create_score_group_based_on_model_weights(w_struct_model=0.5, w_anom_model=0.5):
    if (w_struct_model > 1.0) or (w_struct_model < 0):
        raise ValueError('Model weight cannot exceed one or less than zero')
    if w_anom_model != (1 - w_struct_model):
        w_anom_model = (1 - w_struct_model)
    # access proba scores
    connection, cursor = utils.connect2sqlite3(config_data_dict['CLAIMS_DATA_DB_PATH'])
    struct_proba = pd.read_sql('SELECT * FROM claims_structured_score', connection)
    anomaly_proba = pd.read_sql('SELECT * FROM claims_anomaly_score', connection)
    anomaly_proba = anomaly_proba.set_index('claim_id').loc[struct_proba.claim_id].reset_index()
    weighted_proba = struct_proba.proba_score * w_struct_model + anomaly_proba.proba_score * w_anom_model
    weighted_df = struct_proba.copy()
    weighted_df.proba_score = weighted_proba
    weighted_df['score_grp'] = pd.qcut(weighted_proba.rank(method='first'), 10, labels=False) + 1
    utils.write_to_table(connection, weighted_df, 'claims_weighted_score', clear_existing_data=True)
    
    
if __name__=="__main__":
    
    # Test: Create Alert Page Data
#     result = create_alert_page_data()
    
    # Test: create_claim_level_data
    result = create_claim_level_data(claim_num='V4300552')
    
    
    pdb.set_trace()
    
