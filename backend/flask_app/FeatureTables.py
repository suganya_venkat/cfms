"""
Defines the transformations to create a derived features table
Subset of Driver Data --> Derived Features
"""
import pandas as pd
import base_transforms
import base_feature_pipeline


def create_table_claim_level_features_auto_siu_model(inp_df, **args):
    """
    Features at Claim Number Level for Auto SIU Model.

    Parameters
    ----------
    inp_df : pd.DataFrame, Input Raw DataFrame
    args: Additional arguments needed for transforms
    
    Returns
    -------
    result : pd.DataFrame, Features dataframe at Claim Number Level
    
    """
    
    pipe = base_feature_pipeline.FeaturePipeline()
    
    pipe.add_transformation(on_cols=[('LossDate', 'PolicyExpiry')], 
                            transformation_f=base_transforms.base_transforms_days_between,
                            transformation_args=None,
                             names=['Diff_LossDt_PolExpDt'] 
                             )
    
    pipe.add_transformation(on_cols=[('Diff_LossDt_PolExpDt', )], 
                            transformation_f=base_transforms.base_transforms_range_indicator,
                            transformation_args={"lower":-np.inf,
                                             "upper":0,
                                             "lower_include": False,
                                             "upper_include": False},
                             names=['end_passed_ldate_after'] 
                             )
    
    pipe.add_transformation(on_cols=[('Occupation', )], 
                            transformation_f=base_transforms.base_transforms_isin_check,
                            transformation_args={"value_list": ["swasta bank", "swasta"]},
                             names=['occu_swasta_blank'] 
                             )
    
    pipe.add_transformation(on_cols=[('XXX', )], 
                            transformation_f=base_transforms.XXX,
                            transformation_args=None,
                             names=['rush_hour'] 
                             )
    
    pipe.add_transformation(on_cols=[('Risk Sum Assured', )], 
                            transformation_f=base_transforms.XXX,
                            transformation_args=None,
                             names=['dam_gt50_risksum'] 
                             )
    
    result = pipe.apply_transformations(df, index_cols=['ClaimNumber'])
    
    return {"features": result, "features_pipeline": pipe}



def create_table_prior_features_auto_siu_model(inp_df, **args):
    """
    Features at Claim Number & Prior Claim Number Level for Auto SIU Model.
    
    TODO: Add transforms for required prior features
    
    Parameters
    ----------
    inp_df : pd.DataFrame, Input Raw DataFrame at ClaimNumber & PriorClaimNumber level
    args: Additional arguments needed for transforms
    
    Returns
    -------
    result : pd.DataFrame, Features dataframe at Claim Number level
    
    """
    pipe = base_feature_pipeline.FeaturePipeline()
    
    
def create_dummy_derived_features(inp_df):
    feat_df = pd.read_excel("./databases/dummy_features.xlsx")
    return feat_df
    
    