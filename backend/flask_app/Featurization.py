import pandas as pd
import FeatureTables
import utils
import sql_queries
from datetime import datetime


# Load app configurations

app_config_dict = utils.load_app_config_dict()

# Connect to Driver Table
driver_con, driver_cursor = utils.connect2sqlite3(app_config_dict['DRIVER_DB_PATH']) 
cfms_con, cfms_cursor = utils.connect2sqlite3(app_config_dict['CFMS_DB_PATH']) 

# Load Raw Driver data
raw_claims_df = pd.read_sql(sql_queries.sql_fetch_all_claims, driver_con)

# Get Derived Features Table
feat_df = FeatureTables.create_dummy_derived_features(raw_claims_df)

features_list = app_config_dict['AUTO_SIU_MODEL']['FEATURES']

melt_df = pd.melt(feat_df, 
                 id_vars=['claim_number'], 
                 value_vars=features_list, 
                 var_name=None, 
                 value_name='value', 
                 col_level=None, ignore_index=True)

melt_df['IdField'] = 'ID-1'

# Get Feature Name to Feature ID mapping

feat_name_to_id_map_df = pd.read_sql(sql_queries.sql_feat_inventory, cfms_con).drop_duplicates()
feature2feat_id_map = feat_name_to_id_map_df.set_index(['FeatureDescription']).to_dict()['FeatureID']

melt_df['FeatureID'] = melt_df['variable'].map(feature2feat_id_map)
melt_df.rename(columns={'value': 'FeatureValue', 'claim_number': 'IdValue'}, inplace=True)
melt_df['RID'] = datetime.today()
derived_features_df = melt_df[['IdField', 'IdValue', 'FeatureID', 'FeatureValue', 'RID']].copy()

derived_features_df.to_sql(name='Derived_Features', con=cfms_con, if_exists='replace', index=False)

print("Featurization Done!")

