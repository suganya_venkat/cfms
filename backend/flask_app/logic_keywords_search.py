# Standard Python Libs
import pandas as pd
import numpy as np

# External Python Libs
from flashtext import KeywordProcessor


class KeywordSearch:
    
    def __init__(self, pos_keywords_list, neg_keywords_list=None, **kp_args):
        
        self.pos_keywords_list = pos_keywords_list
        self.neg_keywords_list = neg_keywords_list
        
        # Instantiate Keyword Processor 
        self.kp = KeywordProcessor(**kp_args)
        self.kp.add_keywords_from_list(self.pos_keywords_list)
        
    def compute_check_if_text_has_keywords(self, text):
        
        result = 1 * (len(self.kp.extract_keywords(text)) > 0)
        
        return result
    
    def apply(self, ser):
        
        result = ser.apply(self.compute_check_if_text_has_keywords)
        
        return result
        
        
        
if __name__ == '__main__':
    
    pos_keywords_list = ['corona', 'infection']
    series = pd.Series(['coronary', 'corona', 'infected', 'infection spread'])
    obj = KeywordSearch(pos_keywords_list=pos_keywords_list)
    
    result = obj.apply(series)
    
    print('input: ', series.tolist())
    print('result', result.tolist())
    
    
    
    
    