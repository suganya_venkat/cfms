"""
Excrypting passwords
"""

import sys
import argparse
import logging
from Crypto.Cipher import AES
import base64
import random
import string

out_filepath = 'encrypted.txt'

def main():
    
    pw = input("password = ")
    print("You entered password = ", pw)
    #pw = FLAGS.password.rjust(32)
    pw = pw.rjust(32)
    
    key = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(32))
    
    cipher = AES.new(key.encode(), AES.MODE_ECB)   # b"random1234_123456789_123456789_1"
    encoded = base64.b64encode(cipher.encrypt(pw.encode()))
    
    print("Key:")
    print(key)
    
    print("Encrpyted Password:")
    print(encoded.decode())

    file = open(out_filepath,'w')
    file.write("Key: " + key + '\n')
    file.write("Encrpyted Password: " + encoded.decode())
    file.close()
    
if __name__ == "__main__":
    main()