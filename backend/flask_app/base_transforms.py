"""
Reusable transforms methods - underlying computation logics
"""

import pandas as pd
import numpy as np
from datetime import datetime


def base_transforms_days_between(ser1, ser2):
    """
    Computes Difference between Dates in Days.

    Parameters
    ----------
    ser1 : 'From' Date Series.
    ser2 : 'To' Date Series
    
    Returns
    -------
    result : Series, Difference between ser1 & ser2
    
    """

    ser1 = ser1.fillna('nan')
    ser2 = ser2.fillna('nan')

    s1 = pd.to_datetime(ser1.astype(str))
    s2 = pd.to_datetime(ser2.astype(str))
    result = (s2 - s1).dt.days
    
    return result


def base_transforms_range_indicator(ser1, lower, upper, lower_include=False, upper_include=False):
    """
    Indicator if given value in series is between given range of lower & upper values.

    Parameters
    ----------
    ser1 : pd.Series
    
    Returns
    -------
    result : pd.Series
    
    """
    
    assert lower<upper, 'Lower value must be less than Upper value'
    
    if lower_include and upper_include:
        s = pd.Series(np.where( (ser1 >= lower) & (ser1 <= upper) , 1, 0) )
        s.index = ser1.index
        return s
    elif lower_include and (not upper_include):
        s = pd.Series(np.where( (ser1 >= lower) & (ser1 < upper) , 1, 0) )
        s.index = ser1.index
        return s
    elif (not lower_include) and upper_include:
        s = pd.Series(np.where( (ser1 > lower) & (ser1 <= upper) , 1, 0) )
        s.index = ser1.index
        return s
    elif (not lower_include) and (not upper_include):
        s = pd.Series(np.where( (ser1 > lower) & (ser1 < upper) , 1, 0) )
        s.index = ser1.index
        return s
    else:
        raise Exception('Uknown operation')
        
        
def base_transforms_isin_check(ser1, value_list):
    """
    Indicator if any of the given string values are present.

    Parameters
    ----------
    ser1 : pd.Series
    value_list : List of string values to check
    
    Returns
    -------
    result : pd.Series
    
    """
    
    ser1 = ser1.fillna("")
    ser1 = ser1.astype(str).str.strip().str.lower()
    
    value_list = [str(v).strip().lower() for v in value_list]
    
    result = ser1.apply(lambda x: 1*(x in value_list) )
        
    return result


def base_transforms_keep_column(ser1):
    """
    Add Series in transformed dataframe
    
    ser1: Series
    """
    
    return ser1


def base_transforms_above_threshold_indicator(ser1, threshold, include=False):
    """
    Indicator if given value in series is above given threshold
    
    ser1: pd.Series
    """
    if include:
        s = pd.Series(np.where(ser1 >= threshold, 1, 0))
        s.index = ser1.index
        return s
    else:
        s = pd.Series(np.where(ser1 > threshold, 1, 0))
        s.index = ser1.index
        return s
    
    
def base_transforms_create_indicators_from_list(ser1, ind_list):
    """
    Matches the keywords given in indicators list in ser1 
    and flags it if present
    """
    ser1 = ser1.astype(str).str.strip().str.lower()    
    ser_list = []
    for ind in ind_list:
        ind = str(ind).strip().lower()
        ser = pd.Series(np.where(ser1 == ind, 1, 0))
        ser_list.append(ser)
    result = pd.concat(ser_list, axis=1)
    return result


def base_transforms_series_string_join(*series, sep=' '):
    """
    Joins the values in series in single text string
    series : List of Series
    sep: (str) Separator
    """
    for i, ser in enumerate(series):
        if i==0:
            result = ser.astype(str).str.strip() + sep
        else:
            result += result.astype(str).str.strip() + sep
        
    result = result.str.strip()
    return result


def base_transforms_map_values_from_dict(ser1, key_val_dict, fill_unmapped=np.nan):
    """
    Maps values in the series to corresponding values in dictionary
    
    ser1: pd.Series
    """
    
    ser1 = ser1.astype(str).str.strip().str.lower()
    key_val_dict = {str(k).strip().lower(): v for k,v in key_val_dict.items() }
    
    result = ser1.map(key_val_dict)
    
    if fill_unmapped:
        result.fillna(fill_unmapped, inplace=True)
         
    return result


def base_transforms_rollup_result(ser1, ser2):
    """
    Rolls up the results to original dataframe levels
    Used in case of prior computation where original dataframe 
    is at ClaimNumber & ClaimNumber_Prior level & computed results 
    are at ClaimNumber Level 
    
    ser1 : Input dataframe index series
    ser2 : results series/dataframe
    """
    df1 = ser1.reset_index()[ser1.index.names]
    df1 = df1.astype(str)
    df2 = ser2.reset_index()

    df2[ser2.index.name] = df2[ser2.index.name].astype(str)
    
    if isinstance(ser2, pd.Series):
        result = pd.merge(df1, df2, on=[ser2.index.name], how='left').set_index(ser1.index.names)[0]
    elif isinstance(ser2, pd.DataFrame):
        result = pd.merge(df1, df2, on=[ser2.index.name], how='left').set_index(ser1.index.names)
    else:
        raise Exception('result must be a series or datafrme')
        
    return result


def base_transforms_scale_score_bwn_range(ser1, r_min, r_max, t_min, t_max=100, eps = 1e-4):
    """
    Scaling the raw scores to given range values
    :param m: Measurement to be scaled
    :param r_min: minimum of measurement range
    :param r_max: maximum of measurement range
    :param t_min: minimum of target range
    :param t_max: maximum of target range
    """
    
    scaled_ser = []
    
    for m in ser1.tolist():
        if abs(r_max - r_min) < eps:
            scaled_m = t_max
        else:
            scaled_m = ((m - r_min)/ (r_max - r_min))*(t_max - t_min) + t_min
        
        scaled_ser.append(m)
        
    scaled_ser = pd.Series(scaled_ser)
    scaled_ser.index = ser1.index

    return scaled_ser






def base_transforms_isin_check_invert(ser1, value_list):
    """
    Difference between Dates in Days
    
    ser1: Series
    value_list: Check if values in value list present in ser1   
    """
    
    ser1 = ser1.fillna("")
    ser1 = ser1.astype(str).str.strip().str.lower()
    
    value_list = [str(v).strip().lower() for v in value_list]
    
    result = ser1.apply(lambda x: 1*(x not in value_list) )
        
    return result



        
        
        
def base_transforms_min_val(ser1):
    """
    Get Minimum Value for each row/list of series
    
    ser1: Series containing list of values in each row
    """
    result = ser1.apply(lambda x: min(x) if x != [] else np.nan)
        
    return result


def base_transforms_shift_scale(ser1, a, b):
    """
    shift values by a & scale by b
    
    for x in ser1 --> a + bx
    
    ser1: Series containing list of values in each row
    """
    result = a + b*ser1
    
    return result
    
def base_transforms_day_of_week(ser1):
    """
    Day of the week
    
    ser1: Date Series
    """
    
    ser1 = pd.to_datetime(ser1)
    
    result = ser1.dt.dayofweek
    
    return result


def base_transforms_text_search(ser1, keywords):
    """
    Search given keywords in the text series
    
    ser1: Text Series eg. Loss Description
    keywords: List of Keywords to search
    """
    
    keywords_to_search = list(set([str(k).strip().lower() for k in keywords]))
    
    import logic_keywords_search
    
    obj = logic_keywords_search.KeywordSearch(pos_keywords_list=keywords_to_search)
    
    ser1 = ser1.astype(str).str.strip().str.lower()
    
    result = obj.apply(ser1)
    
    return result


def base_transforms_create_indicators_from_keywords_label_map(ser1, label_keywords_map_dict, clean_text=True, clean_search_text=True):
    """
    Search given list of keywords & map them to their corresponding label
    
    ser1: text searies for search
    """
    import base_text_mining
    from flashtext import KeywordProcessor
    
    s = ser1.astype(str).str.strip().str.lower()
    
    if clean_text:
        txt_obj = base_text_mining.TextClean()
        s = s.apply(lambda x: txt_obj.cleaner1(x))
        
    if clean_search_text:
        txt_obj = base_text_mining.TextClean()
            
    label_keywords_map_dict2 = {}
    for k, v in label_keywords_map_dict.items():
        if clean_search_text:
            label_keywords_map_dict2[str(k).strip().lower()] = list(set([txt_obj.cleaner1(x) for x in v]).union(set(v)) )
        else:
            label_keywords_map_dict2[str(k).strip().lower()] = v
        
    kp_dict = {}
    
    indicators_list = []
    for k, v in label_keywords_map_dict2.items():
        kp = KeywordProcessor()
        keyword_dict = {}
        keyword_dict[k] = v
        kp.add_keywords_from_dict(keyword_dict)
        ind_ser = s.apply(lambda x: 1 * (len(kp.extract_keywords(x)) > 0) )
        ind_ser.name = k
        indicators_list.append(ind_ser)
        
    result = pd.concat(indicators_list, axis=1)
    
    return result


def base_transforms_text_cleaner1(ser1,):
    """
    Search given list of keywords & map them to their corresponding label
    
    ser1: text searies for search
    """
    import base_text_mining
        
    s = ser1.astype(str).str.strip().str.lower()
    
    txt_obj = base_text_mining.TextClean()
    result = s.apply(lambda x: txt_obj.cleaner1(x))
    
    return result


def base_transforms_or_operation(*series):
    """
    Joins the values in series in single text string
    series : List of Series
    sep: (str) Separator
    """
    
    result = 1* (pd.concat(series, axis=1).sum(axis=1) > 0)
    
    return result


def base_transforms_prior_agg_sum(ser1, ser2, prior_days=3*365, fillna_val=0):
    """
    Indicator for high prior loss paid
    
    ser1: PriorTimeDuration
    ser2: TotalLossPaid_Prior (In USD/ Same base Currency)
    """
    ser1 = ser1.astype(float)
    ser2 = ser2.astype(float)
    s_df = pd.concat([ser1, ser2], axis=1)
    s_df.columns = [0, 1]
    
    prior_df = s_df[s_df[0] < prior_days].copy()
    
    if prior_df.shape[0] == 0:
        return pd.Series(np.zeros(len(ser1)), index=ser1.index)
    
    s1 = prior_df[0]
    s2 = prior_df[1]
    
    # Aggregate sum at Claim Number Level
    s3 = s2.groupby(level=0, sort=False).sum()
    s3.name = 0
        
    result = base_transforms_rollup_result(ser1, s3)
    
    if fillna_val == 0:
        result.fillna(0, inplace=True)
    elif fillna_val is None:
        result = result.copy()
    else:
        result = result.fillna(fillna_val, inplace=True)
    
    return result


def base_transforms_check_equal(ser1, value):
    """
    Difference between Dates in Days
    
    ser1: Series
    value: Value to Check   
    """
    
    result = 1 * (ser1 == value)
        
    return result


def base_transforms_ratio(ser1, ser2, fillinf_val=0, fillna_val=None):
    """
    Difference between Dates in Days
    
    ser1: Series
    ser2: 
    """
    
    result = 1 * (ser1/ ser2)
    
    result = result.replace(np.inf, fillinf_val)
    
    if fillna_val is not None:
        result = result.fillna(fillna_val)
        
    return result



def base_transforms_agg_uniq_vals(ser1, val_agg_df, key_col, uniq_val_col):
    """
    Aggregate values at ClaimNumber Level
    
    ser1: Index Series
    val_agg_df: Dataframe from where aggregation of values to be done
    key_col: Key Column Name
    uniq_val_col: Column for which unique values needed
    """
    
    key_uniq_ser = val_agg_df.groupby([key_col])[uniq_val_col].agg(lambda x: get_uniq_vals(x) ).to_dict()
    
    result = ser1.map(key_uniq_ser).apply(lambda x: [] if check_na(x) else x )
    
    return result



def base_transforms_agg_as_indicators(ser1, val_agg_df, key_col, agg_col, map_dict=None):
    """
    Aggregate values at ClaimNumber Level
    
    ser1: Index Series
    val_agg_df: Dataframe from where aggregation of values to be done
    key_col: Key Column Name
    agg_col: Column for which indicators are to be created
    map_dict: Maps Values from agg_col and then creates indicators
    """
    val_agg_df = val_agg_df.copy()
    
    if map_dict:
        val_agg_df[agg_col] = val_agg_df[agg_col].map(map_dict)
        
    val_agg_df.dropna(subset=[agg_col], inplace=True)
    
    if val_agg_df.shape[0] == 0:
        return pd.Series(np.zeros(ser1.__len__()) )
        
    uniq_vals = list(set(map_dict.values())) 
    ind_list = []
    for col in uniq_vals:
        val_agg_df[col] = np.where( val_agg_df[agg_col] == col, 1, 0 )
        r = ser1.map( (1 * (val_agg_df.groupby(key_col)[col].sum() > 0)).to_dict()  ).fillna(0)
        r.name = col
        ind_list.append(r)
        
    result = pd.concat(ind_list, axis=1)
    
    return result


def base_transforms_get_dt_year(ser1):
    """
    Extract Year value from Date Series
    
    ser1: Date Series
    """
    
    ser1 = pd.to_datetime(ser1.astype(str))
    
    result = ser1.dt.year
    
    return result




def base_transforms_prior_claim_order(ser1):
    """
    Computes Prior Claim Count
    
    ser1: PriorTimeDuration
    n_year_prior: last n year prior claim count
    """
    ss1 = ser1.astype(float)
        
    if ss1.shape[0] == 0:
        return pd.Series(np.zeros(len(ser1)), index=ser1.index)
    
    k1 = ss1.index.names[0]
    k2 = ss1.index.names[1]
    
    result = ss1.groupby(level=0, sort=False).rank(method='dense')    
    
    return result


def base_transforms_prior_order1_same_indicator(ser1, ser2, ser3, ser4, prior_days=3*365):
    """
    Indicator to check if current claim has value 1 & prior claim has value 1
    
    ser1: PriorTimeDuration
    ser2: PriorClaimOrder
    ser3: Prior value column (Indicator 0/1)
    ser4: Current Value Column (Indicators 0/1)
    """
    
    ser1 = ser1.astype(float)
    
    s_df = pd.concat([ser1, ser2, ser3, ser4], axis=1)
    s_df.columns = [0, 1, 2, 3]
    
    prior_df = s_df[(s_df[0] < prior_days) & (s_df[1] == 1)].copy()
    
    if prior_df.shape[0] == 0:
        return pd.Series(np.zeros(len(ser1)), index=ser1.index)
    
    k1 = ser1.index.names[0]
    k2 = ser1.index.names[1]
    
    curr_and_prior_present = prior_df[3] * prior_df[2]
    
    r = curr_and_prior_present.reset_index()
    r.columns = [k1, k2, 'flag']
    
    r = 1 * (r.groupby(k1)['flag'].sum() > 0)
    
    r.name = 0  
    r.index.name = k1
        
    result = base_transforms_rollup_result(ser1, r)
    
    result.fillna(0, inplace=True)
    
    return result


def base_transforms_prior_order1_same_val_check_from_list(ser1, ser2, ser3, ser4, checklist, 
                                                          prior_days=3*365):
    """
    Indicator to check if current claim has value 1 & prior claim has value 1
    
    ser1: PriorTimeDuration
    ser2: PriorClaimOrder
    ser3: Prior value column (Indicator 0/1)
    ser4: Current Value Column (Indicators 0/1)
    """
    
    ser1 = ser1.astype(float)
    
    s_df = pd.concat([ser1, ser2, ser3, ser4], axis=1)
    s_df.columns = [0, 1, 2, 3]
    
    prior_df = s_df[(s_df[0] < prior_days) & (s_df[1] == 1)].copy()
    
    if prior_df.shape[0] == 0:
        return pd.Series(np.zeros(len(ser1)), index=ser1.index)
    
    k1 = ser1.index.names[0]
    k2 = ser1.index.names[1]
    
    prior_present = 1 * (prior_df[2].isin(checklist))
    curr_present = 1 * (prior_df[3].isin(checklist))
    keep_these = curr_present * prior_present 
    
    curr_and_prior_same = 1 * (prior_df[3] == prior_df[2])
    
    r = keep_these * curr_and_prior_same
    
    r = r.reset_index()
    r.columns = [k1, k2, 'flag']
    
    r = 1 * (r.groupby(k1)['flag'].sum() > 0)
    
    r.name = 0  
    r.index.name = k1
        
    result = base_transforms_rollup_result(ser1, r)
    
    result.fillna(0, inplace=True)
    
    return result


def base_transforms_check_non_zero_values_for_all(ser1, ser2):
    """
    Check if both input series has non zero values
    """
    
    result = 1 * ((ser1 > 0) & (ser2 > 0))
    return result










