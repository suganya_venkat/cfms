import pandas as pd
from pyod.models.iforest import IForest
import xgboost
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import tree

# local dependencies
import evaluate


def train_and_evaluate_iso_model(train, test, features, target_col='fraud', args_dict={}):
    X_train = train.copy()
    X_test = test.copy()
    
    X_train = X_train.rename(columns={target_col: 'fraud'})
    X_test = X_test.rename(columns={target_col: 'fraud'})

    model = IForest(**args_dict)

    model.fit(X_train[features])

    #outlier scores
    
    x_train_scores = model.decision_scores_
    x_train_scores = pd.Series(x_train_scores)

    threshold = model.threshold_

    #Predict anomaly scores

    x_test_scores = model.decision_function(X_test[features])
    x_test_scores = pd.Series(x_test_scores)
    
    probs = model.predict_proba(X_test[features])
    probs = pd.DataFrame(probs)

    df_test = X_test.copy()
    df_test.reset_index(drop=True, inplace=True)
    df_test['score'] = x_test_scores.tolist()
    df_test.loc[:, 'probability'] = probs[1].tolist()
    

    lift_chart_test = evaluate.lift_chart(df_test['probability'], df_test['fraud'] )
      
    probs = model.predict_proba(X_train[features])
    probs = pd.DataFrame(probs)

    df_train = X_train.copy().reset_index(drop=True)
    df_train['score'] = x_train_scores.tolist()
    df_train.loc[:, 'probability'] = probs[1].tolist()
    
    lift_chart_train = evaluate.lift_chart(df_train['probability'], df_train['fraud'] )
    
    return model, lift_chart_test, lift_chart_train 




def train_and_evaluate_xgb_model(train, test, features, target_col='fraud', args_dict={}):
    X_train = train.copy()
    X_test = test.copy()
    
    X_train = X_train.rename(columns={target_col: 'fraud'})
    X_test = X_test.rename(columns={target_col: 'fraud'})

    model = xgboost.XGBClassifier(**args_dict)
    
    model.fit(X_train[features].copy(), X_train['fraud'].tolist() )   

    #Predict anomaly scores
    
    probs = pd.DataFrame(model.predict_proba(X_test[features]))[1]
    
    df_test = X_test.copy()
    df_test.reset_index(drop=True, inplace=True)
    df_test['probability'] = probs.tolist()

    lift_chart_test = evaluate.lift_chart(df_test['probability'], df_test['fraud'] )
      
    probs = pd.DataFrame(model.predict_proba(X_train[features]))[1]

    df_train = X_train.copy().reset_index(drop=True)
    df_train['probability'] = probs.tolist()
    
    lift_chart_train = evaluate.lift_chart(df_train['probability'], df_train['fraud'] )
    
    return model, lift_chart_test, lift_chart_train 


def train_and_evaluate_logreg_model(train, test, features, target_col='fraud', solver='liblinear'):
    X_train = train.copy()
    X_test = test.copy()
    
    X_train = X_train.rename(columns={target_col: 'fraud'})
    X_test = X_test.rename(columns={target_col: 'fraud'})

    model = LogisticRegression(solver=solver)
    
    model.fit(X_train[features].copy(), X_train['fraud'].tolist() )   

    #Predict anomaly scores
    
    probs = pd.DataFrame(model.predict_proba(X_test[features]))[1]
    
    df_test = X_test.copy()
    df_test.reset_index(drop=True, inplace=True)
    df_test['probability'] = probs.tolist()

    lift_chart_test = evaluate.lift_chart(df_test['probability'], df_test['fraud'] )
      
    probs = pd.DataFrame(model.predict_proba(X_train[features]))[1]

    df_train = X_train.copy().reset_index(drop=True)
    df_train['probability'] = probs.tolist()
    
    lift_chart_train = evaluate.lift_chart(df_train['probability'], df_train['fraud'] )
    
    return model, lift_chart_test, lift_chart_train 


def train_and_evaluate_knn_model(train, test, features, target_col='fraud', args_dict={}):
    X_train = train.copy()
    X_test = test.copy()
    
    X_train = X_train.rename(columns={target_col: 'fraud'})
    X_test = X_test.rename(columns={target_col: 'fraud'})

    model = KNeighborsClassifier(**args_dict)
    
    model.fit(X_train[features].copy(), X_train['fraud'].tolist() )   

    #Predict anomaly scores
    
    probs = pd.DataFrame(model.predict_proba(X_test[features]))[1]
    
    df_test = X_test.copy()
    df_test.reset_index(drop=True, inplace=True)
    df_test['probability'] = probs.tolist()

    lift_chart_test = evaluate.lift_chart(df_test['probability'], df_test['fraud'] )
      
    probs = pd.DataFrame(model.predict_proba(X_train[features]))[1]

    df_train = X_train.copy().reset_index(drop=True)
    df_train['probability'] = probs.tolist()
    
    lift_chart_train = evaluate.lift_chart(df_train['probability'], df_train['fraud'] )
    
    return model, lift_chart_test, lift_chart_train 


def train_and_evaluate_dtree_model(train, test, features, target_col='fraud', args_dict={}):
    X_train = train.copy()
    X_test = test.copy()
    
    X_train = X_train.rename(columns={target_col: 'fraud'})
    X_test = X_test.rename(columns={target_col: 'fraud'})

    model = tree.DecisionTreeClassifier(**args_dict)
    
    model.fit(X_train[features].copy(), X_train['fraud'].tolist() )   

    #Predict anomaly scores
    
    probs = pd.DataFrame(model.predict_proba(X_test[features]))[1]
    
    df_test = X_test.copy()
    df_test.reset_index(drop=True, inplace=True)
    df_test['probability'] = probs.tolist()

    lift_chart_test = evaluate.lift_chart(df_test['probability'], df_test['fraud'] )
      
    probs = pd.DataFrame(model.predict_proba(X_train[features]))[1]

    df_train = X_train.copy().reset_index(drop=True)
    df_train['probability'] = probs.tolist()
    
    lift_chart_train = evaluate.lift_chart(df_train['probability'], df_train['fraud'] )
    
    return model, lift_chart_test, lift_chart_train 



