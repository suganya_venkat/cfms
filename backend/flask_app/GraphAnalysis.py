import pandas as pd
import networkx as nx
import json
from itertools import chain, combinations
import sql_queries
import utils
import re
import pdb


class GraphNetwork:
    
    def __init__(self, ):
        self.app_config_dict = utils.load_app_config_dict()
        self.driver_con, self.driver_cursor = utils.connect2sqlite3(self.app_config_dict['DRIVER_DB_PATH'])
        self.G = None
    
    def load_full_graph(self, ):
        try:
            print("Loaded the Graph DB pickle")
            self.G = nx.read_gpickle(self.app_config_dict['GRAPH_DB_PICKLE_PATH'])
            return True
        except:
            print("Loading failed, trying building the graph")
            try:
                self.G = self.build_graph()
                print("Graph built")
            except:
                print("Error building Graph")
                return False
    
    def build_graph(self, claim_number):
        parti_df = pd.read_sql(sql_queries.sql_fetch_all_related_participants.format(claim_number=claim_number), 
                               self.driver_con)
        # Nodes-Edge List
        G = nx.Graph()
        for i, row in parti_df.iterrows():
            name = str(row['Name'])
            telephone = str(row['Telephone'])
            zipcode = str(row['Zipcode'])
            email = str(row['Email'])
            parti_id = str(row['ParticipantId'])
            clm_number = str(row['claim_number'])
            role = str(row['Role'])

            # Nodes
            G.add_node(clm_number, info_email=email, info_telephone=telephone, label='claim_number')
            G.add_node(parti_id, label=role, info_email=email, info_name=name, info_zipcode=zipcode, info_telephone=telephone)
            G.add_edge(clm_number, parti_id, label=role)
            
        self.G = G
        print("Graph Built using Participants data")

    def add_data_to_graph(self, ):
        pass
    

class Subgraph(GraphNetwork):
    
    def __init__(self ):
        super().__init__()
        self.G = None
        self.all_connected_nodes = None
        self.subgraph = None
        self.subgraph_nodes_data = None
        self._connected_claim_nodes = None
        
    def compute_subgraph(self, n):
        self.build_graph(claim_number=n)
        # Connected components based on given node n
        self.all_connected_nodes = list(nx.node_connected_component(self.G, n))
        self.subgraph = self.G.subgraph(self.all_connected_nodes)
        self.subgraph_nodes_data = dict(eval(self.subgraph.nodes.data().__str__()))
        
    def _connected_claims(self, n):
        connected_claim_nodes = []
        for node in self._neighbours(n):
            if self._label(node) == 'claim_number':
                connected_claim_nodes.append(node)
        return connected_claim_nodes
    
    def _neighbours(self, n):
        return list(self.G.neighbors(n))
    
    def _label(self, n):
        return self.subgraph_nodes_data[n]['label']
    
    def _info_dict(self, n):
        return self.subgraph_nodes_data[n]
    
    def _node_base_dict(self, n, value=10):
        if self._label(n) != "claim_number":
            info_name = self._info_dict(n)['info_name']
            info_name = re.sub(r'[^a-zA-Z]', ' ', info_name).title().replace("Under Construction", "").strip()
            return {"id": n, "name": self._label(n), "value": "1", "info_name": info_name }
        else:
            return {"id": n, "name": self._label(n), "value": "10"}
        
    def create_graph_json(self, n):
        
        self.compute_subgraph(n)
        
        graph_json = {}
        for k, v in self.subgraph_nodes_data.items():
            graph_json[k] = self._node_base_dict(n=k)
            if self._label(k) != "claim_number":
                links = list(set(self._connected_claims(n=k)))
                if len(links) != 0:
                    graph_json[k]['link'] = links
            
        return graph_json
    

if __name__=="__main__":   

    root_node = 'V4302239'

    obj = Subgraph()
    graph_json = obj.create_graph_json(n=root_node)
    
    print(json.dumps(list(graph_json.values())))
    
    
