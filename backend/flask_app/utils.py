import os
import sqlite3
import yaml
import datetime
import json
import numpy as np
import pandas as pd
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt
import joblib
from base64 import b64encode, b64decode
from io import BytesIO
import base64


def load_app_config_dict(config_filepath=None):
    if config_filepath is None:
        try:
            config_filepath = "./configs/app_config.yaml"
            config_dict = yaml.load(open(config_filepath), Loader=yaml.FullLoader)
        except:
            config_filepath = os.getcwd() + "/flask_app/configs/app_config.yaml"
            config_dict = yaml.load(open(config_filepath), Loader=yaml.FullLoader)
    else:
        config_dict = yaml.load(open(config_filepath), Loader=yaml.FullLoader)
        
    return config_dict

def create_db_connection(config_dict=None):
    if config_dict is None:
        app_config_dict = load_app_config_dict()
    else:
        app_config_dict = config_dict
        
    dbpath = app_config_dict['RAW_DATA_DB_PATH']
    connection = sqlite3.connect(dbpath, check_same_thread=False)
    cursor = connection.cursor()
    return connection, cursor


def connect2sqlite3(dbpath):
    connection = sqlite3.connect(dbpath, check_same_thread=False)
    cursor = connection.cursor()
    return connection, cursor


def print_and_log(logger, msg):
    print(msg)
    if logger is not None:
        logger.log(msg)
        
        
def query_tuple(l):
    """
    Convert a list of items to sql query compatible tuple
    :param l: List if items
    """
    l = [str(v) for v in l]
    if len(l) == 1:
        return "( '" + str(l[0]) + "' )"
    else:
        return tuple(l)

    
def scale_to_range(m, r_min, r_max, t_min, t_max=100, eps=1e-8):
    """
    Scaling the raw scores to given range values
    :param m: Measurement to be scaled
    :param r_min: minimum of measurement range
    :param r_max: maximum of measurement range
    :param t_min: minimum of target range
    :param t_max: maximum of target range
    """
    if abs(r_max - r_min) < eps:
        scaled_m = t_max
    else:
        scaled_m = ((m - r_min)/ (r_max - r_min))*(t_max - t_min) + t_min

    return scaled_m

def convert_bool_to_numeric(x):
    if x:
        return 1
    else:
        return 0


def create_column_based_on_date_range(df):
    start_date = datetime.date(2021, 1, 1)
    end_date = datetime.date(2021, 12, 31)
    time_between_dates = end_date - start_date
    days_between_dates = time_between_dates.days
    df['days'] = np.random.randint(days_between_dates, size=df.shape[0])
    claim_date = df['days'].apply(lambda x: create_date(start_date, x))
    df.insert(loc=1, column='claim_date', value=claim_date)
    df = df.drop(columns=['days'])
    return df


def create_date(start_date, days_between_dates):
    random_date = start_date + datetime.timedelta(days=days_between_dates)
    return random_date


def train_data_range(df, retrain=False):
    min_date, max_date = df['claim_date'].min(), df['claim_date'].max()
    if not retrain:
        end_date = min_date + datetime.timedelta(days=int((max_date - min_date).days * 0.75))
        df = df[df['claim_date'] < end_date]
        return df
    else:
        start_date = max_date - datetime.timedelta(days=int((max_date - min_date).days * 0.75))
        df = df[df['claim_date'] > start_date]
        return df


def malaysia_whole_data_preprocess(file_path):
    """
    Preprocesses malaysia dataset

    :param file_path:
    :return: numpy.ndarray
    """

    df = pd.read_excel(file_path)
    # since the data doesn't have date columns
    df = create_column_based_on_date_range(df)

    X = df.iloc[:, 3:-1]
    y = df.iloc[:, -1]
    clm_numbers, clm_dates = df['CLAIMNUMBER'], df['claim_date']

    X['close_claim'] = X['close_claim'].apply(lambda x: convert_bool_to_numeric(x))
    X['is_self_accident'] = X['is_self_accident'].apply(lambda x: convert_bool_to_numeric(x))
    return X, clm_numbers, clm_dates, y


def malaysia_data_preprocess(file_path, retrain=False):
    """
    Preprocesses malaysia dataset

    :param file_path: str
    :param retrain: bool
    :return: tuple of numpy arrays
    """

    df = pd.read_excel(file_path)
    # since the data doesn't have date columns
    df = create_column_based_on_date_range(df)

    if not retrain:
        df = train_data_range(df)
    else:
        df = train_data_range(df, retrain=retrain)

    X = df.iloc[:, 3:-1]
    y = df.iloc[:, -1]

    X['close_claim'] = X['close_claim'].apply(lambda x: convert_bool_to_numeric(x))
    X['is_self_accident'] = X['is_self_accident'].apply(lambda x: convert_bool_to_numeric(x))

    x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.15, random_state=12)
    return x_train, x_test, y_train, y_test


def lift(test, pred, quantiles, return_only_scr_grp=False):
    res = pd.DataFrame(np.column_stack((test, pred)),
                       columns=['Target', 'PR_0', 'PR_1'])

    res['scr_grp'] = pd.qcut(res['PR_0'].rank(method='first'), quantiles, labels=False) + 1
    if return_only_scr_grp:
        return res['scr_grp']

    crt = pd.crosstab(res.scr_grp, res.Target).reset_index()
    crt = crt.rename(columns={'Target': 'Np', 0.0: 'Negatives', 1.0: 'Positives'})

    G = crt['Positives'].sum()
    B = crt['Negatives'].sum()

    avg_resp_rate = G / (G + B)

    crt['resp_rate'] = round(crt['Positives'] / (crt['Positives'] + crt['Negatives']), 2)
    crt['lift'] = round((crt['resp_rate'] / avg_resp_rate), 2)
    crt['rand_resp'] = 1 / quantiles
    crt['cmltv_p'] = round((crt['Positives']).cumsum(), 2)
    crt['cmltv_p_perc'] = round(((crt['Positives'] / G).cumsum()) * 100, 1)
    crt['cmltv_n'] = round((crt['Negatives']).cumsum(), 2)
    crt['cmltv_n_perc'] = round(((crt['Negatives'] / B).cumsum()) * 100, 1)
    crt['cmltv_rand_p_perc'] = (crt.rand_resp.cumsum()) * 100
    crt['cmltv_resp_rate'] = round(crt['cmltv_p'] / (crt['cmltv_p'] + crt['cmltv_n']), 2)
    crt['cmltv_lift'] = round(crt['cmltv_resp_rate'] / avg_resp_rate, 2)
    crt['KS'] = round(crt['cmltv_p_perc'] - crt['cmltv_rand_p_perc'], 2)
    crt = crt.drop(['rand_resp', 'cmltv_p', 'cmltv_n', ], axis=1)

    print('average response rate: ', avg_resp_rate)
    return crt


def create_table_json_v1(df, col=None):
    """
    Creates tabular json output
    :param df: pandas DataFrame or Series
    :param col: String
    :return: Dictionary
    """
    if not col:
        df.to_json('input_data_artifacts/temp.json', orient='table')
        with open('input_data_artifacts/temp.json') as json_file:
            data = json.load(json_file)
        return data
    df[col].value_counts().to_json('input_data_artifacts/temp.json', orient='table')
    with open('input_data_artifacts/temp.json') as json_file:
        data = json.load(json_file)
    return data


def create_table_json(df, col=None):
    """
    Creates tabular json output
    :param df: pandas DataFrame or Series
    :param col: String
    :return: Dictionary
    """
    if not col:
        return df.to_json(orient='table')
#         with open('input_data_artifacts/temp.json') as json_file:
#             data = json.load(json_file)
#         return data
    return df[col].value_counts().to_json(orient='table')
#     with open('input_data_artifacts/temp.json') as json_file:
#         data = json.load(json_file)
    return data


def calculate_closure_rate(df):
    df_new = df.copy()
    df_new.index = df_new.CLAIMREPORTDATE
    df_new = df_new.groupby([pd.Grouper(freq='M'),
                             'CLAIM_STATUS']).agg({'STATE': 'count'}).rename(columns={'STATE': 'COUNT'})
    df_new = df_new.reset_index()
    df_new = df_new[df_new.CLAIMREPORTDATE > '2021-01-01']
    return create_table_json(df_new)


def create_json_for_summary_screen(df):
    summary_dict = {'Claims Initiated': len(df),
                    'Claims in Progress': len(df.CLAIM_STATUS[df.CLAIM_STATUS == 'open']),
                    'Claims Closed': len(df.CLAIM_STATUS[df.CLAIM_STATUS == 'closed']),
                    'Claims Paid': {'counts': df.PAID.value_counts().to_dict(),
                                    'Amount Paid': (df[df.PAID == 'Yes'])['APPRV_AMT_USD'].sum()},
                    'Average Claim Amount': df['CLAIM_AMT_USD'].sum() / len(df),
                    'Average Paid Amount':
                        df['APPRV_AMT_USD'][df.PAID == 'Yes'].sum() / len(df['APPRV_AMT_USD'][df.PAID == 'Yes']),
                    'Closure ratio': df.CLAIM_STATUS.value_counts()['closed'].sum() / len(df),
                    'Claims by Claim Type(Histogram)': create_table_json(df, 'CLAIM_TYPE'),
                    'Claims by state (Histogram)': create_table_json(df, 'STATE'),
                    'Claimed Amount by Claim Type(Histogram)': create_table_json(df.groupby(
                        'CLAIM_TYPE').agg({'APPRV_AMT_USD': 'sum'}).rename(columns={
                        'APPRV_AMT_USD': 'TOTAL APPROVED AMOUNT'})),
                    'Claimed amount by State(Histogram)':
                        create_table_json(df.groupby('STATE').agg({'APPRV_AMT_USD': 'sum'}
                                                                  ).rename(
                            columns={'APPRV_AMT_USD': 'TOTAL APPROVED AMOUNT'})),
                    'Claims by state (Table)': create_table_json(df.groupby('STATE').agg(
                        {'APPRV_AMT_USD': 'sum', 'CLAIM_STATUS': 'count'}
                    ).rename(columns={'CLAIM_STATUS': 'Claim count'})),
                    'Claim Closure Status last 12 Months (Monthly)': calculate_closure_rate(df)}
    if not os.path.isdir('input_data_artifacts'):
        os.mkdir('input_data_artifacts')
    with open('input_data_artifacts/data_summary_params.json', 'w') as outfile:
        json.dump(summary_dict, outfile)


class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return super(NpEncoder, self).default(obj)


def create_json_for_Investigation_screen(df):
    summary_dict = dict()
    summary_dict['Claims Initiated'] = df.shape[0]
    summary_dict['Total Investigated'] = df.Investigated.value_counts()['Y']
    df_inv = df[df['Investigated'] == 'Y']
    summary_dict['Total Closed'] = df_inv.CLAIM_STATUS.value_counts()['closed']
    summary_dict['Paid Claims'] = df_inv.PAID.value_counts()['Yes']
    summary_dict['Rejected'] = df_inv.PAID.value_counts()['Rejected']
    summary_dict['Fraud Suspected Claims'] = df_inv.target.value_counts()[1]
    summary_dict['Investigation_rate'] = df_inv.shape[0]/df.shape[0]
    summary_dict['Fraud detection rate (in Investigated Claims)'] = df_inv.target.value_counts()[1]/df_inv.shape[0]
    df_closed = df_inv[df_inv.CLAIM_STATUS == 'closed']
    summary_dict['Payment status for Investigated claims'] = create_table_json(df_inv, 'PAID')
    summary_dict['Fraud suspected among closed claims'] = create_table_json(df_closed, 'target')
    df_new = df.groupby(['STATE', 'Investigated']).agg(count=('STATE', 'count'))
    df_new = df_new.reset_index()
    df_tot = df_new.groupby('STATE').agg({'count': 'sum'}).reset_index()
    df_tot = df_tot.rename(columns={'count': 'total_count'})
    df_new = df_new.merge(df_tot, on='STATE', how='left')
    df_new = df_new[df_new.Investigated == 'Y']
    df_new['Investigated status'] = df_new['count'] / df_new['total_count'] * 100
    summary_dict['Investigated status by states (%)'] = create_table_json(df_new[['STATE', 'Investigated status']])
    with open('input_data_artifacts/data_summary_investigated.json', 'w') as outfile:
        json.dump(summary_dict, outfile, cls=NpEncoder)


def plot_lift_chart(df, show_plot=False):
    dec = ['Qt 1', 'Qt 2', 'Qt 3', 'Qt 4', 'Qt 5', 'Qt 6', 'Qt 7', 'Qt 8', 'Qt 9', 'Qt 10']
    mlift = df[['Positives', 'Negatives', 'lift']].copy()
    mlift.index = dec
    if show_plot:
        mlift.plot(kind='bar')
        plt.xlabel('Quantiles')
        plt.ylabel('Count')
        plt.show()
    return mlift

def create_model_table(cursor):
    ct = 'CREATE TABLE IF NOT EXISTS models (id INTEGER PRIMARY KEY AUTOINCREMENT, run_id TEXT, experiment_id INT, ' \
         'prod_model_status TEXT, recall REAL, f1_score REAL, lift REAL, max_depth INT, learning_rate REAL ) '
    cursor.execute(ct)


def create_serialized_prod_model_table(cursor):
    ct = 'CREATE TABLE IF NOT EXISTS prod_models (id INTEGER PRIMARY KEY AUTOINCREMENT, run_id TEXT, ' \
         'model_ascii TEXT, exp_id INT, insert_ts DATETIME DEFAULT CURRENT_TIMESTAMP)'
    cursor.execute(ct)


def insert_one_row(cursor, val_tuple):
    insert_query = 'INSERT INTO models (run_id, experiment_id, prod_model_status, recall, f1_score,' \
                   'lift, max_depth, learning_rate) VALUES(?, ?, ?, ?, ?, ?, ?, ?)'
    cursor.execute(insert_query, val_tuple)


def create_pred_table(cursor, connection):
    ct = 'CREATE TABLE IF NOT EXISTS predictions (id INTEGER PRIMARY KEY AUTOINCREMENT, claim_number TEXT,' \
         'score_grp INT, investigated TEXT, claim_report_date DATETIME, claim_amt_usd REAL,' \
         'claim_status TEXT, assigned_to INT)'
    # delete all prior entries
    cursor.execute(ct)
    connection.commit()
    del_query = 'DELETE FROM predictions'
    cursor.execute(del_query)
    cursor.execute(ct)


def create_pred_reason_codes_table(cursor, connection):
    ct = 'CREATE TABLE IF NOT EXISTS predictions_reason_codes (id INTEGER PRIMARY KEY AUTOINCREMENT, ' \
         'claim_number INT, tppd REAL, average_claims_paid_36_cr_Medium REAL, ' \
         'prior_causeofloss REAL,  claims_in_36_cr_High REAL, notif_loss_cr_High REAL, close_claim REAL,' \
         'part_est_ratio_cr_Low REAL, part_labor_ratio_cr_Low REAL, rush_hour REAL,' \
         'tp_tpbi REAL, est_mvalue_ratio_cr_High REAL, week_time_3 REAL, ' \
         'claims_in_36_cr_Medium REAL, expire_loss_cr_Low REAL, is_self_accident REAL, rc_bucket TEXT)'
    cursor.execute(ct)
    connection.commit()
    del_query = 'DELETE FROM predictions_reason_codes'
    cursor.execute(del_query)


def write_to_table(connection, df, table_name, clear_existing_data=False):
    if clear_existing_data:
        q = 'DELETE FROM %s' % table_name
        connection.cursor().execute(q)
        connection.commit()
    df.to_sql(name=table_name, con=connection, if_exists='append', index=False)
    connection.commit()


def insert_model_in_ascii(cursor, run_id, obj, exp_id):
    encoded_obj = serialize_data(obj)
    insert_query = 'INSERT INTO prod_models (run_id, model_ascii, exp_id) VALUES (?, ?, ?)'
    cursor.execute(insert_query, (run_id, encoded_obj, exp_id))


def serialize_data(passed_object):
    with BytesIO() as tmp_bytes:
        joblib.dump(passed_object, tmp_bytes)
        encoded = b64encode(tmp_bytes.getvalue())
    return encoded


def read_model_from_table(run_id, connection):
    query = "SELECT model_ascii from prod_models WHERE run_id = '%s'" % run_id
    model_ascii = (pd.read_sql(query, connection)).loc[0, 'model_ascii']
    model = deserialize(model_ascii)
    return model


def deserialize(passed_bytes):
    decoded = passed_bytes.decode('ascii')
    bytesObj = b64decode(decoded)
    actual_data = joblib.load(BytesIO(bytesObj))

    return actual_data


def create_tool_users_table(cursor, del_contents=False):
    ct = 'CREATE TABLE IF NOT EXISTS tool_users (id INTEGER PRIMARY KEY AUTOINCREMENT, ' \
         'user_name TEXT, user_id INT)'
    cursor.execute(ct)
    # delete all prior entries
    if del_contents:
        del_query = 'DELETE FROM predictions'
        cursor.execute(del_query)


def create_user_comments_table(cursor, del_contents=False):
    ct = 'CREATE TABLE IF NOT EXISTS user_comments (id INTEGER PRIMARY KEY AUTOINCREMENT, ' \
         'user_id INT, claim_number TEXT, comment TEXT, insert_ts DATETIME DEFAULT CURRENT_TIMESTAMP)'
    cursor.execute(ct)
    if del_contents:
        del_query = 'DELETE FROM predictions'
        cursor.execute(del_query)


def create_struct_model_input_data_table(cursor):
    ct = 'CREATE TABLE IF NOT EXISTS input_claims_data (id INTEGER PRIMARY KEY AUTOINCREMENT, claim_number TEXT, ' \
         'Investigated TEXT, STATE TEXT, CLAIM_TYPE TEXT, APPRV_AMT_USD REAL, PAID REAL, pin_code INT, address TEXT' \
         'CLAIM_STATUS TEXT, CLAIM_AGE_DAYS INT,  CLAIM_AMT_USD REAL, VEHICLEMAKE TEXT, YEARMANUFACTURED TEXT,' \
         'TOTALLOSS TEXT, APPROVEDOVERALLGROSS REAL, CLAIM_STATUS TEXT,' \
         'CLAIMREPORTDATE DATE, DATEOFLOSS DATE, CLAIMTYPEDESC TEXT, end_passed_ldate_after INT, occu_swasta_blank INT,' \
         'rush_hour INT, dam_gt50_risksum INT, prior_causeofloss INT, tp_tpbi INT, tppd INT,' \
         'part_labor_ratio_cr_Low INT, part_est_ratio_cr_Low INT,' \
         'est_mvalue_ratio_cr_High INT, loss_inception_cr_Low INT,' \
         'notif_loss_cr_High INT, expire_loss_cr_Low INT,' \
         'average_claims_paid_36_cr_High INT, average_claims_paid_36_cr_Medium INT,' \
         'week_time_3 INT, claims_in_36_cr_High INT, claims_in_36_cr_Medium INT,' \
         'close_claim INT, is_self_accident INT, target INT)'
    cursor.execute(ct)


def create_anomaly_model_input_data_table(connection, cursor):
    ct = 'CREATE TABLE IF NOT EXISTS input_claims_anomaly_data (id INTEGER PRIMARY KEY AUTOINCREMENT, fraud INT,' \
         'claim_number TEXT, part_labor_ratio REAL, part_est_ratio REAL, est_mvalue_ratio REAL, loss_inception REAL,' \
         'notif_loss INT, expire_loss REAL, close_claim TEXT, average_claims_paid_36 REAL, week_time INT, claims_in_36 ' \
         'INT, is_self_accident TEXT, endorse_inc TEXT)'
    cursor.execute(ct)
    connection.commit()


def create_model_score_table(connection, cursor, model):
    if model not in ['structured', 'anomaly', 'weighted']:
        raise ValueError('Only structured & Anomaly models allowed')
    ct = 'CREATE TABLE IF NOT EXISTS claims_%s_score (id INTEGER PRIMARY KEY AUTOINCREMENT, claim_id TEXT,' \
         'proba_score REAL, score_grp INT)' % model
    cursor.execute(ct)
    connection.commit()


def base64_encoder(img_path):
    with open(img_path, "rb") as img_file:
        my_string = base64.b64encode(img_file.read())
    return my_string

