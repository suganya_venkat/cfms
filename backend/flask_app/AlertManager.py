import pandas as pd
import numpy as np
import sqlite3
import utils
import sql_queries
from datetime import datetime
import pdb

config_data_dict = utils.load_app_config_dict()


def combined_tables():
    cfms_db_path = config_data_dict['CFMS_DB_PATH']
    cfms_connection, cursor = utils.connect2sqlite3(cfms_db_path)
    
    driver_db_path = config_data_dict['DRIVER_DB_PATH']
    driver_connection, cursor = utils.connect2sqlite3(driver_db_path)

    # Get RC Buckets
    rc_bucket_df = pd.read_sql(sql_queries.sql_rc_buckets, cfms_connection)
    rc_bucket_df = rc_bucket_df[rc_bucket_df['IdField'] == 'ID-1'].copy()
    
    # Raw Data
    raw_df = pd.read_sql(sql_queries.sql_fetch_all_claims, driver_connection)
    
    # Investigation / Asisgnments
    investigation_feedback_df = pd.read_sql(sql_queries.sql_investigation_feedback, cfms_connection)
    
    # Weighted Scores
#     weighted_scores_db_path = config_data_dict['CFMS_VIEWS_DB_PATH']
#     cfms_views_connection, _ = utils.connect2sqlite3(weighted_scores_db_path)
    weighted_scores_df = pd.read_sql(sql_queries.sql_weighted_scores, cfms_connection)
    
    # Combine
    investigation_feedback_df['investigated'] = np.where(investigation_feedback_df['InvestigationStatusId']=='I0', 'Y', 'N')
    raw_df.rename(columns={'CLAIMREPORTDATE': 'claim_report_date', 
                          'CLAIM_AMT_USD': 'claim_amt_usd',
                          }, inplace=True)    
    weighted_scores_df.rename(columns={'Decile': 'severity'}, inplace=True)
    investigation_feedback_df.rename(columns={'UserName': 'assigned_resource'}, inplace=True)
    
    all_combined_df = raw_df.merge(rc_bucket_df, left_on=['claim_number'], right_on=['IdFieldValue'], how='left')\
                  .merge(investigation_feedback_df, left_on=['IdFieldValue'], right_on=['IdValue'], how='left')\
                  .merge(weighted_scores_df, left_on=['IdFieldValue'], right_on=['IdFieldValue'], how='left')
    
    all_combined_df['investigated'] = np.where(all_combined_df['investigated'].isna(), "N", all_combined_df['investigated'])
    
    all_combined_df = all_combined_df[all_combined_df['CLAIM_STATUS'] == 'open' ].copy()
    all_combined_df = all_combined_df.drop_duplicates(subset=['claim_number'])
    all_combined_df = all_combined_df.reset_index(drop=True)
    
    return all_combined_df


def create_alert_page_data():
    
    all_combined_df = combined_tables()
    
    combined_df = all_combined_df[['claim_number', 'investigated', 'claim_report_date', 'claim_amt_usd', 'severity', 
                              'assigned_resource', 'rc_bucket']].copy()
    
    # Keep scored data since the start of referral date
    combined_df = combined_df[pd.to_datetime(combined_df['claim_report_date']).dt.year == 2021].copy()
    combined_df.reset_index(drop=True, inplace=True)
    combined_df.sort_values(by=['claim_report_date'], ascending=False, inplace=True)
        
    alert_screen_json = dict()
    rcs = combined_df['rc_bucket'].unique()
    for rc in rcs:
        chunk_df = combined_df[combined_df['rc_bucket'] == rc].copy()
        alert_screen_json[rc] = utils.create_table_json(chunk_df)
        
    req_assigned_user_info = all_combined_df[['claim_number', 'severity', 'investigated', 'claim_report_date', 'claim_amt_usd', 
                                             'CLAIM_STATUS', 'assigned_resource']].copy()
    req_assigned_user_info.rename(columns={'severity': 'score_grp',
                                           'CLAIM_STATUS': 'claim_status', 
                                           'assigned_resource': 'assigned_to' }, inplace=True)
    user_info_df = req_assigned_user_info.groupby(['assigned_to', 'claim_status']).count().reset_index()
    user_info_json = utils.create_table_json(user_info_df)
    
    response_dict = {}
    for k in alert_screen_json.keys():
        response_dict[k] = eval(alert_screen_json[k])['data']
    
    response_dict['resources_claim_count'] = eval(user_info_json)['data']
    
    return response_dict


def create_claim_level_data(claim_num):
    driver_db_path = config_data_dict['DRIVER_DB_PATH']
    driver_connection, cursor = utils.connect2sqlite3(driver_db_path)
    
    cfms_db_path = config_data_dict['CFMS_DB_PATH']
    cfms_connection, cursor = utils.connect2sqlite3(cfms_db_path)
        
    claim_df = pd.read_sql(sql_queries.sql_claim_query.format(claim_id=claim_num) , driver_connection)
    
    req_cols = ['claim_number', 'CLAIMREPORTDATE', 'DATEOFLOSS', 'CLAIMTYPEDESC', 'address', 'pin_code']
    data = list(claim_df.loc[0, req_cols].values)
    
    comments = pd.read_sql(sql_queries.sql_comments.format(claim_id=claim_num), cfms_connection)
    assigned_users = pd.read_sql(sql_queries.sql_assigned_users.format(claim_id=claim_num), cfms_connection)
    assigned_users.drop_duplicates(subset=['UserId', 'IdField', 'IdValue'], inplace=True)
    user_comments = comments.merge(assigned_users, 
                                   left_on=['UserId', 'IdField', 'IdValue'], 
                                   right_on=['UserId', 'IdField', 'IdValue'], 
                                   how='left')
    
    print(user_comments)    
    user_comments = user_comments.dropna()
    
    user_comments = user_comments[['UserName', 'Comments', 'insert_ts']].rename(columns={'UserName': 'user_name',
                                                                                        'Comments': 'comment'})    
    claim_details_json = dict()
    claim_details_json['claim specific details'] = dict(zip(req_cols, [str(s) for s in data]))
    
    claim_details_json['comments'] = eval(utils.create_table_json(user_comments))['data']
    
    print("claim_details_json: ", claim_details_json)
            
    return claim_details_json


# use this method to add comments specific to claims
def add_comments_to_claims(u_id, claim_number, txt):
    u_id = f"USER-{u_id}"
    chunk_df = pd.DataFrame({"IdField": ['ID-1'],
                            "IdValue": [claim_number],
                             "UserId": [u_id],
                             "Comments": [txt],
                             "RID": [str(datetime.today())[:10]]
                            })
        
    cfms_con, _ = utils.connect2sqlite3(config_data_dict['CFMS_DB_PATH'])
    chunk_df.to_sql(name='Comments', con=cfms_con, if_exists='append', index=False)
    
    assigned_user = pd.DataFrame({"UserId": [u_id], "IdField": ["ID-1"], "IdValue": [claim_number] })
    assigned_user.to_sql(name='Assigned_Users', con=cfms_con, if_exists='append', index=False)
    
    return {"response": "SUCCESS"}

def get_model_outputs():
    # Model Indicators
    cfms_db_path = config_data_dict['CFMS_DB_PATH']
    cfms_connection, cursor = utils.connect2sqlite3(cfms_db_path)
    model_indicators_df = pd.read_sql(sql_queries.sql_model_indicators, cfms_connection)

    m_ind_feat_id_val_df = model_indicators_df.groupby(['IdField', 'IdFieldValue', 'ModelId', 'RunId'])[
['ModelFeatureName', 'ModelFeatureValue'] ].apply(lambda x: x.set_index('ModelFeatureName').T ).reset_index()
    m_ind_feat_imp_df = model_indicators_df.groupby(['IdField', 'IdFieldValue', 'ModelId', 'RunId'])[
    ['ModelFeatureName', 'ModelFeatureImportance'] ].apply(lambda x: x.set_index('ModelFeatureName').T ).reset_index()

    m_ind_df = pd.merge(m_ind_feat_id_val_df, m_ind_feat_imp_df, 
    on=['IdField', 'IdFieldValue', 'ModelId', 'RunId'], suffixes=('', '_FeatImp'), 
    how='left')
    
    


if __name__=="__main__":
    alert_management_dict = create_alert_page_data()
#     result = create_claim_level_data(claim_num='V4301451')
#     result = get_model_outputs()

    import json
    
    class NpEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, np.integer):
                return int(obj)
            if isinstance(obj, np.floating):
                return float(obj)
            if isinstance(obj, np.ndarray):
                return obj.tolist()
            return super(NpEncoder, self).default(obj)

    f = open("alert_management_dict.json", "w")
    f.write(json.dumps(alert_management_dict, cls=NpEncoder))
    f.close()
    
    print("done!")