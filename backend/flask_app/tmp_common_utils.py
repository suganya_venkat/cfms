# Standard Python libs
import pandas as pd
from datetime import datetime
import pickle
import yaml
import socket
import getpass
from pathlib import Path
import base64
import time

# External Python libs
from Crypto.Cipher import AES  # Use pycryptodome for Crypto
from base_app_user import CONFIG_APP_USER


#################################################################################################################
# User & Environemnt ############################################################################################
#################################################################################################################

def get_hostname():
    return socket.gethostname()


def get_user():
    return getpass.getuser()


def get_pass(key, pw):
    cipher = AES.new(key.encode(), AES.MODE_ECB)
    encoded_pw = base64.b64decode(pw.encode())
    return cipher.decrypt(encoded_pw).decode().strip()


################################################################################################################
# Pickles ######################################################################################################
################################################################################################################

def pdump(obj, file_string):
    with open(file_string, 'wb') as outfile:
        pickle.dump(obj, outfile)
        
def pload(file_string):
    with open(file_string, 'rb') as infile:
        obj = pickle.load(infile)
    return obj

################################################################################################################
# File IO ######################################################################################################
################################################################################################################

def read_textfile_into_dictionary(file_string):
    file_string = str(file_string)
    dict_out = {}
    with open(file_string) as f:
        for line in f:
           (key, val) = line.split()
           dict_out[(key)] = val
    return dict_out

        
def load_data(path):
    path = Path(path)
    f_type = path.suffix
    if f_type == ".pkl":
        data = pd.read_pickle(str(path))
    elif f_type == ".csv":
        data = pd.read_csv(str(path))
    elif f_type == ".xlsx" or f_type == ".xls":
        data = pd.read_excel(str(path))
    else:
        raise Exception("Expected pickle, csv or Excel file")
    return data


################################################################################################################
# Logging Helper
################################################################################################################

def print_and_log(logger, msg, log_type="info"):
    msg = str(msg)
    if logger is None:
        print(msg)
        return
    if log_type == "info":
        logger.info(msg)
    elif log_type == "debug":
        logger.debug(msg)
    elif log_type == "warning":
        logger.warning(msg)
    elif log_type == "error":
        logger.error(msg)
    elif log_type == "critical":
        logger.critical(msg)
    else:
        raise Exception("Unknown logger type")


################################################################################################################
# Configurations
################################################################################################################

def load_app_config(logger=None, app_config_reference_filepath=None):
    if app_config_reference_filepath is None:
        filepath = Path('..\\data\\reference\\config_filepaths\\app_config_files.txt')
    else:
        filepath = Path(app_config_reference_filepath)

    app_config_file_dict = read_textfile_into_dictionary(filepath)

    try:
        app_config_path = app_config_file_dict[CONFIG_APP_USER]
    except KeyError as e:
        raise Exception("Configurations missing for User: {}".format(CONFIG_APP_USER))

    print_and_log(logger, "Loading configurations from {}".format(app_config_path))

    with open(app_config_path, 'r') as f:
        config_dict = yaml.load(f)
        
    config_dict['CONFIG_APP_USER'] = CONFIG_APP_USER
    
    return config_dict
    
    

def load_log_config(log_config_reference_filepath=None):
    
    if log_config_reference_filepath is None:
        filepath = Path('..\\data\\reference\\config_filepaths\\log_config_files.txt')
    else:
        filepath = Path(log_config_reference_filepath)
        
    log_config_file_dict = read_textfile_into_dictionary(filepath)
    
    try:
        log_config_path = log_config_file_dict[CONFIG_APP_USER]
    except KeyError as e:
        raise Exception("Configurations missing for User: {}".format(CONFIG_APP_USER))
        
    with open(log_config_path, 'r') as f:
        return yaml.load(f)
    
    
    
#######################################################################
# Utility Functions
#######################################################################

def scale_to_range(m, r_min, r_max, t_min, t_max=100, eps=1e-8):
    """
    Scaling the raw scores to given range values
    :param m: Measurement to be scaled
    :param r_min: minimum of measurement range
    :param r_max: maximum of measurement range
    :param t_min: minimum of target range
    :param t_max: maximum of target range
    """
    if abs(r_max - r_min) < eps:
        scaled_m = t_max
    else:
        scaled_m = ((m - r_min)/ (r_max - r_min))*(t_max - t_min) + t_min

    return scaled_m


def query_tuple(l):
    """
    Convert a list of items to sql query compatible tuple
    :param l: List if items
    """
    l = [str(v) for v in l]
    if len(l) == 1:
        return "( '" + str(l[0]) + "' )"
    else:
        return tuple(l)
    

def check_na(x):  
    x = str(x)
    if pd.isna(x) or x is None or str(x).lower().strip() == 'nan' or str(x).lower().strip() == 'none':
        return True
    else:
        return False
    
    
def check_column(df, col):
    for c in df.columns:
        if str(col).strip().lower() in str(c).strip().lower():
            print(c)

    
def get_uniq_vals(l):
    ul = []
    for x in l:
        try:
            if pd.isna(x):
                continue
        except:
            pass
        
        x = str(x)
        if x is None or str(x).lower().strip() == 'nan' or str(x).lower().strip() == 'none':
            continue
        else:
            if x not in ul:
                ul.append(x)
    return ul  


def fillna_with_empty_list(ser):
    ser = ser.copy()
    result = ser.apply(lambda x: [] if check_na(x) else x )
    
    return result  


def merge_agg_info(inp_raw, agg_raw, key_col='ClaimNumber', fillna=True):
    inp_raw = inp_raw.copy()
    agg_raw = agg_raw.copy()
    
    inp_raw[key_col] = inp_raw[key_col].astype(str)
    agg_raw[key_col] = agg_raw[key_col].astype(str)
    
    for col in agg_raw.columns.difference([key_col]):
        try:
            del inp_raw[col]
        except:
            pass
        
    result = inp_raw.merge(agg_raw, on=key_col, how='left')
    
    if fillna:
        for col in agg_raw.columns:
            result[col] = fillna_with_empty_list(result[col])
    
    return result


def get_agg_unique_values(df, key_col, uniq_val_col):
    result = df.groupby([key_col])[uniq_val_col].agg(lambda x: get_uniq_vals(x) ) 
    
    return result


def generate_reason_code_distribution(all_reasons, reason_codes_df, key_col='index'):
    result = pd.DataFrame({"Reason Codes": all_reasons})
    total_claims = reason_codes_df[key_col].nunique()
    reason_codes_df.columns = [key_col] + ['reason_code{}'.format(i+1) for i in range(reason_codes_df.shape[1] - 1)]
    for i in range(5):
        rc_count_df = reason_codes_df.groupby(['reason_code{}'.format(i+1)])[key_col].count().reset_index()
        rc_count_df.columns = ['TopRC', 'ClaimCount']
        rc_count_df['% Claims Top Reason{}'.format(i+1)] = rc_count_df['ClaimCount'] / total_claims
        result = result.merge(rc_count_df[['TopRC', '% Claims Top Reason{}'.format(i+1)]],
                              left_on=['Reason Codes'], 
                              right_on=['TopRC'],
                              how='left'
                             )
        result['% Claims Top Reason{}'.format(i+1)] = result['% Claims Top Reason{}'.format(i+1)].fillna(0)
        del result['TopRC']
        
    return result
    
            














 
 