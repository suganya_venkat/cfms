import sqlite3
import pandas as pd
import utils
import sql_queries
import datetime
import re
import uuid
import pdb
import numpy as np


class RuleBank:
    
    def __init__(self, ):
        self.app_config_dict = utils.load_app_config_dict()
        # driver db
        self.driver_con, self.driver_cursor = utils.connect2sqlite3(self.app_config_dict['DRIVER_DB_PATH'])

        # cfms db
        self.cfms_con, self.cfms_cursor = utils.connect2sqlite3(self.app_config_dict['CFMS_DB_PATH'])
                        
    def get_rule_bank(self, ):
        rule_bank_df = pd.read_sql(sql_queries.sql_cfms_rule_bank, self.cfms_con)
        return rule_bank_df
    
    def get_var_op_val(self, x):
        var = x['Variable']
        op = x['Operator']
        val = x['Value']
        return f"{var} {op} {val}"
    
    def get_rule_description(self, variable, operator, value):
        operator_desc_map = {"==": "Equal to",
                            "<>": "Not equal to",
                            ">": "Greater than",
                            ">=": "Greater than or equal to",
                            "<=": "Less than or equal to",
                            "<": "Less than"}
        rule_desc = str(variable) + " " + operator_desc_map[str(operator)] + " " + str(value) 
        return rule_desc
        
        
    def add_rules(self, rules_json):
        """
        rules_json = [{"rule1": {'Variable': 'Claim Amount',
                                  'Operator': '>=',
                                  'Value': 500,
                                  'Status': 'Active'}}, ...]
        
        """
        print(rules_json)
#         try:
        rule_bank_df = pd.read_sql(sql_queries.sql_cfms_rule_bank, self.cfms_con)
        req_cols = ['RuleId', 'Variable', 'Operator', 'Value', 'RuleDescription', 'RID']

        new_rules_df = pd.DataFrame()
        for rule in rules_json:
            chunk_df = pd.DataFrame(rule).T
            new_rules_df = new_rules_df.append(chunk_df)

        model_feat_names = pd.read_sql(sql_queries.sql_modelfeatnames_inventory, self.cfms_con)
        var_list = new_rules_df['Variable'].tolist()

        if len(set(var_list).intersection(model_feat_names['FeatureBusinessName'].tolist())) !=  len(var_list):
            print("Variable not present in Feature Inventory")
            return {"response": "FAILURE"}            

        # variable, operator, value

        new_rules_df['RuleDescription'] = new_rules_df.apply(
            lambda x: self.get_rule_description(variable=x['Variable'], operator=x['Operator'], value=x['Value']) ,
            axis=1)

        new_rules_df["RID"] = datetime.datetime.today()
        new_rules_df['RuleId'] = ["RULE-" + str(uuid.uuid1()) for _ in range(new_rules_df.shape[0])]
        new_rules_df.reset_index(drop=True, inplace=True)

        new_rules_df = new_rules_df[req_cols]
        rule_bank_df.rename(columns={"RuleCreatedDate": "RID"}, inplace=True)
        rule_bank_df = rule_bank_df[req_cols]

        rule_bank_df = new_rules_df.append(rule_bank_df).drop_duplicates(subset=['Variable', 'Operator'])

        rule_bank_df.reset_index(drop=True, inplace=True)
        rule_bank_df['RID'] = rule_bank_df['RID'].astype(str)

        if rule_bank_df.shape[0] == 0:
            print("Empty Rule Bank, skipping updation")
            response = {"response": "FAILURE"}
        else:
            rule_bank_df[req_cols].to_sql('RuleBank', self.cfms_con, index=False, if_exists='replace')
            print("Rule Bank Updated!")
            response = {"response": "SUCCESS"}
#         except:
#             print("Error updating Rule Bank")
#             response = {"response": "FAILURE"}
        
        return response
        
        
    def get_rule_bank_stats(self, ):
        
        rule_bank_df = pd.read_sql(sql_queries.sql_cfms_rule_bank, self.cfms_con)
        feedback_df = pd.read_sql(sql_queries.sql_investigation_feedback, self.cfms_con)        
        feedback_df['Investigated'] = np.where(feedback_df['InvestigationStatusId'] == 'I0', 1, 0)
        
        # Compute Stats for each rule
        rule_stats = {}
        for i, rule in enumerate(rule_bank_df.iterrows()):     
            rule_row = rule[1]
            rule_chunk_stat = {}
            rule_chunk_stat['RuleNumber'] = f"{i+1}"
            rule_chunk_stat['RuleDescription'] = rule_row['RuleDescription']
            rule_chunk_stat['DateCreated'] = rule_row['RuleCreatedDate']            
            rule_ser = pd.DataFrame([rule[1].T])            
            
            filtered_claims = self.filter_claims(rule_bank_df=rule_ser)['filtered_claims']
            
            if filtered_claims:
                sub_ref_df = feedback_df[(feedback_df['IdField'] == 'ID-1') & 
                                         (feedback_df['IdValue'].isin(filtered_claims))].copy()
                tot_alerts = sub_ref_df.shape[0]
                
                tot_investigations = (sub_ref_df['InvestigationStatusId'] != 'I0').sum()
                tot_frauds = (sub_ref_df['FinalStatus'] == 'F2').sum()
                identified_frauds = sub_ref_df[(sub_ref_df['FinalStatus'] == 'F2') & (sub_ref_df['IdField'] == 'ID-1')]['IdValue'].tolist()
                if len(identified_frauds) == 1:
                    identified_frauds = [identified_frauds[0], identified_frauds[0]]
                    
                tot_savings = pd.read_sql(sql_queries.sql_sum_claimed_amount.format(claim_list=tuple(identified_frauds)), 
                                          self.driver_con)['SumClaimedAmount'].tolist()[0]
            else:
                tot_alerts = 0
                tot_investigations = 0
                tot_frauds = 0
                tot_savings = 0
                
            rule_chunk_stat['Alert'] = int(tot_alerts)
            rule_chunk_stat['Investigation'] = int(tot_investigations)
            rule_chunk_stat['Frauds'] = int(tot_frauds)
            rule_chunk_stat['Fraud %'] = "{:.1%}".format(rule_chunk_stat['Frauds']/feedback_df['IdValue'].nunique())
            rule_chunk_stat['Savings'] = tot_savings
            
            rule_stats[f'rule{i+1}'] = rule_chunk_stat
            
            print(rule_stats)
            
        return rule_stats
            
                      
    def filter_claims(self, rule_bank_df=None):
                
        response = {}
        if rule_bank_df is None:
            rule_bank_df = pd.read_sql(sql_queries.sql_cfms_rule_bank, self.cfms_con)

        if rule_bank_df.shape[0] == 0:
            print("Rule Bank is Empty")
            return {"filtered_claims": "None"}
        
        feat_name_dict = rule_bank_df.set_index('FeatureBusinessName')['ModelFeatureName'].to_dict()
        feat_inventory = pd.read_sql(sql_queries.sql_feat_inventory, self.cfms_con)
        feat_inventory_dict = feat_inventory.set_index(['ModelFeatureName'])['FeatureID'].to_dict()
        
#         feat_id = feat_inventory[feat_inventory['FeatureBusinessName'] == feat_name_dict[x['Variable']]]['FeatureID'].tolist()[0]
        id_fieldname = pd.read_sql(sql_queries.sql_claim_number_idfield, self.cfms_con)['FieldName'].to_list()[0]

        try:
            rules_str = " and ".join(rule_bank_df.apply(lambda x: f"{feat_name_dict[x['Variable']]} {x['Operator']} {x['Value']}" , axis=1).tolist()) 
            print(rules_str)
            filtered_claims_list = pd.read_sql(sql_queries.sql_rule_based_fetch_claims.format(rules_str=rules_str), self.driver_con)[id_fieldname].tolist()
        except:
            rules_str = " and ".join(rule_bank_df.apply(lambda x: f" FeatureID='{feat_inventory_dict[feat_name_dict[x['Variable']]]}' and FeatureValue {x['Operator']} '{x['Value']}'  " , axis=1).tolist())
            print(rules_str)
            filtered_claims_list = pd.read_sql(sql_queries.sql_rule_based_fetch_claims2.format(rules_str=rules_str), self.cfms_con)[id_fieldname].tolist()
            
        response = {"filtered_claims": filtered_claims_list }    

        return response
    
    def delete_from_rule_bank(self, rule_description):
#         try:
        self.cfms_cursor.execute(sql_queries.sql_delete_from_rule_bank_ruledesc.format(rule_desc=rule_description))
        self.cfms_con.commit()
        print("Deleted Rule from Rule Bank")
        return {'response': "SUCCESS"}
#         except:
#             print("Error deleting rule from rule bank")
#             return {'response': "FAILURE"}
            
    
if __name__ == '__main__':
    
    obj = RuleBank()
    
#     rules_json = [
#         {"rule1": {'Variable': 'Has Report Lag',
#                                   'Operator': '>=',
#                                   'Value': 14}}
#                  ]
    
#     rules_json = [
#         {"rule1": {'Variable': 'Claims Amount',
#                                   'Operator': '>=',
#                                   'Value': 2000}}
#                  ]

#     rules_json = [
#                 {"rule1": {'Variable': 'Loss happened within a month of policy effective date',
#                                           'Operator': '==',
#                                           'Value': 1}}
#                          ]
    
    rules_json = [
                {"rule1": {'Variable': 'Loss happened withing a month of policy expiry',
                                          'Operator': '==',
                                          'Value': 1}}
                         ]
    
    # test add rule
    obj.add_rules(rules_json)
    
    # Rule Bank Stats
    rule_bank_dict = obj.get_rule_bank_stats()

    # Delete from rulebank
    obj.delete_from_rule_bank(rule_description="Loss happened withing a month of policy expiry")
#     obj.delete_from_rule_bank(rule_description="Claims Amount Greater than or equal to 2000")
    
    pdb.set_trace()
    
    
    
    
