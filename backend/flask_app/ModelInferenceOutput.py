import pandas as pd
import utils
import sql_queries
import json

class ModelInferenceOutput:
    
    def __init__(self, ):
        self.app_config_dict = utils.load_app_config_dict()
        self.cfms_con, self.cfms_cursor = utils.connect2sqlite3(self.app_config_dict['CFMS_DB_PATH'])

    def get_model_inference(self, claim_number, run_id, model_id):

        filter_str = f""" where IdField = 'ID-1' and IdFieldValue = '{claim_number}' 
                            and RunId = '{run_id}' and ModelId = '{model_id}' """
        # Load data
        model_out_df = pd.read_sql(sql_queries.sql_model_output + filter_str, self.cfms_con)
        model_ind_df = pd.read_sql(sql_queries.sql_model_indicators + filter_str, self.cfms_con)
        model_score_grp_df = pd.read_sql(sql_queries.sql_model_score_grp_map, self.cfms_con)
        
        model_score_grp_dict = model_score_grp_df.set_index(['ScoreGrpId'])['ScoreGroup'].to_dict()
        model_out_df['ModelScoreGroup'] = model_out_df['ModelScoreGroupId'].map(model_score_grp_dict)
        model_ind_df = model_ind_df.sort_values(by=['ModelFeatureImportance'], ascending=False)
        model_ind_df['ModelFeatureImportance'] = model_ind_df['ModelFeatureImportance'].fillna(0) 
        model_indicators = model_ind_df.sort_values(by=['ModelFeatureImportance'], 
                                                    ascending=False).head()[['ModelFeatureImportance',
                                                                             'FeatureBusinessName']].reset_index(drop=True)

        model_out_df['RID'] = model_out_df['RID'].apply(lambda x: str(x)[:10] )
        model_out_df['ModelScaledScore'] = model_out_df['ModelScaledScore'].round()

        model_infer_dict = {"model_indicators": eval(model_indicators.to_json(orient='table'))['data']}
        model_infer_dict.update(eval(model_out_df[['IdFieldValue', 'ModelScaledScore', 
                           'ModelScoreGroup', 'RID']].rename(columns={"RID": 'ModelScoreDate', 
                                                                       'IdFieldValue': 'ClaimNumber'}).to_json(orient='table',
                                                                                                index=False))['data'][0])

        result = model_infer_dict

        return result
        
if __name__=="__main__":
    
    obj = ModelInferenceOutput()
    
    result = obj.get_model_inference(claim_number='V4301451', 
                                 run_id='RUN-bde682dc-b413-11ec-930a-062fcc6cb972',
                                 model_id='XGB-8dd62108-b407-11ec-930a-062fcc6cb972')  
    
    print(json.dumps(result))

