import os
import sqlite3
import yaml
import pandas as pd
import utils
from flask import jsonify
import datetime
import numpy as np
import sql_queries
import pdb


class ReportingView():
    
    def __init__(self, config_filepath=None):
        self.app_config_dict = utils.load_app_config_dict(config_filepath)
        self.driver_con, self.drive_cursor = utils.connect2sqlite3(self.app_config_dict['DRIVER_DB_PATH'])
        self.cfms_con, self.cfms_cursor = utils.connect2sqlite3(self.app_config_dict['CFMS_DB_PATH'])
        
        self.default_filter_dict = {"CLAIMREPORTDATE": [{"from":  "2021-01-01", "to": "2022-01-01"}],
                                    "STATE" : ['pahang', 'penang', 'perak', 'johor', ' malacca', 'pertis',
                                               'selangor', 'kedah', 'negeri sembilan', 'kelantan'],
                                    "CLAIM_TYPE": ['self - accident', 'Weather', 'Animal Collision', 'Theft','Windshield damage'],
                                    "CLAIM_AMT_USD": [ {"from": "0" }]
                                   
                                   }
        
    def create_table_json(self, df, col=None):
        """
        Creates tabular json output
        :param df: pandas DataFrame or Series
        :param col: String
        :return: Dictionary
        """
        if col:
            result = df[col].value_counts().to_json()
        else:
            result = df.to_json()
        return result
    
    def calculate_closure_rate(self, df):
        df_new = df.copy()
        df_new.index =pd.to_datetime(df_new.CLAIMREPORTDATE)
        df_new = df_new.groupby([pd.Grouper(freq='M'),
                                 'CLAIM_STATUS']).agg({'STATE': 'count'}).rename(columns={'STATE': 'COUNT'})
        df_new = df_new.reset_index()
        df_new = df_new[df_new.CLAIMREPORTDATE > '2021-01-01']
        return self.create_table_json(df_new)
    
    def create_filters_str(self, filters_json):

        filters_dict = filters_json['reporting_view_filters']

        filter_list = []
        range_filters = []
        inclusion_filters = []
        
        for k, v in filters_dict.items():
            print("k: ", k)
            
            if v == "all" or "all" in v:
                print("Assigned ")
                v = self.default_filter_dict[k]
            else:
                if k == "CLAIM_AMT_USD":
                    v = [{"from": str(val).split("-")[0] , "to": str(val).split("-")[1] } if "-" in val 
                         else {"from": str(val)[:-1]}  for val in v  ]
                
            if k in self.app_config_dict['VIEW_FILTERS']['RANGE_FILTERS']:
                rng_filter_chunk_list = []
                for rng_i, rng in enumerate(v):
                    if "from" in rng and "to" in rng:
                        rng_filter_chunk = f" ({k} >= '{rng['from']}' and {k} <= '{rng['to']}' )"
                    elif "from" in rng and "to" not in rng:
                        rng_filter_chunk = f" ({k} >= '{rng['from']}' )"
                    elif "to" in rng and "from" not in rng:
                        rng_filter_chunk = f" ({k} <= '{rng['to']}' )"
                    else:
                        raise Exception("Range Undefined") 
                    rng_filter_chunk_list.append(rng_filter_chunk)
                        
                range_filters.append( " ( " + " or ".join(rng_filter_chunk_list) + " ) " )
            elif k in self.app_config_dict['VIEW_FILTERS']['INCLUSION_FILTERS']:
                if len(v) == 1:
                    v = [v[0], v[0]]
                incl_tuple = tuple(v)
                inclusion_filters.append(f"{k} in {incl_tuple}")
            else:
                raise Exception("Unknown view filters")

        filter_str = " and ".join(inclusion_filters) + " and " + " and ".join(range_filters) 
        return filter_str
    
    def load_filtered_subset(self, filters_json):
        filter_str = self.create_filters_str(filters_json)
        filter_claims_query = """select * from Claims where """ + filter_str
        print("Query: ", filter_claims_query)
        raw_df = pd.read_sql(filter_claims_query, self.driver_con)
        return raw_df
    
    def create_json_for_Investigation_screen(self, df):
        df = df.copy()
        
        feedback_df = pd.read_sql(sql_queries.sql_investigation_feedback, self.cfms_con) 
        df.drop_duplicates(subset=['claim_number'], inplace=True)
        feedback_df = feedback_df.drop_duplicates(subset=['IdField', 'IdValue'])
        df = df.merge(feedback_df[['IdField', 'IdValue', 'InvestigationStatusId', 'FinalStatus']],
                     left_on=['claim_number'], right_on=['IdValue'], how='left')
        df['Investigated'] = np.where(df['InvestigationStatusId'] != 'I0', 'Y', 'N')
        df['target'] = np.where(df['FinalStatus'].isin(['F4']), 1, 0)
        df.rename(columns={"claim_number": 'CLAIMNUMBER'}, inplace=True) 
        
        summary_dict = dict()
        summary_dict['Claims_Initiated'] = int(df.shape[0])
        summary_dict['Total_Investigated'] = int(df.Investigated.value_counts()['Y'])
        df_inv = df[df['Investigated'] == 'Y'].copy()
        summary_dict['Total_Closed'] = int(df_inv.CLAIM_STATUS.value_counts()['closed'])
        summary_dict['Paid_Claims'] = int(df_inv.PAID.value_counts()['Yes'])
        summary_dict['Rejected'] = int(df_inv.PAID.value_counts()['Rejected'])
        summary_dict['Fraud_Suspected_Claims'] = int(df_inv.target.value_counts()[1])
        summary_dict['Investigation_rate'] = (df_inv.shape[0]/df.shape[0])
        summary_dict['Fraud_detection'] = (df_inv.target.value_counts()[1]/df_inv.shape[0])
        df_closed = df_inv[df_inv.CLAIM_STATUS == 'closed'].copy()
        
        # Claims Investigated By State
        tmp = df_inv.groupby(['STATE'])['CLAIMNUMBER'].nunique()
        tmp = 100 * (tmp / df_inv['CLAIMNUMBER'].nunique())
        tmp = tmp.reset_index().rename(columns={'CLAIMNUMBER': 'Investigated_status'})
        claim_investigated_by_state_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Among Investigated Claims Distribution of Paid Statuses
        tmp = 100*(df_inv['PAID'].value_counts() / df_inv['CLAIMNUMBER'].nunique())
        investigated_paid_status_dist_dict = eval(tmp.to_json(orient='table'))['data']
        
        summary_dict['Investigated_status_by_states_perc'] = claim_investigated_by_state_dict
        summary_dict['Investigated_Payment_Status'] = investigated_paid_status_dist_dict
        
        print(summary_dict)
        
        return summary_dict
      
    def claims_overview(self, df):
        
        feedback_df = pd.read_sql(sql_queries.sql_investigation_feedback, self.cfms_con) 
        
        df.drop_duplicates(subset=['claim_number'], inplace=True)
        
        feedback_df = feedback_df.drop_duplicates(subset=['IdField', 'IdValue'])
        df = df.merge(feedback_df[['IdField', 'IdValue', 'InvestigationStatusId', 'FinalStatus']],
                     left_on=['claim_number'], right_on=['IdValue'], how='left'
                     )
        
        df['Investigated'] = np.where(df['InvestigationStatusId'] != 'I0', 'Y', 'N')
        df.rename(columns={"claim_number": 'CLAIMNUMBER'}, inplace=True) 
        
        # Claim Count by Claim Type
        tmp = df['CLAIM_TYPE'].value_counts()
        tmp.name = 'Claims'
        tmp.index.name = 'Claims_type'
        claims_by_claim_type_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Claim Count by State
        tmp = df['STATE'].value_counts()
        tmp.name = 'Claims'
        tmp.index.name = 'state'
        claims_by_state_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Approved Amount by Claim type Histogram
        tmp = df.groupby('CLAIM_TYPE').agg({'APPRV_AMT_USD': 'sum'}).rename(columns={'APPRV_AMT_USD': 'total_approved_amt'})
        tmp.index.name = 'claim_type'
        approved_claimed_amt_by_clm_type_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Approved Amount by State Histogram
        tmp = df.groupby('STATE').agg({'APPRV_AMT_USD': 'sum'}).rename(columns={'APPRV_AMT_USD': 'total_approved_amt'})
        tmp.index.name = 'state'
        approved_claimed_amt_by_state_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Statewise Total Approved Amount & Claims
        tmp = df.groupby('STATE').agg({'APPRV_AMT_USD': 'sum', 'CLAIMNUMBER': 'count'}).rename(columns={'CLAIMNUMBER': 'Claim_count'})
        tmp = df.groupby('STATE').agg({'APPRV_AMT_USD': 'sum', 'CLAIMNUMBER': 'count'}).rename(columns={'CLAIMNUMBER': 'Claim_count'})
        tmp.index.name = 'STATE'
        table_statewise_apprv_amt_claims_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Last 12 Months claim count
        tmp = df[['CLAIMREPORTDATE', 'CLAIMNUMBER', 'CLAIM_STATUS']].copy()
        tmp['Month'] = pd.to_datetime(tmp['CLAIMREPORTDATE']).dt.month
        tmp['Year'] = pd.to_datetime(tmp['CLAIMREPORTDATE']).dt.year
        tmp = tmp[tmp['Year']>=2021]
        tmp['YearMonth'] = pd.to_datetime(tmp['Year'].astype(str) + "-" + tmp['Month'].astype(str))
        tmp = tmp.groupby(['YearMonth', 'CLAIM_STATUS'])['CLAIMNUMBER'].nunique()
        tmp = tmp.reset_index().rename(columns={"YearMonth": "CLAIMREPORTDATE", 'CLAIMNUMBER': 'COUNT'})
        last_12_months_claim_count_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Claim Count Per Report Date
        tmp = df[['CLAIMREPORTDATE', 'CLAIMNUMBER']].copy()
        base = datetime.datetime.today()
        date_list = [pd.to_datetime(str(base - datetime.timedelta(days=x))[:10] ) for x in range(365*3)]
        dt_df = pd.DataFrame({'CLAIMREPORTDATE': date_list})
        tmp = tmp.groupby(['CLAIMREPORTDATE'])['CLAIMNUMBER'].nunique().reset_index()
        tmp['CLAIMREPORTDATE'] = pd.to_datetime(tmp['CLAIMREPORTDATE'])
        tmp = dt_df.merge(tmp.reset_index(), on=['CLAIMREPORTDATE'], how='left')
        tmp['CLAIMNUMBER'] = tmp['CLAIMNUMBER'].fillna(0)
        tmp = tmp.rename(columns={'CLAIMNUMBER': 'CLAIMREPORTDATE_count'}).drop(columns=['index'])
        daywise_claim_count_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Fraud Ratio
        tmp = pd.Series(np.where(df['Investigated']=='Y', "Fraud-suspected", "Normal"))
        tmp = tmp.value_counts()
        tmp.name = 'target'
        fraud_ratio_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Amounts by type
        tmp = df.groupby(['CLAIM_TYPE'])[['CLAIM_AMT_USD', 'APPRV_AMT_USD']].agg(sum)
        amounts_by_claim_type = eval(tmp.to_json(orient='table'))['data']
        
        # Amounts by State
        tmp = df.groupby(['STATE'])[['CLAIM_AMT_USD', 'APPRV_AMT_USD']].agg(sum)
        amounts_by_state = eval(tmp.to_json(orient='table'))['data']
        
        # Total Amounts
        tot_claimed = df['CLAIM_AMT_USD'].sum()
        tot_approved = df['APPRV_AMT_USD'].sum()
        approved_ratio = tot_approved/tot_claimed 

        summary_dict = {'Claims Initiated': len(df),
                    'Claims in Progress': len(df.CLAIM_STATUS[df.CLAIM_STATUS == 'open']),
                    'Claims Closed': len(df.CLAIM_STATUS[df.CLAIM_STATUS == 'closed']),
                    'Claims Paid': {'counts': df.PAID.value_counts().to_dict(),
                                    'Amount Paid': (df[df.PAID == 'Yes'])['APPRV_AMT_USD'].sum()},
                    'Average Claim Amount': df['CLAIM_AMT_USD'].sum() / len(df),
                    'Average Paid Amount':
                        df['APPRV_AMT_USD'][df.PAID == 'Yes'].sum() / len(df['APPRV_AMT_USD'][df.PAID == 'Yes']),
                    'Closure ratio': df.CLAIM_STATUS.value_counts()['closed'].sum() / len(df),
                        
                    'Claims_by_Claim_Type_Histogram': claims_by_claim_type_dict,
                        
                    'Claims_by_state_Histogram': claims_by_state_dict,
                        
                    'Approved_Claimed_Amount_by_Claim_Type_Histogram': {"TOTAL_APPROVED_AMOUNT": approved_claimed_amt_by_clm_type_dict},
                        
                    'Approved_Claimed_Amount_by_State_Histogram': {"TOTAL_APPROVED_AMOUNT": approved_claimed_amt_by_state_dict},   
                        
                        'Tabledata_statewise': table_statewise_apprv_amt_claims_dict,
                        'Last12MonthCount': last_12_months_claim_count_dict,
                        'Daywise_Claim_Count': daywise_claim_count_dict,
                        'Fraud_Ratio': fraud_ratio_dict,
                        
                        "AmountsByClaimType": amounts_by_claim_type,
                        "AmountsByState": amounts_by_state,
                        "TotalClaimedAmount": tot_claimed,
                        "TotalApprovedAmount": tot_approved,
                        "ApprovedAmountRatio": approved_ratio
                       }
        
        return summary_dict


    
if __name__=="__main__":
    
    filters_json = {
                "reporting_view_filters": {
                    "CLAIMREPORTDATE": [
                        {
                            "from": "2016-06-17 00:00:00",
                            "to": "2021-12-11 00:00:00"
                        }
                    ],
                    "STATE": [
                        "selangor",
                        "perak"
                    ],
                    "CLAIM_TYPE": [
                        "self - accident",
                        "Animal Collision"
                    ],
                    "CLAIM_AMT_USD": [
                        "1-1000",
                        "1000-10000",
                        "10000+"
                ]
                }
            }
    
#     filters_json = {
#                 "reporting_view_filters": {
#                     "CLAIMREPORTDATE": [
#                         {
#                             "from": "2016-06-17 00:00:00",
#                             "to": "2021-12-11 00:00:00"
#                         }
#                     ],
#                     "STATE": ["all"
#                     ],
#                     "CLAIM_TYPE": ["all"
#                     ],
#                     "CLAIM_AMT_USD": ["all"
#                                      ]
#                 }
#             }

    
    obj = ReportingView()
    
    sub_df = obj.load_filtered_subset(filters_json)
    print("shape sub_df: ", sub_df.shape)
    summary_dict = obj.claims_overview(df=sub_df)
#     print("summary_dict: ", summary_dict)
#     investigation_summary_dict = obj.create_json_for_Investigation_screen(df=sub_df)
#     print("Investigation Summary Dict: ", investigation_summary_dict)
    
    pdb.set_trace()
        