import pandas as pd
import numpy as np
from time import time
from datetime import datetime, timedelta

import utils
import sql_queries
import base_loader
import base_writer

import pdb



###################################################
# Data Preparation Utility Functions
###################################################


def get_cc_data_from_claims_list(claims_list, conn, config_dict, table='claims_policy', batch_size=1000):
    """
    Extract Claim Connect APAC data for given table
    
    claims_list: List of claims
    conn: Connection object
    table: claims_policy, line, participant
    """
    
    if table == 'claims_policy':
        sql_query = sql_queries.SQL_cc_claims_policy_from_claims_list
    elif table == 'line':
        sql_query = sql_queries.SQL_cc_line_from_claims_list
    elif table == 'participant':
        sql_query = sql_queries.SQL_cc_participant_from_claims_list
    else:
        raise Exception("Unknown table name")
    
    n_batches = int(len(claims_list) / batch_size) + 1  
    
    df = pd.DataFrame()
    for i, batch in enumerate(np.array_split(claims_list, n_batches)) :
        if i%10 == 0:
            print('Getting data for batch {}/{}'.format(i, n_batches))
            print('df shape: ', df.shape)
            
        if len(batch) == 0:
            continue
        
        if len(batch) == 1:
            batch = [batch[0], batch[0]]
        
        chunk = pd.read_sql(sql_query.format(**config_dict, claims_list=tuple(batch) ), conn)
        df = df.append(chunk)
        
    print("shape df: ", df.shape)
    
    return df


def get_claimant_basic_info(parti_df):
    """
    Aggregate all claimant basic information at claim number level
    """
    
    claimant_df = parti_df[parti_df['Role_1033'] == 'Claimant' ].copy()
    
    clm_claimant_dobs_df = get_agg_unique_values(claimant_df, 
                                        key_col='ClaimNumber',
                                        uniq_val_col='DOB')
    
    clm_claimant_emails_df = get_agg_unique_values(claimant_df, 
                                        key_col='ClaimNumber',
                                        uniq_val_col='EmailAddress')
    
    clm_claimant_telephone_df = get_agg_unique_values(claimant_df, 
                                        key_col='ClaimNumber',
                                        uniq_val_col='TelephoneNumber')
    
    result = join_on_common_key([clm_claimant_dobs_df.reset_index(), 
                      clm_claimant_emails_df.reset_index(),
                      clm_claimant_telephone_df.reset_index()],
                         key_col='ClaimNumber')
    
    return result
    
    
def get_line_basic_info(line_df):
    """
    Aggregate all claimant basic information at claim number level
    """
        
    clm_line_type_df = get_agg_unique_values(line_df, 
                                        key_col='ClaimNumber',
                                        uniq_val_col='LineType_1033')

    clm_src_claimant_id_df = get_agg_unique_values(line_df, 
                                        key_col='ClaimNumber',
                                        uniq_val_col='SourceClaimantId')
    
    result = join_on_common_key([clm_line_type_df.reset_index(), 
                      clm_src_claimant_id_df.reset_index()],
                         key_col='ClaimNumber')
    
    return result 



###########################################################################
# Data Preparation Main Function (From Raw Dump)
###########################################################################


def get_input_raw_data_for_training(data_root_folder='../data/raw', sample_data_size=None):
    
    # Load Countrywise data from different tables
    hk_raw_claims_policy = pd.read_pickle(f"{data_root_folder}/HK/hk_raw_claims_policy-2021-04-22.pkl")
    hk_raw_line = pd.read_pickle(f"{data_root_folder}/HK/hk_raw_line-2021-04-22.pkl")
    hk_raw_parti = pd.read_pickle(f"{data_root_folder}/HK/hk_raw_participant-2021-04-22.pkl")
    
    sg_raw_claims_policy = pd.read_pickle(f"{data_root_folder}/SG/sg_raw_claims_policy-2021-04-22.pkl")
    sg_raw_line = pd.read_pickle(f"{data_root_folder}/SG/sg_raw_line-2021-04-22.pkl")
    sg_raw_parti = pd.read_pickle(f"{data_root_folder}/SG/sg_raw_participant-2021-04-22.pkl")
    
    my_raw_claims_policy = pd.read_pickle(f"{data_root_folder}/MY/my_raw_claims_policy-2021-04-22.pkl")
    my_raw_line = pd.read_pickle(f"{data_root_folder}/MY/my_raw_line-2021-04-22.pkl")
    my_raw_parti = pd.read_pickle(f"{data_root_folder}/MY/my_raw_participant-2021-04-22.pkl")
    
    nz_raw_claims_policy = pd.read_pickle(f"{data_root_folder}/NZ/nz_raw_claims_policy-2021-04-22.pkl")
    nz_raw_line = pd.read_pickle(f"{data_root_folder}/NZ/nz_raw_line-2021-04-22.pkl")
    nz_raw_parti = pd.read_pickle(f"{data_root_folder}/NZ/nz_raw_participant-2021-04-22.pkl")
    
    all_claim_pol_df = pd.concat([hk_raw_claims_policy, sg_raw_claims_policy, my_raw_claims_policy, nz_raw_claims_policy
                             ])
    
    all_line_df = pd.concat([hk_raw_line,
                            sg_raw_line,
                            my_raw_line,
                            nz_raw_line 
                            ])
    
    all_parti_df = pd.concat([hk_raw_parti,
                            sg_raw_parti,
                            my_raw_parti,
                            nz_raw_parti
                            ])
    
    # Format ClaimNumber(key_col) as String
    key_col = 'ClaimNumber'
    all_claim_pol_df[key_col] = all_claim_pol_df[key_col].astype(str)
    all_line_df[key_col] = all_line_df[key_col].astype(str)
    all_parti_df[key_col] = all_parti_df[key_col].astype(str)
    
    
    ## Select sample data
    if sample_data_size:
        
        sample_claim_pol_df = all_claim_pol_df.sample(sample_data_size)
        sample_claims = sample_claim_pol_df[key_col].unique().tolist()
        sample_parti_df = all_parti_df[all_parti_df[key_col].isin(sample_claims)].copy()
        sample_line_df = all_line_df[all_line_df[key_col].isin(sample_claims)].copy()
    else:
        
        sample_claim_pol_df = all_claim_pol_df.copy()
        sample_parti_df = all_parti_df.copy()
        sample_line_df = all_line_df.copy()
        
    sample_claim_pol_df = sample_claim_pol_df.drop_duplicates(subset=[key_col])
    
    sample_claim_pol_df = sample_claim_pol_df.loc[:,~sample_claim_pol_df.columns.duplicated()].copy()
    sample_claim_pol_df = sample_claim_pol_df.reset_index(drop=True)
    
    sample_parti_df = sample_parti_df.loc[:,~sample_parti_df.columns.duplicated()].copy()
    sample_parti_df = sample_parti_df.reset_index(drop=True)
    
    sample_line_df = sample_line_df.loc[:,~sample_line_df.columns.duplicated()].copy()
    sample_line_df = sample_line_df.reset_index(drop=True)
    
    print("Shapes (claims_policy): ", sample_claim_pol_df.shape)
    print("Shapes (line): ", sample_line_df.shape)
    print("Shapes (parti): ", sample_parti_df.shape)
        
    return sample_claim_pol_df, sample_line_df, sample_parti_df


def get_input_raw_data_for_training_thailand(sample_data_size=None):
    
    # Load Countrywise data from different tables   
    all_claim_pol_df = pd.read_pickle("../data/raw/TH/th_raw_claims_policy-2021-04-22.pkl")
    all_line_df = pd.read_pickle("../data/raw/TH/th_raw_line-2021-04-22.pkl")
    all_parti_df = pd.read_pickle("../data/raw/TH/th_raw_participant-2021-04-22.pkl")
    
    # Format ClaimNumber(key_col) as String
    key_col = 'ClaimNumber'
    all_claim_pol_df[key_col] = all_claim_pol_df[key_col].astype(str)
    all_line_df[key_col] = all_line_df[key_col].astype(str)
    all_parti_df[key_col] = all_parti_df[key_col].astype(str)
    
    
    ## Select sample data
    if sample_data_size:
        
        sample_claim_pol_df = all_claim_pol_df.sample(sample_data_size)
        sample_claims = sample_claim_pol_df[key_col].unique().tolist()
        sample_parti_df = all_parti_df[all_parti_df[key_col].isin(sample_claims)].copy()
        sample_line_df = all_line_df[all_line_df[key_col].isin(sample_claims)].copy()
    else:
        
        sample_claim_pol_df = all_claim_pol_df.copy()
        sample_parti_df = all_parti_df.copy()
        sample_line_df = all_line_df.copy()
        
    sample_claim_pol_df = sample_claim_pol_df.drop_duplicates(subset=[key_col])
    
    sample_claim_pol_df = sample_claim_pol_df.loc[:,~sample_claim_pol_df.columns.duplicated()].copy()
    sample_claim_pol_df = sample_claim_pol_df.reset_index(drop=True)
    
    sample_parti_df = sample_parti_df.loc[:,~sample_parti_df.columns.duplicated()].copy()
    sample_parti_df = sample_parti_df.reset_index(drop=True)
    
    sample_line_df = sample_line_df.loc[:,~sample_line_df.columns.duplicated()].copy()
    sample_line_df = sample_line_df.reset_index(drop=True)
    
    print("Shapes (claims_policy): ", sample_claim_pol_df.shape)
    print("Shapes (line): ", sample_line_df.shape)
    print("Shapes (parti): ", sample_parti_df.shape)
        
    return sample_claim_pol_df, sample_line_df, sample_parti_df

    

def create_input_prior_data_for_training(sample_claims=None, key_col='ClaimNumber', prior_key_col='ClaimNumber_Prior'):
    
    # Load Full Prior data
    full_prior_df = pd.read_pickle('../data/raw/prior-data-using-src-claimant.pkl')
    
    if sample_claims:
        sample_prior_df = full_prior_df[full_prior_df[key_col].isin(sample_claims)].copy()
    else:
        sample_prior_df = full_prior_df.copy()
        
    
    sample_prior_df = sample_prior_df.loc[:,~sample_prior_df.columns.duplicated()].copy()
    input_prior_df = sample_prior_df.drop_duplicates(subset=['ClaimNumber', 'ClaimNumber_Prior'])
    input_prior_df = input_prior_df.loc[:,~input_prior_df.columns.duplicated()].copy()
    input_prior_df = input_prior_df.reset_index(drop=True)
    
    input_prior_df['PRIOR_INDEX'] = input_prior_df["ClaimNumber"].astype(str) + "_" + input_prior_df["ClaimNumber_Prior"].astype(str)
    input_prior_df = input_prior_df.drop_duplicates('PRIOR_INDEX')
    
    all_prior_claims_list = sample_prior_df['ClaimNumber_Prior'].astype(str).unique().tolist()
    
#     claim_policy_for_prior_claims_df = get_cc_data_from_claims_list(claims_list=all_prior_claims_list,
#                                                                                  conn=cc_conn,
#                                                                                  config_dict=app_config_dict['db_schemas'],
#                                                                                  table='claims_policy', 
#                                                                                  batch_size=1000)
    
    claim_policy_for_prior_claims_df = pd.read_pickle("../data/raw/claim-policy-info-for-prior-claims-2021-04-29.pkl")
    
#     line_for_prior_claims_df = get_cc_data_from_claims_list(claims_list=all_prior_claims_list,
#                                                                                  conn=cc_conn,
#                                                                                  config_dict=app_config_dict['db_schemas'],
#                                                                                  table='line', 
#                                                                                  batch_size=1000)
    
    line_for_prior_claims_df = pd.read_pickle("../data/raw/line-info-for-prior-claims-2021-04-29.pkl")
    
#     parti_for_prior_claims_df = get_cc_data_from_claims_list(claims_list=all_prior_claims_list,
#                                                                                  conn=cc_conn,
#                                                                                  config_dict=app_config_dict['db_schemas'],
#                                                                                  table='participant', 
#                                                                                  batch_size=1000)
    
    parti_for_prior_claims_df = pd.read_pickle("../data/raw/parti-info-for-prior-claims-2021-04-29.pkl")
    
    claim_policy_for_prior_claims_df = claim_policy_for_prior_claims_df.drop_duplicates(subset=[prior_key_col])
    
    input_prior_df = utils.merge_agg_info(input_prior_df, 
                    agg_raw=claim_policy_for_prior_claims_df, 
                    key_col='ClaimNumber_Prior',
                   fillna=False)
    
    return input_prior_df, line_for_prior_claims_df, parti_for_prior_claims_df


def create_input_prior_data_for_training_thailand(sample_claims=None, cc_conn=None, key_col='ClaimNumber', prior_key_col='ClaimNumber_Prior'):
    
    # Load Full Prior data
    full_prior_df = pd.read_pickle('../data/raw/prior-data-using-src-claimant-thailand.pkl')
    
    full_prior_df = full_prior_df.drop_duplicates(subset=['ClaimNumber', 'ClaimNumber_Prior'])
    
    if sample_claims:
        sample_prior_df = full_prior_df[full_prior_df[key_col].isin(sample_claims)].copy()
    else:
        sample_prior_df = full_prior_df.copy()
        
    
    sample_prior_df = sample_prior_df.loc[:,~sample_prior_df.columns.duplicated()].copy()
    input_prior_df = sample_prior_df.drop_duplicates(subset=['ClaimNumber', 'ClaimNumber_Prior'])
    input_prior_df = input_prior_df.loc[:,~input_prior_df.columns.duplicated()].copy()
    input_prior_df = input_prior_df.reset_index(drop=True)
    
    input_prior_df['PRIOR_INDEX'] = input_prior_df["ClaimNumber"].astype(str) + "_" + input_prior_df["ClaimNumber_Prior"].astype(str)
    input_prior_df = input_prior_df.drop_duplicates('PRIOR_INDEX')
    
    all_prior_claims_list = sample_prior_df['ClaimNumber_Prior'].astype(str).unique().tolist()
    
    print("# Prior Claims: ", len(all_prior_claims_list) )
    
    try:
        claim_policy_for_prior_claims_df = pd.read_pickle("../data/raw/claim-policy-info-for-prior-claims-thailand.pkl")
        line_for_prior_claims_df = pd.read_pickle("../data/raw/line-info-for-prior-claims-thailand.pkl")
        parti_for_prior_claims_df = pd.read_pickle("../data/raw/parti-info-for-prior-claims-thailand.pkl")
    except:
        print('Extracting Claims Data for prior claims')
        claim_policy_for_prior_claims_df = get_cc_data_from_claims_list(claims_list=all_prior_claims_list,
                                                                                     conn=cc_conn,
                                                                                     config_dict=app_config_dict['db_schemas'],
                                                                                     table='claims_policy', 
                                                                                     batch_size=1000)
        
        claim_policy_for_prior_claims_df.columns = [c + "_Prior" for c in claim_policy_for_prior_claims_df.columns]        
        claim_policy_for_prior_claims_df = claim_policy_for_prior_claims_df.drop_duplicates(subset=[prior_key_col])
        claim_policy_for_prior_claims_df.to_pickle("../data/raw/claim-policy-info-for-prior-claims-thailand.pkl")
        
        print('Extracting Line Data for prior claims')
        line_for_prior_claims_df = get_cc_data_from_claims_list(claims_list=all_prior_claims_list,
                                                                                     conn=cc_conn,
                                                                                     config_dict=app_config_dict['db_schemas'],
                                                                                     table='line', 
                                                                                     batch_size=1000)

        line_for_prior_claims_df.columns = [c + "_Prior" for c in line_for_prior_claims_df.columns]
        line_for_prior_claims_df.to_pickle("../data/raw/line-info-for-prior-claims-thailand.pkl")
        
        print('Extracting Participants Data for prior claims')
        parti_for_prior_claims_df = get_cc_data_from_claims_list(claims_list=all_prior_claims_list,
                                                                                     conn=cc_conn,
                                                                                     config_dict=app_config_dict['db_schemas'],
                                                                                     table='participant', 
                                                                                     batch_size=1000)
        
        parti_for_prior_claims_df.columns = [c + "_Prior" for c in parti_for_prior_claims_df.columns]
        parti_for_prior_claims_df.to_pickle("../data/raw/parti-info-for-prior-claims-thailand.pkl")
    
    claim_policy_for_prior_claims_df = claim_policy_for_prior_claims_df.drop_duplicates(subset=[prior_key_col])
    
    input_prior_df = utils.merge_agg_info(input_prior_df, 
                    agg_raw=claim_policy_for_prior_claims_df, 
                    key_col='ClaimNumber_Prior',
                   fillna=False)
    
    return input_prior_df, line_for_prior_claims_df, parti_for_prior_claims_df




if __name__=='__main__':
    
#     sample_claims = ['5200134030', '5200017366', '5200015983', '5200077004', '5200091268']
    
    app_config_dict = utils.load_app_config()

    cc_conn, cursor = base_loader.base_loader_getConnectionObject(app_config_dict)
    result = create_input_prior_data_for_training_thailand(sample_claims=None, cc_conn=cc_conn)
    
            
           
        
        