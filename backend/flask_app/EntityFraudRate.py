import pandas as pd
import numpy as np
import utils
import sql_queries
import pdb


def compute_entity_fraud_rates():
    
    app_config_dict = utils.load_app_config_dict()

    driver_con, driver_cursor = utils.connect2sqlite3(app_config_dict['DRIVER_DB_PATH'])
    cfms_con, cfms_cursor = utils.connect2sqlite3(app_config_dict['CFMS_DB_PATH'])

    # Participant data
    parti_df = pd.read_sql(sql_queries.sql_fetch_participants, driver_con)

    # feedback data
    investigation_feedback_df = pd.read_sql(sql_queries.sql_investigation_feedback, cfms_con)
    
    parti_investigation_df = parti_df.merge(investigation_feedback_df, left_on=['claim_number'], right_on=['IdValue'], how='left')
    parti_investigation_df['Fraud_Ind'] = np.where(parti_investigation_df['InvestigationStatusId'].isin(['I4']) | 
                                                  parti_investigation_df['FinalStatus'].isin(['F2', 'F4']), 1 , 0)
    result = parti_investigation_df.groupby(['ParticipantId'])['Fraud_Ind'].mean()
    result = result.apply(lambda x: "{:.0%}".format(x)).to_dict()
    
    investigation_feedback_df['Fraud_Ind'] = np.where(investigation_feedback_df['InvestigationStatusId'].isin(['I4']) | investigation_feedback_df['FinalStatus'].isin(['F2', 'F4']), 1 , 0)
    
    clm_frd_ind = investigation_feedback_df.set_index(['IdValue'])['Fraud_Ind'].to_dict()
    result.update(clm_frd_ind)
    
    return result
    

if __name__ == "__main__":
    
    result = compute_entity_fraud_rates()
    pdb.set_trace()
    
    