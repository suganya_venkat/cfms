import pandas as pd
import numpy as np
from time import time
import pickle
import sys

import sqlite3
import pickle


# local dependencies
import base_feature_pipeline
import base_transforms
import base_explainer
import base_loader
import utils
import transforms
import data_preparation
import base_text_mining


def feature_table_claim_level(input_raw_data_df, app_config_dict, refer_data_dict, key_col='ClaimNumber', **kwargs):
    
    input_raw_data_df = input_raw_data_df.drop_duplicates(subset=[key_col])
    input_raw_data_df = input_raw_data_df.loc[:,~input_raw_data_df.columns.duplicated()].copy()
    input_raw_data_df = input_raw_data_df.reset_index(drop=True)
    input_raw_data_df[key_col] = input_raw_data_df[key_col].astype(str)
    
    ff = base_feature_pipeline.FeaturePipeline()
    
    ff.add_transformation(on_cols=('Claimed_Amount', 'ClaimedAmt_Currency'), 
                      transformation_f=transforms.transforms_currency_convert,
                      transformation_args={'forex_curr_map': refer_data_dict['CurrencyUSDEqMap']},
                      name='Claimed_Amount_USD') \
    .add_transformation(on_cols=('ClaimSource_1033'), 
                      transformation_f=base_transforms.base_transforms_isin_check_invert,
                      transformation_args={'value_list': ['CSP Webservice'] },
                      name='ClaimSourceNotCSP_Ind') \
    .add_transformation(on_cols=('Inception_Date', 'LossDate'), 
                      transformation_f=base_transforms.base_transforms_days_between,
                      transformation_args=None,
                      name='Diff_Inception_Loss') \
    .add_transformation(on_cols=('ExpirationCancellationDate'), 
                      transformation_f=transforms.transforms_clean_pol_expiry_col,
                      transformation_args=None,
                      name='ExpirationCancellationDate_v2') \
    .add_transformation(on_cols=('ExpirationCancellationDate_v2', 'LossDate'), 
                      transformation_f=base_transforms.base_transforms_days_between,
                      transformation_args=None,
                      name='Diff_ExpireCancel_Loss') \
    .add_transformation(on_cols=('Diff_ExpireCancel_Loss'), 
                      transformation_f=base_transforms.base_transforms_above_threshold_indicator,
                      transformation_args={'threshold': 0,
                                          'include': False },
                      name='LossPostExpiry_Ind') \
    .add_transformation(on_cols=('LossDate', 'ReportDate'), 
                      transformation_f=base_transforms.base_transforms_days_between,
                      transformation_args=None,
                      name='ReportLag') \
    .add_transformation(on_cols=('DepartureDate', 'ReturnDate'), 
                      transformation_f=base_transforms.base_transforms_days_between,
                      transformation_args=None,
                      name='TravelLength') \
    .add_transformation(on_cols=('DateofCancellationCurtailmentDelayJourny', 'LossDate'), 
                      transformation_f=base_transforms.base_transforms_days_between,
                      transformation_args=None,
                      name='CancellationDays_Lag') \
    .add_transformation(on_cols=('Hospital_Start_Date', 'Hospital_End_Date'), 
                      transformation_f=base_transforms.base_transforms_days_between,
                      transformation_args=None,
                      name='HospitalStayLength') \
    .add_transformation(on_cols=('TravelLength', 'HospitalStayLength'), 
                      transformation_f=transforms.transforms_short_travel_with_hospital_stay,
                      transformation_args=None,
                      name='ShortTravelwithHospitalStay') \
    .add_transformation(on_cols=('LossCountry_1033'), 
                      transformation_f=base_transforms.base_transforms_map_values_from_dict,
                      transformation_args={'key_val_dict': refer_data_dict['LossCountryRegionMap'],
                                           'fill_unmapped': 'Unknown'},
                      name='LossCountryRegion') \
    .add_transformation(on_cols=('LossCountryRegion'), 
                      transformation_f=base_transforms.base_transforms_create_indicators_from_list,
                      transformation_args={'ind_list': list(set(list(refer_data_dict['LossCountryRegionMap'].values())))
                                          },
                      name=["LossRegion_" + "_".join(l.split()) for l in list(set(list(refer_data_dict['LossCountryRegionMap'].values())))]
                     ) \
    .add_transformation(on_cols=('DOB'), 
                      transformation_f=base_transforms.base_transforms_min_val,
                      transformation_args=None,
                      name='MinDOB') \
    .add_transformation(on_cols=('MinDOB', 'LossDate'), 
                      transformation_f=base_transforms.base_transforms_days_between,
                      transformation_args=None,
                      name='Diff_Loss_DOB') \
    .add_transformation(on_cols=('Diff_Loss_DOB', 'PolicyCountry_1033'), 
                      transformation_f=transforms.transforms_years_to_retire,
                      transformation_args=None,
                      name='YearsToRetire') \
    .add_transformation(on_cols=('LoadDate', 'PolicyNumber', 'SourceClaimantId'), 
                      transformation_f=transforms.transforms_same_src_claimant_diff_policy,
                      transformation_args=None,
                      name='SameClaimantLoss_MultiplePolicy') \
    .add_transformation(on_cols=('ReportDate'), 
                      transformation_f=base_transforms.base_transforms_day_of_week,
                      transformation_args=None,
                      name='ReportDayoftheweek') \
    .add_transformation(on_cols=('LossDate'), 
                      transformation_f=base_transforms.base_transforms_day_of_week,
                      transformation_args=None,
                      name='LossDayoftheweek') \
    .add_transformation(on_cols=('LossDescription'), 
                      transformation_f=base_transforms.base_transforms_text_cleaner1,
                      transformation_args=None,
                      name='LossDescription_Cleaned') \
    .
    
    