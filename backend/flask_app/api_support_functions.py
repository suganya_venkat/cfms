import pandas as pd
import json
from sqlite_db_add import create_db_connection, write_to_table
from cfms.backend.flask_app.helper_functions_auto_claims import create_table_json


def create_alert_page_data():
    connection, cursor = create_db_connection('artifacts/db_files/claims_data.db')
    rc_query = 'SELECT DISTINCT rc_bucket FROM predictions_reason_codes'
    alert_screen_json = dict()
    rcs = pd.read_sql(rc_query, connection).rc_bucket.values
    for rc in rcs:
        query = "SELECT p.claim_number, p.investigated, p.claim_report_date, p.claim_amt_usd, " \
                "ws.score_grp as severity, tu.user_name as assigned_resource " \
                "FROM predictions p LEFT JOIN predictions_reason_codes rc ON p.claim_number = rc.claim_number " \
                "LEFT JOIN tool_users tu ON p.assigned_to = tu.user_id " \
                "LEFT JOIN claims_weighted_score ws ON p.claim_number = ws.claim_id " \
                "WHERE rc.rc_bucket = '%s' ORDER BY p.score_grp DESC" % rc
        df = pd.read_sql(query, connection)
        alert_screen_json[rc] = create_table_json(df)
    # populating users info
    count_query = 'SELECT user_name, claim_status, COUNT(*) AS count FROM predictions ' \
                  'LEFT JOIN tool_users ON assigned_to = user_id ' \
                  'GROUP BY assigned_to, claim_status '
    df_new = pd.read_sql(count_query, connection)
    alert_screen_json['resources claim count'] = create_table_json(df_new)
    with open('artifacts/alert_page_artifacts/alert_screen_json', 'w') as outfile:
        json.dump(alert_screen_json, outfile)


def create_claim_level_data(claim_num):
    connection, cursor = create_db_connection('artifacts/db_files/input_data.db')
    claim_query = "SELECT * FROM input_claims_data WHERE claim_number = '%s' " % claim_num
    claim_df = pd.read_sql(claim_query, connection)

    req_cols = ['claim_number', 'CLAIMREPORTDATE', 'DATEOFLOSS', 'CLAIMTYPEDESC', 'address',
                'pin_code']
    data = list(claim_df.loc[0, req_cols].values)
    conn, cur = create_db_connection('artifacts/db_files/claims_data.db')
    comment_query = "SELECT user_name, comment, insert_ts FROM user_comments " \
                    "LEFT JOIN tool_users ON user_comments.user_id = tool_users.user_id " \
                    "WHERE claim_number = '%s' " % claim_num
    comments = pd.read_sql(comment_query, conn)
    claim_details_json = dict()
    claim_details_json['claim specific details'] = dict(zip(req_cols, [str(s) for s in data]))
    claim_details_json['comments'] = create_table_json(comments)
    with open('artifacts/alert_page_artifacts/claims_details_json', 'w') as outfile:
        json.dump(claim_details_json, outfile)
    connection.close()
    conn.close()


# use this method to add comments specific to claims
def add_comments_to_claims(u_id, claim_number, txt):
    connection, cursor = create_db_connection('artifacts/db_files/claims_data.db')
    insert_query = 'INSERT INTO user_comments (user_id, claim_number, comment) VALUES (?, ?, ?)'
    cursor.execute(insert_query, (u_id, claim_number, txt))
    connection.commit()
    connection.close()


def create_score_group_based_on_model_weights(w_struct_model=0.5, w_anom_model=0.5):
    if (w_struct_model > 1.0) or (w_struct_model < 0):
        raise ValueError('Model weight cannot exceed one or less than zero')
    if w_anom_model != (1 - w_struct_model):
        w_anom_model = (1 - w_struct_model)
    # access proba scores
    connection, cursor = create_db_connection('artifacts/db_files/claims_data.db')
    struct_proba = pd.read_sql('SELECT * FROM claims_structured_score', connection)
    anomaly_proba = pd.read_sql('SELECT * FROM claims_anomaly_score', connection)
    anomaly_proba = anomaly_proba.set_index('claim_id').loc[struct_proba.claim_id].reset_index()
    weighted_proba = struct_proba.proba_score * w_struct_model + anomaly_proba.proba_score * w_anom_model
    weighted_df = struct_proba.copy()
    weighted_df.proba_score = weighted_proba
    weighted_df['score_grp'] = pd.qcut(weighted_proba.rank(method='first'), 10, labels=False) + 1
    write_to_table(connection, weighted_df, 'claims_weighted_score', clear_existing_data=True)


# anomaly_proba.set_index('claim_id').loc[sk].reset_index()

create_alert_page_data()
# create_claim_level_data('V4304723')
#
# comment_text1 = 'claim looks suspicious in the initial investigation'
# comment_text2 = 'It will be passed to Investigation team'
# comment_text3 = 'Please pass it on for further investigation, ' \
#                 'there is a mismatch observed in submitted documents'
# comment_text4 = 'SUI team concluded that the submitted documents are good, ' \
#                 'No more investigation needed'
# user_id = 3
# add_comments_to_claims(user_id, 'V4304723', comment_text2)
# create_score_group_based_on_model_weights(w_struct_model=0.5, w_anom_model=0.5)