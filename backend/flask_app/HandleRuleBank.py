import sqlite3
import pandas as pd
import utils
import sql_queries
import datetime
import re
import pdb


class RuleBank:
    
    def __init__(self, ):
        self.app_config_dict = utils.load_app_config_dict()
        self.rules_db_con = None
        self.raw_db_con = None
        self.referral_db_con = None
        
    def connect2rulebank(self, ):
        self.rules_db_con, self.rules_db_cursor = utils.connect2sqlite3(self.app_config_dict['RULE_BANK_DB_PATH'])
        
    def connect2rawdata(self, ):
        self.raw_db_con, self.raw_db_cursor = utils.connect2sqlite3(self.app_config_dict['RAW_DATA_DB_PATH'])
        
    def connect2referraldata(self, ):
        self.referral_db_con, self.referral_db_con_cursor = utils.connect2sqlite3(self.app_config_dict['REFERRAL_DB_PATH'])
        
    def add_rules(self, rules_json):
        """
        rules_json = [{"rule1": {'Variable': 'Claim Amount',
                                  'Operator': '>=',
                                  'Value': 500,
                                  'Status': 'Active'}}, ...]
        
        """
        print(rules_json)
        try:
            if self.rules_db_con is None:
                self.connect2rulebank()

            rule_bank_df = pd.read_sql(sql_queries.sql_rule_bank, self.rules_db_con)
            req_cols = rule_bank_df.columns.tolist()

            new_rules_df = pd.DataFrame()
            for rule in rules_json:
                chunk_df = pd.DataFrame(rule).T
                new_rules_df = new_rules_df.append(chunk_df)

            new_rules_df["Date"] = pd.to_datetime(datetime.date.today())
            new_rules_df["Status"] = "Active"
            new_rules_df = new_rules_df[req_cols]
            rule_bank_df = rule_bank_df[req_cols]
            rule_bank_df['Date'] = pd.to_datetime(rule_bank_df['Date']) 
            rule_bank_df = new_rules_df.append(rule_bank_df).drop_duplicates(subset=['Variable', 'Operator'])

            rule_bank_df.reset_index(drop=True, inplace=True)

            rule_bank_df[req_cols].to_sql('rules', self.rules_db_con, index=False, if_exists='replace')
            print("Rule Bank Updated!")
            print("Current Rule Bank head", rule_bank_df[req_cols].head())
            response = {"response": "Success"}
        except:
            response = {"response": "Failure"}
            
        return response
    
    def _get_var_op_val(self, x):
        var = x['ModelFeatureName']
        op = x['Operator']
        val = x['Value']
        return f"{var} {op} {val}"
    
    def get_rule_bank(self, ):
        if self.rules_db_con is None:
            self.connect2rulebank()
        rule_bank_df = pd.read_sql(sql_queries.sql_rule_variable_feats, self.rules_db_con)
        
        operator_desc_map = {"=": "equal to",
                            "<>": "Not equal to",
                            ">": "Greater than",
                            ">=": "Greater than or equal to",
                            "<=": "Less than or equal to",
                            "<": "Less than"}
        rule_bank_df['Rule Description'] = rule_bank_df['Variable'].astype(str) + " " \
                                          + rule_bank_df['Operator'].astype(str).map(operator_desc_map).fillna("") + " " \
                                          + rule_bank_df['Value'].astype(str)
        
        
        if self.referral_db_con is None:
            self.connect2referraldata()
            
        referral_df = pd.read_sql(sql_queries.sql_referrals, self.referral_db_con)
        
        # Compute Stats for each rule
        rule_stats = {}
        for i, rule in enumerate(rule_bank_df.iterrows()):
            rule_row = rule[1]
            rule_chunk_stat = {}
            rule_chunk_stat['RuleNumber'] = f"{i+1}"
            rule_chunk_stat['RuleDescription'] = rule_row['Rule Description']
            rule_chunk_stat['DateCreated'] = rule_row['Date']
            rule_ser = pd.DataFrame([rule[1].T])
            filtered_claims = self.filter_claims(rule_bank_df=rule_ser, filter_active=False)['filtered_claims']
            if filtered_claims:
                sub_ref_df = referral_df[referral_df['CLAIMNUMBER'].astype(str).isin(filtered_claims)].copy()
                tot_alerts = sub_ref_df.shape[0]
                tot_investigations = sub_ref_df['REVIEW_STATUS'].astype(str).str.contains('Investigation').sum()
                tot_frauds = sub_ref_df['FINAL_STATUS'].astype(str).str.contains('Found Fraud').sum()
                tot_savings = sub_ref_df[sub_ref_df['FINAL_STATUS'].astype(str).str.contains('Found Fraud')]['CLAIMAMOUNT'].sum()
            else:
                tot_alerts = 0
                tot_investigations = 0
                tot_frauds = 0
                tot_savings = 0
                
            rule_chunk_stat['Alert'] = int(tot_alerts)
            rule_chunk_stat['Investigation'] = int(tot_investigations)
            rule_chunk_stat['Frauds'] = int(tot_frauds)
            rule_chunk_stat['Savings'] = tot_savings
            
            rule_stats[f'rule{i+1}'] = rule_chunk_stat
            
        return rule_stats
            
                      
    def filter_claims(self, rule_bank_df=None, filter_active=True):
        response = {}
        if self.rules_db_con is None:
            self.connect2rulebank()
        
        if rule_bank_df is None:
            rule_bank_df = pd.read_sql(sql_queries.sql_rule_variable_feats, self.rules_db_con)
        
        print("Current Rule bank")
        print(rule_bank_df)
        
        if filter_active:
            rule_bank_df = rule_bank_df[(rule_bank_df['Status'] == 'Active') & (~rule_bank_df['ModelFeatureName'].isna())].copy()

        if rule_bank_df.shape[0] == 0:
            print("Rule Bank is Empty")
            return {"filtered_claims": "None"}

        rules_str = " and ".join(rule_bank_df.apply(lambda x: self._get_var_op_val(x) , axis=1).tolist())
        print(rules_str)

        if self.raw_db_con is None:
            self.connect2rawdata()
                
        filtered_claims_list = pd.read_sql(sql_queries.sql_dynamic_fetch_claims.format(rules_str=rules_str), self.raw_db_con)['CLAIMNUMBER'].tolist()
        
        print(sql_queries.sql_dynamic_fetch_claims.format(rules_str=rules_str))
        
        response = {"filtered_claims": filtered_claims_list }
        
            
        return response
    
    def delete_from_rule_bank(self, rule_description, rule_bank_df=None):
        operator_desc_map = {"=": "equal to",
                            "<>": "Not equal to",
                            ">": "Greater than",
                            ">=": "Greater than or equal to",
                            "<=": "Less than or equal to",
                            "<": "Less than"}

        operator = re.sub("[0-9a-zA-Z]+", "", rule_description.replace("Greater than or equal to", ">=").replace("Less than or equal to", "<=").replace("Not equal to", "<>").replace("Greater than", ">").replace("Less than", "<").replace("equal to", "=") ).strip()
        variable = re.sub("[^a-zA-Z ]+", "", rule_description.replace("Greater than or equal to", ">=").replace("Less than or equal to", "<=").replace("Not equal to", "<>").replace("Greater than", ">").replace("Less than", "<").replace("equal to", "=") ).strip()
        value = re.sub("[a-zA-Z>=<]", "", rule_description.replace("Greater than or equal to", ">=").replace("Less than or equal to", "<=").replace("Not equal to", "<>").replace("Greater than", ">").replace("Less than", "<").replace("equal to", "=") ).strip()
        if '.' in value:
            value = float(value)

        print("Variable: ", variable)
        print("Operator: ", operator)
        print("Value: ", value)

        response = {}
        if self.rules_db_con is None:
            self.connect2rulebank()

        self.rules_db_cursor.execute(sql_queries.sql_delete_from_rule_bank.format(variable=variable, operator=operator, value=value))
        self.rules_db_con.commit()

        return {'response': "SUCCESS"}
        
        
    
if __name__ == '__main__':
    
    obj = RuleBank()
    
    rules_json = [{"rule1": {'Variable': 'Claims Amount',
                                  'Operator': '>=',
                                  'Value': 2500,
                                  'Status': 'Active'}}, 
                  {"rule2": {'Variable': 'Claims Age',
                                  'Operator': '>=',
                                  'Value': 20,
                                  'Status': 'Active'}}, 
                 ]
    
    # test add rule
    obj.add_rules(rules_json)
    
    # test filter_claims based on rules
    filtered_claims = obj.filter_claims()
    
    # Test get rule bank
    rule_bank_dict = obj.get_rule_bank()
    print(rule_bank_dict)
    
    pdb.set_trace()
    
    
