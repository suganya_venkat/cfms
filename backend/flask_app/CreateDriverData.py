"""
Extracts the subset of required Raw Data and dumps to Driver Tables
These Driver Tables are then used for downstream operations

For purpose of demo, sample tables are taken
    claims: Claims Level Details
    Policy: PolicyNumber Level Details
    Participants: ClaimNumber & Participant Level Details

In Practice, we expect that all the data required to run the downstream analysis 
to be dumped inside cfms database
"""