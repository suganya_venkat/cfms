import os
import sqlite3
import yaml
import pandas as pd
import utils
from flask import jsonify
import datetime
import numpy as np
import sql_queries
import pdb


class ReportingView():
    
    def __init__(self, config_filepath=None):
        self.app_config_dict = utils.load_app_config_dict(config_filepath)
        self.driver_con, self.drive_cursor = utils.connect2sqlite3(self.app_config_dict['DRIVER_DB_PATH'])
        self.cfms_con, self.cfms_cursor = utils.connect2sqlite3(self.app_config_dict['CFMS_DB_PATH'])
        
        self.default_filter_dict = {"CLAIMREPORTDATE": [{"from":  "2021-01-01", "to": "2022-01-01"}],
                                    "STATE" : ['Wilayah', 'Sarawak', 'Perak', 'Selangor', 'Johor', 'Negeri',
                                           'Pulau', 'Melaka', 'Sabah', 'Penang', 'Jalan', 'Kedah', 'Taman',
                                           'Bandar', 'Chain', 'Senai', 'Terengganu', 'Pahang', 'Malacca',
                                           'Perlis', 'Kelantan', 'Off', 'Kuala'],
                                    "CLAIM_TYPE": ['TPBI', 'Natural Calamities', 'Accidental', 'Fire', 'Theft',
                                                   'Vandalism'],
                                    "CLAIM_AMT_USD": [ {"from": "0" }]
                                   
                                   }
        
    def create_table_json(self, df, col=None):
        """
        Creates tabular json output
        :param df: pandas DataFrame or Series
        :param col: String
        :return: Dictionary
        """
        if col:
            result = df[col].value_counts().to_json()
        else:
            result = df.to_json()
        return result
    
    def calculate_closure_rate(self, df):
        df_new = df.copy()
        df_new.index =pd.to_datetime(df_new.CLAIMREPORTDATE)
        df_new = df_new.groupby([pd.Grouper(freq='M'),
                                 'CLAIM_STATUS']).agg({'STATE': 'count'}).rename(columns={'STATE': 'COUNT'})
        df_new = df_new.reset_index()
        df_new = df_new[df_new.CLAIMREPORTDATE > '2021-01-01']
        return self.create_table_json(df_new)
    
    def get_hist_data_investigation_summary(self, ):
        """
        Get relevant KPIs for Investigation Overview of historical investigated data
        (This indicates the Baseline)
        """
        
        raw_df = pd.read_sql(sql_queries.sql_fetch_all_claims, self.driver_con)
        
        # Merge from Investigation DBs
        feedback_df = pd.read_sql(sql_queries.sql_investigation_feedback, self.cfms_con)
        claims_investigation_df = raw_df.merge(feedback_df,
                                               left_on=['claim_number'],
                                               right_on=['IdValue'], how='left')
        
        # Model output
        model_out_df = pd.read_sql(sql_queries.sql_model_output, self.cfms_con)
        req_model_out_df = model_out_df[model_out_df['ModelId'] == 'XGB-8dd62108-b407-11ec-930a-062fcc6cb972'].copy()
        req_model_out_df.reset_index(drop=True, inplace=True)
        
        df = claims_investigation_df.merge(req_model_out_df, left_on=['claim_number'], right_on=['IdFieldValue'], how='left')
                
        summary_json = {}

        # Total data for modelling
        hist_df = df[pd.to_datetime(df['CLAIMREPORTDATE']).dt.year == 2020].copy()
        summary_json["hist_total_data"] = hist_df['claim_number'].nunique()

        # Used for modelling
        summary_json["hist_total_claims_for_modelling"] = hist_df['claim_number'].nunique()

        # Total Investigated
        summary_json["hist_total_investigated"] = (hist_df['InvestigationStatusId'].isin(['IN-SIU', 'I1', 'I2', 'I3', 'I4', 'I5'])).sum()
        summary_json["hist_total_investigated_perc"] = summary_json["hist_total_investigated"] / summary_json["hist_total_data"]

        summary_json["hist_total_investigated_str"] = "{:,} / ({:.1%})".format(summary_json["hist_total_investigated"],
                                                                      summary_json["hist_total_investigated_perc"])

        # Total Found Fraud from the Investigated cases
        summary_json["hist_total_fraud_found"] = (hist_df['FinalStatus'].isin(['F4', 'F2'])).sum()
        summary_json["hist_investigated_fraud_found_perc"] = summary_json["hist_total_fraud_found"] / summary_json["hist_total_investigated"]

        summary_json["hist_total_fraud_found_str"] = "{:,} / ({:.1%})".format(summary_json["hist_total_fraud_found"],
                                                                      summary_json["hist_investigated_fraud_found_perc"])

        # Fraud Capture Rate
        summary_json["hist_overall_fraud_capture_rate"] = "{:.1%}".format(summary_json["hist_total_fraud_found"]/summary_json["hist_total_data"])
        
        # Total Fraud Claimed Amount
        tot_frd_claimed = hist_df[hist_df['FinalStatus'].isin(['F4', 'F2'])]['CLAIM_AMT_USD'].sum()
        summary_json['hist_total_claimed_amt'] = tot_frd_claimed
        
        # In Sample Validation KPIs
#         summary_json['hist_val_insample_total_claims'] = summary_json["hist_total_data"]
#         summary_json['hist_val_insample_total_predicted_frauds'] = '266 / (+{:.1%})'.format( 83/183 )

        return summary_json
    
    def get_referrals_summary(self, df):
        summary_json = {}
        current_df = df[pd.to_datetime(df['CLAIMREPORTDATE']).dt.year > 2020].copy()
        # Referrals
        referral_df = current_df[current_df['ModelScoreGroupId'].isin(['SH', 'H'])].copy()

        referral_df.reset_index(drop=True, inplace=True)
                
        tot_overall_claims = current_df['claim_number'].nunique()
        
        tot_referred = referral_df['claim_number'].nunique()
        
        summary_json["cur_total_referred"] = tot_referred
        summary_json["cur_total_referred_perc"] = summary_json["cur_total_referred"] / tot_overall_claims
        summary_json['cur_total_referred'] = "{:,} / ({:.1%})".format(summary_json["cur_total_referred"],
                                                                      summary_json["cur_total_referred_perc"])
        

        # Total Referrals Assigned / Picked up for Investigation
        summary_json['cur_referrals_assigned'] = (referral_df['InvestigationStatusId'].isin(['I1', 'I3', 'I4'])).sum()
        summary_json['cur_referrals_assigned_perc'] = summary_json['cur_referrals_assigned'] / tot_overall_claims
        summary_json['cur_referrals_assigned_str'] = "{:,} / ({:.1%})".format(summary_json["cur_referrals_assigned"],
                                                                      summary_json["cur_referrals_assigned_perc"])

        # Total Referrals Assigned / Picked up for Investigation
        summary_json['cur_referrals_fraud_captured'] = (referral_df['FinalStatus'].isin(['F2'])).sum()
        summary_json['cur_referrals_fraud_captured_perc'] = summary_json['cur_referrals_fraud_captured']/tot_referred
        summary_json['cur_referrals_fraud_captured_str'] = "{:,} / ({:.1%})".format(summary_json["cur_referrals_fraud_captured"],
                                                                      summary_json["cur_referrals_fraud_captured_perc"])
        
        # Total Claimed Amount In Fraud Claims
        tot_frd_claimed = referral_df[referral_df['FinalStatus'].isin(['F2'])]['CLAIM_AMT_USD'].sum()
        summary_json['cur_referrals_total_claimed_amt'] = tot_frd_claimed

        return summary_json
    
    def get_nonreferrals_summary(self, df):
        summary_json = {}
        current_df = df[pd.to_datetime(df['CLAIMREPORTDATE']).dt.year > 2020].copy()
        tot_overall_claims = current_df['claim_number'].nunique()
        
        # Non-Referrals
        nonreferral_df = current_df[~current_df['ModelScoreGroupId'].isin(['SH', 'H'])].copy()
        tot_nonreferred = nonreferral_df['claim_number'].nunique()

        nonreferral_df.reset_index(drop=True, inplace=True)

        summary_json["cur_total_nonreferred"] = tot_nonreferred
        summary_json["cur_total_nonreferred_perc"] = summary_json["cur_total_nonreferred"] / tot_overall_claims
        summary_json['cur_total_nonreferred'] = "{:,} / ({:.1%})".format(summary_json["cur_total_nonreferred"],
                                                                      summary_json["cur_total_nonreferred_perc"])

        # Total Referrals Assigned / Picked up for Investigation
        summary_json['cur_nonreferrals_assigned'] = (nonreferral_df['InvestigationStatusId'].isin(['IN-SIU', 'I1',
                                                                                                    'I3', 'I4'])).sum()

        summary_json['cur_nonreferrals_assigned_perc'] = summary_json['cur_nonreferrals_assigned'] / tot_nonreferred
        summary_json['cur_nonreferrals_assigned_str'] = "{:,} / ({:.1%})".format(summary_json["cur_nonreferrals_assigned"],
                                                                      summary_json["cur_nonreferrals_assigned_perc"])

        # Total Referrals Assigned / Picked up for Investigation
        summary_json['cur_nonreferrals_fraud_captured'] = (nonreferral_df['FinalStatus'].isin(['F2'])).sum()
        summary_json['cur_nonreferrals_fraud_captured_perc'] = summary_json['cur_nonreferrals_fraud_captured'] / tot_nonreferred
        summary_json['cur_nonreferrals_fraud_captured_str'] = "{:,} / ({:.1%})".format(summary_json["cur_nonreferrals_fraud_captured"],
                                                                      summary_json["cur_nonreferrals_fraud_captured_perc"])
        
        
        # Total Claimed Amount In Fraud Claims
        tot_frd_claimed = nonreferral_df[nonreferral_df['FinalStatus'].isin(['F2'])]['CLAIM_AMT_USD'].sum()
        summary_json['cur_nonreferrals_total_claimed_amt'] = tot_frd_claimed

        return summary_json
    
    def open_claims_investigation_summary(self, df):
    
        summary_json = {}

        open_df = df[df['CLAIM_STATUS'] == "open"].copy()

        # total open claims
        summary_json["total_open_claims"] = open_df['claim_number'].nunique()

        # total_open claims which are referred 
        summary_json["total_open_reffered_claims"] = (open_df['ModelScoreGroupId'].isin(['SH', 'H'])).sum()

        # total_open claims which are nonreferred 
        summary_json["total_open_nonreffered_claims"] = (~open_df['ModelScoreGroupId'].isin(['SH', 'H'])).sum()

        open_referred_df = open_df[open_df['ModelScoreGroupId'].isin(['SH', 'H'])].copy()

        summary_json['total_referrals_assigned_under_investigation'] = open_referred_df[open_referred_df['InvestigationStatusId'].isin(['I1', 'I3', 'I4'])]['claim_number'].nunique()
        summary_json['total_referrals_assigned_under_investigation_perc'] = summary_json['total_referrals_assigned_under_investigation']/ summary_json["total_open_reffered_claims"]
        summary_json['total_referrals_assigned_under_investigation_str'] = "{:,} / ({:.1%})".format(summary_json["total_referrals_assigned_under_investigation"],
                                                                      summary_json["total_referrals_assigned_under_investigation_perc"])

        summary_json['total_nonreferrals_under_investigation'] = open_referred_df[open_referred_df['InvestigationStatusId'].isin(['IN-SIU'])]['claim_number'].nunique()

        # Un-assigned claims
        summary_json['total_unassigned_under_investigation'] = open_referred_df[open_referred_df['InvestigationStatusId'].isin(['I0'])]['claim_number'].nunique()
        summary_json['total_unassigned_under_investigation_perc'] =  summary_json['total_unassigned_under_investigation']/ summary_json["total_open_claims"]
        summary_json['total_unassigned_under_investigation_str'] = "{:,} / ({:.1%})".format(summary_json["total_unassigned_under_investigation"],
                                                                      summary_json["total_unassigned_under_investigation_perc"])
        
        summary_json['charts'] = {}
        # Open claims charts
        # Open claims by Investigation Status Description
        tmp = open_df['InvestigationStatusDescription'].value_counts()
        tmp.name = 'Claims'
        tmp.index.name = 'Investigation Status'
        claims_by_investigation_stat_dict = eval(tmp.to_json(orient='table'))['data']
        summary_json["charts"]['open_claims_by_investigation_status'] = claims_by_investigation_stat_dict

        # Open claims by State
        tmp = open_df['STATE'].value_counts()
        tmp.name = 'Claims'
        tmp.index.name = 'State'
        claims_by_state_dict = eval(tmp.to_json(orient='table'))['data']
        summary_json["charts"]['open_claims_by_state'] = claims_by_state_dict

        # Open claims by CLAIM_TYPE
        tmp = open_df['CLAIM_TYPE'].value_counts()
        tmp.name = 'Claims'
        tmp.index.name = 'Claim Type'
        claims_by_clm_type_dict = eval(tmp.to_json(orient='table'))['data']
        summary_json["charts"]['open_claims_by_clm_type'] = claims_by_clm_type_dict

        open_df['MonthYear'] = open_df['CLAIMREPORTDATE'].apply(lambda x: pd.to_datetime(str(x)).strftime("%b") + "-" + \
                                                        str(pd.to_datetime(str(x)).strftime("%Y")) ) 

        # Open claims by Month
        tmp = open_df['MonthYear'].value_counts()
        tmp.name = 'Claims'
        tmp.index.name = 'Month'
        claims_by_month_dict = eval(tmp.to_json(orient='table'))['data']
        summary_json["charts"]['open_claims_by_month'] = claims_by_month_dict

        return summary_json
    
    def create_filters_str(self, filters_json):

        filters_dict = filters_json['reporting_view_filters']

        filter_list = []
        range_filters = []
        inclusion_filters = []
        
        for k, v in filters_dict.items():
            print("k: ", k)
            
            if v == "all" or "all" in v:
                print("Assigned ")
                v = self.default_filter_dict[k]
            else:
                if k == "CLAIM_AMT_USD":
                    v = [{"from": str(val).split("-")[0] , "to": str(val).split("-")[1] } if "-" in val 
                         else {"from": str(val)[:-1]}  for val in v  ]
                
            if k in self.app_config_dict['VIEW_FILTERS']['RANGE_FILTERS']:
                rng_filter_chunk_list = []
                for rng_i, rng in enumerate(v):
                    if "from" in rng and "to" in rng:
                        rng_filter_chunk = f" ({k} >= '{rng['from']}' and {k} <= '{rng['to']}' )"
                    elif "from" in rng and "to" not in rng:
                        rng_filter_chunk = f" ({k} >= '{rng['from']}' )"
                    elif "to" in rng and "from" not in rng:
                        rng_filter_chunk = f" ({k} <= '{rng['to']}' )"
                    else:
                        raise Exception("Range Undefined") 
                    rng_filter_chunk_list.append(rng_filter_chunk)
                        
                range_filters.append( " ( " + " or ".join(rng_filter_chunk_list) + " ) " )
            elif k in self.app_config_dict['VIEW_FILTERS']['INCLUSION_FILTERS']:
                if len(v) == 1:
                    v = [v[0], v[0]]
                incl_tuple = tuple(v)
                inclusion_filters.append(f"{k} in {incl_tuple}")
            else:
                raise Exception("Unknown view filters")

        filter_str = " and ".join(inclusion_filters) + " and " + " and ".join(range_filters) 
        return filter_str
    
    def load_filtered_subset(self, filters_json):
        filter_str = self.create_filters_str(filters_json)
        filter_claims_query = """select * from Claims where """ + filter_str
        print("Query: ", filter_claims_query)
        raw_df = pd.read_sql(filter_claims_query, self.driver_con)
        
        # Merge from Investigation DBs
        feedback_df = pd.read_sql(sql_queries.sql_investigation_feedback, self.cfms_con)
        claims_investigation_df = raw_df.merge(feedback_df,
                                               left_on=['claim_number'],
                                               right_on=['IdValue'], how='left')
        
        # Model output
        model_out_df = pd.read_sql(sql_queries.sql_model_output, self.cfms_con)
        req_model_out_df = model_out_df[model_out_df['ModelId'] == 'XGB-8dd62108-b407-11ec-930a-062fcc6cb972'].copy()
        req_model_out_df.reset_index(drop=True, inplace=True)
        
        df = claims_investigation_df.merge(req_model_out_df, left_on=['claim_number'], right_on=['IdFieldValue'], how='left')
        
        # subset
        df = df[~df['STATE'].isin(['Off', 'Chain'])].reset_index(drop=True)
        
        return df
    
    def create_json_for_Investigation_screen_v2(self, df):
        final_json = {}
        current_df = df[pd.to_datetime(df['CLAIMREPORTDATE']).dt.year > 2020].copy()
        final_json['total_claims_since_ref_start_date'] = current_df['claim_number'].nunique()
        final_json['hist_summary'] = self.get_hist_data_investigation_summary()
        final_json['referral_summary'] = self.get_referrals_summary(df)
        final_json['nonreferral_summary'] = self.get_nonreferrals_summary(df)
        final_json['under_investigation_summary'] = self.open_claims_investigation_summary(df)
        tot_frds = current_df['FinalStatus'].isin(['F2']).sum()
        final_json['overall_fraud_capture'] = tot_frds
        final_json['overall_fraud_capture_perc'] = tot_frds/ current_df['claim_number'].nunique()
        final_json['overall_fraud_capture_str'] = "{:,} / ({:.1%})".format(final_json["overall_fraud_capture"],
                                                                      final_json["overall_fraud_capture_perc"])
        
        return final_json
        
    
    def create_json_for_Investigation_screen(self, df):
        df = df.copy()
        
#         feedback_df = pd.read_sql(sql_queries.sql_investigation_feedback, self.cfms_con) 
#         df.drop_duplicates(subset=['claim_number'], inplace=True)
#         feedback_df = feedback_df.drop_duplicates(subset=['IdField', 'IdValue'])
#         df = df.merge(feedback_df[['IdField', 'IdValue', 'InvestigationStatusId', 'FinalStatus']],
#                      left_on=['claim_number'], right_on=['IdValue'], how='left')
        df['Investigated'] = np.where(df['InvestigationStatusId'] != 'I0', 'Y', 'N')
        df['target'] = np.where(df['FinalStatus'].isin(['F4']), 1, 0)
#         df.rename(columns={"claim_number": 'claim_number'}, inplace=True) 
        
        summary_dict = dict()
        summary_dict['Claims_Initiated'] = int(df.shape[0])
        summary_dict['Total_Investigated'] = int(df.Investigated.value_counts()['Y'])
        df_inv = df[df['Investigated'] == 'Y'].copy()
        summary_dict['Total_Closed'] = int(df_inv.CLAIM_STATUS.value_counts()['closed'])
        summary_dict['Paid_Claims'] = int(df_inv.PAID.value_counts()['Yes'])
        summary_dict['Rejected'] = int(df_inv.PAID.value_counts()['Rejected'])
        summary_dict['Fraud_Suspected_Claims'] = int(df_inv.target.value_counts()[1])
        summary_dict['Investigation_rate'] = (df_inv.shape[0]/df.shape[0])
        summary_dict['Fraud_detection'] = (df_inv.target.value_counts()[1]/df_inv.shape[0])
        df_closed = df_inv[df_inv.CLAIM_STATUS == 'closed'].copy()
        
        # Claims Investigated By State
        tmp = df_inv.groupby(['STATE'])['claim_number'].nunique()
        tmp = 100 * (tmp / df_inv['claim_number'].nunique())
        tmp = tmp.reset_index().rename(columns={'claim_number': 'Investigated_status'})
        claim_investigated_by_state_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Among Investigated Claims Distribution of Paid Statuses
        tmp = 100*(df_inv['PAID'].value_counts() / df_inv['claim_number'].nunique())
        investigated_paid_status_dist_dict = eval(tmp.to_json(orient='table'))['data']
        
        summary_dict['Investigated_status_by_states_perc'] = claim_investigated_by_state_dict
        summary_dict['Investigated_Payment_Status'] = investigated_paid_status_dist_dict
        
        print(summary_dict)
        
        return summary_dict
      
    def claims_overview(self, df):
        
#         feedback_df = pd.read_sql(sql_queries.sql_investigation_feedback, self.cfms_con) 
        
#         df.drop_duplicates(subset=['claim_number'], inplace=True)
        
#         feedback_df = feedback_df.drop_duplicates(subset=['IdField', 'IdValue'])
#         df = df.merge(feedback_df[['IdField', 'IdValue', 'InvestigationStatusId', 'FinalStatus']],
#                      left_on=['claim_number'], right_on=['IdValue'], how='left'
#                      )
        
        df['Investigated'] = np.where(df['InvestigationStatusId'] != 'I0', 'Y', 'N')
#         df.rename(columns={"claim_number": 'claim_number'}, inplace=True) 
        
        # Claim Count by Claim Type
        tmp = df['CLAIM_TYPE'].value_counts()
        tmp.name = 'Claims'
        tmp.index.name = 'Claims_type'
        claims_by_claim_type_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Claim Count by State
        tmp = df['STATE'].value_counts()
        tmp.name = 'Claims'
        tmp.index.name = 'state'
        claims_by_state_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Approved Amount by Claim type Histogram
        tmp = df.groupby('CLAIM_TYPE').agg({'APPRV_AMT_USD': 'sum'}).rename(columns={'APPRV_AMT_USD': 'total_approved_amt'})
        tmp.index.name = 'claim_type'
        approved_claimed_amt_by_clm_type_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Approved Amount by State Histogram
        tmp = df.groupby('STATE').agg({'APPRV_AMT_USD': 'sum'}).rename(columns={'APPRV_AMT_USD': 'total_approved_amt'})
        tmp.index.name = 'state'
        approved_claimed_amt_by_state_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Statewise Total Approved Amount & Claims
        tmp = df.groupby('STATE').agg({'APPRV_AMT_USD': 'sum', 'claim_number': 'count'}).rename(columns={'claim_number': 'Claim_count'})
        tmp = df.groupby('STATE').agg({'APPRV_AMT_USD': 'sum', 'claim_number': 'count'}).rename(columns={'claim_number': 'Claim_count'})
        tmp.index.name = 'STATE'
        table_statewise_apprv_amt_claims_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Last 12 Months claim count
        tmp = df[['CLAIMREPORTDATE', 'claim_number', 'CLAIM_STATUS']].copy()
        tmp['Month'] = pd.to_datetime(tmp['CLAIMREPORTDATE']).dt.month
        tmp['Year'] = pd.to_datetime(tmp['CLAIMREPORTDATE']).dt.year
        tmp = tmp[tmp['Year']>=2021]
        tmp['YearMonth'] = pd.to_datetime(tmp['Year'].astype(str) + "-" + tmp['Month'].astype(str))
        tmp = tmp.groupby(['YearMonth', 'CLAIM_STATUS'])['claim_number'].nunique()
        tmp = tmp.reset_index().rename(columns={"YearMonth": "CLAIMREPORTDATE", 'claim_number': 'COUNT'})
        last_12_months_claim_count_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Claim Count Per Report Date
        tmp = df[['CLAIMREPORTDATE', 'claim_number']].copy()
        base = pd.to_datetime(df['CLAIMREPORTDATE']).max()
        date_list = [pd.to_datetime(str(base - datetime.timedelta(days=x))[:10] ) for x in range(365*3)]
        dt_df = pd.DataFrame({'CLAIMREPORTDATE': date_list})
        tmp = tmp.groupby(['CLAIMREPORTDATE'])['claim_number'].nunique().reset_index()
        tmp['CLAIMREPORTDATE'] = pd.to_datetime(tmp['CLAIMREPORTDATE'])
        tmp = dt_df.merge(tmp.reset_index(), on=['CLAIMREPORTDATE'], how='left')
        tmp['claim_number'] = tmp['claim_number'].fillna(0)
        tmp = tmp.rename(columns={'claim_number': 'CLAIMREPORTDATE_count'}).drop(columns=['index'])
        daywise_claim_count_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Fraud Ratio
        tmp = pd.Series(np.where(df['Investigated']=='Y', "Fraud-suspected", "Normal"))
        tmp = tmp.value_counts()
        tmp.name = 'target'
        fraud_ratio_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Amounts by type
        tmp = df.groupby(['CLAIM_TYPE'])[['CLAIM_AMT_USD', 'APPRV_AMT_USD']].agg(sum)
        amounts_by_claim_type = eval(tmp.to_json(orient='table'))['data']
        
        # Amounts by State
        tmp = df.groupby(['STATE'])[['CLAIM_AMT_USD', 'APPRV_AMT_USD']].agg(sum)
        amounts_by_state = eval(tmp.to_json(orient='table'))['data']
        
        # Total Amounts
        tot_claimed = df['CLAIM_AMT_USD'].sum()
        tot_approved = df['APPRV_AMT_USD'].sum()
        approved_ratio = tot_approved/tot_claimed

        # Claims By Final Status
        tmp = df['FinalStatusDescription'].value_counts()
        tmp.name = 'Claims'
        tmp.index.name = 'Final Claim Status'
        claims_by_claim_final_status = eval(tmp.to_json(orient='table'))['data']

        summary_dict = {'Claims Initiated': len(df),
                    'Claims in Progress': len(df.CLAIM_STATUS[df.CLAIM_STATUS == 'open']),
                    'Claims Closed': len(df.CLAIM_STATUS[df.CLAIM_STATUS == 'closed']),
                    'Claims Paid': {'counts': df.PAID.value_counts().to_dict(),
                                    'Amount Paid': (df[df.PAID == 'Yes'])['APPRV_AMT_USD'].sum()},
                    'Average Claim Amount': df['CLAIM_AMT_USD'].sum() / len(df),
                    'Average Paid Amount':
                        df['APPRV_AMT_USD'][df.PAID == 'Yes'].sum() / len(df['APPRV_AMT_USD'][df.PAID == 'Yes']),
                    'Closure ratio': df.CLAIM_STATUS.value_counts()['closed'].sum() / len(df),
                        
                    'Claims_by_Claim_Type_Histogram': claims_by_claim_type_dict,
                        
                    'Claims_by_state_Histogram': claims_by_state_dict,
                        
                    'Approved_Claimed_Amount_by_Claim_Type_Histogram': {"TOTAL_APPROVED_AMOUNT": approved_claimed_amt_by_clm_type_dict},
                        
                    'Approved_Claimed_Amount_by_State_Histogram': {"TOTAL_APPROVED_AMOUNT": approved_claimed_amt_by_state_dict},   
                        
                        'Tabledata_statewise': table_statewise_apprv_amt_claims_dict,
                        'Last12MonthCount': last_12_months_claim_count_dict,
                        'Daywise_Claim_Count': daywise_claim_count_dict,
                        'Fraud_Ratio': fraud_ratio_dict,
                        
                        "AmountsByClaimType": amounts_by_claim_type,
                        "AmountsByState": amounts_by_state,
                        "TotalClaimedAmount": tot_claimed,
                        "TotalApprovedAmount": tot_approved,
                        "ApprovedAmountRatio": approved_ratio,
                        "claims_by_claim_final_status": claims_by_claim_final_status
                       }
        
        return summary_dict


    
if __name__=="__main__":
    
#     filters_json = {
#                 "reporting_view_filters": {
#                     "CLAIMREPORTDATE": [
#                         {
#                             "from": "2016-06-17 00:00:00",
#                             "to": "2021-12-11 00:00:00"
#                         }
#                     ],
#                     "STATE": [
#                         "selangor",
#                         "perak"
#                     ],
#                     "CLAIM_TYPE": [
#                         "self - accident",
#                         "Animal Collision"
#                     ],
#                     "CLAIM_AMT_USD": [
#                         "1-1000",
#                         "1000-10000",
#                         "10000+"
#                 ]
#                 }
#             }
    
    filters_json = {
                "reporting_view_filters": {
                    "CLAIMREPORTDATE": [
                        {
                            "from": "2020-01-01 00:00:00",
                            "to": "2021-12-31 00:00:00"
                        }
                    ],
                    "STATE": ["all"
                    ],
                    "CLAIM_TYPE": ["all"
                    ],
                    "CLAIM_AMT_USD": ["all"
                                     ]
                }
            }

    
    obj = ReportingView()
    
    sub_df = obj.load_filtered_subset(filters_json)
    print("shape sub_df: ", sub_df.shape)
#     summary_dict = obj.claims_overview(df=sub_df)
#     print("summary_dict: ", summary_dict)
#     investigation_summary_dict = obj.create_json_for_Investigation_screen(df=sub_df)
#     print("Investigation Summary Dict: ", investigation_summary_dict)
#     r0 = obj.get_hist_data_investigation_summary()
#     r1 = obj.get_referrals_summary(sub_df)
#     r2 = obj.get_nonreferrals_summary(sub_df)
#     r3 = obj.open_claims_investigation_summary(sub_df)
    investigation_summary_dict = obj.create_json_for_Investigation_screen_v2(sub_df)
    
    import json
    
    class NpEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, np.integer):
                return int(obj)
            if isinstance(obj, np.floating):
                return float(obj)
            if isinstance(obj, np.ndarray):
                return obj.tolist()
            return super(NpEncoder, self).default(obj)
    
#     f = open("claims_overview_summary_dict.json", "w")
#     f.write(json.dumps(summary_dict, cls=NpEncoder))
#     f.close()
    
#     f = open("investigation_summary_dict.json", "w")
#     f.write(json.dumps(investigation_summary_dict, cls=NpEncoder))
#     f.close()
            