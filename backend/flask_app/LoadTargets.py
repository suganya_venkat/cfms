import pandas as pd
import numpy as np
import sql_queries
import utils


def get_fraud_claims():
    
    app_config_dict = utils.load_app_config_dict()
    cfms_con, cfms_cursor = utils.connect2sqlite3(app_config_dict['CFMS_DB_PATH'])
    investigation_feedback_df = pd.read_sql(sql_queries.sql_investigation_feedback, cfms_con)
    
    result = investigation_feedback_df[investigation_feedback_df['FinalStatus'].isin(["F4", "F2"])]['IdValue'].unique().tolist()
    return result


if __name__ == "__main__":
    
    result = get_fraud_claims()
    print(result.__len__())
    