import pandas as pd
import networkx as nx
import json
import sql_queries
import utils
import re
import EntityFraudRate
import pdb

class GraphNetwork:
    
    def __init__(self, ):
        self.app_config_dict = utils.load_app_config_dict()
        self.driver_con, self.driver_cursor = utils.connect2sqlite3(self.app_config_dict['DRIVER_DB_PATH'])
        self.G = None
        self.parti_frd_rate = None
    
    def load_full_graph(self, ):
        try:
            print("Loaded the Graph DB pickle")
            self.G = nx.read_gpickle(self.app_config_dict['GRAPH_DB_PICKLE_PATH'])
            return True
        except:
            print("Loading failed, trying building the graph")
            try:
                self.G = self.build_graph()
                print("Graph built")
            except:
                print("Error building Graph")
                return False
            
    def get_claim_basic_info(self, clm):
        
        claims_df = pd.read_sql(f"""select * from Claims where claim_number='{clm}' """, self.driver_con)
        clm_info_dict = list(claims_df[claims_df['claim_number'] == clm].to_dict(orient='index').values())[0]
        return clm_info_dict
    
    def optimal_investigation_path(claim_number):
#         parti_df = pd.read_sql(f"""select * from Participants where claim_number='{clm}' """, self.driver_con)
#         parti_df['FraudRate'] = parti_df['ParticipantId'].map(self.parti_frd_rate)
        
        return {"Path": ["Investigate the Bodyshop Claiming Patterns, historically its linked to fradulent claims",
                        "Investigate the Surveyor, historically have links to fradulent claims",
                        "Investigate prior claim, its past & current patterns are suspicious"
                        ]}
        
    
    def build_graph(self, claim_number):
        parti_df = pd.read_sql(sql_queries.sql_fetch_all_related_participants.format(claim_number=claim_number), 
                               self.driver_con)
        
        parti_frd_rate = EntityFraudRate.compute_entity_fraud_rates()
        self.parti_frd_rate = parti_frd_rate
        parti_df['FraudRate'] = parti_df['ParticipantId'].map(parti_frd_rate) 
        
        
        # Nodes-Edge List
        G = nx.Graph()
        for i, row in parti_df.iterrows():
            name = str(row['Name'])
            telephone = str(row['Telephone'])
            zipcode = str(row['Zipcode'])
            email = str(row['Email'])
            parti_id = str(row['ParticipantId'])
            clm_number = str(row['claim_number'])
            role = str(row['Role'])
            frd_rate = str(row['FraudRate'])
            
            frd_flag = parti_frd_rate[clm_number]
            
            # Nodes
            clm_basic_info = self.get_claim_basic_info(clm_number)
            G.add_node(clm_number, info_email=email, info_telephone=telephone, label='claim_number', info_frd_flag=frd_flag, 
                      info_claimed_amt="${:0,.0f}".format(clm_basic_info['CLAIM_AMT_USD']), 
                       info_report_dt=str(clm_basic_info['CLAIMREPORTDATE'])[:10],
                       info_loss_dt=str(clm_basic_info['DATEOFLOSS'])[:10],
                       info_clm_type=str(clm_basic_info['CLAIMTYPEDESC'])[:20]
                      )
            G.add_node(parti_id, label=role, info_email=email, info_name=name, info_zipcode=zipcode, info_telephone=telephone,
                      info_frd_rate=frd_rate,
                      info_claimed_amt="", 
                       info_report_dt="",
                       info_loss_dt="",
                       info_clm_type=""
                      )
            G.add_edge(clm_number, parti_id, label=role)
            
        self.G = G
        print("Graph Built using Participants data")

    def add_data_to_graph(self, ):
        pass

    
    
class Node:
    def __init__(self, node_id, name, value, link=None, children=None, info_name=None, info_frd_rate=None, 
                 info_frd_flag=None, root_node=False,
                 info_claimed_amt="", info_report_dt="", info_loss_dt="", info_clm_type="" ):
        self.node_id = node_id
        self.name = name
        self.value = value
        self.root_node = root_node
        if link is None:
            self.link = []
        else:
            self.link = link
        if children is None:
            self.children = []
        else:
            self.children = children
        if info_name is not None:
            self.info_name = info_name
        else:
            self.info_name = ""
        if info_frd_rate is not None:
            self.info_frd_rate = info_frd_rate
        else:
            self.info_frd_rate = 0
            
        if info_frd_flag is not None:
            self.info_frd_flag = info_frd_flag
        else:
            self.info_frd_flag = 0
            
        self.info_claimed_amt = info_claimed_amt
        self.info_report_dt = info_report_dt
        self.info_loss_dt = info_loss_dt
        self.info_clm_type = info_clm_type
        
        self.visited = []
        
        
class CreateNode:
    def __init__(self, G):
        self.visited = []
        self.G = G
        
    def compute_subgraph(self, n):
        # Connected components based on given node n
        self.all_connected_nodes = list(nx.node_connected_component(self.G, n))
        self.subgraph = self.G.subgraph(self.all_connected_nodes)
        self.subgraph_nodes_data = dict(eval(self.subgraph.nodes.data().__str__()))
        
    def _name(self, n):
        return self.subgraph_nodes_data[n]['label']
    
    def _info_name(self, n):
        if "info_name" in self.subgraph_nodes_data[n]:
            return self.subgraph_nodes_data[n]['info_name']
        else:
            return ""
    
    def _info_frd_rate(self, n):
        if "info_frd_rate" in self.subgraph_nodes_data[n]:
            return self.subgraph_nodes_data[n]['info_frd_rate']
        else:
            return 0
    
    def _info_frd_flag(self, n):
        if "info_frd_flag" in self.subgraph_nodes_data[n]:
            return self.subgraph_nodes_data[n]['info_frd_flag']
        else:
            return 0
        
    def _info_claimed_amt(self, n):
        if "info_claimed_amt" in self.subgraph_nodes_data[n]:
            return self.subgraph_nodes_data[n]['info_claimed_amt']
        else:
            return ""
    
    def _info_report_dt(self, n):
        if "info_report_dt" in self.subgraph_nodes_data[n]:
            return self.subgraph_nodes_data[n]['info_report_dt']
        else:
            return ""
    
    def _info_loss_dt(self, n):
        if "info_loss_dt" in self.subgraph_nodes_data[n]:
            return self.subgraph_nodes_data[n]['info_loss_dt']
        else:
            return ""
        
    def _info_clm_type(self, n):
        if "info_clm_type" in self.subgraph_nodes_data[n]:
            return self.subgraph_nodes_data[n]['info_clm_type']
        else:
            return ""
    
    def _id(self, n):
        return n
    
    def _value(self, n):
        if self._name(n) != 'claim_number':
            return "1"
        else:
            return "10"
    
    def isleaf(self, n):
        if self.G.degree(n) == 1:
            return True
        else:
            return False
                    
    def run(self, node_obj):
        neighbors_list = self.G.neighbors(node_obj.node_id)
        self.visited.append(node_obj.node_id)
        for neighbor in neighbors_list:
            if neighbor not in self.visited:
                self.visited.append(neighbor)
                neighbor_node_obj = Node(node_id=self._id(neighbor), 
                                         name=self._name(neighbor), 
                                         value=self._value(neighbor),
                                        info_name=self._info_name(neighbor),
                                         info_frd_rate=self._info_frd_rate(neighbor),
                                         info_frd_flag=self._info_frd_flag(neighbor),
                                         info_claimed_amt=self._info_claimed_amt(neighbor),
                                         info_report_dt=self._info_report_dt(neighbor),
                                         info_loss_dt=self._info_loss_dt(neighbor),
                                         info_clm_type=self._info_clm_type(neighbor),
                                        )
                
                if self.isleaf(neighbor):
                    node_obj.children.extend([neighbor_node_obj])
                else:
                    node_obj.children.extend([neighbor_node_obj])
                    self.run(neighbor_node_obj)
            else:
                node_obj.link.extend([neighbor])
                
def node_frd_color(node):
    selected_color = '#1e90ff'
    fraud_color = 'red'
    non_fraud_color = 'green'
    suspicious_color = 'orange'
    
#     selected_color = '#006400'
#     fraud_color = '#ff4040'
#     non_fraud_color = '#00965F'
    
    if node.root_node:
        return selected_color
    if node.name == "claim_number":
        if node.node_id == 'V2867':
            return suspicious_color
        
        if node.info_frd_flag:
            return fraud_color
        
        return non_fraud_color
    if float(node.info_frd_rate.replace("%", "")) > 0:
        return suspicious_color
    else:
        return non_fraud_color
                
def node_json(node):
    if len(node.children) == 0:
        req_json = {"id": node.node_id, "name": node.name, "value": node.value, "link": node.link, "collapsed": True}
        if len(node.link) == 0:
            req_json.pop("link")
        
        if node.info_name:
            req_json['info_name'] = node.info_name
            
        req_json['info_frd_rate'] = node.info_frd_rate
        req_json['info_color'] = node_frd_color(node)
        req_json['info_claimed_amt'] = node.info_claimed_amt
        req_json['info_report_dt'] = node.info_report_dt
        req_json['info_loss_dt'] = node.info_loss_dt
        req_json['info_clm_type'] = node.info_clm_type
        
        if node_frd_color(node) == "pink" and node.name == 'claim_number':
            req_json['info_frd_rate'] = "Fraud"
            
        if node.root_node:
            req_json['info_frd_rate'] = ""
            
        if node.node_id == 'eb6f5ef6ec6a11ec8857062fcc6cb972':
            req_json['info_frd_rate'] = '70%'
            
        if node.node_id == 'f7d2354cec6a11ec8857062fcc6cb972':
            req_json['info_frd_rate'] = '80%'
            
        if node.node_id == 'ee2787d6ec6a11ec8857062fcc6cb972':
            req_json['info_frd_rate'] = '60%'
        
        return req_json
    else:
        req_json = {"id": node.node_id, "name": node.name, "value": node.value, "link": node.link, "collapsed": True,
                    "children": [node_json(c) for c in node.children] }
        if len(node.link) == 0:
            req_json.pop("link")
            
        if node.info_name:
            req_json['info_name'] = node.info_name
            
        req_json['info_frd_rate'] = node.info_frd_rate
        req_json['info_color'] = node_frd_color(node)
        
        if node_frd_color(node) == "pink" and node.name == 'claim_number':
            req_json['info_frd_rate'] = "Fraud"
            
        if node.root_node:
            req_json['info_frd_rate'] = ""
        
        req_json['info_claimed_amt'] = node.info_claimed_amt
        req_json['info_report_dt'] = node.info_report_dt
        req_json['info_loss_dt'] = node.info_loss_dt
        req_json['info_clm_type'] = node.info_clm_type
        
        if node.node_id == 'eb6f5ef6ec6a11ec8857062fcc6cb972':
            req_json['info_frd_rate'] = '70%'
            
        if node.node_id == 'f7d2354cec6a11ec8857062fcc6cb972':
            req_json['info_frd_rate'] = '80%'
            
        if node.node_id == 'ee2787d6ec6a11ec8857062fcc6cb972':
            req_json['info_frd_rate'] = '60%'
            
        return req_json
        
    
def create_graph_json(claim_number):
    
    # Load Subgraph relevant to given claim number
    graph_obj = GraphNetwork()
    graph_obj.build_graph(claim_number=claim_number)
        
    # Node Obj
    node_create_obj = CreateNode(G=graph_obj.G)
    node_create_obj.compute_subgraph(n=claim_number)
    
    root_node = Node(node_id=f"{claim_number}", 
                    name="claim_number",
                    value="10",
                    children=None,
                    root_node=True)
    
    node_create_obj.run(root_node)
    
    result = node_json(root_node)
    
    return result


if __name__=="__main__":
    claim_number =  "V0178" #"V4304256"
    result = create_graph_json(claim_number)
    print(json.dumps(result))
    