import pandas as pd
import numpy as np

import shap

import base_explainer
import base_transforms
import utils


def transforms_get_decile_score(ser1, decile_min_score_series):
    """
    Computes decile scores
    
    ser1: PredProba (Probability values)  
    decile_min_score_series: Series of Minimum scores in each decile
    """
    decile_min_score_series = decile_min_score_series.reset_index(drop=True)
    
    decile_list = []
    
    for val in ser1.tolist():
        try:
            decile = decile_min_score_series[decile_min_score_series < val].head(1).index.tolist()[0] + 1
        except:
            decile = decile_min_score_series.shape[0]
            
        decile_list.append(decile)
        
    result = pd.Series(decile_list)
    result.index = ser1.index
    
    return result
    


def transforms_predict_model_scores(df, features, model_pkl):
    """
    
    df: Model Features Dataframe
    model_pkl: trained model pickle object
    """
    
    model_data = df[features].copy()
    model_data = model_data.fillna(0)
        
    # Classifier probability for class 1
    result = pd.DataFrame({'pred_proba': model_pkl.predict_proba(model_data)[:, 1]})
    
    result.index = df.index
    
    return result


def transforms_scale_model_score(ser1, decile_cuts):
    """
    Scaling the raw scores to given range values
    
    ser1: XGBPredProba
    decile_cuts: List of decile cuts for top 3 deciles [top1_max, top1_min, top2_min, top3_min, rest_min]
    """
    
    scaled_scores = []
    
    for score in ser1.tolist():
                
        top1_max = decile_cuts[0]
        top1_min = decile_cuts[1]
        
        top2_max = decile_cuts[1]
        top2_min = decile_cuts[2]
        
        top3_max = decile_cuts[2]
        top3_min = decile_cuts[3]
        
        rest_max = decile_cuts[3]
        rest_min = decile_cuts[4]
        
        # Scaling top1 decile between 80-100
        if score >= top1_min:
            s = utils.scale_to_range(m=score,
                                    r_min=top1_min,
                                    r_max=top1_max,
                                    t_min=80,
                                    t_max=100)
            
        # Scaling top2 decile between 70-80
        elif score >= top2_min:
            s = utils.scale_to_range(m=score,
                                    r_min=top2_min,
                                    r_max=top2_max,
                                    t_min=70,
                                    t_max=80)
        
        # Scaling top3 decile between 60-70
        elif score >= top3_min:
            s = utils.scale_to_range(m=score,
                                    r_min=top3_min,
                                    r_max=top3_max,
                                    t_min=60,
                                    t_max=70)
        else:
            # Scaling remaining deciles between 0-60
            s = utils.scale_to_range(m=score,
                                    r_min=rest_min,
                                    r_max=rest_max,
                                    t_min=0,
                                    t_max=60)
            
        scaled_scores.append(s)
        
        
    result = pd.Series(scaled_scores)
    result.index = ser1.index

    return result


def transforms_model_score_group(ser1):
    """
    get Model Score Group
    
    ser1: Scaled Model Score
    """
    score_grp_list = []
    
    for s in ser1.tolist():
        if s >= 80:
            score_grp_list.append('SIGNIFICANTLY HIGH')
        elif s>=70:
            score_grp_list.append('HIGH')
        elif s>=60:
            score_grp_list.append('MEDIUM')
        else:
            score_grp_list.append('LOW')
            
    result = pd.Series(score_grp_list)
    result.index = ser1.index
    
    return result
            
            
            
    


def transforms_shap_reason_codes(df, model_pkl, model_features, business_rules=None, topn=5):
    """
    Computes Shap Reason codes descriptions based on business rules
    
    df: Model Feature DataFrame 
    model_pkl: trained model pickle file
    features: List of features
    business_rules: (Optional) Business Rules sheet
    """
    
    obj = base_explainer.ShapReasonCodes(model=model_pkl,
                                         model_features=model_features,
                                         business_rules=business_rules,
                                         topn=topn, 
                                         logger=None, 
                                         out_col_pref='top_')
    
    result = obj.apply(df)
    
    return result
    
    
    
    


    



    
    
    
    
    