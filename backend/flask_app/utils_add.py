col_rename_dict = {'dam_gt50_risksum': 'Damage amount > 50% of risk sum insured',
                   'part_est_ratio_cr_Low': 'Low ratio between part and estimate cost',
                   'part_labor_ratio_cr_Low': 'Low ratio between part and labour cost',
                   'notif_loss_cr_High': 'Notification of claim long after claim loss date',
                   'rush_hour': 'Accident during peak hours',
                   'tppd': 'Possibility of Third Party Property Damage claim',
                   'week_time_3': 'Accident occurred on weekends',
                   'is_self_accident': 'Self Accident',
                   'occu_swasta_blank': 'Clients occupation code is swasta or left blank',
                   'est_mvalue_ratio_cr_High': 'High ratio between estimate and market value',
                   'expire_loss_cr_Low': 'Claim within close proximity of expiration date',
                   'loss_inception_cr_Low': 'Claim within close proximity of inception date',
                   'claims_in_36_cr_Medium': 'Prior claim history',
                   'tp_tpbi': 'Third party and third party bodily injury claim',
                   'close_claim': 'Previous claim within close proximity of current claim',
                   'end_passed_ldate_after': 'endorsement passed and posting date after accident',
                   'average_claims_paid_36_cr_Medium': 'Average claim amount in the last 36 months is less than 10K',
                   'average_claims_paid_36_cr_High': 'Average claim amount in the last 36 months greater than 10K',
                   'prior_causeofloss': 'Prior claims with same cause of loss as the current claim',
                   'claims_in_36_cr_High': 'Prior claim history',
                   }

col_rename_dict2 = {'Investigated': 'investigated', 'CLAIMREPORTDATE': 'claim_report_date',
                    'CLAIM_AMT_USD': 'claim_amt_usd', 'CLAIM_STATUS': 'claim_status'}

feature_cols = ['end_passed_ldate_after', 'occu_swasta_blank', 'rush_hour',
                'dam_gt50_risksum', 'prior_causeofloss', 'tp_tpbi', 'tppd',
                'part_labor_ratio_cr_Low', 'part_est_ratio_cr_Low',
                'est_mvalue_ratio_cr_High', 'loss_inception_cr_Low',
                'notif_loss_cr_High', 'expire_loss_cr_Low',
                'average_claims_paid_36_cr_High', 'average_claims_paid_36_cr_Medium',
                'week_time_3.0', 'claims_in_36_cr_High', 'claims_in_36_cr_Medium',
                'close_claim', 'is_self_accident']
feature_cols_2 = ['end_passed_ldate_after', 'occu_swasta_blank', 'rush_hour',
                  'dam_gt50_risksum', 'prior_causeofloss', 'tp_tpbi', 'tppd',
                  'part_labor_ratio_cr_Low', 'part_est_ratio_cr_Low',
                  'est_mvalue_ratio_cr_High', 'loss_inception_cr_Low',
                  'notif_loss_cr_High', 'expire_loss_cr_Low',
                  'average_claims_paid_36_cr_High', 'average_claims_paid_36_cr_Medium',
                  'week_time_3', 'claims_in_36_cr_High', 'claims_in_36_cr_Medium',
                  'close_claim', 'is_self_accident']

columns_to_pred_table = ['claim_number', 'score_grp',
                         'investigated', 'claim_report_date', 'claim_amt_usd', 'claim_status', 'assigned_to']

reason_code_ids = {1: 'part_labor_ratio_cr_Low',
                   2: 'part_est_ratio_cr_Low',
                   3: 'close_claim',
                   4: 'notif_loss_cr_High',
                   5: 'claims_in_36_cr_High',
                   6: 'prior_causeofloss',
                   7: 'average_claims_paid_36_cr_Medium',
                   8: 'tppd'}
adjuster_names = ['Haissam ', 'Fehmeed ', 'Adiputera', 'Jun Jie', 'Azarnoush ', 'Zhi Hao']

tool_users = ['Haissam ', 'Fehmeed ', 'Bintang', 'Adiputera', 'Jun Jie', 'Maymuun', 'Azarnoush ', 'Zhi Hao', 'Jiu Lin']
tool_users_dict = {k: v for (k, v) in zip(tool_users, list(range(1, len(tool_users) + 1)))}

malaysia_states = ['pahang', 'penang', 'perak', 'johor', ' malacca', 'pertis',
                   'selangor', 'kedah', 'negeri sembilan', 'kelantan']

random_address = [' No. 43Jalan Bk 5/11, Bandar Kinrara', 'Menggatal Newtownship, Menggatal,',
                  '18 Jln 1/60B Taman Segambut Damai', '10 Jln Ss6/8 Ss6 Petaling Jaya',
                  ' 4802 Jalan Bagan Luar', '185A 1St Floor Jalan San Peng',
                  '13 Ground Floor Jalan Bus Express Taman Bukit',
                  'Suite 7.01 7Th Floor Semua House Off Jalan',
                  '14 Jln Tasek Kawasan Perindustrian Tasek', 'Jalan 5/118C, Desa Tun Razak',
                  '14 Jln Tasek Kawasan Perindustrian Tasek',
                  'Bandar Tun Razak, Cheras,', ' D 03A 02 Plaza Mont Kiara 2 Jalan 1/70C',
                  '19/F Empire Tower Zone B Jalan Tun Razak', 'Jln Persisiran Perling Taman Perling',
                  '112 Lorong Bercham 18 Kampung Bercham']

pin_codes = [79502, 75502, 88502, 93502, 1502, 5502, 15502, 25502,
             40502, 70502, 80500, 40500, 70500, 20500]
