import os
import sqlite3
import yaml
import pandas as pd
import utils
from flask import jsonify
import datetime
import numpy as np


class SummaryKPIChartsData():
    
    def __init__(self, config_filepath=None):
        self.app_config_dict = utils.load_app_config_dict(config_filepath)
        self.raw_data_conn, _ = utils.create_db_connection(config_dict=self.app_config_dict)
        
        
    def create_table_json(self, df, col=None):
        """
        Creates tabular json output
        :param df: pandas DataFrame or Series
        :param col: String
        :return: Dictionary
        """
        if col:
            result = df[col].value_counts().to_json()
        else:
            result = df.to_json()
        return result
    
    def calculate_closure_rate(self, df):
        df_new = df.copy()
        df_new.index =pd.to_datetime(df_new.CLAIMREPORTDATE)
        df_new = df_new.groupby([pd.Grouper(freq='M'),
                                 'CLAIM_STATUS']).agg({'STATE': 'count'}).rename(columns={'STATE': 'COUNT'})
        df_new = df_new.reset_index()
        df_new = df_new[df_new.CLAIMREPORTDATE > '2021-01-01']
        return self.create_table_json(df_new)
    
    def load_raw_data(self, ):
        sql = """select * from raw_data"""
        df = pd.read_sql(sql, self.raw_data_conn)
        return df   
    
    def create_json_for_Investigation_screen(self, df=None):
        if df is None:
            df = self.load_raw_data()
            
        summary_dict = dict()
        summary_dict['Claims_Initiated'] = int(df.shape[0])
        summary_dict['Total_Investigated'] = int(df.Investigated.value_counts()['Y'])
        df_inv = df[df['Investigated'] == 'Y'].copy()
        summary_dict['Total_Closed'] = int(df_inv.CLAIM_STATUS.value_counts()['closed'])
        summary_dict['Paid_Claims'] = int(df_inv.PAID.value_counts()['Yes'])
        summary_dict['Rejected'] = int(df_inv.PAID.value_counts()['Rejected'])
        summary_dict['Fraud_Suspected_Claims'] = int(df_inv.target.value_counts()[1])
        summary_dict['Investigation_rate'] = (df_inv.shape[0]/df.shape[0])
        summary_dict['Fraud_detection'] = (df_inv.target.value_counts()[1]/df_inv.shape[0])
        df_closed = df_inv[df_inv.CLAIM_STATUS == 'closed'].copy()
        
        # Claims Investigated By State
        tmp = df_inv.groupby(['STATE'])['CLAIMNUMBER'].nunique()
        tmp = 100 * (tmp / df_inv['CLAIMNUMBER'].nunique())
        tmp = tmp.reset_index().rename(columns={'CLAIMNUMBER': 'Investigated_status'})
        claim_investigated_by_state_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Among Investigated Claims Distribution of Paid Statuses
        tmp = 100*(df_inv['PAID'].value_counts() / df_inv['CLAIMNUMBER'].nunique())
        investigated_paid_status_dist_dict = eval(tmp.to_json(orient='table'))['data']
        
        summary_dict['Investigated_status_by_states_perc'] = claim_investigated_by_state_dict
        summary_dict['Investigated_Payment_Status'] = investigated_paid_status_dist_dict
        
        print(summary_dict)
        
        return summary_dict
        
      
    def df_kpi_all_summary(self, df=None):
        if df is None:
            sql = """select * from raw_data"""
            df = pd.read_sql(sql, self.raw_data_conn)
            
        # Claim Count by Claim Type
        tmp = df['CLAIM_TYPE'].value_counts()
        tmp.name = 'Claims'
        tmp.index.name = 'Claims_type'
        claims_by_claim_type_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Claim Count by State
        tmp = df['STATE'].value_counts()
        tmp.name = 'Claims'
        tmp.index.name = 'state'
        claims_by_state_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Approved Amount by Claim type Histogram
        tmp = df.groupby('CLAIM_TYPE').agg({'APPRV_AMT_USD': 'sum'}).rename(columns={'APPRV_AMT_USD': 'total_approved_amt'})
        tmp.index.name = 'claim_type'
        approved_claimed_amt_by_clm_type_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Approved Amount by State Histogram
        tmp = df.groupby('STATE').agg({'APPRV_AMT_USD': 'sum'}).rename(columns={'APPRV_AMT_USD': 'total_approved_amt'})
        tmp.index.name = 'state'
        approved_claimed_amt_by_state_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Statewise Total Approved Amount & Claims
        tmp = df.groupby('STATE').agg({'APPRV_AMT_USD': 'sum', 'CLAIMNUMBER': 'count'}).rename(columns={'CLAIMNUMBER': 'Claim_count'})
        tmp = df.groupby('STATE').agg({'APPRV_AMT_USD': 'sum', 'CLAIMNUMBER': 'count'}).rename(columns={'CLAIMNUMBER': 'Claim_count'})
        tmp.index.name = 'STATE'
        table_statewise_apprv_amt_claims_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Last 12 Months claim count
        tmp = df[['CLAIMREPORTDATE', 'CLAIMNUMBER', 'CLAIM_STATUS']].copy()
        tmp['Month'] = pd.to_datetime(tmp['CLAIMREPORTDATE']).dt.month
        tmp['Year'] = pd.to_datetime(tmp['CLAIMREPORTDATE']).dt.year
        tmp = tmp[tmp['Year']>=2021]
        tmp['YearMonth'] = pd.to_datetime(tmp['Year'].astype(str) + "-" + tmp['Month'].astype(str))
        tmp = tmp.groupby(['YearMonth', 'CLAIM_STATUS'])['CLAIMNUMBER'].nunique()
        tmp = tmp.reset_index().rename(columns={"YearMonth": "CLAIMREPORTDATE", 'CLAIMNUMBER': 'COUNT'})
        last_12_months_claim_count_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Claim Count Per Report Date
        tmp = df[['CLAIMREPORTDATE', 'CLAIMNUMBER']].copy()
        base = datetime.datetime.today()
        date_list = [pd.to_datetime(str(base - datetime.timedelta(days=x))[:10] ) for x in range(365*3)]
        dt_df = pd.DataFrame({'CLAIMREPORTDATE': date_list})
        tmp = tmp.groupby(['CLAIMREPORTDATE'])['CLAIMNUMBER'].nunique().reset_index()
        tmp['CLAIMREPORTDATE'] = pd.to_datetime(tmp['CLAIMREPORTDATE'])
        tmp = dt_df.merge(tmp.reset_index(), on=['CLAIMREPORTDATE'], how='left')
        tmp['CLAIMNUMBER'] = tmp['CLAIMNUMBER'].fillna(0)
        tmp = tmp.rename(columns={'CLAIMNUMBER': 'CLAIMREPORTDATE_count'}).drop(columns=['index'])
        daywise_claim_count_dict = eval(tmp.to_json(orient='table'))['data']
        
        # Fraud Ratio
        tmp = pd.Series(np.where(df['Investigated']=='Y', "Fraud-suspected", "Normal"))
        tmp = tmp.value_counts()
        tmp.name = 'target'
        fraud_ratio_dict = eval(tmp.to_json(orient='table'))['data']

        summary_dict = {'Claims Initiated': len(df),
                    'Claims in Progress': len(df.CLAIM_STATUS[df.CLAIM_STATUS == 'open']),
                    'Claims Closed': len(df.CLAIM_STATUS[df.CLAIM_STATUS == 'closed']),
                    'Claims Paid': {'counts': df.PAID.value_counts().to_dict(),
                                    'Amount Paid': (df[df.PAID == 'Yes'])['APPRV_AMT_USD'].sum()},
                    'Average Claim Amount': df['CLAIM_AMT_USD'].sum() / len(df),
                    'Average Paid Amount':
                        df['APPRV_AMT_USD'][df.PAID == 'Yes'].sum() / len(df['APPRV_AMT_USD'][df.PAID == 'Yes']),
                    'Closure ratio': df.CLAIM_STATUS.value_counts()['closed'].sum() / len(df),
                        
                    'Claims_by_Claim_Type_Histogram': claims_by_claim_type_dict,
                        
                    'Claims_by_state_Histogram': claims_by_state_dict,
                        
                    'Approved_Claimed_Amount_by_Claim_Type_Histogram': {"TOTAL_APPROVED_AMOUNT": approved_claimed_amt_by_clm_type_dict},
                        
                    'Approved_Claimed_Amount_by_State_Histogram': {"TOTAL_APPROVED_AMOUNT": approved_claimed_amt_by_state_dict},   
                        
                        'Tabledata_statewise': table_statewise_apprv_amt_claims_dict,
                        'Last12MonthCount': last_12_months_claim_count_dict,
                        'Daywise_Claim_Count': daywise_claim_count_dict,
                        'Fraud_Ratio': fraud_ratio_dict
                       }
        
        return summary_dict
    
    
if __name__=="__main__":
    print("Creates kpi numbers for Overview Summary Page")
    