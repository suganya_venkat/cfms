"""
Defines methods for loading data from client sources
"""

import pandas as pd
import logging
import logging.config
from time import time

import pymssql

import utils
from exceptions import *

import pdb



def base_loader_getConnectionObject(app_config_dict, logger=None):
    
    user = app_config_dict['db_user']
    
    utils.print_and_log(logger, "Getting connection & Cursor Objects")
    utils.print_and_log(logger, "base_loader_getConnectionObject() User : {}".format(user))
    
    conn = pymssql.connect(server=app_config_dict['db_server'],
                           user=user,
                           password=utils.get_pass(app_config_dict['db_key'],
                                                   app_config_dict['db_password']))
    cursor = conn.cursor()
    
    return conn, cursor


def create_sql_query_batches(query, app_config_dict, sql_where_dict, batch_key, sql_batch_size=5000):
    
    base_query = query
    item_list = sql_where_dict[batch_key]
    frmt_dict = app_config_dict['db_schemas'].copy()
    
    d = {}
    for k,v in sql_where_dict.items():
        if k == batch_key:
            continue
        else:
            d[k] = v
    
    frmt_dict.update(d)
    
    item_list = list(set(item_list))
    n_items = len(item_list)
    if n_items <= sql_batch_size:
        if item_list.__len__() == 1:
            item_list = [item_list[0], item_list[0]]
            
        frmt_dict[batch_key] = tuple(item_list)
        return [base_query.format(**frmt_dict)]

    if n_items % sql_batch_size == 0:
        n_batches = int(n_items // sql_batch_size)
    else:
        n_batches = int(n_items // sql_batch_size) + 1
    
    query_list = []
    for batch in range(n_batches):
        batch_items = item_list[batch * sql_batch_size:(batch + 1) * sql_batch_size]

        if batch_items.__len__() == 1:
            batch_items = [batch_items[0], batch_items[0]]
            
        frmt_dict[batch_key] = tuple(item_list)
        query_list.append(base_query.format(**frmt_dict))
        
    return query_list

        

def base_loader_sql_to_df(query, conn, app_config_dict, logger=None, **kwargs):
    
    if 'sql_batch_size' not in kwargs:
        sql_batch_size = 5000
    else:
        sql_batch_size = kwargs['sql_batch_size']
    
    pdb.set_trace()
    if 'sql_where_dict' in kwargs:
        if 'batch_key' in kwargs:
            query_to_exec = create_sql_query_batches(query,
                                                    app_config_dict,
                                                    sql_where_dict=kwargs['sql_where_dict'], 
                                                    batch_key=kwargs['batch_key'],
                                                    sql_batch_size=sql_batch_size)
        else:
            sql_where = {}
            for k,v in kwargs['sql_where_dict'].items():
                if isinstance(v, list):
                    sql_where[k] = utils.query_tuple(v)
                else:
                    sql_where[k] = v
            
            query_to_exec = query.format(**sql_where, **app_config_dict['db_schemas'] )
    else:
        query_to_exec = query.format(**app_config_dict['db_schemas'])
    
    utils.print_and_log(logger, f"query to execute: {query_to_exec}")
    utils.print_and_log(logger, "base_loader_sql_to_df() ")
    utils.print_and_log(logger, "base_loader_sql_to_df() SQL execution started")

    start_sql = time()
    
    df = pd.DataFrame()
    if isinstance(query_to_exec, list):
        for i, q in enumerate(query_to_exec):
            utils.print_and_log(logger, "Executing batch query # {} / {} batches".format(i+1, len(query_to_exec)))
            chunk = pd.read_sql(q, conn)
            df = df.append(chunk)
    else:
        df = pd.read_sql(query_to_exec, conn)
            
    df.drop_duplicates(inplace=True)
    df = df.loc[:, ~df.columns.duplicated()]

    stop_sql = time()

    utils.print_and_log(logger, "base_loader_sql_to_df() SQL execution complete")
    utils.print_and_log(logger, "base_loader_sql_to_df() SQL duration = {}".format(stop_sql - start_sql))
    utils.print_and_log(logger, "base_loader_sql_to_df() SQL returned {} rows".format(df.shape[0]))
    
    return df



