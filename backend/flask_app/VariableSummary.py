import pandas as pd
import numpy as np
import sql_queries
import utils
import LoadTargets
import json
import pdb

class VarSummary:
    
    def __init__(self, config_filepath=None):
        self.data = None
        self.app_config_dict = utils.load_app_config_dict(config_filepath)
        self.driver_con, self.drive_cursor = utils.connect2sqlite3(self.app_config_dict['DRIVER_DB_PATH'])
        self.cfms_con, self.cfms_cursor = utils.connect2sqlite3(self.app_config_dict['CFMS_DB_PATH'])
        
        feature_inventory = pd.read_sql(sql_queries.sql_feat_inventory, self.cfms_con)
        feature_inventory.drop_duplicates(subset=['FeatureDescription'], inplace=True)
        self.feature_business_name_id_map = feature_inventory.set_index(['FeatureDescription'])['FeatureID'].to_dict()
        
        
    def univariate_feature_analysis(self, x, features, target_col='Target'):
        trn_df = x.copy()
        y = target_col
        new_features_list = []
        for i in features:
            try:
                trn_df[i] = trn_df[i].astype(float)
                var_type = "C"
                new_features_list.append(i)
            except:
                var_type = "D"
                trn_df[i] = trn_df[i].astype(str)
            if var_type == "D":
                uniq_vals = trn_df[i].value_counts().index.tolist()[:10]

                for val in uniq_vals:
                    trn_df[val] = np.where(trn_df[i] == val, 1, 0)
                    new_features_list.append(val)

        iv_df1 = pd.DataFrame()
        for i in new_features_list:            
            trn_df[i] = trn_df[i].fillna(0)                
            if (trn_df[i].fillna(0).nunique() < 10):                
                cutbins = sorted(list(set(trn_df[i].unique().tolist() + [-np.inf, np.inf])))
                temp = trn_df.groupby(pd.cut(trn_df[i], cutbins, duplicates='drop'))[y].count()
                temp1 = trn_df.groupby(pd.cut(trn_df[i], cutbins, duplicates='drop'))[y].sum()
            else:
                check, bins = pd.qcut(trn_df[i], 10, duplicates='drop', retbins=True)
                ncats = check.nunique()
                random_cuts = pd.Series(trn_df[i].unique()).sample(5).tolist()
                q_cuts = bins.tolist()
                if (ncats <= 3) :
                    cutbins = sorted(list(set(random_cuts + [-np.inf, np.inf, 0])))
                    temp = trn_df.groupby(pd.cut(trn_df[i], cutbins, duplicates='drop'))[y].count()
                    temp1 = trn_df.groupby(pd.cut(trn_df[i], cutbins, duplicates='drop'))[y].sum()
                else:
                    cutbins = sorted(list(set(q_cuts + [-np.inf, np.inf, 0])))
                    temp = trn_df.groupby(pd.cut(trn_df[i], cutbins, duplicates='drop'))[y].count()
                    temp1 = trn_df.groupby(pd.cut(trn_df[i], cutbins, duplicates='drop'))[y].sum()

            iv_df = pd.DataFrame({'Levels': temp.index, 'trn_total': temp.values})
            iv_df['feature'] = i
            iv_df['feature_type'] = 'Categorical' if trn_df[i].fillna(0).nunique() <= 2 else 'Numerical'
            iv_df['trn_perc_in_feature'] = (iv_df['trn_total'] / iv_df['trn_total'].sum())
            iv_df['trn_Target_counts'] = temp1.values
            iv_df['trn_non_Target_counts'] = iv_df['trn_total'] - iv_df['trn_Target_counts']
            iv_df['trn_Target_count_percent_in_feature'] = (iv_df['trn_Target_counts'] / iv_df['trn_Target_counts'].sum())
            iv_df['trn_Target_perc'] = (iv_df['trn_Target_counts'] / (iv_df['trn_non_Target_counts'] + iv_df['trn_Target_counts']))
            iv_df['trn_non_Target_count_percent_in_feature'] = (iv_df['trn_non_Target_counts'] / iv_df['trn_non_Target_counts'].sum()) 
            iv_df['trn_WOE'] = -np.log(iv_df['trn_Target_count_percent_in_feature'] / iv_df['trn_non_Target_count_percent_in_feature'])
            iv_df['trn_IV'] = (iv_df['trn_non_Target_count_percent_in_feature'] - iv_df['trn_Target_count_percent_in_feature']) * iv_df['trn_WOE']
            iv_df1 = pd.concat([iv_df1, iv_df], axis=0)

        iv_df1['trn_OverallTargetPerc'] = trn_df[y].sum() / trn_df.shape[0]

        return iv_df1
    
    def format_levels(self, x):
        return str("{:,}".format(round(x.left, 1))) + "<=x<=" + str("{:,}".format(round(x.right, 1)))
    
    def get_summary_json(self, feature):
        
        derived_feat_df = pd.read_sql(sql_queries.sql_derived_features, self.cfms_con)
        clm_amt_df = pd.read_sql(sql_queries.sql_queries_claims_amnt, self.driver_con)
        targets = LoadTargets.get_fraud_claims()
        
        derived_feat_df2 = derived_feat_df.pivot(index=['IdValue'], columns=["FeatureID"], values=['FeatureValue'])
        derived_feat_df2.columns = derived_feat_df2.columns.droplevel(level=0)
        derived_feat_df = derived_feat_df2.reset_index(drop=False)
        derived_feat_df['Target'] = np.where(derived_feat_df['IdValue'].isin(targets),1,0)
        
        # get feature id
        feat_id = self.feature_business_name_id_map[feature]
        
        derived_feat_df = derived_feat_df.merge(clm_amt_df, left_on=['IdValue'], right_on=['claim_number'], how='left')
        derived_feat_df.rename(columns={'CLAIM_AMT_USD': self.feature_business_name_id_map['Claims Amount']}, inplace=True)
        
        var_summary = self.univariate_feature_analysis(x=derived_feat_df, features=[feat_id], target_col='Target')
        var_summary['VariableLevel'] = var_summary['Levels'].apply(lambda x: self.format_levels(x))
        var_summary = var_summary[var_summary['trn_total'] !=0].reset_index()
        var_summary.rename(columns={"trn_total": "Total"}, inplace=True)
        var_summary.rename(columns={"trn_Target_counts": "Frauds"}, inplace=True)
        var_summary.rename(columns={"trn_Target_perc": "Fraud %",
                                   "trn_OverallTargetPerc": "Overall Fraud %"
                                   }, inplace=True)
        var_summary = var_summary[['VariableLevel', 'Total', 'Frauds', 'Fraud %', "Overall Fraud %"]].copy()
        var_summary.reset_index(drop=True, inplace=True)
        var_summary = var_summary.dropna(subset=['Fraud %'])
        
        var_summary_dict = list(var_summary.reset_index(drop=True).to_dict(orient='index').values())
        result = {"VariableName": feature, "VariableSummary": var_summary_dict}
        
        return json.dumps(result)
        
    
if __name__ == "__main__":
    
    obj = VarSummary()
        
#     feature = 'Has Report Lag'
#     feature = 'Loss happened within a month of policy expiry'
    feature = 'Claims Amount'
    result = obj.get_summary_json(feature)
    print(result)
