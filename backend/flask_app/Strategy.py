import numpy as np
import pandas as pd
import json
import sqlite3
import utils
import pdb
import os
import HandleRuleBank
import sql_queries


config_data_dict = utils.load_app_config_dict()


def create_score_group_based_on_model_weights(w_struct_model=0.5, w_anom_model=0.5):
    try:
        if (w_struct_model > 1.0) or (w_struct_model < 0):
            raise ValueError('Model weight cannot exceed one or less than zero')
        if w_anom_model != (1 - w_struct_model):
            w_anom_model = (1 - w_struct_model)
        # access proba scores
        claims_data_db_path = config_data_dict['CLAIMS_DATA_DB_PATH']
        connection, cursor = utils.connect2sqlite3(claims_data_db_path)

        struct_proba = pd.read_sql('SELECT * FROM claims_structured_score', connection)
        anomaly_proba = pd.read_sql('SELECT * FROM claims_anomaly_score', connection)
        anomaly_proba = anomaly_proba.set_index('claim_id').loc[struct_proba.claim_id].reset_index()
        weighted_proba = struct_proba.proba_score * w_struct_model + anomaly_proba.proba_score * w_anom_model
        weighted_df = struct_proba.copy()
        weighted_df.proba_score = weighted_proba
        weighted_df['score_grp'] = pd.qcut(weighted_proba.rank(method='first'), 10, labels=False) + 1
        utils.write_to_table(connection, weighted_df, 'claims_weighted_score', clear_existing_data=True)
        print("Updated claims_weighted_score")
        return {"response": "SUCCESS"}
    except:
        return {"response": "FAILURE"}
    
      
        
    
    