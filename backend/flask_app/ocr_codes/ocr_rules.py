# -*- coding: utf-8 -*-
import ast
import os, json

import matplotlib.pyplot as plt
from pylab import rcParams
from IPython.display import Image

rcParams['figure.figsize'] = 8, 16
from PIL import Image
import pandas as pd
import numpy as np
import re
from scipy.ndimage import interpolation as inter
import itertools
import sqlite3, sys, path
from itertools import combinations
from collections import OrderedDict
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from datetime import datetime

directory = path.Path(__file__).abspath()
sys.path.append(directory.parent.parent)
os.chdir(directory.parent.parent)

from forecasting.forecasting_utils import get_doc_db_connection

rules_dict = {'dl_pr_rules': ['Name in drivers licence matches with police report',
                              'Drivers License expiry after Claim report date',
                              'Claimant address in Drivers license same as Address in police report'],
              'pr_hos_inp_rules': ['Hospital admission date within 7 days of police report date',
                                   'Medical procedure description related to accident',
                                   'Name in police report matches with Inpatient name',
                                   'Hospital location close to incident location']}

rules_dict = OrderedDict(rules_dict)

acdnt_proc_key_words = ['accident', 'fracture', 'orthopaedic', 'injury', 'wound',
                        'concussion']

doc_combos_dict = {'d1_d2': 'dl_pr_rules',
                   'd2_d3': 'pr_hos_inp_rules'}


class CreateRules():
    """
    Creates rules based on identified entities from claim docs
    """

    def __init__(self, path_db=None, clm_number=None, conn=None):
        self.db_path = path_db
        self.clm_number = clm_number
        self.docs_dict = None
        self.conn = conn
        self.doc_combos = None
        self.invert_docs_dict = None
        self.rules = dict()

    def apply_rules_pipeline(self):
        self.create_db_connection()
        self.check_for_available_docs()
        self.create_doc_combos()
        self.check_and_add_rules_based_on_doctypes()

    def apply_prod_rules_pipeline(self):
        self.check_for_available_docs()
        self.create_doc_combos()
        self.check_and_add_rules_based_on_doctypes()
        self.get_encoded_doc_images()

    def check_for_available_docs(self):
        query = 'SELECT DISTINCT(DocId), DocTypeID from Claim_Docs WHERE claim_number = "%s"' % self.clm_number
        docs_list_df = pd.read_sql(query, self.conn)
        ##
        docs_list_df = docs_list_df.set_index('DocId')
        self.docs_dict = docs_list_df.to_dict()['DocTypeID']
        self.invert_docs_dict = {v: k for k, v in self.docs_dict.items()}

    def create_db_connection(self):
        self.conn = sqlite3.connect(self.db_path)

    def create_doc_combos(self):
        self.doc_combos = list(combinations(self.docs_dict.values(), 2))

    def check_and_add_rules_based_on_doctypes(self):
        for combo in self.doc_combos:
            if set(combo) == {'D1', 'D2'}:
                self.pr_dl_rules(combo)
            if set(combo) == {'D2', 'D3'}:
                self.pr_hos_inp_rules(combo)

    def create_r_dict(self, combo):
        r_dict = dict()
        for docid in self.docs_dict:
            if self.docs_dict[docid] in combo:
                query = 'SELECT IDName, EntityValue FROM Detected_Entities WHERE DocId = "%s"' % docid
                entities_dict = pd.read_sql(query, self.conn).set_index('IDName').to_dict()['EntityValue']
                r_dict[docid] = entities_dict
            continue
        return r_dict

    @staticmethod
    def date_parse(dtime):
        dtime = str(dtime)
        return dtime.split()[0]

    def pr_dl_rules(self, combo):
        r_dict = self.create_r_dict(combo)
        self.rules['pr_dl_rules'] = {}
        # rule 1 check
        doc_1_dict = r_dict[self.invert_docs_dict['D1']]
        doc_2_dict = r_dict[self.invert_docs_dict['D2']]
        dl_name, dl_address, dl_expiry = (doc_1_dict['claimant_name'],
                                          doc_1_dict['claimant_address_dl'], doc_1_dict['dl_expiry_date'])
        pr_name, pr_address, pr_date = (doc_2_dict['claimant_name'],
                                        doc_2_dict['claimant_address_police_report'], doc_2_dict['police_report_date'])
        # pr_name = r_dict[self.invert_docs_dict['D2']]['claimant_name']
        rule_name = rules_dict['dl_pr_rules'][0]
        if fuzz.ratio(dl_name.lower(), pr_name.lower()) > 80:
            self.rules['pr_dl_rules'][rule_name] = 'Yes'
        else:
            self.rules['pr_dl_rules'][rule_name] = 'No'
        rule_name = rules_dict['dl_pr_rules'][1]
        if datetime.strptime(self.date_parse(dl_expiry),
                             '%Y-%m-%d').date() < datetime.strptime(self.date_parse(pr_date), '%Y-%m-%d').date():
            self.rules['pr_dl_rules'][rule_name] = 'No'
        else:
            self.rules['pr_dl_rules'][rule_name] = 'Yes'
        rule_name = rules_dict['dl_pr_rules'][2]
        if fuzz.ratio(dl_address.lower(), pr_address.lower()) > 80:
            self.rules['pr_dl_rules'][rule_name] = 'Yes'
        else:
            self.rules['pr_dl_rules'][rule_name] = 'No'

    def pr_hos_inp_rules(self, combo):
        self.rules['pr_hos_inp_rules'] = {}
        r_dict = self.create_r_dict(combo)
        doc_2_dict = r_dict[self.invert_docs_dict['D2']]
        doc_3_dict = r_dict[self.invert_docs_dict['D3']]

        pr_name, pr_address, pr_date = (doc_2_dict['claimant_name'],
                                        doc_2_dict['claimant_address_police_report'], doc_2_dict['police_report_date'])
        (inp_name, inp_address, inp_admission_date,
         inp_discharge_date, med_pro) = (doc_3_dict['patient_name'],
                                         doc_3_dict['hc_provider_address'],
                                         doc_3_dict['admission_date'],
                                         doc_3_dict['discharge_date'],
                                         doc_3_dict['medical_procedures'])
        rule_name = rules_dict['pr_hos_inp_rules'][2]
        if fuzz.ratio(inp_name.lower(), pr_name.lower()) > 80:
            self.rules['pr_hos_inp_rules'][rule_name] = 'Yes'
        else:
            self.rules['pr_hos_inp_rules'][rule_name] = 'No'
        rule_name = rules_dict['pr_hos_inp_rules'][0]
        if abs((datetime.strptime(self.date_parse(inp_admission_date), '%Y-%m-%d') -
                datetime.strptime(self.date_parse(pr_date), '%Y-%m-%d')).days) < 7:
            self.rules['pr_hos_inp_rules'][rule_name] = 'No'
        else:
            self.rules[rule_name] = 'Yes'
        rule_name = rules_dict['pr_hos_inp_rules'][1]
        str_word_list = ast.literal_eval(med_pro.lower())
        if any([word in acdnt_proc_key_words for word in str_word_list]):
            self.rules['pr_hos_inp_rules'][rule_name] = 'Yes'
        else:
            self.rules['pr_hos_inp_rules'][rule_name] = 'No'

    def get_encoded_doc_images(self):
        query = 'SELECT DISTINCT(cd.DocId), BinaryData, DocName FROM Claim_Docs cd LEFT JOIN Doc_Binaries db ON ' \
                'cd.DocId=db.DocID LEFT JOIN Documents doc ON cd.DocTypeID=doc.DocTypeID ' \
                'WHERE cd.claim_number="%s"' % self.clm_number
        doc_data = pd.read_sql(query, self.conn)
        img_json = doc_data.to_json()
        self.rules['encoded_images'] = img_json


if __name__ == '__main__':
    api_run = True
    if not api_run:
        db_path = 'databases/claim_docs.db'
        claim_number = 'V4293805'
        create_rules = CreateRules(db_path, claim_number)
        create_rules.apply_rules_pipeline()
        print(create_rules.rules)

    connection = get_doc_db_connection()
    create_rules = CreateRules(clm_number='V4293805', conn=connection)
    create_rules.apply_prod_rules_pipeline()
    print(create_rules.rules)
