#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Importing all necessary libraries
import os, json

os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"
import cv2
import easyocr
import matplotlib.pyplot as plt
from pylab import rcParams
from IPython.display import Image

rcParams['figure.figsize'] = 8, 16
from PIL import Image
import pandas as pd
import numpy as np
import re
from scipy.ndimage import interpolation as inter
import itertools


# In[2]:


def image_text_extraction(img, reader, fil_name):
    bounds = reader.readtext(fil_name)
    # Extracting the text from image
    text = []
    ignore = []
    break_word = ['as concerned citizen', 'pengadu menyatakan']
    flag = 0
    textc = ''
    for i in range(len(bounds)):
        if i in set(ignore):
            continue
        x = bounds[i]
        y1 = x[0][0][1]
        y2 = x[0][1][1]
        row = ''
        row = x[1]
        # print(x[1],x[0],y1,y2)
        for j in range(i + 1, len(bounds)):
            y = bounds[j]

            y11 = y[0][0][1]
            y22 = y[0][1][1]
            if abs(y11 - y1) < 5 or abs(y22 - y2) < 5:
                row = row + ' ' + y[1]
                ignore.append(j)
            else:
                break
        if break_word[0] in row.lower() or break_word[1] in row.lower():
            break
        if len(row) > 0:
            text.append(row)

        elif flag == 1 and len(row) > 0:
            textc += row
    return [text, textc]


# In[3]:


def extract_features(text, textc):
    key2 = [r'\bbala[i,l](.*)\b', r'\bdae[a-z]+h(.*)\b', r'\bta[a-z]+kh\s+(\d{2}[/,\,]\d{2}[/,\,]\d{4})\b',
            r'\bpegawai\s+penyiasat(.*)\b', r'\bno\s+report\s+bersangkut(.*)\b', r'\bkontinjen(.*)\b',
            r'\bno\s+paspot([a-z0-9,\s]*)\b', r'\bno\s+polis([0-9,\s]*)\b', r'\bbahasa\s+d[a-z]+ma([a-z,\s]*)\b',
            r'\bbahasa\s+asal([a-z,\s]*)\b', r'\bno\s+r[a-z]+t([a-z0-9,/,\s]*)\b', r'\bwaktu([0-9a-z,\s]*)\b',
            r'\bno\s+k[/,i]+p\s+[{,},(,))]*baru[{,},(,))]*([0-9a-z,\s]*)no\s+polis',
            r'\bjantina([0-9a-z,\s]*)t[a-z]+kh\b', r'\btarikh\s+lahir\s+(\d{2}[/,\,]\d{2}[/,\,]\d{4})\b',
            r'\bumur[\=,\s]+([0-9a-z,\s]*)\b', r'\bketurunan([a-z,\s]*)war[a-z]+ra\b', r'\bno\s+sijil\s+benarak(.*)\b',
            r'\bwarganegara([a-z,\s]*)\b', r'p[a-z]+jaan\s*(.*)', r'\balamat\s+t[a-z]+at\s+tinggal(.*)\b',
            r'\bno\s*t[oe]l\s*\(rumah\)\s*\d*\b', r'\bno\s*tel\s*\(pejabat\)\s*\d*\b',
            r'\bno\s*tel\s*\(hp\)\s*([\d,\-]*)\b']
    key1 = ['balai', 'daerah', 'tarikh', 'Pegawai penyiasat', 'No report Bersangkut', 'Kontinjen', 'No Paspot',
            'No polis', 'Bahasa Deterima', 'Bahasa Asal', 'No report', 'waktu', 'No K/P Baru', 'Jantina',
            'Tarikh lahir', 'Umur', 'Keturunan', 'No sijil benarak', 'Warganegara', 'Pekerjaan',
            'Alamat tempat tinggal', 'Rumah No tel', 'Pejabat No tel', 'No tel HP']
    len(key2)
    dict1 = {x: {} for x in key1}
    dict1['alamat'] = {}
    dict1['Nama'] = {}
    dict1['No personel'] = {}
    dict1['Pangkat'] = {}
    dict1['alamat bejabat'] = {}
    dict1['Alamat Ibu'] = {}

    for i in range(len(text)):
        if re.findall(r'\balamat\s+[p,i,t]+', text[i].lower()):
            if re.findall(r'\balamat\s+p[a-z]+t', text[i].lower()):
                z = len(list(dict1['alamat bejabat'].keys()))

                dict1['alamat bejabat'][str(z)] = re.findall(r'\balamat\s+p[a-z]+t\s+[\=]*(.*)', text[i].lower())
            if re.findall(r'\balamat\s+i[a-z,/]*pa', text[i].lower()):
                z = len(list(dict1['Alamat Ibu'].keys()))

                dict1['Alamat Ibu'][str(z)] = re.findall(r'\balamat\s+p[a-z,/]*pa(.*)', text[i].lower())

        elif re.findall(r'\balamat', text[i].lower()):
            # print(re.findall(r'\balamat[a-z,\s]*',text[i].lower()))
            z = len(list(dict1['alamat'].keys()))

            dict1['alamat'][str(z)] = re.findall(r'\balamat([a-z,\s]*)', text[i].lower())

        if re.findall(r'\bnama\b', text[i].lower()):

            if re.findall(r'\bno\b', text[i].lower()):
                # print(re.findall(r'\b(nama[a-z,\s]+)no\b',text[i].lower()))
                z = len(list(dict1['Nama'].keys()))
                dict1['Nama'][str(z)] = re.findall(r'\bnama([a-z,\s]+)no\b', text[i].lower())

                if re.findall(r'\bno\s+personel\s+[A-z0-9]*\b', text[i].lower()):
                    # print(re.findall(r'\bno\s+personel\s+[A-z0-9]*\b',text[i].lower()))
                    z = len(list(dict1['No personel'].keys()))
                    dict1['No personel'][str(z)] = re.findall(r'\bno\s+personel\s+([a-z0-9]*)\b', text[i].lower())

                if re.findall(r'\bpangkat\s*[a-z0-9,/]*', text[i].lower()):
                    # print(re.findall(r'\bpangkat\s*[a-z0-9,/]*',text[i].lower()))
                    z = len(list(dict1['Pangkat'].keys()))
                    dict1['Pangkat'][str(z)] = re.findall(r'\bpangkat\s*([a-z0-9,/]*)', text[i].lower())

                if re.findall(r'\b(no\s+k[/,i]+p\s+[{,},(,))]*baru[{,},(,))]*[0-9a-z,\s]*)no\s+polis', text[i].lower()):
                    # print(re.findall(r'\b(no\s+k[/,i]+p\s+[{,},(,))]*baru[{,},(,))]*[0-9a-z,\s]*)no\s+polis',text[i].lower()))
                    z = len(list(dict1['No K/P Baru'].keys()))
                    dict1['No K/P Baru'][str(z)] = re.findall(
                        r'\bno\s+k[/,i]+p\s+[{,},(,))]*baru[{,},(,))]*([0-9a-z,\s]*)no\s+polis', text[i].lower())
            else:
                # print(re.findall(r'\bnama[a-z,\s]+\b',text[i].lower()))
                z = len(list(dict1['Nama'].keys()))
                dict1['Nama'][str(z)] = re.findall(r'\bnama([a-z,\s]+)\b', text[i].lower())
            continue
        if re.findall(r'\bpangkat\s*[a-z0-9,/]*', text[i].lower()):
            # print(re.findall(r'\bpangkat\s*[a-z0-9,/]*',text[i].lower()))
            z = len(list(dict1['Pangkat'].keys()))
            dict1['Pangkat'][str(z)] = re.findall(r'\bpangkat\s*([a-z0-9,/]*)', text[i].lower())

        if re.findall(r'\bno\s+personel\s+[A-z0-9]*\b', text[i].lower()):
            # print(re.findall(r'\bno\s+personel\s+[A-z0-9]*\b',text[i].lower()))
            z = len(list(dict1['No personel'].keys()))
            dict1['No personel'][str(z)] = re.findall(r'\bno\s+personel\s+([a-z0-9]*)\b', text[i].lower())

        for j in range(len(key1)):
            x = key2[j]

            if re.findall(x, text[i].lower()):
                # print(re.findall(x,text[i].lower()))
                z = len(list(dict1[key1[j]].keys()))

                dict1[key1[j]][str(z)] = re.findall(x, text[i].lower())
    dict1['Complaint Statement'] = {}
    dict1['Complaint Statement']['0'] = re.findall(r'[p]*engadu\s+[m]+en[a-z]+an\s*[\:,\-]*(.*)', textc.lower())[
        0] if re.findall(r'[p]*engadu\s+[m]+en[a-z]+an\s*[\:,\-]*(.*)', textc.lower()) else textc.lower()
    s = pd.Series(dict1, index=dict1.keys())
    return dict1


def extract_req_features(dict1):
    df = pd.DataFrame(columns=['column_value', 'values', 'col_name'])
    list1 = ['balai', 'daerah', 'tarikh', 'Pegawai penyiasat', 'No report Bersangkut', 'Kontinjen', 'Bahasa Deterima',
             'No report', 'waktu']
    for i in range(len(list1)):
        df.at[i, 'column_value'] = list1[i]
        df.at[i, 'values'] = [dict1[list1[i]][x] for x in dict1[list1[i]]]
        df.at[i, 'col_name'] = 'General Police report details'
    df2 = pd.DataFrame(columns=['column_value', 'values', 'col_name'])
    list2 = ['Nama', 'No Paspot', 'alamat', 'No K/P Baru', 'No polis', 'Bahasa Asal']
    for i in range(len(list2)):
        df2.at[i, 'column_value'] = list2[i]
        df2.at[i, 'values'] = [dict1[list2[i]][x] for x in dict1[list2[i]]]
        df2.at[i, 'col_name'] = 'Recipient details'
    df3 = pd.DataFrame(columns=['column_value', 'values', 'col_name'])
    list3 = ['Nama', 'No Paspot', 'alamat', 'No K/P Baru', 'No polis', 'Bahasa Asal']
    for i in range(len(list3)):
        df3.at[i, 'column_value'] = list3[i]
        df3.at[i, 'values'] = [dict1[list3[i]][x] for x in dict1[list3[i]]]
        df3.at[i, 'col_name'] = 'Interpreter details'
    df4 = pd.DataFrame(columns=['column_value', 'values', 'col_name'])
    list4 = ['Nama', 'No Paspot', 'Alamat Ibu', 'No K/P Baru', 'No polis', 'Jantina', 'Tarikh lahir', 'Umur',
             'Keturunan', 'No sijil benarak', 'Warganegara', 'Pekerjaan', 'Alamat tempat tinggal', 'Rumah No tel',
             'Pejabat No tel', 'No tel HP']
    for i in range(len(list4)):
        df4.at[i, 'column_value'] = list4[i]
        df4.at[i, 'values'] = [dict1[list4[i]][x] for x in dict1[list4[i]]]
        df4.at[i, 'col_name'] = 'Details related to complaint'
    df5 = pd.DataFrame(columns=['column_value', 'values', 'col_name'])
    list5 = ['Complaint Statement']
    for i in range(len(list5)):
        df5.at[i, 'column_value'] = list5[i]
        df5.at[i, 'values'] = [dict1[list5[i]][x] for x in dict1[list5[i]]]
        df5.at[i, 'col_name'] = 'Complaint Statement'
    final = pd.concat([df, df2, df3, df4, df5]).reset_index(drop=True)
    return final


# In[4]:


def func_run(param):
    fil_name = param[0]
    clm = param[1]
    img = Image.open(fil_name)
    reader = easyocr.Reader(['en'])
    text_li = image_text_extraction(img, reader, fil_name)
    text = text_li[0]
    textc = text_li[1]
    dict1 = extract_features(text, textc)
    final = extract_req_features(dict1)
    final['Claim_Number'] = clm
    final['key'] = final['col_name'] + "_" + final['column_value']
    final_req = final[['key', 'values', 'Claim_Number']]
    final_req1 = final_req.pivot(index='Claim_Number', columns='key')
    return final_req1


def extract_specific_fields(extract_df):
    ext_dict = {'Claimant Name': extract_df.loc[0, 'Details related to complaint_Nama'][0],
                'telephone number': extract_df.loc[0, 'Details related to complaint_No tel HP'][0],
                'date of birth': extract_df.loc[0, 'Details related to complaint_Tarikh lahir'][0],
                'police report date': extract_df.loc[0, 'General Police report details_tarikh'][0],
                'Address': extract_df.loc[0, 'Details related to complaint_Umur'][0],
                'Claim_Number': extract_df.loc[0, 'Claim_Number']}
    return ext_dict

# In[7]:


# reading images
if __name__ == "__main__":
    os.chdir(r'D:\MLflow\cts_learning\ocr_codes')
    all_claims = pd.DataFrame()
    fil_name = [["PR1.jpg", 'v1033']]
    for i in fil_name:
        final1 = func_run(i)
        all_claims = pd.concat([all_claims, final1])
    all_claims.columns = ['Complaint Statement_Complaint Statement', 'Details related to complaint_Alamat Ibu',
                          'Details related to complaint_Alamat tempat tinggal', 'Details related to complaint_Jantina',
                          'Details related to complaint_Keturunan', 'Details related to complaint_Nama',
                          'Details related to complaint_No K/P Baru', 'Details related to complaint_No Paspot',
                          'Details related to complaint_No polis', 'Details related to complaint_No sijil benarak',
                          'Details related to complaint_No tel HP', 'Details related to complaint_Pejabat No tel',
                          'Details related to complaint_Pekerjaan', 'Details related to complaint_Rumah No tel',
                          'Details related to complaint_Tarikh lahir', 'Details related to complaint_Umur',
                          'Details related to complaint_Warganegara', 'General Police report details_Bahasa Deterima',
                          'General Police report details_Kontinjen', 'General Police report details_No report',
                          'General Police report details_No report Bersangkut',
                          'General Police report details_Pegawai penyiasat', 'General Police report details_balai',
                          'General Police report details_daerah', 'General Police report details_tarikh',
                          'General Police report details_waktu', 'Interpreter details_Bahasa Asal',
                          'Interpreter details_Nama', 'Interpreter details_No K/P Baru',
                          'Interpreter details_No Paspot', 'Interpreter details_No polis', 'Interpreter details_alamat',
                          'Recipient details_Bahasa Asal', 'Recipient details_Nama', 'Recipient details_No K/P Baru',
                          'Recipient details_No Paspot', 'Recipient details_No polis', 'Recipient details_alamat']
    all_claims = all_claims.reset_index()
    for i in range(0, len(all_claims)):
        for j in all_claims.columns[1:]:
            all_claims.loc[i][j] = list(itertools.chain(*all_claims.loc[i][j]))
    # all_claims.to_excel("Extracted_fields_from_policeReport.xlsx", index=False)
    extraction_dict = extract_specific_fields(all_claims)
    with open('D:\MLflow\cts_learning\prod_model_artifacts\my_ocr_police_report.json', 'w') as outfile:
        json.dump(extraction_dict, outfile)

# In[ ]:
