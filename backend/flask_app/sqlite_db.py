import sqlite3
import joblib
from base64 import b64encode, b64decode
from io import BytesIO
from datetime import datetime


def create_db_connection(db_f_path):
    connection = sqlite3.connect(db_f_path)
    cursor = connection.cursor()
    return connection, cursor


def create_model_table(cursor):
    ct = 'CREATE TABLE IF NOT EXISTS models (id INTEGER PRIMARY KEY AUTOINCREMENT, run_id TEXT, experiment_id INT, ' \
         'prod_model_status TEXT, recall REAL, f1_score REAL, lift REAL, max_depth INT, learning_rate REAL ) '
    cursor.execute(ct)


def create_serialized_prod_model_table(cursor):
    ct = 'CREATE TABLE IF NOT EXISTS prod_models (id INTEGER PRIMARY KEY AUTOINCREMENT, run_id TEXT, ' \
         'model_ascii TEXT, exp_id INT, insert_ts DATETIME DEFAULT CURRENT_TIMESTAMP)'
    cursor.execute(ct)


def insert_one_row(cursor, val_tuple):
    insert_query = 'INSERT INTO models (run_id, experiment_id, prod_model_status, recall, f1_score,' \
                   'lift, max_depth, learning_rate) VALUES(?, ?, ?, ?, ?, ?, ?, ?)'
    cursor.execute(insert_query, val_tuple)


def create_pred_table(cursor):
    ct = 'CREATE TABLE IF NOT EXISTS predictions (id INTEGER PRIMARY KEY AUTOINCREMENT, claim_number TEXT,' \
         'score_grp INT, investigated TEXT, claim_report_date DATETIME, claim_amt_usd REAL, assigned_to TEXT)'
    # delete all prior entries
    del_query = 'DELETE FROM predictions'
    cursor.execute(del_query)
    cursor.execute(ct)


def create_pred_reason_codes_table(cursor):
    ct = 'CREATE TABLE IF NOT EXISTS predictions_reason_codes (id INTEGER PRIMARY KEY AUTOINCREMENT, ' \
         'claim_number INT, tppd REAL, average_claims_paid_36_cr_Medium REAL, ' \
         'prior_causeofloss REAL,  claims_in_36_cr_High REAL, notif_loss_cr_High REAL, close_claim REAL,' \
         'part_est_ratio_cr_Low REAL, part_labor_ratio_cr_Low REAL, rc_bucket TEXT)'
    cursor.execute(ct)
    del_query = 'DELETE FROM predictions_reason_codes'
    cursor.execute(del_query)


def write_to_table(connection, df, table_name):
    df.to_sql(name=table_name, con=connection, if_exists='append', index=False)


def insert_model_in_ascii(cursor, run_id, obj, exp_id):
    encoded_obj = serialize_data(obj)
    insert_query = 'INSERT INTO prod_models (run_id, model_ascii, exp_id) VALUES (?, ?, ?)'
    cursor.execute(insert_query, (run_id, encoded_obj, exp_id))


def serialize_data(passed_object):
    with BytesIO() as tmp_bytes:
        joblib.dump(passed_object, tmp_bytes)
        encoded = b64encode(tmp_bytes.getvalue())

    return encoded


def deserialize(passed_bytes):
    decoded = passed_bytes.decode('ascii')
    bytesObj = b64decode(decoded)
    actual_data = joblib.load(BytesIO(bytesObj))

    return actual_data


def create_tool_users_table(cursor, del_contents=False):
    ct = 'CREATE TABLE IF NOT EXISTS tool_users (id INTEGER PRIMARY KEY AUTOINCREMENT, ' \
         'user_name TEXT, user_id INT)'
    cursor.execute(ct)
    # delete all prior entries
    if del_contents:
        del_query = 'DELETE FROM predictions'
        cursor.execute(del_query)


def create_user_comments_table(cursor, del_contents=False):
    ct = 'CREATE TABLE IF NOT EXISTS user_comments (id INTEGER PRIMARY KEY AUTOINCREMENT, ' \
         'user_name TEXT, user_id INT, claim_number TEXT, insert_ts DATETIME DEFAULT CURRENT_TIMESTAMP)'
    cursor.execute(ct)
    if del_contents:
        del_query = 'DELETE FROM predictions'
        cursor.execute(del_query)


def create_input_data_table(cursor):
    ct = 'CREATE TABLE IF NOT EXISTS input_claims_data (id INTEGER PRIMARY KEY AUTOINCREMENT, claim_number TEXT, ' \
         'Investigated TEXT, STATE TEXT, CLAIM_TYPE TEXT, APPRV_AMT_USD REAL, PAID REAL, pin_code INT, address TEXT' \
         'CLAIM_STATUS TEXT, CLAIM_AGE_DAYS INT,  CLAIM_AMT_USD REAL, VEHICLEMAKE TEXT, YEARMANUFACTURED TEXT,' \
         'TOTALLOSS TEXT, APPROVEDOVERALLGROSS REAL, CLAIM_STATUS TEXT,' \
         'CLAIMREPORTDATE DATE, DATEOFLOSS DATE, CLAIMTYPEDESC TEXT, end_passed_ldate_after INT, occu_swasta_blank INT,' \
         'rush_hour INT, dam_gt50_risksum INT, prior_causeofloss INT, tp_tpbi INT, tppd INT,'\
         'part_labor_ratio_cr_Low INT, part_est_ratio_cr_Low INT,' \
         'est_mvalue_ratio_cr_High INT, loss_inception_cr_Low INT,' \
         'notif_loss_cr_High INT, expire_loss_cr_Low INT,' \
         'average_claims_paid_36_cr_High INT, average_claims_paid_36_cr_Medium INT,'\
         'week_time_3 INT, claims_in_36_cr_High INT, claims_in_36_cr_Medium INT,'\
         'close_claim INT, is_self_accident INT, target INT)'
    cursor.execute(ct)