import os
import sys
import time
from datetime import datetime
from flask import Flask
from flask_cors import CORS, cross_origin
import sqlite3
import yaml
import pandas as pd

import utils
import CreateJsons
from flask import jsonify, request
import json
import HandleRuleBank
import AlertManagement
import Strategy
# import reporting_claims_overview
from forecasting.forecasting_utils import (time_series_data_retrieval,
                                           get_connection, apply_forecasting_algorithmn_amounts,
                                           apply_forecasting_algorithm_clm_cnt,
                                           compute_forecasting_aggregates_clm_cnt,
                                           compute_forecasting_aggregates_amounts)

app = Flask(__name__)
CORS(app)


@app.route('/time')
def get_current_time():
    return {'time': datetime.now().strftime('%Y-%m-%d %H:%M:%S')}


@app.route('/kpicards')
def get_kpi_cards_hist_data():
    result = CreateJsons.SummaryKPIChartsData().df_kpi_all_summary()
    result = json.dumps(result, indent = 4)
    return result


@app.route('/invesigation_overview')
def get_investigation_overview_json():
    result = CreateJsons.SummaryKPIChartsData().create_json_for_Investigation_screen()
    result = json.dumps(result, indent = 4)
    return result


@app.route('/test')
def test():
    content = request.json
    print(content)
    return {"response": "Method Called"}


@app.route('/add_rules', methods=['POST'])
def add_rules():
    rules_json = request.json
    print("Input :", rules_json)

    obj = HandleRuleBank.RuleBank()
    response = obj.add_rules(rules_json)

    return response

@app.route('/delete_rules', methods=['POST'])
def delete_rules():
    rules_json = request.json
    print("Input :", rules_json)

    obj = HandleRuleBank.RuleBank()
    response = obj.delete_from_rule_bank(rule_description=rules_json['rule_description'])

    return response

@app.route('/rule_bank', methods=['GET'])
def get_rule_bank():
    obj = HandleRuleBank.RuleBank()
    response = json.dumps({"data": list(obj.get_rule_bank().values())})
    return response

@app.route('/alert_management_frdtyp', methods=['GET'])
def get_fraud_type_alerts():
    response_dict = AlertManagement.create_alert_page_data()
    return json.dumps(response_dict)

@app.route('/claim_level_data', methods=['POST'])
def get_claim_level_data():
    input_json = request.json
    print("Input :", input_json)
    response = AlertManagement.create_claim_level_data(claim_num=input_json['claim_number'])
    response = json.dumps(response)
    return response

@app.route('/add_comments', methods=['POST'])
def add_comments():
    input_json = request.json
    print("Input :", input_json)
    response = AlertManagement.add_comments_to_claims(input_json['user_id'],
                                                      input_json['claim_number'],
                                                      input_json['comment'])
    return json.dumps(response)


@app.route('/apply_strategy', methods=['POST'])
def apply_strategy():
    input_json = request.json
    print("Input :", input_json)
    input_json = pd.DataFrame(input_json).set_index('Type').to_dict()
    response = Strategy.create_score_group_based_on_model_weights(float(input_json['Weight']['Supervised']),
                                                                  float(input_json['Weight']['anomaly']))
    return json.dumps(response)


@app.route('/get_forecast', methods=['POST'])
def get_forecasting_data():
    input_json = request.json
    connection = get_connection()
    smoothed_ts_clm_cnt, smoothed_ts_appr_amt, smoothed_ts_claim_amt = time_series_data_retrieval(connection,
                                                                                                  input_json['state'])
    forecast, ts_clm_cnt = apply_forecasting_algorithm_clm_cnt(smoothed_ts_clm_cnt, weeks=input_json['weeks'])
    pred_apprvd, pred_clmd, ts_appr_amt, ts_clm_amt = apply_forecasting_algorithmn_amounts(smoothed_ts_appr_amt,
                                                                                           smoothed_ts_claim_amt,
                                                                                           input_json['weeks'])

    forecast_json_clm_amts = compute_forecasting_aggregates_amounts(pred_apprvd, pred_clmd, ts_appr_amt, ts_clm_amt)
    forecast_json_clm_cnt = compute_forecasting_aggregates_clm_cnt(forecast, ts_clm_cnt)
    return json.dumps(forecast_json_clm_cnt | forecast_json_clm_amts)

    
@app.route('/claims_by_claim_type_dict', methods=['GET'])
def claims_by_claim_type_dict():
    response_dict = reporting_claims_overview.get_claims_claimtype()
    return json.dumps(response_dict)

@app.route('/claims_by_state', methods=['GET'])
def claims_by_state_dict():
    response_dict = reporting_claims_overview.get_claims_state()
    return json.dumps(response_dict)    

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0',port=8015)
