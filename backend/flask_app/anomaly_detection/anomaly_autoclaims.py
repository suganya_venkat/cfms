#!/usr/bin/env python
# coding: utf-8

# In[13]:


import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)

import matplotlib.pyplot as plt
from cfms.backend.flask_app.helper_functions_auto_claims import lift, plot_lift_chart
import numpy as np
import pandas as pd
import os, json
# import scikitplot as skplt
import scikitplot as skplt
from cfms.backend.flask_app.sqlite_db_add import create_db_connection, write_to_table
from cfms.backend.flask_app.helper_functions_auto_claims import create_table_json

from pyod.models.iforest import IForest
from sklearn.metrics import roc_auc_score, accuracy_score, precision_score, recall_score
from sklearn.model_selection import train_test_split
from sklearn import preprocessing

pd.set_option('display.float_format', lambda x: '%.3f' % x)
os.chdir(r'/')
import pandas as pd

# data_impute = pd.read_excel(r"D:\CFMS_updated_code\cfms\backend\python_code\spreadsheet_files\auto_anomaly.xlsx")
connection, cursor = create_db_connection('db_files/input_data.db')
data_impute = pd.read_sql('SELECT * FROM input_claims_anomaly_data', connection)
connection.close()


def dump_and_save_to_json(ft_imp, lift_df, model_params):
    m_num = model_params['model_number']
    if not os.path.isdir('prod_model_artifacts'):
        os.mkdir('prod_model_artifacts')
    ft_imp.to_json('prod_model_artifacts/model_%s_shap.json' % m_num, orient='table')
    lift_df.to_json('prod_model_artifacts/model_%s_lift.json' % m_num, orient='table')
    with open('prod_model_artifacts/model_%s_params.json' % m_num, 'w') as outfile:
        json.dump(model_params, outfile)


def create_lift_chart(y_test, y_pred, show_plot=False):
    model_metrics = lift(y_test, y_pred, 10)
    lift_df = plot_lift_chart(model_metrics, show_plot)
    return lift_df


def iso_forest(model_params, scale_params, in_data, contamination, show_graph=False, feat_imp=False):
    data = in_data.copy()

    # data[scale_params] = preprocessing.normalize(data[scale_params])
    # data[scale_params] = preprocessing.scale(data[scale_params])

    X_train, X_test = train_test_split(data, test_size=.2, random_state=42)

    scaler = preprocessing.StandardScaler()
    scaler.fit(X_train[scale_params])
    X_train[scale_params] = scaler.transform(X_train[scale_params])
    X_test[scale_params] = scaler.transform(X_test[scale_params])

    model1 = IForest(verbose=0, contamination=contamination, bootstrap=True, random_state=42)

    model1.fit(X_train[model_params])

    # outlier scores

    x_train_scores = model1.decision_scores_
    x_train_scores = pd.Series(x_train_scores)

    threshold = model1.threshold_

    # Predict anomaly scores

    x_test_scores = model1.decision_function(X_test[model_params])
    x_test_scores = pd.Series(x_test_scores)

    probs_test = model1.predict_proba(X_test[model_params])
    probs_test = pd.DataFrame(probs_test)

    probs_train = model1.predict_proba(X_train[model_params])
    probs_train = pd.DataFrame(probs_train)

    X_train[scale_params] - scaler.inverse_transform(X_train[scale_params])
    X_test[scale_params] - scaler.inverse_transform(X_test[scale_params])

    df_test = X_test.copy().reset_index()
    df_test['score'] = x_test_scores
    df_test.loc[:, 'probability'] = pd.Series(probs_test.iloc[:, 1])
    df_test['cluster'] = np.where(df_test['score'] < threshold, False, True)

    print('ROC AUC: ', roc_auc_score(df_test['fraud'], df_test['score']))
    print('Accuracy: ', accuracy_score(df_test['fraud'], df_test['cluster']))
    print('Precision: ', precision_score(df_test['fraud'], df_test['cluster']))
    print('Recall: ', recall_score(df_test['fraud'], df_test['cluster']))

    lift_df = create_lift_chart(df_test['fraud'], probs_test.values)

    if show_graph:
        fig = plt.figure(figsize=(15, 10))
        ax1 = plt.subplot(1, 2, 1)
        skplt.metrics.plot_confusion_matrix(df_test['fraud'], df_test['cluster'], ax=ax1)
        ax2 = plt.subplot(1, 2, 2)
        skplt.metrics.plot_roc(df_test['fraud'], probs_test, ax=ax2)
        plt.savefig('prod_model_artifacts/anomaly/roc_auc.jpg')
        plt.show()

        fig = plt.figure(figsize=(15, 10))
        ax1 = plt.subplot(1, 2, 1)
        skplt.metrics.plot_lift_curve(df_test['fraud'], probs_test, ax=ax1)
        plt.title('Lift Curve for Test Data')
        ax2 = plt.subplot(1, 2, 2)
        skplt.metrics.plot_lift_curve(X_train['fraud'], model1.predict_proba(X_train[model_params]), ax=ax2)
        plt.title('Lift Curve for Train Data')
        plt.show()

    probs_anom = pd.concat([probs_train, probs_test]).values[:, 0]
    clm_num = pd.concat([X_train.claim_number, X_test.claim_number])

    data = {'claim_id': clm_num, 'proba_score': probs_anom}
    score_df = pd.DataFrame(data)

    feat_imp_df = RFE_FeatImp(X_train[model_params], X_train['fraud'], X_test[model_params],
                              X_test['fraud'], model1, cont=0)
    save_to_json(feat_imp_df, lift_df, contamination)
    return score_df


def save_to_json(shap_df, lift_df, cnt_val):
    model_params = dict()
    model_params['name'] = 'Isolation Forest'
    model_params['contamination'] = cnt_val
    if not os.path.isdir('prod_model_artifacts'):
        os.mkdir('prod_model_artifacts')
    model_params['Shap -feature importance'] = create_table_json(shap_df)
    model_params['Lift Chart'] = create_table_json(lift_df)
    with open('prod_model_artifacts/anomaly/model_params.json', 'w') as outfile:
        json.dump(model_params, outfile)


def lift_chart(estimator, evalX, evalY, buckets=None, printsum=0, avg='median', cont=0):
    '''
    estimator = model
    evalX = df of X values
    evalY = df of Y values
    buckets default to None; can instead use training bins if already created
    printsum default to 0; change to 1 if you want to output summary
    avg is mean or median
    cont = 0 (continuous); cont = 1 (binary)

    '''

    X = evalX.copy()
    Y = evalY.copy()

    if cont == 1:
        preds = estimator.predict(X)
    else:
        # preds = estimator.predict(X)
        preds = estimator.predict_proba(X)[:, 1]

    ActPred = pd.DataFrame({"Predicted": preds, "Actual": Y})

    if buckets == None:
        ActPred.sort_values(by=['Predicted'], ascending=False, inplace=True)
        ActPred.reset_index(drop=True, inplace=True)
        ActPred['Decile'] = pd.qcut(ActPred.index, 10, labels=False)
    else:
        # ActPred['Decile']=pd.cut(ActPred.Predicted, buckets, labels = False, right = False, duplicates = 'drop')
        ActPred['Decile'] = pd.cut(ActPred.Predicted, buckets, labels=False, right=False)
        ActPred['Decile'] = [9 - x for x in ActPred['Decile']]

    grouped = ActPred.groupby('Decile', as_index=False)

    liftchart = pd.DataFrame({'Measure': avg,
                              'Decile': 0,
                              'MinPred': grouped.min().Predicted,
                              'MaxPred': grouped.max().Predicted,
                              'MeanPred': grouped.mean().Predicted,
                              'MeanAct': grouped.mean().Actual,
                              'MedianPred': grouped.median().Predicted,
                              'MedianAct': grouped.median().Actual,
                              'MinAct': grouped.min().Actual,
                              'MaxAct': grouped.max().Actual,
                              # 'NumNonZero': grouped['Actual'].agg(lambda x: x.ne(0).sum()),
                              'TotalRecs': grouped.count().Actual
                              })

    liftchart['Decile'] = liftchart.index + 1
    liftchart = liftchart.sort_values(by='MinPred', ascending=False).reset_index(drop=True)

    if cont == 0:
        liftchart['NumEvents'] = grouped.sum().Actual
        liftchart['HitRate'] = liftchart['NumEvents'] / liftchart['TotalRecs']
        liftchart['PctEvents'] = liftchart['NumEvents'] / liftchart['NumEvents'].sum()
        liftchart['PctRecs'] = liftchart['TotalRecs'] / liftchart['TotalRecs'].sum()
        liftchart['CumRecs'] = liftchart['PctRecs'].cumsum()
        liftchart['CumEvents'] = liftchart['PctEvents'].cumsum()
        liftchart['Lift'] = liftchart['HitRate'] / (liftchart['NumEvents'].sum() / liftchart['TotalRecs'].sum())

        colorder = ['Measure', 'Decile', 'MinPred', 'MaxPred',
                    'TotalRecs', 'CumRecs', 'NumEvents', 'HitRate',
                    'PctRecs', 'Lift', 'PctEvents', 'CumEvents']

    if cont == 1:
        if avg == 'mean':
            overall_avg = evalY.mean()
            liftchart = liftchart.drop(['MedianPred', 'MedianAct'], axis=1)
            liftchart = liftchart.rename(index=str, columns={"MeanPred": "AvgPred", "MeanAct": "AvgAct"})

        if avg == 'median':
            overall_avg = evalY.median()
            liftchart = liftchart.drop(['MeanPred', 'MeanAct'], axis=1)
            liftchart = liftchart.rename(index=str, columns={"MedianPred": "AvgPred", "MedianAct": "AvgAct"})

        liftchart['PctRecs'] = liftchart['TotalRecs'] / liftchart['TotalRecs'].sum()
        liftchart['CumRecs'] = liftchart['PctRecs'].cumsum()

        liftchart['Lift'] = (liftchart['AvgAct']) / overall_avg

        colorder = ['Measure', 'Decile', 'MinPred', 'MaxPred',
                    'MinAct', 'MaxAct', 'AvgAct', 'AvgPred',
                    'TotalRecs', 'PctRecs', 'CumRecs', 'Lift']

    if avg == 'mean':
        overall_avg = ActPred.Actual.mean()
    if avg == 'median':
        overall_avg = ActPred.Actual.median()

    # if printsum == 1:
    # print("Total Records: ", liftchart['TotalRecs'].sum())
    # print("Overall Average: ", "{:.2%}".format(overall_avg))

    liftchart = liftchart[colorder].copy()

    return (liftchart)


def RFE_FeatImp(train_x, train_y, test_x, test_y, model, cont=0):
    '''
    returns df of variables sorted by change in lift on test data
    when variable is removed from the model
    '''
    ref_imp = []
    allvars = list(test_x.columns)

    model1 = model.fit(train_x)

    if cont == 0:
        lift1 = lift_chart(model1, test_x, test_y, cont=0)['Lift'][0]  # calculate lift of full model
    if cont == 1:
        lift1 = lift_chart(model1, test_x, test_y, cont=1, printsum=0)['Lift'][0]

        # print("Number of variables to test: ", len(allvars))

    for index, i in enumerate(allvars):
        # if (index + 1) % 7 == 0:
        # print(DT.now(), 'Testing var #', str(index + 1))

        train_trunc = train_x.drop(i, axis=1)
        test_trunc = test_x.drop(i, axis=1)

        model2 = model.fit(train_trunc)  # fit model to all but removed variable

        if cont == 0:
            lift2 = lift_chart(model2, test_trunc, test_y, cont=0)['Lift'][0]
        if cont == 1:
            lift2 = lift_chart(model2, test_trunc, test_y, printsum=0, cont=1)['Lift'][0]

        ref_imp.append([i, (lift2 - lift1)])

    ref_imp = pd.DataFrame(ref_imp, columns=['Variable_Removed', 'LiftChg'])

    # Reset model to fit back to all data
    model3 = model.fit(train_x)

    ref_imp['Neg'] = -ref_imp.LiftChg
    ref_imp['Importance'] = (ref_imp.Neg - ref_imp.Neg.min()
                             ) / (ref_imp.Neg.max() - ref_imp.Neg.min())
    ref_imp = ref_imp.drop(columns=['Neg'])
    ref_imp['Features:'] = ref_imp['Variable_Removed']
    ref_imp = ref_imp.drop(columns=['Variable_Removed'])

    return ref_imp.sort_values(by='LiftChg').reset_index()


def write_scores_to_claims_db(df):
    con, cur = create_db_connection('db_files/claims_data.db')
    write_to_table(con, df, 'claims_anomaly_score')


model_param = ['part_labor_ratio', 'part_est_ratio', 'est_mvalue_ratio', 'loss_inception', 'notif_loss', 'expire_loss',
               'close_claim', 'average_claims_paid_36', 'week_time', 'claims_in_36', 'is_self_accident', 'endorse_inc']

scale_param = ['part_labor_ratio', 'part_est_ratio', 'est_mvalue_ratio', 'loss_inception', 'notif_loss', 'expire_loss',
               'average_claims_paid_36', 'claims_in_36']
data_impute1 = data_impute.copy()
# =pd.merge(data_impute,dat,left_on="claim_number",right_on="CLAIMNUMBER",how="left")
# data_impute1['fraud']=data_impute1['target']


scored_data = iso_forest(model_param, scale_param, data_impute1, show_graph=True,
                         feat_imp=True, contamination=.107)

write_scores_to_claims_db(scored_data)

