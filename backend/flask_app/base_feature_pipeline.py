"""
Defines Classes to handle Transformations on Raw Data
"""

import pandas as pd
from time import time
import numpy as np

class Transformation():
    """
    Applies a transformation on one or more Pandas Series
    in order to produce one or more features.

    Parameters
    ----------
    on_col: str
        The string to preprocess
    f: <function>
        The underlying transformation function.
    name: str
        Name of transformation
    args: dict
        Dictionary of transformation arguments

    """
    
    def __init__(self, on_col, f, name, args=None):
        self.on_col = on_col
        self.transformation_f = f
        self.name = name
        self.transformation_args = args
        self.transformation_applied = False
        self.transformation_stats = {}
        
    def apply(self, args):
        start = time()

        tdf = self.transformation_f(**args)

        if self.name is not None:
            if isinstance(tdf, pd.DataFrame):
                if isinstance(self.name, list) or isinstance(self.name, tuple):
                    tdf.columns = [c for c in self.name]
                else:
                    if self.name is None:
                        tdf.columns = [str(col) for col in tdf.columns]
                    else:
                        tdf.columns = [str(self.name) + "_" + str(col) for col in tdf.columns]
            elif isinstance(tdf, pd.Series):
                tdf = pd.DataFrame(tdf)
                tdf.columns = [c for c in self.name]
                
        tdf = tdf.reset_index(drop=True)
        self.transformation_applied = True
        end = time()
        tot_time = end - start
        self.transformation_stats["Execution Time"] = tot_time
        if 'ser1' in args:
            inp_size = args['ser1'].shape[0]
        else:
            inp_size = np.nan
        
        self.transformation_stats["Input Size"] = inp_size
        
        return tdf
        

class FeaturePipeline():
    """
    Creates a feature pipeline using transformations.
    """

    def __init__(self,):
        self.transformations = []
        self.transformed_df = None
                
    def add_transformation(self, on_cols, transformation_f, transformation_args=None, names=None):
        """
        Applies a transformation on one or more Pandas Series
        in order to produce one or more features.

        Parameters
        ----------
        on_col: str or list or tuple
            - str: Applies transformation on single column
            - tuple: Applies transformation on given set of columns in tuple
            - list: Applied same transformation on each set of columns given in tuples
        transformation_f: <function>
            The underlying transformation function.
        transformation_args: dict
            Dictionary of transformation arguments
        names: str or list or tuple
            Names for each transformation
        """
        
        if transformation_f in self.transformations:
            print('Transformation: {} already added, skipping...'.format(transformation_f))
            return self
                
        if type(on_cols) in [str]:
            on_cols = [on_cols]

        if isinstance(on_cols, str):
            on_cols = [(on_cols, )]
        elif isinstance(on_cols, tuple):
            on_cols = [on_cols]
        elif isinstance(on_cols, list):
            for v in on_cols:
                assert isinstance(v, tuple), 'Expected tuple got {}'.format(type(v))

        if isinstance(names, str):
            names = [(names, )]
        elif isinstance(names, tuple):
            names = [names]
        elif isinstance(names, list):
            names_ = []
            for v in names:
                assert isinstance(v, tuple) or isinstance(v, str), 'Expected tuple got {}'.format(type(v))
                if isinstance(v, str):
                    names_.append((v, ))
            names = names_

        assert len(on_cols) == len(names), 'Lenght of on_cols must match with names'

        for on_col, name in zip(on_cols, names):
            self.transformations.append(
                Transformation(on_col=on_col,
                            f=transformation_f,
                            args=transformation_args, 
                            name=name,
                            ))
        
        return self
    
    def get_args(self, t, df, transformed_df):
        
        list_of_series = []
        on_col_list = []
        if isinstance(t.on_col, str):
            on_col_list = [t.on_col]
        else:
            on_col_list = t.on_col
        
        for c in on_col_list:
            if c in df.columns:
                s = df[c]
                list_of_series.append(s)
            else:
                try:
                    s = transformed_df[c]
                    list_of_series.append(s)
                except:
                    raise Exception('Column {} not found in input dataframes'.format(c))                
                        
        args = {'ser{}'.format(i+1): l for i, l in enumerate(list_of_series)}
            
        if t.transformation_args:
            args.update(t.transformation_args)
            
        return args
            
    def apply_transformations(self, df, index_cols=None):
        """
        Apply the feature pipline to the raw data DataFrame
        Args:
            df: DataFrame to create features from
        Returns:
            (Features, Column Names to One-Hot Encode)
        """
        
        # Deduplicate the input dataframe on columns
        df = df.loc[:,~df.columns.duplicated()].copy()
        
        # Create index column 
        if index_cols:
            df['INDEX'] = df[index_cols].apply(lambda x: "".join([v for v in x[index_cols]]), axis=1)
            
            # Deduplicate the input dataframe at INDEX level
            df = df.drop_duplicates(['INDEX'])
        
            # Set The new index
            index_list = df['INDEX'].tolist()
            df.index = index_list
            del df['INDEX']
        else:
            index_list = df.index
                
        transformed_df = None or self.transformed_df
        for t in self.transformations:
            if t.transformation_applied:
                print('Transformation: {} applied, skipping...'.format(t))
                continue
                
            args = self.get_args(t=t, df=df, transformed_df=transformed_df)

            if transformed_df is None:
                transformed_df = t.apply(args)
                transformed_df.index = index_list
            else:
                tdf = t.apply(args)
                tdf.index = index_list
                transformed_df = pd.concat([transformed_df, tdf], axis=1)
                
            self.transformed_df = transformed_df
        return transformed_df


if __name__=="__main__":

    df = pd.DataFrame({"ClaimNumber": ['c100', 'c101', 'c102'],
                        "ReportDate": [pd.to_datetime('2022-03-28'), 
                        pd.to_datetime('2022-02-28'), 
                        pd.to_datetime('2022-01-28')],
                        "LossDate": [pd.to_datetime('2022-02-28'), 
                        pd.to_datetime('2022-01-25'), 
                        pd.to_datetime('2021-12-28')],
                        "PolicyEffective": [pd.to_datetime('2022-03-28'), 
                        pd.to_datetime('2022-02-28'), 
                        pd.to_datetime('2022-01-28')],
                        "PolicyExpiry": [pd.to_datetime('2022-02-28'), 
                        pd.to_datetime('2022-01-25'), 
                        pd.to_datetime('2021-09-28')],
                        'ClaimAmount': [100, 200, 300],
                        'PaidAmount': [50, 100, 200]
                        })
    
    def base_transforms_days_between(ser1, ser2):
        """
        Difference between Dates in Days
        
        ser1: 'From' Date Series
        ser2: 'To' Date Series   
        """
        
        ser1 = ser1.fillna('nan')
        ser2 = ser2.fillna('nan')
        
        s1 = pd.to_datetime(ser1.astype(str))
        s2 = pd.to_datetime(ser2.astype(str))
        ser1 = (s2 - s1).dt.days
        return ser1

    def base_transform_double(ser1):
        return 2*ser1
        
    pipe = FeaturePipeline()

    pipe.add_transformation(on_cols=[('LossDate', 'ReportDate'),
                                    ('LossDate', 'PolicyExpiry')
                                    ], 
                            transformation_f=base_transforms_days_between,
                            transformation_args=None,
                             names=['Report_Lag',
                                    'Diff_LossPolicyExp'] 
                             )
    
    pipe.add_transformation(on_cols=[('ClaimAmount',),
#                                     ('PaidAmount',)
                                    ], 
                            transformation_f=base_transform_double,
                            transformation_args=None,
                             names=['DoubleClaimed',
                                    'DoublePaid'] 
                             )

    result = pipe.apply_transformations(df, index_cols=['ClaimNumber'])
    print(result)

        