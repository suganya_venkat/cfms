import datetime
import json, os
import numpy as np
import pandas as pd
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt

# from xgboost import XGBClassifier

f_path = r'D:\MLflow\cts_learning\my_auto_data.xlsx'


def convert_bool_to_numeric(x):
    if x:
        return 1
    else:
        return 0


def create_column_based_on_date_range(df):
    start_date = datetime.date(2021, 1, 1)
    end_date = datetime.date(2021, 12, 31)
    time_between_dates = end_date - start_date
    days_between_dates = time_between_dates.days
    df['days'] = np.random.randint(days_between_dates, size=df.shape[0])
    claim_date = df['days'].apply(lambda x: create_date(start_date, x))
    df.insert(loc=1, column='claim_date', value=claim_date)
    df = df.drop(columns=['days'])
    return df


def create_date(start_date, days_between_dates):
    random_date = start_date + datetime.timedelta(days=days_between_dates)
    return random_date


def train_data_range(df, retrain=False):
    min_date, max_date = df['claim_date'].min(), df['claim_date'].max()
    if not retrain:
        end_date = min_date + datetime.timedelta(days=int((max_date - min_date).days * 0.75))
        df = df[df['claim_date'] < end_date]
        return df
    else:
        start_date = max_date - datetime.timedelta(days=int((max_date - min_date).days * 0.75))
        df = df[df['claim_date'] > start_date]
        return df


def malaysia_whole_data_preprocess(file_path):
    """
    Preprocesses malaysia dataset

    :param file_path:
    :return: numpy.ndarray
    """

    df = pd.read_excel(file_path)
    # since the data doesn't have date columns
    df = create_column_based_on_date_range(df)

    X = df.iloc[:, 3:-1]
    y = df.iloc[:, -1]
    clm_numbers, clm_dates = df['CLAIMNUMBER'], df['claim_date']

    X['close_claim'] = X['close_claim'].apply(lambda x: convert_bool_to_numeric(x))
    X['is_self_accident'] = X['is_self_accident'].apply(lambda x: convert_bool_to_numeric(x))
    return X, clm_numbers, clm_dates, y


def train_data_loader(connection):
    target_vals_query = 'SELECT IdValue, CategoryID  FROM Target_categories'
    target_vals_df = pd.read_sql(target_vals_query, connection)

    features_query = 'SELECT * FROM Derived_Features'
    features_df = pd.read_sql(features_query, connection)

    features_df = pd.pivot_table(features_df, values='FeatureValue', index='IdValue', columns='FeatureID', aggfunc='max')
    features_df = features_df.reset_index()

    field_vals_query = 'SELECT ID_1, ID_15 FROM claim_id_field_vals'
    date_df = pd.read_sql(field_vals_query, connection)

    merged_df = pd.merge(features_df, target_vals_df, how='left', on='IdValue')
    merged_df = pd.merge(merged_df, date_df, how='left', left_on='IdValue', right_on='ID_1')
    merged_df = merged_df.drop(columns=['ID_1'])

    feature_names_query = 'SELECT FeatureID, ModelFeatureName  FROM  Features_Inventory'
    feature_names_df = pd.read_sql(feature_names_query, connection)

    ft_id, ft_names = feature_names_df['FeatureID'], feature_names_df['ModelFeatureName']

    feature_dict = {ft_id[i]: ft_names[i] for i in range(len(feature_names_df))}
    feature_dict['IdValue'] = 'claim_number'
    feature_dict['ID_15'] = 'claim_report_date'

    merged_df = merged_df.rename(columns=feature_dict)

    X = merged_df.drop(columns=['CategoryID', 'claim_report_date'])
    y = merged_df['CategoryID'].apply(lambda x: 1 if x == 'CAT-1' else 0)
    x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.15, random_state=12)
    return x_train, x_test, y_train, y_test


def malaysia_data_preprocess(file_path, retrain=False):
    """
    Preprocesses malaysia dataset

    :param file_path: str
    :param retrain: bool
    :return: tuple of numpy arrays
    """

    df = pd.read_excel(file_path)
    # since the data doesn't have date columns
    df = create_column_based_on_date_range(df)

    if not retrain:
        df = train_data_range(df)
    else:
        df = train_data_range(df, retrain=retrain)

    X = df.iloc[:, 3:-1]
    y = df.iloc[:, -1]

    X['close_claim'] = X['close_claim'].apply(lambda x: convert_bool_to_numeric(x))
    X['is_self_accident'] = X['is_self_accident'].apply(lambda x: convert_bool_to_numeric(x))

    x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.15, random_state=12)
    return x_train, x_test, y_train, y_test


def lift(test, pred, quantiles, return_only_scr_grp=False):
    res = pd.DataFrame(np.column_stack((test, pred)),
                       columns=['Target', 'PR_0', 'PR_1'])

    res['scr_grp'] = pd.qcut(res['PR_0'].rank(method='first'), quantiles, labels=False) + 1
    if return_only_scr_grp:
        return res['scr_grp']

    crt = pd.crosstab(res.scr_grp, res.Target).reset_index()
    crt = crt.rename(columns={'Target': 'Np', 0.0: 'Negatives', 1.0: 'Positives'})

    G = crt['Positives'].sum()
    B = crt['Negatives'].sum()

    avg_resp_rate = G / (G + B)

    crt['resp_rate'] = round(crt['Positives'] / (crt['Positives'] + crt['Negatives']), 2)
    crt['lift'] = round((crt['resp_rate'] / avg_resp_rate), 2)
    crt['rand_resp'] = 1 / quantiles
    crt['cmltv_p'] = round((crt['Positives']).cumsum(), 2)
    crt['cmltv_p_perc'] = round(((crt['Positives'] / G).cumsum()) * 100, 1)
    crt['cmltv_n'] = round((crt['Negatives']).cumsum(), 2)
    crt['cmltv_n_perc'] = round(((crt['Negatives'] / B).cumsum()) * 100, 1)
    crt['cmltv_rand_p_perc'] = (crt.rand_resp.cumsum()) * 100
    crt['cmltv_resp_rate'] = round(crt['cmltv_p'] / (crt['cmltv_p'] + crt['cmltv_n']), 2)
    crt['cmltv_lift'] = round(crt['cmltv_resp_rate'] / avg_resp_rate, 2)
    crt['KS'] = round(crt['cmltv_p_perc'] - crt['cmltv_rand_p_perc'], 2)
    crt = crt.drop(['rand_resp', 'cmltv_p', 'cmltv_n', ], axis=1)

    print('average response rate: ', avg_resp_rate)
    return crt


def create_table_json(df, col=None):
    """
    Creates tabular json output
    :param df: pandas DataFrame or Series
    :param col: String
    :return: Dictionary
    """
    if not col:
        df.to_json('input_data_artifacts/temp.json', orient='table')
        with open('artifacts/input_data_artifacts/temp.json') as json_file:
            data = json.load(json_file)
        return data
    df[col].value_counts().to_json('input_data_artifacts/temp.json', orient='table')
    with open('artifacts/input_data_artifacts/temp.json') as json_file:
        data = json.load(json_file)
    return data


def calculate_closure_rate(df):
    df_new = df.copy()
    df_new.index = df_new.CLAIMREPORTDATE
    df_new = df_new.groupby([pd.Grouper(freq='M'),
                             'CLAIM_STATUS']).agg({'STATE': 'count'}).rename(columns={'STATE': 'COUNT'})
    df_new = df_new.reset_index()
    df_new = df_new[df_new.CLAIMREPORTDATE > '2021-01-01']
    return create_table_json(df_new)


def create_json_for_summary_screen(df):
    summary_dict = {'Claims Initiated': len(df),
                    'Claims in Progress': len(df.CLAIM_STATUS[df.CLAIM_STATUS == 'open']),
                    'Claims Closed': len(df.CLAIM_STATUS[df.CLAIM_STATUS == 'closed']),
                    'Claims Paid': {'counts': df.PAID.value_counts().to_dict(),
                                    'Amount Paid': (df[df.PAID == 'Yes'])['APPRV_AMT_USD'].sum()},
                    'Average Claim Amount': df['CLAIM_AMT_USD'].sum() / len(df),
                    'Average Paid Amount':
                        df['APPRV_AMT_USD'][df.PAID == 'Yes'].sum() / len(df['APPRV_AMT_USD'][df.PAID == 'Yes']),
                    'Closure ratio': df.CLAIM_STATUS.value_counts()['closed'].sum() / len(df),
                    'Claims by Claim Type(Histogram)': create_table_json(df, 'CLAIM_TYPE'),
                    'Claims by state (Histogram)': create_table_json(df, 'STATE'),
                    'Claimed Amount by Claim Type(Histogram)': create_table_json(df.groupby(
                        'CLAIM_TYPE').agg({'APPRV_AMT_USD': 'sum'}).rename(columns={
                        'APPRV_AMT_USD': 'TOTAL APPROVED AMOUNT'})),
                    'Claimed amount by State(Histogram)':
                        create_table_json(df.groupby('STATE').agg({'APPRV_AMT_USD': 'sum'}
                                                                  ).rename(
                            columns={'APPRV_AMT_USD': 'TOTAL APPROVED AMOUNT'})),
                    'Claims by state (Table)': create_table_json(df.groupby('STATE').agg(
                        {'APPRV_AMT_USD': 'sum', 'CLAIM_STATUS': 'count'}
                    ).rename(columns={'CLAIM_STATUS': 'Claim count'})),
                    'Claim Closure Status last 12 Months (Monthly)': calculate_closure_rate(df)}
    if not os.path.isdir('artifacts/input_data_artifacts'):
        os.mkdir('artifacts/input_data_artifacts')
    with open('artifacts/input_data_artifacts/data_summary_params.json', 'w') as outfile:
        json.dump(summary_dict, outfile)


class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return super(NpEncoder, self).default(obj)


def create_json_for_Investigation_screen(df):
    summary_dict = dict()
    summary_dict['Claims Initiated'] = df.shape[0]
    summary_dict['Total Investigated'] = df.Investigated.value_counts()['Y']
    df_inv = df[df['Investigated'] == 'Y']
    summary_dict['Total Closed'] = df_inv.CLAIM_STATUS.value_counts()['closed']
    summary_dict['Paid Claims'] = df_inv.PAID.value_counts()['Yes']
    summary_dict['Rejected'] = df_inv.PAID.value_counts()['Rejected']
    summary_dict['Fraud Suspected Claims'] = df_inv.target.value_counts()[1]
    summary_dict['Investigation_rate'] = df_inv.shape[0]/df.shape[0]
    summary_dict['Fraud detection rate (in Investigated Claims)'] = df_inv.target.value_counts()[1]/df_inv.shape[0]
    df_closed = df_inv[df_inv.CLAIM_STATUS == 'closed']
    summary_dict['Payment status for Investigated claims'] = create_table_json(df_inv, 'PAID')
    summary_dict['Fraud suspected among closed claims'] = create_table_json(df_closed, 'target')
    df_new = df.groupby(['STATE', 'Investigated']).agg(count=('STATE', 'count'))
    df_new = df_new.reset_index()
    df_tot = df_new.groupby('STATE').agg({'count': 'sum'}).reset_index()
    df_tot = df_tot.rename(columns={'count': 'total_count'})
    df_new = df_new.merge(df_tot, on='STATE', how='left')
    df_new = df_new[df_new.Investigated == 'Y']
    df_new['Investigated status'] = df_new['count'] / df_new['total_count'] * 100
    summary_dict['Investigated status by states (%)'] = create_table_json(df_new[['STATE', 'Investigated status']])
    with open('artifacts/input_data_artifacts/data_summary_investigated.json', 'w') as outfile:
        json.dump(summary_dict, outfile, cls=NpEncoder)


def plot_lift_chart(df, show_plot=False):
    dec = ['Qt 1', 'Qt 2', 'Qt 3', 'Qt 4', 'Qt 5', 'Qt 6', 'Qt 7', 'Qt 8', 'Qt 9', 'Qt 10']
    mlift = df[['Positives', 'Negatives', 'lift']].copy()
    mlift.index = dec
    if show_plot:
        mlift.plot(kind='bar')
        plt.xlabel('Quantiles')
        plt.ylabel('Count')
        plt.show()
    return mlift

# X_train, X_test, y_train, y_test = malaysia_data_preprocess(f_path)
# xgb_classifier = XGBClassifier(learning_rate=0.05, n_estimators=1000, max_depth=5)
#
# xgb_classifier.fit(X_train, y_train)
# y_pred = xgb_classifier.predict(X_test)
# y_pred2 = xgb_classifier.predict_proba(X_test)
#
# target_names = ['N', 'Y']
#
# cr = classification_report(y_test, y_pred, target_names=target_names, output_dict=True)
#
# model_lift = lift(y_test, y_pred2, 5)
#
# dec = ['Qt 1', 'Qt 2', 'Qt 3', 'Qt 4', 'Qt 5']
# MLift = model_lift[['Positives', 'Negatives', 'cmltv_lift', 'KS']].copy()
# MLift.index = (dec)
#
# MLift[['Positives','Negatives']].plot(kind='bar', title='Positives & Negatives by model decile')
# MLift.plot(kind='bar')
