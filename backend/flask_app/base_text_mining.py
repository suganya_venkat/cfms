"""
Defines basic methods for text mining 
"""

import pandas as pd
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
import string


class TextClean:
    
    def __init__(self, ):
        
        self.lemmatizer = WordNetLemmatizer()
        
    def cleaner1(self, text):
        
        # Remove punctutaions
        punct_str = string.punctuation 
        t = text.translate(str.maketrans('', '', punct_str))

        # Word tokenized
        t = " ".join(word_tokenize(t))

        # lowercase
        t = str(t).strip().lower()

        # Lemmatization
        t = " ".join([self.lemmatizer.lemmatize(v) for v in word_tokenize(t)])

        return t
    
    def cleaner2(self, text):
        if pd.isna(text):
            return ""

        text = str(text).strip()
        text = str(text).replace('\t_', '').replace('\t', '')
        text = str(text).replace('none_', '').replace('none', '')
        text = str(text).replace('unknown_', '').replace('unknown', '')
        text = str(text).strip()

        return text
            
