sql_rule_variable_feats = """select a.Variable, a.Operator, a.Value, a.Date, a.Status, b.ModelFeatureName
                            from rules a 
                            left join variable_feature_map b 
                            on a.Variable=b.Variable 
                            """

sql_variable_feature_map = """select * from  variable_feature_map"""

sql_rule_bank = """select * from  rules"""

sql_delete_from_rule_bank = """ DELETE FROM rules WHERE Variable='{variable}' and Operator='{operator}' and Value='{value}' """

sql_delete_from_rule_bank_ruledesc = """ DELETE FROM RuleBank WHERE RuleDescription='{rule_desc}' or Variable='{rule_desc}' """

sql_rule_based_fetch_claims = """ select distinct claim_number from Claims where {rules_str} """

sql_rule_based_fetch_claims2 = """ select distinct IdValue as claim_number from Derived_Features where {rules_str} """

sql_referrals = """select * from referrals """

sql_delete_from_claims_weighted_scores = """delete from claims_weighted_score where claim_id in {claims_list}  """

sql_fetch_all_claims = """select * from Claims"""

sql_filtered_claims = """select * from Claims where CLAIM_TYPE in {CLAIM_TYPE}"""

sql_feat_inventory = """select * from Features_Inventory """

sql_rc_buckets = """select distinct a.ModelId, a.RunId, a.IdField, a.IdFieldValue, a.ModelFeatureId, b.max_imp,
c.ModelFeatureName as rc_bucket
  from model_indicators a
 join (select  ModelId, RunId, IdFieldValue, Max(ModelFeatureImportance) as max_imp from model_indicators 
        group by ModelId, RunId, IdFieldValue) b
 on a.IdFieldValue=b.IdFieldValue 
 and a.ModelFeatureImportance=b.max_imp
and a.ModelId=b.ModelId
and a.RunId=b.RunId 
join Features_Inventory c 
on a.ModelFeatureId=c.FeatureId
"""

sql_investigation_feedback = """ select a.IdField, a.IdValue, a.ScoredDate, a.ReferredDate,
a.InvestigatorId, a.InvestigationStatusId, b.StatusDescription as InvestigationStatusDescription,
 a.FinalStatus, d.StatusDescription as FinalStatusDescription, e.UserName, e.UserEmail, e.UserRoleId,
 f.UserRole, f.UserRoleDescription
 from Feedback a 
left join Investigation_Status b 
on a.InvestigationStatusId=b.StatusId
left join Assigned_Users c
on a.IdField=c.IdField 
and a.IdValue=c.IdValue
and a.InvestigatorId=c.UserId
left join Investigation_Status d
on a.FinalStatus=d.StatusId
left join Users e
on a.InvestigatorId=e.UserID
left join User_Roles f
on e.UserRoleId=f.UserRoleId
 """

sql_model_output = """select * from Model_Output """
sql_model_indicators = """select a.IdField, a.IdFieldValue, a.RunId, a.ModelId,
a.ModelFeatureId, a.ModelFeatureImportance, a.ModelFeatureValue, b.ModelFeatureName, b.FeatureBusinessName, b.RID as score_date
from Model_Indicators a
join Features_Inventory b on a.ModelFeatureId=b.FeatureID """

sql_weighted_scores = """select * from WeightedScores """

sql_model_score_grp_map = """ select * from Model_ScoreGroup """

sql_claim_query = """SELECT * FROM Claims WHERE claim_number = '{claim_id}' """

sql_comments = """select IdField, IdValue, UserId, Comments, RID as insert_ts from Comments 
                       where IdField='ID-1' and IdValue = '{claim_id}' """

sql_assigned_users = """ select a.UserId, a.IdField, a.IdValue, b.UserName, b.UserEmail, b.UserLocation
from Assigned_Users a 
left join Users b on a.UserId=b.UserID 
where IdField='ID-1' and IdValue='{claim_id}'
"""

sql_insert_comments_query = """INSERT INTO Comments (IdField, IdValue, UserId, Comments, RID) VALUES (?, ?, ?, ?, ?)"""

sql_supervised_model_output = """ select * from Model_Output where ModelId='XGB-8dd62108-b407-11ec-930a-062fcc6cb972' """
sql_unsupervised_model_output = """ select * from Model_Output where ModelId='ISO-a934c428-d29d-11ec-b2e3-107d1a28c31b' """

sql_cfms_rule_bank = """select a.RuleId, 
                                a.Variable,
                                a.Operator,
                                a.Value,
                                a.RuleDescription,
                                a.RID as RuleCreatedDate,
                                b.FeatureID,
                                b.ModelFeatureName,
                                b.FeatureBusinessName
                                from  RuleBank a 
                        left join Features_Inventory b 
                        on a.Variable=b.FeatureBusinessName """


sql_modelfeatnames_inventory = """select distinct ModelFeatureName, FeatureBusinessName from Features_Inventory """

sql_claim_number_idfield = "select FieldName from Id_Fields where IdField='ID-1' "

sql_sum_claimed_amount = """select sum(CLAIM_AMT_USD) as SumClaimedAmount from Claims where claim_number in {claim_list} """

sql_fetch_participants = """select * from Participants """

sql_fetch_all_related_participants = """ select distinct * from Participants where claim_number in
(select distinct claim_number from Participants where ParticipantId in 
    (select distinct ParticipantId from Participants where claim_number in ('{claim_number}')))"""

sql_unique_states = """select distinct STATE as STATE from Claims where STATE not in  ('Off', 'Chain') """
sql_unique_claim_types = """select distinct CLAIM_TYPE as CLAIM_TYPE from  Claims """ 
sql_unique_claim_status = """select distinct CLAIM_STATUS as CLAIM_STATUS from Claims """
sql_unique_feature_names = """select distinct FeatureDescription as FeatureDescription from Features_Inventory 
                                where FeatureID='FEAT-11' """

sql_derived_features = """select * from Derived_Features """

sql_queries_claims_amnt = """ select distinct claim_number, CLAIM_AMT_USD from Claims""" 

sql_model_val_in_sample = """ select * from Model_Val_InSample  """


