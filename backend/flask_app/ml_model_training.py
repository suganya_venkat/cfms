from xgboost import XGBClassifier
from pyod.models.iforest import IForest
from sklearn.model_selection import RandomizedSearchCV
from sklearn.metrics import roc_curve, auc
from sklearn.neighbors import KNeighborsClassifier
from sklearn import tree

import pandas as pd
import numpy as np

import ml_models
import evaluate
import pdb


class MlModelTrain:
    
    def __init__(self, model_type='xgb'):
        
        self.ml_model = None
        self.model_feat_list = None
        self.trn_lift = None
        self.val_lift = None
        self.oot_lift = None
        self.best_params = {}
        self.model_type = model_type

    def train_and_generate_model_results(self, df, model_feat_list, target_col='Target2', merge_trn_val=False, key_col='ClaimNumber'):
        
        self.model_feat_list = model_feat_list

        # Get Train, Val Test Split
        trnXY = df[df['SplitType']  == 'Train'].copy()
        trnXY.set_index(key_col, inplace=True)
        trnXY = trnXY[model_feat_list + [target_col]].fillna(0)
        trnX = trnXY[model_feat_list].fillna(0)
        trnY = trnXY[target_col].tolist()
        
        self.trnX = trnX
        self.trnY = trnY


        valXY = df[df['SplitType']  == 'Val'].copy()
        if valXY.shape[0] != 0:
            valXY.set_index(key_col, inplace=True)
            valXY = valXY[model_feat_list + [target_col]].fillna(0)
            valX = valXY[model_feat_list].fillna(0)
            valY = valXY[target_col].tolist()
        
            self.valX = valX
            self.valY = valY
        else:
            self.valX = pd.DataFrame(columns=[target_col])
            self.valY = []
            
        ootXY = df[df['SplitType']  == 'Oot'].copy()
        ootXY.set_index(key_col, inplace=True)
        ootXY = ootXY[model_feat_list + [target_col]].fillna(0)
        ootX = ootXY[model_feat_list].fillna(0)
        ootY = ootXY[target_col].tolist()
        
        self.ootX = ootX
        self.ootY = ootY
        
        print("== Data Stats ==")
        print("Train: ", trnXY.shape)
        print("Val: ", valXY.shape)
        print("Oot: ", ootXY.shape)
        print("Train Target Count {}, %: {}".format(trnXY[target_col].sum(), 100*trnXY[target_col].mean() ) )
        print("Val Target Count {}, %: {}".format(valXY[target_col].sum(), 100*valXY[target_col].mean() ) ) 
        print("Oot Target Count {}, %: {}".format(ootXY[target_col].sum(), 100*ootXY[target_col].mean() ) )
        
        if merge_trn_val:
            print("Train & Val will be merged for model building")
            trnXY = pd.concat([trnXY, valXY])
            trnX = pd.concat([trnX, valX])
            trnY = trnY + valY
            print("trnXY: ", trnXY.shape)
            print("trnY: ", trnY.__len__())
            print("Train Target %:", 100*(sum(trnY)/len(trnY)) )
            
        xgb_param_grid = {
        'silent': [1],
        'max_depth': [3, 4,5,6,7],
        'learning_rate': [0.001, 0.01, 0.1, 0.2, 0,3],
        'subsample': [0.5, 0.6, 0.7, 0.8, 0.9, 1.0],
        'colsample_bytree': [0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0],
        'colsample_bylevel': [0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0],
        'min_child_weight': [0.5, 1.0, 3.0, 5.0, 7.0, 10.0],
        'gamma': [0, 0.25, 0.5, 1.0],
        'reg_lambda': [0.1, 1.0, 5.0, 10.0, 50.0, 100.0],
        'n_estimators': [10, 50, 100, 500, 1000],
#         'scale_pos_weight': [20, 25, 30, ]
        }
        
        iso_param_grid = {'contamination': [0.1, 0.2, 0.4],
                        'bootstrap': [True, False],
                        'n_estimators': [10, 50, 100, 300, 500, 1000],
                         'max_features': [1.0]}
        
        knn_param_grid = {'n_neighbors': [5, 3, 7],
                         'weights': ['uniform', 'distance'],
                         'leaf_size': [20, 30, 40],
                          'p': [2],
                         }
        
        decision_tree_params = {'max_depth': [3, 5, 10],
                               'criterion': ['gini', 'entropy'],
                               }
        
        if self.model_type == 'xgb':
            clf = XGBClassifier(n_jobs=-1)
            randomized_search = RandomizedSearchCV(clf, 
                                                   xgb_param_grid, 
                                                   n_iter=20,
                                                   n_jobs=-1,
        #                                            verbose=3, 
                                                   cv=5,
                                                   scoring='roc_auc',
                                                   refit=False,
                                                   random_state=1729)
            print("Running Random Searcv CV")
            randomized_search.fit(trnX, trnY)
            
            self.best_params = randomized_search.best_params_
            print("Best Params: ", self.best_params)
            
            xgb_model, _, _  = ml_models.train_and_evaluate_xgb_model(train=trnXY.copy(),
                                                                      test=ootXY.copy(),
                                                                      features=model_feat_list, 
                                                                      target_col=target_col, 
                                                                      args_dict=self.best_params)
            self.ml_model = xgb_model
        
        elif self.model_type == 'iso':
            clf = IForest(n_jobs=-1)
            randomized_search = RandomizedSearchCV(clf, 
                                                   iso_param_grid, 
                                                   n_iter=10,
                                                   n_jobs=-1,
        #                                            verbose=3, 
                                                   cv=5,
                                                   scoring='roc_auc',
                                                   refit=False,
                                                   random_state=1729)
            print("Running Random Searcv CV")
            randomized_search.fit(trnX, trnY)
            self.best_params = randomized_search.best_params_
            print("Best Params: ", self.best_params)
            
            iso_model, _, _  = ml_models.train_and_evaluate_iso_model(train=trnXY.copy(),
                                                                      test=ootXY.copy(),
                                                                      features=model_feat_list, 
                                                                      target_col=target_col, 
                                                                      args_dict=self.best_params)
            self.ml_model = iso_model
            
        elif self.model_type == 'knn':
            
            clf = KNeighborsClassifier(n_jobs=-1)
            randomized_search = RandomizedSearchCV(clf, 
                                                   knn_param_grid, 
                                                   n_iter=10,
                                                   n_jobs=-1,
                                                   cv=5,
                                                   scoring='roc_auc',
                                                   refit=False,
                                                   random_state=1729)
            print("Running Random Searcv CV")
            randomized_search.fit(trnX, trnY)
            self.best_params = randomized_search.best_params_
            print("Best Params: ", self.best_params)
            
            knn_model, _, _  = ml_models.train_and_evaluate_knn_model(train=trnXY.copy(),
                                                                      test=ootXY.copy(),
                                                                      features=model_feat_list, 
                                                                      target_col=target_col, 
                                                                      args_dict=self.best_params)
            self.ml_model = knn_model
            
        elif self.model_type == 'dtree':
            clf = tree.DecisionTreeClassifier()
            randomized_search = RandomizedSearchCV(clf, 
                                                   decision_tree_params, 
                                                   n_iter=10,
                                                   n_jobs=-1,
        #                                            verbose=3, 
                                                   cv=5,
                                                   scoring='roc_auc',
                                                   refit=False,
                                                   random_state=1729)
            print("Running Random Searcv CV")
            randomized_search.fit(trnX, trnY)
            self.best_params = randomized_search.best_params_
            print("Best Params: ", self.best_params)
            
            dtree_model, _, _  = ml_models.train_and_evaluate_dtree_model(train=trnXY.copy(),
                                                                      test=ootXY.copy(),
                                                                      features=model_feat_list, 
                                                                      target_col=target_col, 
                                                                      args_dict=self.best_params)
            self.ml_model = dtree_model
            
        else:
            print("Unknown model type")

        ### Train Evaluate
        trn_pred = pd.DataFrame(self.ml_model.predict_proba(trnX))[1].tolist()
        self.trn_lift = evaluate.lift_chart(p=trn_pred, y=trnY)
        
        ### Val Evaluate
        if self.valX.shape[0] != 0:
            val_pred = pd.DataFrame(self.ml_model.predict_proba(valX))[1].tolist()
            self.val_lift =evaluate.lift_chart(p=val_pred, y=valY)
        
        ### Oot Evaluate
        oot_pred = pd.DataFrame(self.ml_model.predict_proba(ootX))[1].tolist()
        self.oot_lift = evaluate.lift_chart(p=oot_pred, y=ootY)
        
        # Train Gini
        fpr, tpr, thresholds = roc_curve(trnY, trn_pred, pos_label=1)
        
        self.trn_gini = 2*auc(fpr, tpr) - 1
        
        # Val Gini
        if self.valX.shape[0] != 0:
            fpr, tpr, thresholds = roc_curve(valY, val_pred, pos_label=1)
            self.val_gini = 2*auc(fpr, tpr) - 1
        
        # Oot Gini
        fpr, tpr, thresholds = roc_curve(ootY, oot_pred, pos_label=1)
        self.oot_gini = 2*auc(fpr, tpr) - 1
        
    def evaluate_using_trained_model(self, df):
        pass
    
    def compute_rfe_feature_importance(self, ):
        
        trnXY = self.trnX.copy()
        ootXY = self.ootX.copy()
        
        trnXY['Target'] = trnY        
        ootXY['Target'] = ootY
        
        self.feat_imp = evaluate.RFE_FeatImp(train=trnXY.copy(),
                          test=ootXY.copy(),
                          model=self.ml_model, 
                          features=self.model_feat_list,
                          target_col='Target',
                          model_params=self.best_params)
        
    def save_trained_model(self, path):
        assert self.ml_model is not None, "ml model not trained"
        
        trained_model_dict = {'ml_model': self.ml_model,
                              'model_feat_list': self.model_feat_list,
                              'trnX': self.trnX,
                               'trnY': self.trnY,
                              'ootX': self.ootX,
                              'ootY': self.ootY,
                              'trn_lift': self.trn_lift,
                              'oot_lift': self.oot_lift}
        utils.pdump(trained_model_data_dict, path)
        print("Trained model data saved at: ", path)
        
        
def format_rc_dist_df(rc_dist_df):
    prct_cols = [c for c in rc_dist_df.columns if "%" in c]
    frmt_dct = {}
    for col in prct_cols:
        frmt_dct[col] = '{:.2%}'
    
    return rc_dist_df.style.format(frmt_dct)
        
def generate_reason_code_distribution(all_reasons, reason_codes_df, key_col='ClaimNumber'):
    result = pd.DataFrame({"Reason Codes": all_reasons})
    total_claims = reason_codes_df[key_col].nunique()
    reason_codes_df.columns = [key_col] + ['reason_code{}'.format(i+1) for i in range(reason_codes_df.shape[1] - 1)]
    for i in range(5):
        rc_count_df = reason_codes_df.groupby(['reason_code{}'.format(i+1)])[key_col].nunique().reset_index()
        clm_cnt_col = 'ClaimCount_TopReason{}'.format(i+1) 
        rc_count_df.columns = ['TopRC', clm_cnt_col]
        rc_count_df['% Claims Top Reason{}'.format(i+1)] = rc_count_df[clm_cnt_col] / total_claims
        
        result = result.merge(rc_count_df[['TopRC', '% Claims Top Reason{}'.format(i+1), clm_cnt_col ]],
                              left_on=['Reason Codes'], 
                              right_on=['TopRC'],
                              how='left'
                             )
        result['% Claims Top Reason{}'.format(i+1)] = result['% Claims Top Reason{}'.format(i+1)].fillna(0)
        result['ClaimCount_TopReason{}'.format(i+1)] = result['ClaimCount_TopReason{}'.format(i+1)].fillna(0)
        del result['TopRC']
        
#         result = format_rc_dist_df(result)
        
    return result
        
        