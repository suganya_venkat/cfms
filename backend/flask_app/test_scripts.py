import json
from forecasting.forecasting_utils import (time_series_data_retrieval_full,
                                           time_series_data_retrieval_statewise,
                                           get_connection,
                                           apply_forecasting_algorithm_clm_cnt,
                                           compute_forecasting_aggregates_clm_cnt)


def get_forecasting_data(input_json):
    connection = get_connection()
    if input_json['state'] == 'all':
        ts, df = time_series_data_retrieval_full(connection)
        forecast, ts = apply_forecasting_algorithm_clm_cnt(ts)
        forecast_json = compute_forecasting_aggregates_clm_cnt(forecast, df, ts)
        return json.dumps(forecast_json)
    else:
        ts, df = time_series_data_retrieval_statewise(connection, input_json['state'])
        forecast, ts = apply_forecasting_algorithm_clm_cnt(ts)
        forecast_json = compute_forecasting_aggregates_clm_cnt(forecast, df, ts)
        return json.dumps(forecast_json)


if __name__ == '__main__':
    dic = {"state": "penang"}
    get_forecasting_data(dic)
