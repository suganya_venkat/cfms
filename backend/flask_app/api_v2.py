import os
import sys
import time
from datetime import datetime
from flask import Flask
from flask_cors import CORS, cross_origin
import sqlite3
import yaml
import pandas as pd
from flask import jsonify, request
import json

import utils
import CreateJsons
import ReportingViewV2 as ReportingView
import RuleBankHandler
import AlertManager
import ConfigurationsPage
import GraphAnalysisDynamic
from forecasting.forecasting_utils import (time_series_data_retrieval,
                                           get_connection, apply_forecasting_algorithmn_amounts,
                                           apply_forecasting_algorithm_clm_cnt,
                                           compute_forecasting_aggregates_clm_cnt,
                                           compute_forecasting_aggregates_amounts,
                                           get_doc_db_connection)
from ocr_codes.ocr_rules import CreateRules
import ModelInferenceOutput
import sql_queries
import VariableSummary

app = Flask(__name__)
CORS(app)

app_config_dict = utils.load_app_config_dict()

# Instantiate Classes
reporting_overview_obj = ReportingView.ReportingView()
rule_bank_obj = RuleBankHandler.RuleBank()
# graph_network_obj = GraphAnalysis.Subgraph()
model_infer_out_obj = ModelInferenceOutput.ModelInferenceOutput()
var_summary_obj = VariableSummary.VarSummary()

@app.route('/time')
def get_current_time():
    return {'time': datetime.now().strftime('%Y-%m-%d %H:%M:%S')}


@app.route('/reporting_overview_claims', methods=['POST'])
def get_reporting_overview_claims():
    """
    Input json request: {"reporting_view_filters" : 
                          {"CLAIMREPORTDATE" : [{"from":  "2016-06-17 00:00:00", 
                                              "to": "2021-12-11 00:00:00"}],
                          "STATE" : ["selangor", "perak"], 
                          "CLAIM_TYPE": ["self - accident", "Animal Collision"], 
                          "CLAIM_AMT_USD": ["1-1000", "1000-10000", "10000+"}] 
                        }
                        }
    """
    filters_json = request.json
    print("In get_reporting_overview_claims::")
    print("Input Request: ", filters_json)
    
    sub_df = reporting_overview_obj.load_filtered_subset(filters_json)
    print("shape sub_df: ", sub_df.shape)
    summary_dict = reporting_overview_obj.claims_overview(df=sub_df)
#     print("summary_dict: ", summary_dict)
    result = json.dumps(summary_dict, indent=4)
    return result


@app.route('/reporting_overview_investigation', methods=['POST'])
def get_reporting_overview_investigation():
    """
    Input json request: {"reporting_view_filters" : 
                          {"CLAIMREPORTDATE" : [{"from":  "2016-06-17 00:00:00", 
                                              "to": "2021-12-11 00:00:00"}],
                          "STATE" : ["selangor", "perak"], 
                          "CLAIM_TYPE": ["self - accident", "Animal Collision"], 
                          "CLAIM_AMT_USD": ["1-1000", "1000-10000", "10000+"}] 
                        }
                        }
    """
    filters_json = request.json
    print("In get_reporting_overview_claims::")
    print("Input Request: ", filters_json)
    
    sub_df = reporting_overview_obj.load_filtered_subset(filters_json)
    print("shape sub_df: ", sub_df.shape)
    investigation_summary_dict = reporting_overview_obj.create_json_for_Investigation_screen(df=sub_df)
    print("Investigation Summary Dict: ", investigation_summary_dict)
    result = json.dumps(investigation_summary_dict, indent=4)
    return result

@app.route('/add_rules', methods=['POST'])
def add_rules():
    """
    Input json request:
        [
        {"rule1": {'Variable': 'Claims Amount',
        'Operator': '>=',
        'Value': 2500}}, 
        {"rule2": {'Variable': 'Claims Age',
        'Operator': '>=',
        'Value': 20}}, 
        ]
    
    """
    rules_json = request.json
    print("Input :", rules_json)

    response = rule_bank_obj.add_rules(rules_json)
    return response

@app.route('/delete_rules', methods=['POST'])
def delete_rules():
    rules_json = request.json
    print("Input :", rules_json)

    response = rule_bank_obj.delete_from_rule_bank(rule_description=rules_json['rule_description'])
    return response

@app.route('/rule_bank', methods=['GET'])
def get_rule_bank():
    response = json.dumps({"data": list(rule_bank_obj.get_rule_bank_stats().values())})
    return response

@app.route('/alert_management_frdtyp', methods=['GET'])
def get_fraud_type_alerts():
    response_dict = AlertManager.create_alert_page_data()
    return json.dumps(response_dict)

@app.route('/claim_level_data', methods=['POST'])
def get_claim_level_data():
    input_json = request.json
    print("Input :", input_json)
    response = AlertManager.create_claim_level_data(claim_num=input_json['claim_number'])
    response = json.dumps(response)
    return response

@app.route('/add_comments', methods=['POST'])
def add_comments():
    input_json = request.json
    print("Input :", input_json)
    response = AlertManager.add_comments_to_claims(input_json['user_id'],
                                                      input_json['claim_number'],
                                                      input_json['comment'])
    return json.dumps(response)


@app.route('/apply_strategy', methods=['POST'])
def apply_strategy():
    input_json = request.json
    print("Input :", input_json)
    input_json = pd.DataFrame(input_json).set_index('Type').to_dict()
    response = ConfigurationsPage.create_score_group_based_on_model_weights(float(input_json['Weight']['Supervised']),
                                                                  float(input_json['Weight']['anomaly']))
    
#     response = json.dumps({"response": "SUCCESS"})
    return json.dumps(response)

@app.route('/get_forecast', methods=['POST'])
def get_forecasting_data():
    input_json = request.json
    connection = get_connection()
    smoothed_ts_clm_cnt, smoothed_ts_appr_amt, smoothed_ts_claim_amt = time_series_data_retrieval(connection,
                                                                                                  input_json['state'])
    forecast, ts_clm_cnt = apply_forecasting_algorithm_clm_cnt(smoothed_ts_clm_cnt, weeks=input_json['weeks'])
    pred_apprvd, pred_clmd, ts_appr_amt, ts_clm_amt = apply_forecasting_algorithmn_amounts(smoothed_ts_appr_amt,
                                                                                           smoothed_ts_claim_amt,
                                                                                           input_json['weeks'])

    forecast_json_clm_amts = compute_forecasting_aggregates_amounts(pred_apprvd, pred_clmd, ts_appr_amt, ts_clm_amt)
    forecast_json_clm_cnt = compute_forecasting_aggregates_clm_cnt(forecast, ts_clm_cnt)
    forecast_json_clm_cnt.update(forecast_json_clm_amts)
    return json.dumps(forecast_json_clm_cnt)

@app.route('/graph_view', methods=['POST'])
def get_graph_json():
    input_json = request.json
    print("In get_graph_json")
    print("Input Request: ", input_json)
    graph_json = GraphAnalysisDynamic.create_graph_json(input_json['claim_number'])
    return json.dumps([graph_json])

@app.route('/model_infer_output', methods=['POST'])
def get_model_inference_output():
    input_json = request.json
    print("In get_model_inference_output")
    print("Input Request: ", input_json)
    result = model_infer_out_obj.get_model_inference(claim_number=input_json['claim_number'], 
                                 run_id=input_json['run_id'],
                                 model_id=input_json['model_id'])
    return json.dumps(result)


@app.route('/reporting_overview_investigation_v2', methods=['POST'])
def get_reporting_overview_investigation_v2():
    """
    Input json request: {"reporting_view_filters" : 
                          {"CLAIMREPORTDATE" : [{"from":  "2016-06-17 00:00:00", 
                                              "to": "2021-12-11 00:00:00"}],
                          "STATE" : ["selangor", "perak"], 
                          "CLAIM_TYPE": ["self - accident", "Animal Collision"], 
                          "CLAIM_AMT_USD": ["1-1000", "1000-10000", "10000+"}] 
                        }
                        }
    """
    filters_json = request.json
    print("In get_reporting_overview_claims::")
    print("Input Request: ", filters_json)
    
    sub_df = reporting_overview_obj.load_filtered_subset(filters_json)
    print("shape sub_df: ", sub_df.shape)
    investigation_summary_dict = reporting_overview_obj.create_json_for_Investigation_screen_v2(df=sub_df)
    print("Investigation Summary Dict: ", investigation_summary_dict)
    result = json.dumps(investigation_summary_dict, cls=utils.NpEncoder)
    return result

@app.route("/unique_values", methods=["GET"])
def get_unique_values():
    """
    Get all unique_values for state, Claim Types etc
    for filter drop down
    """
    app_config_dict = utils.load_app_config_dict()
    driver_con, _ = utils.connect2sqlite3(app_config_dict['DRIVER_DB_PATH'])
    cfms_con, _ = utils.connect2sqlite3(app_config_dict['CFMS_DB_PATH'])
    unique_states = pd.read_sql(sql_queries.sql_unique_states, driver_con)['STATE'].tolist()
    unique_claim_types = pd.read_sql(sql_queries.sql_unique_claim_types, driver_con)['CLAIM_TYPE'].tolist()
    unique_claim_status = pd.read_sql(sql_queries.sql_unique_claim_status, driver_con)['CLAIM_STATUS'].tolist()
#     unique_features = pd.read_sql(sql_queries.sql_unique_feature_names, cfms_con)['FeatureDescription'].tolist()
    unique_features = ['Has Report Lag', 'Claims Amount', 'Loss happened within a month of policy effective date',
                      'Loss happened within a month of policy expiry'
                      ]
    
    result = {"States": ["all"] + unique_states,
             "Claim_Types": ["all"] + unique_claim_types,
              "Claim_Status": ["all"] + unique_claim_status,
              "VariableSummaryFeatures": unique_features,
              "ClaimAmountRange": ["all", "0-1000", "1000-10000", "10000+"]
             }
    
    return json.dumps(result)


@app.route('/var_summary', methods=['POST'])
def get_var_summary():
    input_json = request.json
    print("In get_var_summary")
    print("Input Request: ", input_json)
    result = var_summary_obj.get_summary_json(feature=input_json['feature'])
    return result


@app.route('/doc_data', methods=['GET'])
def get_doc_inference_and_data():

    """
    Input json request: None
    """
    connection = get_doc_db_connection()
    create_rules = CreateRules(clm_number='V4302488', conn=connection)
    create_rules.apply_prod_rules_pipeline()
    return json.dumps(create_rules.rules)
    

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0',port=5000)
    #app.run(debug=True, port=5000)
