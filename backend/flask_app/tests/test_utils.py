import sys
sys.path.append("/home/bridgei2i/CFMS/cfms_dev/cfms/backend/flask_app")

import pandas as pd
import os


def load_breast_cancer_data(datafolder="/home/bridgei2i/CFMS/cfms_dev/cfms/backend/flask_app/databases/toy_data/breast_cancer/"):
    trn_df = pd.read_csv(os.path.join(datafolder, 'train.csv'))
    tst_df = pd.read_csv(os.path.join(datafolder, 'test.csv'))
    return trn_df, tst_df
    