import sys
sys.path.append("/home/bridgei2i/CFMS/cfms_dev/cfms/backend/flask_app")
sys.path.append("/home/bridgei2i/CFMS/cfms_dev/cfms/backend/flask_app/tests")

from xgboost import XGBClassifier
import shap
import pandas as pd

import base_explainer
import utils

import test_utils

app_config_dict = utils.load_app_config_dict(config_filepath='/home/bridgei2i/CFMS/cfms/backend/flask_app/configs/app_config.yaml')

trn_df, tst_df = test_utils.load_breast_cancer_data()

model_features, target_col = trn_df.columns.tolist()[:-1], trn_df.columns.tolist()[-1]

X_train, y_train = trn_df[model_features].fillna(0), trn_df[target_col].tolist()
X_test, y_test = tst_df[model_features].fillna(0), tst_df[target_col].tolist()

# Build Baseline Model
model = XGBClassifier()
model.fit(X_train, y_train)

test_model_data_df = X_test.copy()

shap_explainer = shap.TreeExplainer(model, random_state=12345)
test_model_data_shap_df = shap_explainer.shap_values(test_model_data_df)

# Business Rules : Specific to each feature, shows reason code only when the underlying 
# Value of feature satisfy the condition given as Range
business_rules_df = pd.DataFrame({'Features': model_features,
                                   'Description': model_features,
                                   'Range': len(model_features)*["!=0"],
                                   'Final Points': len(model_features)*[1]})


feat_imp_obj = base_explainer.FeatureImportance(model=model, 
                                 model_features=model_features,
                                 model_type='tree-based')

test_data_feat_imp_df = feat_imp_obj.compute_shap_feature_importance(X=test_model_data_df)

explainer_obj = base_explainer.Explainer(model_data=test_model_data_df, 
                                         feature_imp_df=test_data_feat_imp_df,
                                         model_features=model_features, 
                                         business_rules=business_rules_df, 
                                         topn=5, 
                                         out_col_pref='top_')

result = explainer_obj.apply()
print(result)

