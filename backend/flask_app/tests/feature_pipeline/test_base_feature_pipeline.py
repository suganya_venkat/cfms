import pandas as pd
import numpy as np
import base_feature_pipeline
import base_transforms

df = pd.DataFrame({"ClaimNumber": ['c100', 'c101', 'c102'],
                        "ReportDate": [pd.to_datetime('2022-03-28'), 
                        pd.to_datetime('2022-02-28'), 
                        pd.to_datetime('2022-01-28')],
                        "LossDate": [pd.to_datetime('2022-02-28'), 
                        pd.to_datetime('2022-01-25'), 
                        pd.to_datetime('2021-12-28')],
                        "PolicyEffective": [pd.to_datetime('2022-03-28'), 
                        pd.to_datetime('2022-02-28'), 
                        pd.to_datetime('2022-01-28')],
                        "PolicyExpiry": [pd.to_datetime('2022-02-28'), 
                        pd.to_datetime('2022-01-25'), 
                        pd.to_datetime('2021-09-28')],
                        'ClaimAmount': [100, 200, 300],
                        'PaidAmount': [50, 100, 200]
                        })
    
    
        
pipe = base_feature_pipeline.FeaturePipeline()

pipe.add_transformation(on_cols=[('LossDate', 'ReportDate'),
                                ('LossDate', 'PolicyExpiry')
                                ], 
                        transformation_f=base_transforms.base_transforms_days_between,
                        transformation_args=None,
                         names=['Report_Lag',
                                'Diff_LossPolicyExp'] 
                         )

pipe.add_transformation(on_cols=[('Diff_LossPolicyExp', ),
                                ], 
                        transformation_f=base_transforms.base_transforms_range_indicator,
                        transformation_args={"lower":-np.inf,
                                             "upper":0,
                                             "lower_include": False,
                                             "upper_include": False},
                         names=['Diff_LossPolicyExp_Gt1'] 
                         )

result = pipe.apply_transformations(df, index_cols=['ClaimNumber'])
print(result)