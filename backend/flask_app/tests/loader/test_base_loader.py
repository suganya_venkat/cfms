import sys
sys.path.append("/home/bridgei2i/CFMS/cfms_dev/cfms/backend/flask_app")
sys.path.append("/home/bridgei2i/CFMS/cfms_dev/cfms/backend/flask_app/tests")
import pandas as pd
import utils
import base_loader
import sql_queries
import pdb

app_config_dict = utils.load_app_config_dict(config_filepath='/home/bridgei2i/CFMS/cfms_dev/cfms/backend/flask_app/configs/app_config.yaml')

driver_con, driver_cursor = utils.connect2sqlite3(app_config_dict['DRIVER_DB_PATH'])

sql_where_dict = {"sql_where_dict": {"CLAIM_TYPE": ['self - accident'] , 
                                    }}

result = base_loader.base_loader_sql_to_df(query=sql_queries.sql_filtered_claims,
                                            conn=driver_con,
                                            app_config_dict=app_config_dict,
                                            logger=None, 
                                            **sql_where_dict
                                            )



print(result)