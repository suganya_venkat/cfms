"""
Methods to track/validate model performance
"""

import pandas as pd
import numpy as np
from sklearn.metrics import roc_curve, auc

import ml_models
import pdb


def compute_bivar(y, features, df):
    """
    y: Target Column Name
    features: Features List 
    df: feature dataframe
    """
    df = df.copy()
    iv_df1 = pd.DataFrame()
    for i in features:
        if i != y:
            print(i)
            
            if df[i].dtypes == 'object':
                temp = df.groupby(df[i])[y].count()
                temp1 = df.groupby(df[i])[y].sum()
            elif (df[i].nunique() < 10):
#                 df[i] = [1 if j == 1 else 0 for j in df[i].values]
                df[i] = df[i].fillna(0)
                temp = df.groupby(df[i])[y].count()
                temp1 = df.groupby(df[i])[y].sum()
            else:
                df[i] = df[i].fillna(0)
                min_dfi = df[i].min() - 0.001
                max_dfi = df[i].max() 
                
                check, bins = pd.qcut(df[i], 10, duplicates='drop', retbins=True)
                ncats = check.nunique()
                
                random_cuts = pd.Series(df[i].unique()).sample(5).tolist()
                
                cutbins = sorted(list(set(random_cuts + [min_dfi, max_dfi])))
                
                if (ncats <= 3) :
                    temp = df.groupby(pd.cut(df[i], cutbins, duplicates='drop'))[y].count()
                    temp1 = df.groupby(pd.cut(df[i], cutbins, duplicates='drop'))[y].sum()
                else:
                    temp = df.groupby(pd.qcut(df[i], 10, duplicates='drop'))[y].count()
                    temp1 = df.groupby(pd.qcut(df[i], 10, duplicates='drop'))[y].sum()
                
            iv_df = pd.DataFrame({'Levels': temp.index, 'total': temp.values})
            if df[i].dtypes == 'object':
                iv_df['keys'] = i
            else:
                iv_df['keys'] = 'bin_' + i
            iv_df['perc_in_feature'] = (iv_df['total'] / iv_df['total'].sum()) * 100
            iv_df['Target_counts'] = temp1.values
            iv_df['non_Target_counts'] = iv_df['total'] - iv_df['Target_counts']
            iv_df['Target_count_percent_in_feature'] = (iv_df['Target_counts'] / iv_df['Target_counts'].sum()) * 100
            iv_df['Target_perc'] = (iv_df['Target_counts'] / (iv_df['non_Target_counts'] + iv_df['Target_counts'])) * 100
            iv_df['non_Target_count_percent_in_feature'] = (iv_df['non_Target_counts'] / iv_df[
                'non_Target_counts'].sum()) * 100
            iv_df['WOE'] = np.log(iv_df['Target_count_percent_in_feature'] / iv_df['non_Target_count_percent_in_feature'])
            iv_df['IV'] = (iv_df['Target_count_percent_in_feature'] - iv_df['non_Target_count_percent_in_feature']) / \
                          iv_df['WOE']
            iv_df = iv_df[
                ['keys', 'Levels', 'total', 'Target_counts', 'non_Target_counts', 'Target_perc', 'perc_in_feature',
                 'Target_count_percent_in_feature'
                    , 'non_Target_count_percent_in_feature', 'WOE', 'IV']]
            iv_df1 = pd.concat([iv_df1, iv_df], axis=0)
    return iv_df1


def feat_analysis(y, feature, df, cutbins=None):
    """
    y: Target Column Name
    features: Features List 
    df: feature dataframe
    """
    df = df.copy()
    iv_df1 = pd.DataFrame()
    i = feature
    
    if df[i].dtypes == 'object':
        temp = df.groupby(df[i])[y].count()
        temp1 = df.groupby(df[i])[y].sum()
    elif (df[i].nunique() < 10):
#                 df[i] = [1 if j == 1 else 0 for j in df[i].values]
        df[i] = df[i].fillna(0)
        temp = df.groupby(df[i])[y].count()
        temp1 = df.groupby(df[i])[y].sum()
    else:
        df[i] = df[i].fillna(0)
        min_dfi = df[i].min() - 0.001
        max_dfi = df[i].max() 

        check, bins = pd.qcut(df[i], 10, duplicates='drop', retbins=True)
        ncats = check.nunique()

        random_cuts = pd.Series(df[i].unique()).sample(5).tolist()

        if cutbins is None:
            cutbins = sorted(list(set(random_cuts + [min_dfi, max_dfi])))
        else:
            cutbins = cutbins

        if (ncats <= 3) or len(cutbins) :
            temp = df.groupby(pd.cut(df[i], cutbins, duplicates='drop'))[y].count()
            temp1 = df.groupby(pd.cut(df[i], cutbins, duplicates='drop'))[y].sum()
        else:
            temp = df.groupby(pd.qcut(df[i], 10, duplicates='drop'))[y].count()
            temp1 = df.groupby(pd.qcut(df[i], 10, duplicates='drop'))[y].sum()

    iv_df = pd.DataFrame({'Levels': temp.index, 'total': temp.values})
    if df[i].dtypes == 'object':
        iv_df['keys'] = i
    else:
        iv_df['keys'] = 'bin_' + i
    iv_df['perc_in_feature'] = (iv_df['total'] / iv_df['total'].sum()) * 100
    iv_df['Target_counts'] = temp1.values
    iv_df['non_Target_counts'] = iv_df['total'] - iv_df['Target_counts']
    iv_df['Target_count_percent_in_feature'] = (iv_df['Target_counts'] / iv_df['Target_counts'].sum()) * 100
    iv_df['Target_perc'] = (iv_df['Target_counts'] / (iv_df['non_Target_counts'] + iv_df['Target_counts'])) * 100
    iv_df['non_Target_count_percent_in_feature'] = (iv_df['non_Target_counts'] / iv_df[
        'non_Target_counts'].sum()) * 100
    iv_df['WOE'] = np.log(iv_df['Target_count_percent_in_feature'] / iv_df['non_Target_count_percent_in_feature'])
    iv_df['IV'] = (iv_df['Target_count_percent_in_feature'] - iv_df['non_Target_count_percent_in_feature']) / \
                  iv_df['WOE']
    iv_df = iv_df[
        ['keys', 'Levels', 'total', 'Target_counts', 'non_Target_counts', 'Target_perc', 'perc_in_feature',
         'Target_count_percent_in_feature'
            , 'non_Target_count_percent_in_feature', 'WOE', 'IV']]
    iv_df1 = pd.concat([iv_df1, iv_df], axis=0)
    return iv_df1



def lift_chart(p, y, buckets=20):
    """
    p: Model Scores/Probabilities
    y: Target
    """
    if isinstance(p, pd.Series):
        df_data = pd.DataFrame({"PredProb": p.tolist(), "Actual": y.tolist()})
    elif isinstance(p, list):
        df_data = pd.DataFrame({"PredProb": p, "Actual": y})
    else:
        raise Exception("Expected list or Series like input for p & y")
        
    df_data.sort_values(by=['PredProb'], ascending=False, inplace=True)
    df_data.reset_index(drop=True, inplace=True)
    df_data['Decile'] = pd.qcut(df_data.index, buckets, labels=False)
    
    liftchart = pd.DataFrame()

    grouped = df_data.groupby('Decile', as_index=False)

    liftchart['MaxScore'] = grouped.max().PredProb
    liftchart['MinScore'] = grouped.min().PredProb
    liftchart['NumEvents'] = grouped.sum().Actual
    liftchart['TotalRecs'] = grouped.count().Actual

    liftchart = liftchart.sort_values(by='MinScore', ascending=False).reset_index(drop=True)
    liftchart['HitRate'] = liftchart['NumEvents'] / liftchart['TotalRecs']
    liftchart['PctEvents'] = liftchart['NumEvents'] / liftchart['NumEvents'].sum()

    liftchart['PctRecs'] = liftchart['TotalRecs'] / liftchart['TotalRecs'].sum()
    liftchart['CumRecs'] = liftchart['PctRecs'].cumsum()
    liftchart['Lift'] = liftchart['HitRate'] / (liftchart['NumEvents'].sum() / liftchart['TotalRecs'].sum())

    return liftchart


def RFE_FeatImp(train, test, model, features, target_col='fraud', model_params={}):
    '''
    returns df of variables sorted by change in lift on test data
    when variable is removed from the model
    '''
    ref_imp = []
    allvars = features
    
    train = train.fillna(0)
    test = test.fillna(0)
     
    if 'XGBClassifier' in str(model):
        base_model, base_lift_test, base_lift_train = ml_models.train_and_evaluate_xgb_model(train=train.fillna(0), 
                                                                                   test=test.fillna(0), 
                                                                                   features=features,
                                                                                   target_col=target_col,
                                                                                    args_dict=model_params)
    elif "IForest" in str(model):
        base_model, base_lift_test, base_lift_train = ml_models.train_and_evaluate_iso_model(train=train.fillna(0), 
                                                                                           test=test.fillna(0), 
                                                                                           features=features,
                                                                                           target_col=target_col,
                                                                                           )
    elif "decisiontree" in str(model).lower():
        base_model, base_lift_test, base_lift_train = ml_models.train_and_evaluate_dtree_model(train=train.fillna(0), 
                                                                                           test=test.fillna(0), 
                                                                                           features=features,
                                                                                           target_col=target_col,
                                                                                           )
        
    trn_base_model_pred = pd.DataFrame(base_model.predict_proba(train[features].copy()))[1].tolist()
    tst_base_model_pred = pd.DataFrame(base_model.predict_proba(test[features].copy()))[1].tolist()
    trn_y = train[target_col].tolist()
    tst_y = test[target_col].tolist()
    
    trn_gini_base = get_gini_score(pred=trn_base_model_pred, y=trn_y)
    tst_gini_base = get_gini_score(pred=tst_base_model_pred, y=tst_y)
        
    base_test_lift = base_lift_test['Lift'][0] #calculate lift of full model - Test
    base_train_lift = base_lift_train['Lift'][0] #calculate lift of full model - Train
    base_delta = base_train_lift - base_test_lift
    
    base_delta_gini = trn_gini_base - tst_gini_base
    
    print("=== Base Model Stats === ")
    print("Train Lift in Top %tile: ", base_train_lift)
    print("Test Lift in Top %tile: ", base_test_lift)
    base_diff_trn_tst_lift = base_train_lift - base_test_lift
    print("Diff Bwn Train&Test Lift: ", base_diff_trn_tst_lift)
    print("Train Gini Score: ", trn_gini_base)
    print("Test Gini Score: ", tst_gini_base)
    base_diff_trn_tst_gini = trn_gini_base - tst_gini_base 
    print("Diff Bwn Train&Test Gini: ", base_diff_trn_tst_gini)
    
    print("=======================\n")
    
    for index, i in enumerate(allvars): 
        #if (index + 1) % 7 == 0:
            #print(DT.now(), 'Testing var #', str(index + 1))
        print("= Feature Eliminated: ", i)
     
        features2 = [f for f in features if f != i]
        
        if 'XGBClassifier' in str(model):
            model2, lift_chart_test2, lift_chart_train2 = ml_models.train_and_evaluate_xgb_model(train=train, 
                                                                                   test=test, 
                                                                                   features=features2,
                                                                                   target_col=target_col,
                                                                                    args_dict=model_params)
        elif "IForest" in str(model):
            model2, lift_chart_test2, lift_chart_train2 = ml_models.train_and_evaluate_iso_model(train=train, 
                                                                                       test=test, 
                                                                                       features=features2,
                                                                                       target_col=target_col)
        elif "decisiontree" in str(model).lower():
            model2, lift_chart_test2, lift_chart_train2 = ml_models.train_and_evaluate_dtree_model(train=train, 
                                                                                       test=test, 
                                                                                       features=features2,
                                                                                       target_col=target_col)
        
        
        test_lift2 = lift_chart_test2['Lift'][0]
        train_lift2 = lift_chart_train2['Lift'][0]
        
        trn_new_model_pred = pd.DataFrame(model2.predict_proba(train[features2].copy()))[1].tolist()
        tst_new_model_pred = pd.DataFrame(model2.predict_proba(test[features2].copy()))[1].tolist()
        trn_gini_new = get_gini_score(pred=trn_new_model_pred, y=trn_y)
        tst_gini_new = get_gini_score(pred=tst_new_model_pred, y=tst_y)
        
        print("Train Lift in Top %tile: ", train_lift2)
        print("Test Lift in Top %tile: ", test_lift2)
        diff_trn_tst_lift = train_lift2 - test_lift2  
        print("Diff Train & Test Lift: ", diff_trn_tst_lift)
        tst_lift_change = base_test_lift - test_lift2 
        trn_lift_change = base_train_lift - train_lift2
        print("Train Lift Change: ", trn_lift_change)
        print("Test Lift Change: ", tst_lift_change)
        print("Train Gini Score: ", trn_gini_new)
        print("Test Gini Score: ", tst_gini_new)
        tst_gini_change = tst_gini_base - tst_gini_new
        trn_gini_change = trn_gini_base - trn_gini_new
        print("Train Gini Change: ", trn_gini_change)
        print("Test Gini Change: ", tst_gini_change)
        diff_trn_tst_gini = trn_gini_change - tst_gini_change  

                
        
        ref_imp.append([i, base_train_lift, base_test_lift,  trn_lift_change, tst_lift_change,  trn_gini_change, tst_gini_change,
                        diff_trn_tst_lift, diff_trn_tst_gini, train_lift2, test_lift2, trn_gini_new, tst_gini_new
                       ])
        
        print("\n")
        
    ref_imp = pd.DataFrame(ref_imp, columns = ['Variable_Removed',
                                               'TrnLiftOrig',
                                               'TstLiftOrig',
                                               'TrnLiftChg',
                                               'TstLiftChg',
                                               'TrnGiniChg',
                                               'TstGiniChg',
                                               'Sub_DiffTrainTestLift',
                                               'Sub_DiffTrainTestGini',
                                               'Sub_TrnLift',
                                               'Sub_TstLift',
                                               'Sub_TrnGini',
                                               'Sub_TstGini'
                                              ])

#     ref_imp['Importance'] = (ref_imp['LiftChg'].abs() - ref_imp['LiftChg'].abs().min()
#                              ) / (ref_imp['LiftChg'].abs().max() - ref_imp['LiftChg'].abs().min())
    
    ref_imp['AbsTstLiftChg'] = ref_imp['TstLiftChg'].abs()
    result = ref_imp.sort_values(by = 'AbsTstLiftChg', ascending=False).reset_index(drop=True)
    del result['AbsTstLiftChg']
    
    return result

def comparative_feature_analysis(y, features, trn_df, tst_df, y_hat=None):
    """
    y: Target Column Name
    features: Features List 
    df: feature dataframe
    """
    trn_df = trn_df.copy()
    tst_df = tst_df.copy()
    
    iv_df1 = pd.DataFrame()
    tst_temp2 = pd.DataFrame()
    for i in features:
        if i != y:
            print(i)
            trn_df[i] = trn_df[i].fillna(0)
            tst_df[i] = tst_df[i].fillna(0)
                
            if (trn_df[i].fillna(0).nunique() < 10):                
                cutbins = sorted(list(set(trn_df[i].unique().tolist() + [-np.inf, np.inf])))
                temp = trn_df.groupby(pd.cut(trn_df[i], cutbins, duplicates='drop'))[y].count()
                temp1 = trn_df.groupby(pd.cut(trn_df[i], cutbins, duplicates='drop'))[y].sum()
                
                tst_temp = tst_df.groupby(pd.cut(tst_df[i], cutbins, duplicates='drop'))[y].count()
                tst_temp1 = tst_df.groupby(pd.cut(tst_df[i], cutbins, duplicates='drop'))[y].sum()
                
                if y_hat:
                    # Predicted Average Probability
                    tst_temp2 = tst_df.groupby(pd.cut(tst_df[i], cutbins, duplicates='drop'))[y_hat].mean()
            else:
                check, bins = pd.qcut(trn_df[i], 10, duplicates='drop', retbins=True)
                ncats = check.nunique()
                
                random_cuts = pd.Series(trn_df[i].unique()).sample(5).tolist()
                q_cuts = bins.tolist()
            
                if (ncats <= 3) :
                    cutbins = sorted(list(set(random_cuts + [-np.inf, np.inf, 0])))
                    temp = trn_df.groupby(pd.cut(trn_df[i], cutbins, duplicates='drop'))[y].count()
                    temp1 = trn_df.groupby(pd.cut(trn_df[i], cutbins, duplicates='drop'))[y].sum()
                    
                    tst_temp = tst_df.groupby(pd.cut(tst_df[i], cutbins, duplicates='drop'))[y].count()
                    tst_temp1 = tst_df.groupby(pd.cut(tst_df[i], cutbins, duplicates='drop'))[y].sum()
                    
                    if y_hat:
                        # Predicted Average Probability
                        tst_temp2 = tst_df.groupby(pd.cut(tst_df[i], cutbins, duplicates='drop'))[y_hat].mean()
                    
                else:
                    cutbins = sorted(list(set(q_cuts + [-np.inf, np.inf, 0])))
                    temp = trn_df.groupby(pd.cut(trn_df[i], cutbins, duplicates='drop'))[y].count()
                    temp1 = trn_df.groupby(pd.cut(trn_df[i], cutbins, duplicates='drop'))[y].sum()
                    
                    tst_temp = tst_df.groupby(pd.cut(tst_df[i], cutbins, duplicates='drop'))[y].count()
                    tst_temp1 = tst_df.groupby(pd.cut(tst_df[i], cutbins, duplicates='drop'))[y].sum()
            
                    if y_hat:
                        # Predicted Average Probability
                        tst_temp2 = tst_df.groupby(pd.cut(tst_df[i], cutbins, duplicates='drop'))[y_hat].mean()
            
            iv_df = pd.DataFrame({'Levels': temp.index, 'trn_total': temp.values})
            iv_df['feature'] = i
            iv_df['feature_type'] = 'Categorical' if trn_df[i].fillna(0).nunique() <= 2 else 'Numerical'
            iv_df['trn_perc_in_feature'] = (iv_df['trn_total'] / iv_df['trn_total'].sum())
            iv_df['trn_Target_counts'] = temp1.values
            iv_df['trn_non_Target_counts'] = iv_df['trn_total'] - iv_df['trn_Target_counts']
            iv_df['trn_Target_count_percent_in_feature'] = (iv_df['trn_Target_counts'] / iv_df['trn_Target_counts'].sum())
            iv_df['trn_Target_perc'] = (iv_df['trn_Target_counts'] / (iv_df['trn_non_Target_counts'] + iv_df['trn_Target_counts']))
            iv_df['trn_non_Target_count_percent_in_feature'] = (iv_df['trn_non_Target_counts'] / iv_df['trn_non_Target_counts'].sum()) 
            iv_df['trn_WOE'] = -np.log(iv_df['trn_Target_count_percent_in_feature'] / iv_df['trn_non_Target_count_percent_in_feature'])
            iv_df['trn_IV'] = (iv_df['trn_non_Target_count_percent_in_feature'] - iv_df['trn_Target_count_percent_in_feature']) * iv_df['trn_WOE']
            
            iv_df['tst_total'] = tst_temp.values
            iv_df['tst_perc_in_feature'] = (iv_df['tst_total'] / iv_df['tst_total'].sum())
            iv_df['tst_Target_counts'] = tst_temp1.values
            iv_df['tst_non_Target_counts'] = iv_df['tst_total'] - iv_df['tst_Target_counts']
            iv_df['tst_Target_count_percent_in_feature'] = (iv_df['tst_Target_counts'] / iv_df['tst_Target_counts'].sum())
            iv_df['tst_Target_perc'] = (iv_df['tst_Target_counts'] / (iv_df['tst_non_Target_counts'] + iv_df['tst_Target_counts']))
            iv_df['tst_non_Target_count_percent_in_feature'] = (iv_df['tst_non_Target_counts'] / iv_df[
                'tst_non_Target_counts'].sum())
            iv_df['tst_WOE'] = -np.log(iv_df['tst_Target_count_percent_in_feature'] / iv_df['tst_non_Target_count_percent_in_feature'])
            iv_df['tst_IV'] = (iv_df['tst_non_Target_count_percent_in_feature'] - iv_df['tst_Target_count_percent_in_feature'])*iv_df['tst_WOE']
            
            iv_df['diff_trn_tst_perc'] = iv_df['trn_perc_in_feature'] - iv_df['tst_perc_in_feature']
            iv_df['psi'] = iv_df['diff_trn_tst_perc'] * np.log(iv_df['trn_perc_in_feature']/iv_df['tst_perc_in_feature'] )
            
            if tst_temp2.shape[0]:
                iv_df['predicted_avg_proba'] = tst_temp2.values
            
            iv_df1 = pd.concat([iv_df1, iv_df], axis=0)
            
    iv_df1['trn_OverallTargetPerc'] = trn_df[y].sum() / trn_df.shape[0]
    iv_df1['tst_OverallTargetPerc'] = tst_df[y].sum() / tst_df.shape[0]
    
    return iv_df1

def get_gini_score(pred, y):
    fpr, tpr, thresholds = roc_curve(y, 
                                 pred,
                                 pos_label=1)
    result = 2*auc(fpr, tpr) - 1
    return result



def comparative_lift_chart(p, y, base_lift):
    """
    p: Model Scores/Probabilities
    y: Target
    """
    if isinstance(p, pd.Series):
        df_data = pd.DataFrame({"PredProb": p.tolist(), "Actual": y.tolist()})
    elif isinstance(p, list):
        df_data = pd.DataFrame({"PredProb": p, "Actual": y})
    else:
        raise Exception("Expected list or Series like input for p & y")
        
    df_data.sort_values(by=['PredProb'], ascending=False, inplace=True)
    df_data.reset_index(drop=True, inplace=True)
    
    df_data['Decile'] = pd.qcut(df_data.index, 20, labels=False)
        
    
    liftchart = pd.DataFrame()

    grouped = df_data.groupby('Decile', as_index=False)

    liftchart['MaxScore'] = grouped.max().PredProb
    liftchart['MinScore'] = grouped.min().PredProb
    liftchart['NumEvents'] = grouped.sum().Actual
    liftchart['TotalRecs'] = grouped.count().Actual

    liftchart = liftchart.sort_values(by='MinScore', ascending=False).reset_index(drop=True)
    liftchart['HitRate'] = liftchart['NumEvents'] / liftchart['TotalRecs']
    liftchart['PctEvents'] = liftchart['NumEvents'] / liftchart['NumEvents'].sum()

    liftchart['PctRecs'] = liftchart['TotalRecs'] / liftchart['TotalRecs'].sum()
    liftchart['CumRecs'] = liftchart['PctRecs'].cumsum()
    liftchart['Lift'] = liftchart['HitRate'] / (liftchart['NumEvents'].sum() / liftchart['TotalRecs'].sum())

    return liftchart


