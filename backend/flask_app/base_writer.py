"""
Handles operations related to writing data to databases
"""

import sqlite3
import pandas as pd
import numpy as np
from time import time

import utils
import pdb


def base_writer_getConnectionObject(app_config_dict, db_name, logger=None):
    
    db_cfg = app_config_dict[db_name + '_config']
    db_type = db_cfg['db_type']
    
    if app_config_dict['CONFIG_APP_USER'] == 'dev':
        if db_type == 'sqlite3':
            conn = sqlite3.connect(db_cfg['db_path'])
            cursor = conn.cursor()
        else:
            raise Exception('Not Implemented for db_type: {}'.format(db_type))
            
        return conn, cursor
    
    elif app_config_dict['CONFIG_APP_USER'] == 'uat':
        raise Exception('Not Implemented!!! ')
        
    elif app_config_dict['CONFIG_APP_USER'] == 'prod':
        raise Exception('Not Implemented!!! ')
        
    else:
        raise Exception('Not Implemented!!! ')
        
    


def base_writer_insert_table_sqldw(app_config_dict, insert_df, db_name, table_name, logger=None, overwrite_key='ScoreDate'):
    
    utils.print_and_log(logger, "base_writer_insert_table_sqldw() start")
    utils.print_and_log(logger, "base_writer_insert_table_sqldw() database name = {}".format(db_name))
    utils.print_and_log(logger, "base_writer_insert_table_sqldw() table name = {}".format(table_name))
    
    # handling base case 
    if insert_df.shape[0] == 0:
        utils.print_and_log(logger, "insert dataframe for table : {} is empty, nothing to insert".format(table_name))
        return    
    
    # non-zero shaped case    
    start = time()
        
    conn, cursor = base_writer_getConnectionObject(app_config_dict, db_name, logger=logger)
    
    db_cfg = app_config_dict[db_name + '_config']
    db_type = db_cfg['db_type']
    
    utils.print_and_log(logger, "base_writer_insert_table_sqldw() write start")   
    
    start_write = time()
    
    # Clearing any inserted records for given overwrite key
    if overwrite_key in insert_df.columns:
        overwritelist = set(insert_df[overwrite_key].tolist())
                
        for s in overwritelist:
            # Check if any existing records        
            check = pd.read_sql("""select * from {tbl_name} where {overwrite_key}='{val}' limit 10""".format(tbl_name=table_name,
                                                                                                    overwrite_key=overwrite_key,
                                                                                                    val=s), conn)
            if check.shape:
                utils.print_and_log(logger, 'Records exist for overwrite key table: {}, deleting...'.format(table_name))
                
                cursor.execute("""delete from {tbl_name} where {overwrite_key}='{val}' """.format(tbl_name=table_name,
                                                                                                 overwrite_key=overwrite_key,
                                                                                                 val=s)) 
    conn.commit()
    
    if app_config_dict['CONFIG_APP_USER'] == 'dev':
        if db_type == 'sqlite3':
            try:
                insert_df.astype(str).to_sql(table_name, conn, index=False, if_exists='append')
            except:
                pdb.set_trace()
            conn.commit()
        else:
            raise Exception('Not Implemented for db_type: {}'.format(db_type))
                
    elif app_config_dict['CONFIG_APP_USER'] == 'uat':
        raise Exception('Not Implemented!!! ')
        
    elif app_config_dict['CONFIG_APP_USER'] == 'prod':
        raise Exception('Not Implemented!!! ')
        
    else:
        raise Exception('Not Implemented!!! ')

    stop_write = time()
    utils.print_and_log(logger, "base_writer_insert_table_sqldw() dataframe write complete")   
    utils.print_and_log(logger, "base_writer_insert_table_sqldw() dataframe write duration = {}".format(stop_write - start_write))     
   
    return True 
