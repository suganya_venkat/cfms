import ast
from datetime import datetime, timedelta
import json
import logging
import os
import warnings
from itertools import product
from urllib.parse import urlparse

import mlflow
import mlflow.sklearn
import numpy as np
import pandas as pd
from catboost import CatBoostClassifier
from lightgbm import LGBMClassifier
from matplotlib import pylab as plt
from shap import TreeExplainer
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from utils_add import (feature_cols, columns_to_pred_table, col_rename_dict2,
                       tool_users_dict, adjuster_names, feature_cols_2)
from xgboost import XGBClassifier

from helper_functions_auto_claims import (malaysia_data_preprocess, lift, plot_lift_chart,
                                          create_json_for_summary_screen, create_json_for_Investigation_screen,
                                          malaysia_whole_data_preprocess, train_data_loader)
from sqlite_db_add import (create_db_connection, insert_one_row, serialize_data,
                           create_model_table, insert_model_in_ascii, create_serialized_prod_model_table,
                           create_pred_table, create_pred_reason_codes_table, create_user_comments_table,
                           write_to_table, create_tool_users_table, read_model_from_table)

logging.basicConfig(level=logging.WARN)
logger = logging.getLogger(__name__)


# running_folder = os.path.abspath(r'/')
# os.chdir(running_folder)


def set_experiment_id():
    experiments = mlflow.list_experiments()
    if not experiments:
        return '0'
    n_exp = len(experiments)
    return str(n_exp)


def create_lift_chart(model, data, show_plot=False):
    _, x_test, _, y_test = data
    y_pred2 = model.predict_proba(x_test)
    model_metrics = lift(y_test, y_pred2, 10)
    lift_df = plot_lift_chart(model_metrics, show_plot)
    return lift_df


def create_shap_plots(model, data, show_plot=False):
    explainer = TreeExplainer(model)
    _, x_test, _, _ = data
    max_display = 10
    axis_color = "#333333"
    shap_values = explainer.shap_values(x_test)
    ### ------------------
    # summary_plot(shap_values, x_test, plot_type='bar')
    ### ------------------
    feature_names = x_test.columns
    global_shap_values = np.abs(shap_values).mean(0)
    feature_order = np.argsort(np.sum(np.abs(shap_values), axis=0))
    feature_order = feature_order[-min(max_display, len(feature_order)):]
    feature_inds = feature_order[:max_display]
    y_pos = np.arange(len(feature_inds))
    y_labels = [feature_names[i] for i in feature_inds]
    y_vals = list(global_shap_values[feature_inds])

    shap_df = pd.Series(y_vals, index=y_labels)

    if show_plot:
        plt.barh(y_pos, y_vals, 0.7, align='center')
        plt.yticks(y_pos, fontsize=13)
        plt.gca().set_yticklabels(y_labels)
        plt.gca().xaxis.set_ticks_position('bottom')
        plt.gca().yaxis.set_ticks_position('none')
        plt.gca().spines['right'].set_visible(False)
        plt.gca().spines['top'].set_visible(False)
        plt.gca().spines['left'].set_visible(False)
        plt.gca().tick_params(color=axis_color, labelcolor=axis_color)
        plt.yticks(range(len(feature_order)), [feature_names[i] for i in feature_order], fontsize=13)
        plt.show()
    return shap_df


def preprocess_kaggle_data():
    """
    Function to process the kaggle dataset
    """
    warnings.filterwarnings("ignore")
    np.random.seed(40)

    file_path = r'D:\MLflow\cts_learning\claims_preprocessed.xlsx'

    feature_names = ['policy_annual_premium', 'umbrella_limit', 'insured_zip', 'insured_occupation',
                     'insured_hobbies', 'collision_type', 'incident_severity', 'incident_hour_of_the_day',
                     'property_claim', 'auto_make', 'auto_year', 'incident_month', 'fraud_reported']
    claims_data = pd.read_excel(file_path)
    claims_data = claims_data[feature_names]

    x = claims_data.drop(['fraud_reported'], axis=1)
    y = claims_data['fraud_reported']
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=42)

    target_names = ['N', 'Y']

    md_list = [3, 4, 5]
    lr_list = [0.1, 0.01]

    # Extract Combination Mapping in two lists
    # using zip() + product()
    unique_combinations = list(product(md_list, lr_list))
    return x_train, x_test, y_train, y_test, unique_combinations, target_names, claims_data


def workflow(wf_data, max_depth, learning_rate, model, experiment_id, conn, connection_cursor):
    """
    Executes the training workflow, logs the performance parameters and trained model in a sqllite database.

    :param wf_data: tuple of pandas.DataFrame
    :param max_depth: int
    :param learning_rate: float
    :param model: string
    :param experiment_id: int
    :param conn: database.Connection
    :param connection_cursor: database.cursor
    """
    with mlflow.start_run(experiment_id=experiment_id) as run:
        if model == 'xgb':
            ml_model = XGBClassifier(max_depth=max_depth, learning_rate=learning_rate)
        elif model == 'lightgbm':
            ml_model = LGBMClassifier(max_depth=max_depth, learning_rate=learning_rate)

        elif model == 'catboost':
            ml_model = CatBoostClassifier(max_depth=max_depth,
                                          learning_rate=learning_rate, logging_level='Silent')
        else:
            raise ValueError('Model not in scope')
        x_train, x_test, y_train, y_test = wf_data
        ml_model.fit(x_train, y_train)

        print("model (max_depth=%f, learning_rate=%f):" % (max_depth, learning_rate))

        mlflow.log_param("max_depth", max_depth)
        mlflow.log_param("learning rate", learning_rate)

        if model == 'xgb':
            m_name = 'xgb_model_%s_%s' % (max_depth, learning_rate)
            mlflow.xgboost.log_model(ml_model, m_name)
        elif model == 'lightgbm':
            m_name = 'lgb_model_%s_%s' % (max_depth, learning_rate)
            mlflow.lightgbm.log_model(ml_model, m_name)
        elif model == 'catboost':
            m_name = 'ctbst_model_%s_%s' % (max_depth, learning_rate)
            mlflow.catboost.log_model(ml_model, m_name)
        model_hist = json.loads(mlflow.get_run(run.info.run_id).data.tags.get('mlflow.log-model.history'))[0]
        model_id = model_hist['model_uuid']
        run_id = run.info.run_id

        target_names = ['N', 'Y']
        set_type = ['TRN', 'TST']
        combinations = list(product(target_names, set_type))
        for tgt, set_type in combinations:
            if set_type == 'TRN':
                y_label = y_train
                y_pred = (ml_model.predict(x_train), ml_model.predict_proba(x_train))
            else:
                y_label = y_test
                y_pred = (ml_model.predict(x_test), ml_model.predict_proba(x_test))
            insert_model_details(target=tgt, set_type=set_type, y_label=y_label, y_pred=y_pred, conn=conn,
                                 model_id=model_id, run_id=run_id)


def insert_model_details(target=None, set_type=None,
                         y_label=None, y_pred=None, conn=None,
                         model_id=None, run_id=None):
    target_names = ['N', 'Y']
    if y_label is None or y_pred is None or model_id is None or run_id is None:
        raise ValueError('Provide proper values for labels/predictions or model/run ids')
    cat_dict = {'N': 'CAT-0', 'Y': 'CAT-1'}
    tgt = cat_dict[target]
    cr = classification_report(y_label, y_pred[0], target_names=target_names, output_dict=True)
    recall_metric = "recall_%s_%s" % (set_type, tgt)
    f1_score_metric = "f1_score_%s_%s" % (set_type, tgt)
    lift_metric = "lift_%s" % set_type
    recall = cr[target]['recall']
    f1_score = cr[target]['f1-score']
    mlflow.log_metric(recall_metric, recall)
    mlflow.log_metric(f1_score_metric, f1_score)

    model_metrics = lift(y_label, y_pred[1], 10)
    lift_val = model_metrics['lift'].max()

    mlflow.log_metric(lift_metric, lift_val)
    data = [[run_id, model_id, 'EVAL-1', f1_score, len(y_label), set_type, tgt, None],
            [run_id, model_id, 'EVAL-3', recall, len(y_label), set_type, tgt, None],
            [run_id, model_id, 'EVAL-5', lift_val, len(y_label), set_type, tgt, None]]
    df = pd.DataFrame(data, columns=['RunID', 'ModelID', 'EvalMetricID', 'EvalMetricValue', 'Support',
                                     'DatasetTypeID', 'CategoryID', 'DatasetID'])
    df.to_sql(name='Model_Performance', con=conn, if_exists='append', index=False)


class ContinuousLearning:
    """
    This class enables continuous learning based on recent datasets.
    It can log, deploy & monitor the performance metrics
    Also it can trigger retraining based on the current performance metrics and redeploy the model in to production.
    """

    def __init__(self, combinations, eval_metric, db_connection, db_cursor):
        self.combinations = combinations
        self.eval_metric = eval_metric
        self.workflow_data = None
        self.prod_model_path = None
        self.best_model_in_curr_exp = None
        self.model_name = None
        self.best_model_metric_val = None
        self.current_exp_id = None
        self.exp_names = None
        self.db_connection = db_connection
        self.db_cursor = db_cursor
        self.prod_model = None
        self.prod_model_metric_val = None
        self.best_model_run_id = None
        self.top_three_run_ids = None
        self.struct_model_claims_proba = None
        self.clm_ids_train = None
        self.clm_ids_test = None
        self.ft_names = None
        self.best_model_model_id = None

    def run_experiments(self, wf_data):
        """
        Run all the combinations in given experiment
        :param wf_data: tuple of pandas.DataFrame
        """
        x_train, x_test, y_train, y_test = wf_data
        self.clm_ids_train, self.clm_ids_test = x_train['claim_number'], x_test['claim_number']
        x_train, x_test = x_train.drop(columns=['claim_number']), x_test.drop(columns=['claim_number'])
        self.ft_names = x_train.columns
        self.workflow_data = (x_train, x_test, y_train, y_test)
        current_datetime = datetime.now().strftime('%M%H%d%m%Y')
        self.current_exp_id = mlflow.create_experiment(name='exp_%s' % current_datetime)
        for md, lr, model in self.combinations:
            workflow(self.workflow_data, md, lr, model, self.current_exp_id, self.db_connection, self.db_cursor)

    def choose_best_model_in_experiment(self, exp_id):
        """
        Out of the models generated, select the best model based on the evaluation metric
        and deploy it in production

        """
        run_ids = mlflow.search_runs(exp_id).run_id
        best_run_id = None
        best_metric_val = None
        top_3_runs = {}
        # evaluation metric on test data
        eval_metric = self.eval_metric + '_TST'
        for i, run_id in enumerate(run_ids):
            metric_value = mlflow.get_run(run_id).data.metrics[eval_metric]
            if not i:
                best_run_id = run_id
                best_metric_val = metric_value
                top_3_runs[run_id] = metric_value
                continue
            if metric_value > best_metric_val:
                best_run_id, best_metric_val = run_id, metric_value
            if len(top_3_runs) < 3:
                top_3_runs[run_id] = metric_value
                continue
            if any(t < metric_value for t in top_3_runs.values()):
                del top_3_runs[min(top_3_runs, key=top_3_runs.get)]
                top_3_runs[run_id] = metric_value

        model = self.load_model_data(best_run_id)
        self.best_model_in_curr_exp = model
        self.best_model_metric_val = best_metric_val
        self.best_model_run_id = best_run_id
        self.top_three_run_ids = top_3_runs

    @staticmethod
    def load_model_data(run_id):
        logs = mlflow.get_run(run_id).data.tags['mlflow.log-model.history']
        model_name = json.loads(logs)[0]['artifact_path']
        artifact_path = mlflow.get_run(run_id).info.artifact_uri
        model_path = artifact_path + '/' + model_name
        if 'xgb' in model_name:
            model = mlflow.xgboost.load_model(model_path)
        elif 'lgb' in model_name:
            model = mlflow.lightgbm.load_model(model_path)
        elif 'ct' in model_name:
            model = mlflow.catboost.load_model(model_path)
        else:
            raise ValueError('Model not in scope')
        return model

    def create_model_performance_plots(self, show_plot=False):
        for i, run_id in enumerate(self.top_three_run_ids):
            model = self.load_model_data(run_id)
            shap_df = create_shap_plots(model, self.workflow_data, show_plot=show_plot)
            lift_df = create_lift_chart(model, self.workflow_data, show_plot=show_plot)
            model_name = model.__module__.split('.')[0]
            if 'catboost' in model_name:
                learning_rate = model.get_all_params()['learning_rate']
                max_depth = model.get_all_params()['depth']
                n_estimators = model.get_all_params()['n_estimators']
                min_child_weight = model.get_all_params()['min_child_weight']
                gamma = model.get_all_params()['gamma']
            else:
                learning_rate = model.learning_rate
                max_depth = model.max_depth
                n_estimators = model.n_estimators
                min_child_weight = model.min_child_weight
                gamma = model.gamma
            model_params = {'learning_rate': learning_rate, 'gamma': gamma,
                            'max_depth': max_depth, 'n_estimators': n_estimators, 'min_child_weight': min_child_weight}
            model_query = 'SELECT DISTINCT ModelID FROM Model_Performance WHERE ' \
                          'RunID = "%s"' % run_id
            model_id = pd.read_sql(model_query, self.db_connection).values[0][0]

            # write summary parameters to DB
            data_summ = {'ModelID': model_id, 'RunID': run_id, 'SummaryShapValues': shap_df.to_json(),
                         'ModelLiftChart': lift_df.to_json()}
            df_summ = pd.DataFrame(data=data_summ, index=[0])
            df_summ.to_sql(name='Model_Summary_Indicators', con=self.db_connection, if_exists='append', index=False)

            # write model params to DB

            param_id_df = pd.read_sql('SELECT ParamID, ParamName FROM Parameters', self.db_connection)
            param_id_df = param_id_df.set_index('ParamName')
            param_id_dict = param_id_df.to_dict()['ParamID']

            data_params = {'ModelID': np.repeat(model_id, len(model_params.keys())),
                           'ModelName': np.repeat(model_name, len(model_params.keys())),
                           'ModelDescription': np.repeat(model_name, len(model_params.keys())),
                           'ParamID': [param_id_dict[key] for key in list(model_params.keys())],
                           'ParamValue': model_params.values()}
            df_param = (pd.DataFrame(data=data_params, index=None)).fillna(0)
            df_param.to_sql(name='Model_Repository', con=self.db_connection, if_exists='append', index=False)

    @staticmethod
    def dump_and_save_to_json(shap_df, lift_df, model_params):
        m_num = model_params['model_number']
        if not os.path.isdir('artifacts/prod_model_artifacts'):
            os.mkdir('artifacts/prod_model_artifacts')
        shap_df.to_json('prod_model_artifacts/model_%s_shap.json' % m_num, orient='table')
        lift_df.to_json('prod_model_artifacts/model_%s_lift.json' % m_num, orient='table')
        with open('prod_model_artifacts/model_%s_params.json' % m_num, 'w') as outfile:
            json.dump(model_params, outfile)

    def create_detailed_prediction_df(self, ip_data_conn):
        """
        Create dataframe with detailed predicted parameters.
        :return: pandas DataFrame, list of strings
        """
        query = "SELECT * FROM models WHERE prod_model_status= 'Y'"
        prod_model_data = pd.read_sql(query, self.db_connection)
        run_id = prod_model_data.loc[0]['run_id']

        model = read_model_from_table(run_id, self.db_connection)
        # prediction_df = self.load_df_with_complete_claims_data()

        pred_df = self.load_df_from_input_data_db(ip_data_conn)
        pred_df = pred_df.rename(columns={'week_time_3': 'week_time_3.0'})
        predicted = model.predict_proba(pred_df[feature_cols])
        proba_data = {'claim_id': pred_df.claim_number, 'proba_score': predicted[:, 0]}
        self.struct_model_claims_proba = pd.DataFrame(proba_data)
        pred_df['score_grp'] = lift(pred_df.target, predicted, 10, return_only_scr_grp=True)
        explainer = TreeExplainer(model)
        shap_values = explainer.shap_values(pred_df[feature_cols])

        pred_df = pred_df.drop(columns=feature_cols)
        max_display = 6
        shap_values = np.abs(shap_values)
        feature_order = np.argsort(np.sum(np.abs(shap_values), axis=0))
        feature_order = feature_order[-min(max_display, len(feature_order)):]
        feature_inds = feature_order[:max_display]
        ft_names = [feature_cols_2[i] for i in feature_inds]
        final_shap_values = np.array([sv[feature_inds] for sv in shap_values])

        for i, name in enumerate(ft_names):
            pred_df[name] = final_shap_values[:, i]
        pred_df['assigned_to'] = np.random.choice(list(range(1, len(adjuster_names) + 1)), pred_df.shape[0])

        # rename columns
        pred_df.rename(columns=col_rename_dict2, inplace=True)
        return pred_df[['claim_number'] + columns_to_pred_table + ft_names], ft_names

    @staticmethod
    def create_reason_code_bucket(df, ft_names):
        df_new = df.loc[:, ft_names]
        rc_bucket = []
        for _, row in df_new.iterrows():
            rc_bucket.append(row.idxmax())
        return rc_bucket

    def write_prediction_to_tables(self, pred_df, ft_names):
        """
        Creates tables for storing detailed claims predictions data. Two tables are created,
        one for storing general claim realted data, another for feature shap values

        :param pred_df: pandas DataFrame
        :param ft_names: list of strings
        :return: None
        """
        create_pred_table(self.db_cursor, self.db_connection)
        create_pred_reason_codes_table(self.db_cursor, self.db_connection)
        self.db_connection.commit()
        pred_df['rc_bucket'] = self.create_reason_code_bucket(pred_df, ft_names)
        write_to_table(self.db_connection, pred_df[['claim_number'] + columns_to_pred_table], 'predictions')
        write_to_table(self.db_connection, pred_df[['claim_number'] + ft_names + ['rc_bucket']],
                       'predictions_reason_codes')
        write_to_table(self.db_connection, self.struct_model_claims_proba, 'claims_structured_score')
        self.db_connection.commit()

    def create_users_table(self):
        # create assigned user_table
        create_tool_users_table(self.db_cursor)
        self.db_connection.commit()
        data = {'user_name': list(tool_users_dict.keys()), 'user_id': list(tool_users_dict.values())}
        df = pd.DataFrame(data=data)
        write_to_table(self.db_connection, df, 'tool_users')
        self.db_connection.commit()

    def create_user_comments_table(self):
        # create user comments table
        create_user_comments_table(self.db_cursor)
        self.db_connection.commit()

    @staticmethod
    def load_df_with_complete_claims_data():
        df_com = pd.read_excel('auto_claims_data_new.xlsx')
        return df_com

    @staticmethod
    def load_df_from_input_data_db(conn):
        query = 'SELECT * FROM input_claims_data'
        df_com = pd.read_sql(query, conn)
        return df_com

    def log_and_deploy_model_into_prod(self):
        """
        Deploy the current model to production and assign/update its status in database accordingly

        """
        # tracking_url_type_store = urlparse(python_code.get_tracking_uri()).scheme

        # update production model status
        update_query = 'UPDATE Prod_Model_Store SET ProdModelStatus = "N" WHERE ProdModelStatus = "Y"'
        self.db_cursor.execute(update_query)
        self.db_connection.commit()

        for run_id in list(self.top_three_run_ids.keys()):
            model = self.load_model_data(run_id)
            encoded_model = serialize_data(model)
            modelid_query = 'SELECT DISTINCT ModelID FROM Model_Performance WHERE ' \
                            'RunID = "%s"' % run_id
            model_id = pd.read_sql(modelid_query, self.db_connection).values[0][0]
            insert_query = 'INSERT INTO Prod_Model_Store (ModelID, RunID, ProdModelStatus, ModelAscii) VALUES ' \
                           '(?, ?, ?, ?)'
            if run_id == self.best_model_run_id:
                self.best_model_model_id = model_id
                val_tuple = (model_id, run_id, 'Y', encoded_model)
                self.db_cursor.execute(insert_query, val_tuple)
                continue
            val_tuple = (model_id, run_id, 'N', encoded_model)
            self.db_cursor.execute(insert_query, val_tuple)
        self.db_connection.commit()
        self.write_model_outputs_todb()

    def write_model_outputs_todb(self, remove_old_data=True):
        """
        :param remove_old_data: Boolean
        """
        x_train, x_test, _, _ = self.workflow_data
        # pred_proba = self.best_model_in_curr_exp.predict_proba(np.vstack((x_train, x_test)))
        claim_numbers = self.clm_ids_train.append(self.clm_ids_test)
        explainer = TreeExplainer(self.best_model_in_curr_exp)
        shap_values = explainer.shap_values(np.vstack((x_train, x_test)))
        if remove_old_data:
            now = datetime.now()
            str_time = (now - timedelta(days=15)).strftime('%Y-%m-%d')
            del_query = 'DELETE FROM Model_Indicators WHERE RID < "%s"' % str_time
            self.db_cursor.execute(del_query)
            self.db_connection.commit()

        ft_query = 'SELECT FeatureID, ModelFeatureName FROM Features_Inventory'
        feature_names_df = pd.read_sql(ft_query, self.db_connection)
        ft_id, ft_names = feature_names_df['FeatureID'], feature_names_df['ModelFeatureName']
        feature_dict = {ft_names[i]: ft_id[i] for i in range(len(feature_names_df))}

        # create DF to write to DB
        df = pd.DataFrame(shap_values, columns=self.ft_names)
        # remaining columns
        data_length = len(claim_numbers)
        df['IdField'] = np.repeat('ID-1', data_length)
        df['IdFieldValue'] = claim_numbers.values
        df['RunID'] = np.repeat(self.best_model_run_id, data_length)
        df['ModelID'] = np.repeat(self.best_model_model_id, data_length)
        df = df.rename(columns=feature_dict)
        df_ft_imp = df.melt(id_vars=['IdField', 'IdFieldValue', 'ModelID', 'RunID'], var_name='ModelFeatureID',
                            value_name='ModelFeatureImportance')

        # feature valur df
        ft_df = pd.DataFrame(data=np.vstack((x_train, x_test)), columns=self.ft_names)
        ft_df['IdFieldValue'] = claim_numbers.values
        ft_df = ft_df.rename(columns=feature_dict)
        df_ft_val = ft_df.melt(id_vars=['IdFieldValue'], var_name='ModelFeatureID', value_name='ModelFeatureValue')

        finaldf = pd.merge(df_ft_imp, df_ft_val, on=['IdFieldValue', 'ModelFeatureID'])
        finaldf.to_sql(name='Model_Indicators', con=self.db_connection, if_exists='append', index=False)

    def check_production_model_performance(self, data_to_predict, label_col=None, ref_metric_value=None):
        """
        Parameters
        ----------
        data_to_predict: pandas.DataFrame
        label_col: str
        ref_metric_value: float
        """
        _, X_test, _, y_test = data_to_predict
        y_pred2 = self.prod_model.predict_proba(X_test)

        model_metrics = lift(y_test, y_pred2, 10)
        metric_val = model_metrics['lift'].max()
        print('Metric Used for Validation', self.eval_metric, '\nValue:', metric_val)
        if metric_val > self.prod_model_metric_val:
            return 1
        else:
            return 0

    @staticmethod
    def get_experiments_names():
        exp_list = mlflow.list_experiments()
        names = [e.name for e in exp_list]
        return names

    @staticmethod
    def datetime_parser(name_string):
        d_string = name_string.split('_')[1]
        return datetime.strptime(d_string, '%M%H%d%m%Y')

    def get_latest_experiment_name(self):
        self.exp_names = self.get_experiments_names()
        latest_exp_name = self.exp_names[0]
        for name in self.exp_names:
            if self.datetime_parser(name) > self.datetime_parser(latest_exp_name):
                latest_exp_name = name
        return latest_exp_name

    def get_latest_production_model(self):
        query = "SELECT * FROM models WHERE prod_model_status= 'Y'"
        prod_model_data = pd.read_sql(query, self.db_connection)
        run_id = prod_model_data.loc[0]['run_id']
        logs = mlflow.get_run(run_id).data.tags['python_code.log-model.history']
        model_name = ast.literal_eval(logs)[0]['artifact_path']
        artifact_path = mlflow.get_run(run_id).info.artifact_uri
        model_path = artifact_path + '/' + model_name
        if 'lgb' in model_name:
            self.prod_model = mlflow.lightgbm.load_model(model_path)
        elif 'xgb' in model_name:
            self.prod_model = mlflow.xgboost.load_model(model_path)
        self.prod_model_metric_val = mlflow.get_run(run_id).data.metrics[self.eval_metric]

    def apply_rules_run_experiments_and_deploy_prod_model(self, wf_data):
        """
        Apply the pipeline to run the list of experiments,
        choose the best model among them and deploy it in to production

        :param wf_data: pandas.DataFrame
        """
        self.run_experiments(wf_data)
        self.choose_best_model_in_experiment(self.current_exp_id)
        self.create_model_performance_plots(show_plot=False)
        self.log_and_deploy_model_into_prod()

    def apply_rules_retrain_log_and_deploy_prod_model(self, wf_data, label_col):

        """
        Applying pipeline to trigger retraining of model based on the performance of production model.
        Model is evaluated based on the validation dataset (Typically more recent data)

        :param wf_data: pandas.DataFrame
        :param label_col: str
        """
        self.get_latest_production_model()
        if self.check_production_model_performance(data_to_predict=wf_data, label_col=label_col):
            self.run_experiments(wf_data)
            self.choose_best_model_in_experiment(self.current_exp_id)
            if self.best_model_metric_val > self.prod_model_metric_val:
                self.log_and_deploy_model_into_prod()

    def view_model_db_contents(self):
        query = 'SELECT * FROM models'
        print(pd.read_sql(query, self.db_connection))


if __name__ == "__main__":
    # Execute workflow with Kaggle dataset
    # x_train, x_test, y_train, y_test, unique_combinations, target_names, data = preprocess_kaggle_data()
    # cl = ContinuousLearning(unique_combinations, eval_metric='recall', db_connection=connection, db_cursor=cursor)
    # # cl.apply_rules_run_experiments_and_deploy_prod_model()
    # cl.apply_rules_retrain_log_and_deploy_prod_model(drift_validation_data=data[200:], label_col='fraud_reported')
    # connection.close()

    create_data_summary = False
    if create_data_summary:
        df_to_summary = pd.read_excel('auto_claims_data_new.xlsx')
        create_json_for_summary_screen(df_to_summary)
        create_json_for_Investigation_screen(df_to_summary)

    # Execute workflow with MY dataset
    f_path = r'artifacts/spreadsheet_files/auto_claims_data_new.xlsx'

    # Database connection
    # connection, cursor = create_db_connection('cfms/backend/flask_app/artifacts/db_files/claims_data.db')
    # create_model_table(cursor=cursor)
    # create_serialized_prod_model_table(cursor=cursor)

    md_list = [4, 5]
    lr_list = [0.1, 0.05]
    model_list = ['xgb']  # catboost, xgboost

    unique_combinations = list(product(md_list, lr_list, model_list))

    # Retrain pipeline check
    train = True
    predict = False
    retrain = False
    run_misc_tasks = False

    connection, cursor = create_db_connection('databases/cfms.db')
    doc_conn, _ = create_db_connection('databases/claim_docs.db')

    cl = ContinuousLearning(unique_combinations, eval_metric='lift', db_connection=connection, db_cursor=cursor)
    # ----------------------------------------------------------------------------------------------
    # Apply rules to run experiments and deploy production model
    if train:
        workflow_data = train_data_loader(connection)
        # workflow_data = malaysia_data_preprocess(f_path)
        cl.apply_rules_run_experiments_and_deploy_prod_model(workflow_data)

    if predict:
        # write data with predictions to db
        # whole_data, clm_nos, clm_dates, label = malaysia_whole_data_preprocess(f_path)
        conn, csr = create_db_connection('artifacts/db_files/input_data.db')
        prediction_df, feature_names = cl.create_detailed_prediction_df(conn)
        cl.write_prediction_to_tables(prediction_df, feature_names)
    # ----------------------------------------------------------------------------------------------
    # Apply rules to check production model drift, retrain and redeploy production model
    if retrain:
        workflow_data = malaysia_data_preprocess(f_path, retrain=True)
        cl.apply_rules_retrain_log_and_deploy_prod_model(workflow_data, label_col='fraud_reported')
    connection.close()

    if run_misc_tasks:
        # user table creation
        # cl.create_users_table()
        # cl.create_user_comments_table()
        ## -------------------
        # create input claims data db
        connection, cursor = create_db_connection('db_files\input_claims_data.db')
