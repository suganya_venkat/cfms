import datetime
import json, os
import numpy as np
import pandas as pd
import yaml
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt
from datetime import timedelta
import sqlite3
import matplotlib.pyplot as plt
# SARIMA example
from statsmodels.tsa.statespace.sarimax import SARIMAX
from random import random
from scipy.signal import savgol_filter
from scipy.integrate import simps
import sys, path

directory = path.Path(__file__).abspath()
sys.path.append(directory.parent.parent)
os.chdir(directory.parent.parent)

from utils import load_app_config_dict


class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return super(NpEncoder, self).default(obj)


def create_db_connection(db_f_path):
    conn = sqlite3.connect(db_f_path)
    cur = conn.cursor()
    return conn, cur


def create_table_json(df):
    """
    Creates tabular json output
    :param df: pandas DataFrame or Series
    :return: Dictionary
    """
    if df.index.name == 'CLAIMREPORTDATE':
        df.index.name = 'date'
        data = {'actual_value': df.values}
        df = pd.DataFrame(data, index=df.index)

    df.to_json('temp.json', orient='table')
    with open('temp.json') as json_file:
        data = json.load(json_file)
    return data


# db_path = r'D:\CFMS\cfms_dev\cfms\backend\flask_app\artifacts\db_files\input_data.db'
# connection, cursor = create_db_connection(db_path)


def get_connection():
    db_file_path = 'databases/driver.db'
    conn, cur = create_db_connection(db_file_path)
    return conn

def get_doc_db_connection():
    db_file_path = 'databases/claim_docs.db'
    conn, cur = create_db_connection(db_file_path)
    return conn


def get_doc_db_connection():
    db_file_path = 'databases/claim_docs.db'
    conn, cur = create_db_connection(db_file_path)
    return conn


def time_series_data_retrieval(db_connection, state_name=None):
    """
    Connects and retrieve data to perform forecasting operations
    :param state_name: State Name
    :param db_connection: database connection
    :return: Tuple of time series data
    """
    if not state_name or state_name == 'all':
        read_query = 'SELECT claim_number, STATE, APPRV_AMT_USD, CLAIM_AMT_USD, CLAIMREPORTDATE  FROM Claims'
    else:
        read_query = 'SELECT claim_number, STATE, APPRV_AMT_USD, CLAIM_AMT_USD, CLAIMREPORTDATE  FROM Claims ' \
                     'WHERE STATE="%s"' % state_name
    df = pd.read_sql(read_query, db_connection)
    
    print(df.STATE.value_counts())
    
    df['CLAIMREPORTDATE'] = df['CLAIMREPORTDATE'].astype('datetime64[ns]')
    ts_df = df.groupby(pd.Grouper(key='CLAIMREPORTDATE', axis=0, freq='7D')).agg(count=('claim_number', 'count'),
                                                                                 apprv_amt=('APPRV_AMT_USD', 'sum'),
                                                                                 claimed_amt=('CLAIM_AMT_USD', 'sum'))
    # ts = df.groupby(pd.Grouper(key='CLAIMREPORTDATE', axis=0, freq='7D')).agg({'id':})['count']
    smoothed_ts_clm_cnt = pd.Series(data=savgol_filter(ts_df['count'], 19, 3), index=ts_df.index)
    smoothed_ts_appr_amt = pd.Series(data=savgol_filter(ts_df['apprv_amt'], 19, 3), index=ts_df.index)
    smoothed_ts_claim_amt = pd.Series(data=savgol_filter(ts_df['claimed_amt'], 19, 3), index=ts_df.index)
    return smoothed_ts_clm_cnt, smoothed_ts_appr_amt, smoothed_ts_claim_amt


def apply_forecasting_algorithm_clm_cnt(t_series, weeks=8, plot=False):
    """
    Forecasting capbaility using SARIMAX
    """
    t_series = t_series[:-4]
    model = SARIMAX(t_series, order=(1, 1, 1), seasonal_order=(1, 1, 1, 12))
    model_fit = model.fit(disp=False)
    # make prediction
    pred = model_fit.get_prediction(len(t_series), len(t_series) + weeks - 1)
    pred_ci = pred.conf_int(alpha=0.2)
    if plot:
        plt.figure(figsize=(7, 2))
        plt.plot(pred.predicted_mean, 'r')
        plt.plot(t_series, 'b')
        plt.fill_between(pred_ci.index,
                         pred_ci.iloc[:, 1],
                         pred_ci.iloc[:, 0], color='k', alpha=.2)
        plt.show()
    return pred, t_series


def apply_forecasting_algorithmn_amounts(smthd_ts_appr_amt, smthd_ts_claim_amt, weeks=None):
    ts_appr_amt = smthd_ts_appr_amt[:-4]
    ts_clm_amt = smthd_ts_claim_amt[:-4]
    index = ts_appr_amt.index
    model_appr = SARIMAX(ts_appr_amt, order=(1, 1, 1), seasonal_order=(1, 1, 1, 12))
    model_clm = SARIMAX(ts_clm_amt, order=(1, 1, 1), seasonal_order=(1, 1, 1, 12))
    # Fit models
    model_fit_appr = model_appr.fit(disp=False)
    model_fit_clm = model_clm.fit(disp=False)
    # make predictions
    pred_apprvd = model_fit_appr.get_prediction(len(ts_appr_amt), len(ts_appr_amt) + weeks - 1)
    pred_clmd = model_fit_clm.get_prediction(len(ts_clm_amt), len(ts_clm_amt) + weeks - 1)
    return pred_apprvd, pred_clmd, ts_appr_amt, ts_clm_amt


def compute_forecasting_aggregates_amounts(prd_aprvd, prd_clmd, ts_apr_amt, ts_cl_amt):
    # Trimming some past data
    ts_apr_amt = ts_apr_amt[40:]
    ts_cl_amt = ts_cl_amt[40:]
    est_clm_payout = simps(prd_aprvd.predicted_mean, dx=1)
    est_clm_amt = simps(prd_clmd.predicted_mean, dx=1)
    est_clm_payout_pred = prd_aprvd.predicted_mean
    est_clm_amt_pred = prd_clmd.predicted_mean
    forecast_data = np.vstack([est_clm_payout_pred, est_clm_amt_pred])
    fc_df = pd.DataFrame(np.transpose(forecast_data), columns=['Predicted Total claim payout',
                                                               'Predicted Total claim amount'])
    fc_df.index = est_clm_payout_pred.index.astype(str)
    fc_df.index.name = 'date'
    pre_data_df = pd.DataFrame(data={'Payout Amount': ts_apr_amt, 'Claim Amount': ts_cl_amt},
                               index=ts_apr_amt.index)
    pre_data_df.index = pre_data_df.index.astype(str)
    pre_data_df.index.name = 'date'
    forecast_json = {'Data_Amounts': create_table_json(pre_data_df)['data'] + create_table_json(fc_df)['data'],
                     'Y_axis_label_for Claimed/Payout Amounts': 'Amount $(US) / week',
                     'Forecasted Total Payout $(US)': est_clm_payout, 'Forecasted Total Claim Amount $(US)': est_clm_amt}
    return forecast_json


def compute_forecasting_aggregates_clm_cnt(pred, ts):
    ts.index = ts.index.astype(str)
    ts = ts[40:]
    ub = pred.conf_int(alpha=0.2).iloc[:, 1]
    ub.index = ub.index.astype(str)
    lb = pred.conf_int(alpha=0.2).iloc[:, 0]
    lb.index = ub.index.astype(str)
    est_clm_cnt = simps(pred.predicted_mean, dx=1)
    pred_mean = pred.predicted_mean
    pred_mean.index = pred_mean.index.astype(str)
    forecast_data = np.vstack([pred_mean, ub, lb])
    fc_df = pd.DataFrame(np.transpose(forecast_data), columns=['mean_forecast_value', 'upper_bound', 'lower_bound'])
    fc_df.index = pred_mean.index.astype(str)
    fc_df.index.name = 'date'

    forecast_json = {'Data_Claim_Counts': (create_table_json(ts)['data'] + create_table_json(fc_df)['data']),
                     'Y_axis_label': 'claim count/ week',
                     'Estimated claim count': est_clm_cnt}
    print(json.dumps(forecast_json))
    return forecast_json


if __name__ == "__main__":
    weeks = 4
    state_names = ['all', 'Pahang', 'Penang', 'Perak','Wilayah', 'Johor', ' Malacca', 'Pertis',
                   'Selangor', 'Kedah', 'Negeri', 'Kelantan', 'Pulau', 'Sabah']
    connection = get_connection()
    (smoothed_ts_clm_cnt, smoothed_ts_appr_amt, smoothed_ts_claim_amt) = time_series_data_retrieval(
        connection, state_names[4])
    forecast, ts_clm_cnt = apply_forecasting_algorithm_clm_cnt(smoothed_ts_clm_cnt, weeks=8)
    pred_apprvd, pred_clmd, ts_appr_amt, ts_clm_amt = apply_forecasting_algorithmn_amounts(smoothed_ts_appr_amt,
                                                                                           smoothed_ts_claim_amt,
                                                                                           weeks=weeks)

    forecast_json_clm_amts = compute_forecasting_aggregates_amounts(pred_apprvd, pred_clmd, ts_appr_amt, ts_clm_amt)
    forecast_json_clm_cnt = compute_forecasting_aggregates_clm_cnt(forecast, ts_clm_cnt)
    print('Done')
