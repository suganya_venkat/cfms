import pandas as pd
import numpy as np
import sqlite3
import utils
import sql_queries
import pdb
from datetime import datetime

config_data_dict = utils.load_app_config_dict()

def create_score_group_based_on_model_weights(w_struct_model=0.5, w_anom_model=0.5):
    if (w_struct_model > 1.0) or (w_struct_model < 0):
        raise ValueError('Model weight cannot exceed one or less than zero')
    if w_anom_model != (1 - w_struct_model):
        w_anom_model = (1 - w_struct_model)
    # access proba scores
    cfms_con, _ = utils.connect2sqlite3(config_data_dict['CFMS_DB_PATH'])
    supervised_output = pd.read_sql(sql_queries.sql_supervised_model_output, cfms_con)
    unsupervised_output = pd.read_sql(sql_queries.sql_unsupervised_model_output, cfms_con)

    supervised_output.drop_duplicates(subset=['IdField', 'IdFieldValue'], inplace=True)
    unsupervised_output.drop_duplicates(subset=['IdField', 'IdFieldValue'], inplace=True)    
    supervised_output['Weighted_Score'] = supervised_output['ModelScaledScore']*w_struct_model
    unsupervised_output['Weighted_Score'] = unsupervised_output['ModelScaledScore']*w_anom_model

    model_output_df = supervised_output.merge(unsupervised_output, 
                                              on=['IdField', 'IdFieldValue'],
                                              how='left', 
                                              suffixes=('_sup', '_unsup') )

    model_output_df['WeightedScore'] = model_output_df[['Weighted_Score_sup', 'Weighted_Score_unsup']].sum(axis=1)
    model_output_df['Decile'] = pd.qcut(model_output_df['WeightedScore'].rank(method='first'), 10, labels=False) + 1
    model_output_df['ScoreGroup'] = np.where(model_output_df['Decile']==1, "SH", np.where(model_output_df['Decile'].isin([2, 3, 4]),
                                                 "H", np.where(model_output_df['Decile'].isin([5, 6, 7]), "M", "L")))

    print("Columns: ", model_output_df.columns)
    weighted_df = model_output_df[['IdField', 'IdFieldValue', 'WeightedScore', 'Decile', 'ScoreGroup']].copy()
    weighted_df.to_sql(name='WeightedScores', con=cfms_con, if_exists='replace', index=False)
    print("SUCCESS: Claims Weighted Scores Updated")
    return {"response": "SUCCESS"}
        
    

if __name__=="__main__":
    
    create_score_group_based_on_model_weights()
    
    