"""
Handles the Reason Code Creation/ Prediction Explanation Part

"""

import pandas as pd
import numpy as np
import shap
import pdb


class FeatureImportance:
    
    def __init__(self, model, model_features, model_type='tree-based'):
        self.model = model
        self.model_type = model_type
        self.model_features = model_features
        
    def compute_shap_feature_importance(self, X):
        if self.model_type=='tree-based':
            shap_explainer = shap.TreeExplainer(self.model, random_state=12345)
            X_shap = shap_explainer.shap_values(X)
            result = pd.DataFrame(X_shap)
            result.columns = self.model_features
            result.index = X.index
        else:
            raise Exception(f"Not Implemented for model type: {model_type}")
        
        return result
        



class Explainer():
    
    def __init__(self, model_data, feature_imp_df, model_features, business_rules=None, topn=5, out_col_pref='top_'):
        self.model_data = model_data
        self.model_features = model_features
        self.feature_imp_df = feature_imp_df
        self.business_rules = business_rules
        self.topn = topn
        self.out_col_pref = out_col_pref
        
    def apply(self,):
        
        model_data = self.model_data[self.model_features].copy()
                        
        # Step 1: Get Shap Values
        shap_vals = self.feature_imp_df.copy()
                    
        shap_values_df = pd.DataFrame(shap_vals)
        shap_values_df.columns = self.model_features
        shap_values_df.index = model_data.index
                
        # Step 2: Order the features as per Shap values
        def order_and_keep_gt0(x):
            x1 = x.copy()
            # Keeps only those features for which the Shap value is greater than 0
            ordered_rc_list = x1[x1>0].sort_values(ascending=False).index.tolist()
            nfill_empty = len(self.model_features) - len(ordered_rc_list) 
            ordered_rc_list = ordered_rc_list + [np.nan for _ in range(nfill_empty)]
            return pd.Series(ordered_rc_list)
            
        top_rc = shap_values_df.apply(order_and_keep_gt0, axis=1)
        top_rc.index = shap_values_df.index

        ordered_rc = top_rc.copy()
        ordered_rc.index = model_data.index
        
        if self.business_rules is None:
            result = pd.DataFrame(columns=[self.out_col_pref + str(i+1) for i in range(self.topn)],
                                 index=model_data.index)
            n_cols = min(self.topn, ordered_rc.shape[1])
            fill_cols = [self.out_col_pref + str(i+1) for i in range(n_cols)]
            result[fill_cols] = ordered_rc[[c for j, c in enumerate(ordered_rc.columns)  if j<n_cols]].copy()
            return result
            
        # For each index, create feature value mapping dictionary
        index_features_dict = model_data.T.to_dict()
        
        ordered_rc_feat_values = ordered_rc.apply(lambda x: x.map(index_features_dict[x.name]), axis=1)
        
        # Step 4: Map the Features to Corresponding Descriptions using business rules file
        final_rc_list = []
        for rc_rows, rc_val_rows in zip(ordered_rc.iterrows(), ordered_rc_feat_values.iterrows()):
            r1 = rc_rows[1]
            r2 = rc_val_rows[1]
            f = pd.concat([r1, r2], axis=1)
            f.columns = ['Features', 'Values']
            f = f.dropna()

            rc_list = [np.nan for _ in range(len(self.model_features)) ]
            if f.shape[0] == 0:
                final_rc_list.append(rc_list)
                continue
                
            m = f.merge(self.business_rules, left_on=['Features'], right_on=['Features'], how='left')
            m2 = m[m.apply(lambda x: eval(str(x['Values']) + x['Range']) , axis=1)].copy()
            m2 = m2[m2['Final Points'] != 0]
            l1 = m2.sort_values('Final Points', ascending=False)['Description'].tolist()
            rc_list[:len(l1)] = l1
            final_rc_list.append(rc_list)
            
        # Create final output dataframe
        topn_rc_df = pd.DataFrame(final_rc_list)
        topn_rc_df.index = model_data.index
        n_cols = min(self.topn, len(topn_rc_df.columns))
        topn_rc_df = topn_rc_df[[i for i in range(n_cols)]]
        
        out_cols = [self.out_col_pref + str(i+1) for i in range(self.topn)] 
        result = pd.DataFrame(columns=out_cols,
                              index=model_data.index)
        
        fill_cols = [self.out_col_pref + str(i+1) for i in range(topn_rc_df.shape[1])]
        result[fill_cols] = topn_rc_df.copy()
        
        return result


class ShapReasonCodes:
    
    def __init__(self, model, model_features, business_rules=None, topn=5, logger=None, out_col_pref='top_'):
        self.model = model
        self.model_features = model_features
        self.business_rules = business_rules
        self.topn = topn
        self.out_col_pref = out_col_pref
        
        self.shap_explainer = shap.TreeExplainer(model, random_state=12345)
        
    def apply(self, model_data):
        
        model_data = model_data[self.model_features].fillna(0)
                        
        # Step 1: Get Shap Values
        shap_vals = self.shap_explainer.shap_values(model_data)
        if 'IsolationForest' in  str(self.model):
            shap_vals = -1 * shap_vals
        
        if isinstance(shap_vals, list):
            shap_vals = shap_vals[1]
            
        shap_values_df = pd.DataFrame(shap_vals)
        shap_values_df.columns = self.model_features
        shap_values_df.index = model_data.index
                
        # Step 2: Order the features as per Shap values
        def order_and_keep_gt0(x):
            x1 = x.copy()
            ordered_rc_list = x1[x1>0].sort_values(ascending=False).index.tolist()
            nfill_empty = len(self.model_features) - len(ordered_rc_list) 
            ordered_rc_list = ordered_rc_list + [np.nan for _ in range(nfill_empty)]
            return pd.Series(ordered_rc_list)
            
        top_rc = shap_values_df.apply(order_and_keep_gt0, axis=1)
        top_rc.index = shap_values_df.index

        ordered_rc = top_rc.copy()
        ordered_rc.index = model_data.index
        
        if self.business_rules is None:
            result = pd.DataFrame(columns=[self.out_col_pref + str(i+1) for i in range(self.topn)],
                                 index=model_data.index)
            n_cols = min(self.topn, ordered_rc.shape[1])
            fill_cols = [self.out_col_pref + str(i+1) for i in range(n_cols)]
            result[fill_cols] = ordered_rc[[c for j, c in enumerate(ordered_rc.columns)  if j<n_cols]].copy()
            return result
            
        # For each index, create feature value mapping dictionary
        index_features_dict = model_data.T.to_dict()
        
        ordered_rc_feat_values = ordered_rc.apply(lambda x: x.map(index_features_dict[x.name]), axis=1)
        
        # Step 4: Map the Features to Corresponding Descriptions using business rules file
        final_rc_list = []
        for rc_rows, rc_val_rows in zip(ordered_rc.iterrows(), ordered_rc_feat_values.iterrows()):
            r1 = rc_rows[1]
            r2 = rc_val_rows[1]
            f = pd.concat([r1, r2], axis=1)
            f.columns = ['Features', 'Values']
            f = f.dropna()

            rc_list = [np.nan for _ in range(len(self.model_features)) ]
            if f.shape[0] == 0:
                final_rc_list.append(rc_list)
                continue
                
            m = f.merge(self.business_rules, left_on=['Features'], right_on=['Features'], how='left')
            m2 = m[m.apply(lambda x: eval(str(x['Values']) + x['Range']) , axis=1)].copy()
            m2 = m2[m2['Final Points'] != 0]
            l1 = m2.sort_values('Final Points', ascending=False)['Description'].tolist()
            rc_list[:len(l1)] = l1
            final_rc_list.append(rc_list)
            
        # Create final output dataframe
        topn_rc_df = pd.DataFrame(final_rc_list)
        topn_rc_df.index = model_data.index
        n_cols = min(self.topn, len(topn_rc_df.columns))
        topn_rc_df = topn_rc_df[[i for i in range(n_cols)]]
        
        out_cols = [self.out_col_pref + str(i+1) for i in range(self.topn)] 
        result = pd.DataFrame(columns=out_cols,
                              index=model_data.index)
        
        fill_cols = [self.out_col_pref + str(i+1) for i in range(topn_rc_df.shape[1])]
        result[fill_cols] = topn_rc_df.copy()
        
        return result
    

    
class BusinessRulesReasonCodes:
    
    def __init__(self, model_features, business_rules=None, topn=5, logger=None, out_col_pref='top_'):
        self.model_features = model_features
        self.business_rules = business_rules
        self.topn = topn
        self.out_col_pref = out_col_pref
        
        
    def apply(self, model_data):
        
        model_data = model_data[self.model_features].fillna(0)
                        
        # Step 1: Get Shap Values
        shap_vals =np.ones_like(model_data)  #self.shap_explainer.shap_values(model_data)
        
        shap_values_df = pd.DataFrame(shap_vals)
        shap_values_df.columns = self.model_features
        shap_values_df.index = model_data.index
                
        # Step 2: Order the features as per Shap values
        def order_and_keep_gt0(x):
            x1 = x.copy()
            ordered_rc_list = x1[x1>0].sort_values(ascending=False).index.tolist()
            nfill_empty = len(self.model_features) - len(ordered_rc_list) 
            ordered_rc_list = ordered_rc_list + [np.nan for _ in range(nfill_empty)]
            return pd.Series(ordered_rc_list)
            
        top_rc = shap_values_df.apply(order_and_keep_gt0, axis=1)
        top_rc.index = shap_values_df.index
#             lambda x: pd.Series(x.sort_values(ascending=False).index.tolist()), axis=1)
        
        # Step 3: Remove features with negative Shap values
#         keep_mask = shap_values_df > 0
#         keep_mask.columns = top_rc.columns
        
#         ordered_rc = top_rc.where(keep_mask).apply(lambda x: pd.Series(x.dropna().tolist()), axis=1 )
        ordered_rc = top_rc.copy()
        ordered_rc.index = model_data.index
        
        if self.business_rules is None:
            result = pd.DataFrame(columns=[self.out_col_pref + str(i+1) for i in range(self.topn)],
                                 index=model_data.index)
            n_cols = min(self.topn, ordered_rc.shape[1])
            fill_cols = [self.out_col_pref + str(i+1) for i in range(n_cols)]
            result[fill_cols] = ordered_rc[[c for j, c in enumerate(ordered_rc.columns)  if j<n_cols]].copy()
            return result
            
        # For each index, create feature value mapping dictionary
        index_features_dict = model_data.T.to_dict()
        
        ordered_rc_feat_values = ordered_rc.apply(lambda x: x.map(index_features_dict[x.name]), axis=1)
        
        # Step 4: Map the Features to Corresponding Descriptions using business rules file
        final_rc_list = []
        rc_score_list = []
        for rc_rows, rc_val_rows in zip(ordered_rc.iterrows(), ordered_rc_feat_values.iterrows()):
            r1 = rc_rows[1]
            r2 = rc_val_rows[1]
            f = pd.concat([r1, r2], axis=1)
            f.columns = ['Features', 'Values']
            f = f.dropna()

            rc_list = [np.nan for _ in range(len(self.model_features)) ]
            if f.shape[0] == 0:
                final_rc_list.append(rc_list)
                rc_score_list.append(0)
                continue
                
            m = f.merge(self.business_rules, left_on=['Features'], right_on=['Features'], how='left')
            m2 = m[m.apply(lambda x: eval(str(x['Values']) + x['Range']) , axis=1)].copy()
            m2 = m2[m2['Final Points'] != 0]
            rc_score = m2['Final Points'].sum()
            l1 = m2.sort_values('Final Points', ascending=False)['Description'].tolist()
            rc_list[:len(l1)] = l1
            final_rc_list.append(rc_list)
            rc_score_list.append(rc_score)
            
        # Create final output dataframe
        topn_rc_df = pd.DataFrame(final_rc_list)
        rc_score_ser = pd.Series(rc_score_list)
        rc_score_ser.index = model_data.index
        rc_score_ser.name = 'RC_Score'
        
        topn_rc_df.index = model_data.index
        n_cols = min(self.topn, len(topn_rc_df.columns))
        topn_rc_df = topn_rc_df[[i for i in range(n_cols)]]
        
        out_cols = [self.out_col_pref + str(i+1) for i in range(self.topn)] 
        result = pd.DataFrame(columns=out_cols,
                              index=model_data.index)
        
        fill_cols = [self.out_col_pref + str(i+1) for i in range(topn_rc_df.shape[1])]
        result[fill_cols] = topn_rc_df.copy()
        
        result = pd.concat([result, rc_score_ser], axis=1)
        
        return result
        



if __name__ == '__main__':
    
    model_features = ['F1', 'F2', 'F3']
    model_data = pd.DataFrame({"F1": [1, 2, 3, 4], 
                               "F2": [1, 0, 1, 0],
                               "F3": [0, 0, 0, 1]})
    
    shap_values_df = pd.DataFrame({'F1': [0.11, -0.05, 0.3, 0.1],
                                   'F2': [0.1, 0.2, -0.5, 0.1],
                                   'F3': [0.6, -0.2, 0.2, -0.2],
                                  })
    business_rules = pd.DataFrame({'Features': ['F1', 'F1', 'F2', 'F3'],
                                   'Description': ['F1<2', "F1>= 2", 'Feat F2', 'Feat F3'],
                                   'Range': ["<2", ">=2", "==1", "==1"],
                                   'Final Points': [0, 1, 1, 2]})
    
    obj = ShapReasonCodes(model=None, model_features=model_features, business_rules=business_rules)
    
    result = obj.apply(model_data)
    
    pdb.set_trace()
    
    
    
    