import React, { useState } from 'react';
import { makeStyles } from "@material-ui/core/styles";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import { Typography } from "@material-ui/core";
import ShapeFeatureImportance from './ShapeFeatureImportance';
import LiftChart from './LiftChart';
import AnamolyCharts from '../../assets/images/AnamolyCharts.png'
import CardMedia from "@material-ui/core/CardMedia"

import Grid from "@material-ui/core/Grid";

// import './Filters.css'
import NavBar from '../../Components/Navbar_configuration'

import MainNavBar from '../../Components/MainNavBar';
// import Configuration_navbar from '../../Components/Configuration_navbar'

// import Rule_navbar from '../../Components/Rule_navbar'
import Configuration_navbar from '../../Components/Configuration_navbar';

function AnamolyPage() {

  
  const useStyles = makeStyles(theme => ({
   
    cardMediaStyle: {
      paddingTop: "40%",
      // width:'250px'
    }
  }));
  const styles = useStyles();

  return (
  
    <div className='divalign'>
         <MainNavBar/>
      <Configuration_navbar/>
     
            <NavBar/>
            <br/>
    
      {/* <div>
        <br/>
      <Filters/>
      </div>
      <br/> */}
      <Grid  container spacing={1}>
      <Grid item xs = {6} > 
        <Paper elevation={3} className="a" >
        <CardActions>
          <Typography variant="h6" color="primary">
         Model Lift Chart
          </Typography>
        </CardActions>
        <CardContent>
        <LiftChart/>
        </CardContent>
        </Paper>
        </Grid>
       
      
       
        <Grid item xs = {6} > 
        <Paper elevation={3} className="a" >
        <CardActions>
          <Typography variant="h6" color="primary">
          Shap Feature Importance
          </Typography>
        </CardActions>
        <CardContent>
        <ShapeFeatureImportance/>
        </CardContent>
        </Paper>
        </Grid>
       
        </Grid>

        <Grid  > 
        <Paper elevation={3} className="a" >
        <CardActions>
          <Typography variant="h6" color="primary">
        
          </Typography>
        </CardActions>
        <CardContent>
        <CardMedia
           className={styles.cardMediaStyle}    
              image={AnamolyCharts}

/>
        </CardContent>
        </Paper>
        </Grid>
      </div>
    
    
  )
}

export default AnamolyPage
