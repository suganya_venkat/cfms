import React, { useRef, useState, useEffect } from "react";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import Paper from "@material-ui/core/Paper";
import data from './AnamolyPageData.json'


// import "./linechart_paper.css";

am4core.useTheme(am4themes_animated);

const CHART_ID = "shape_chart_anamoly";

// const fetchURL = "http://localhost:5000/insights/actualforecast_chart";

function ShapeFeatureImportance() {
  const chartRef = useRef(null);
//   const [items, setItems] = useState([]);

  // console.log(data["Shap-feature-importance"][0]["Features:"])
  useEffect(() => {
    chartRef.current = am4core.create(CHART_ID, am4charts.XYChart);
    chartRef.current.data = data["Shap-feature-importance"]

    // chartRef.current.padding(60, 0, 60, 0);

    var categoryAxis = chartRef.current.yAxes.push(new am4charts.CategoryAxis());
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.dataFields.category = "Features:";
    // categoryAxis.renderer.minGridDistance = 1;
    categoryAxis.renderer.inversed = true;
    categoryAxis.renderer.labels.template.fontSize = 12;
    categoryAxis.renderer.minGridDistance = 20;

categoryAxis.renderer.cellStartLocation = 0;
categoryAxis.renderer.cellEndLocation = 1;
categoryAxis.title.text = "Shap Variable";
categoryAxis.title.fontSize = 12;


    // categoryAxis.renderer.grid.template.disabled = true;
    
    var valueAxis = chartRef.current.xAxes.push(new am4charts.ValueAxis());
    valueAxis.min = 0;
     valueAxis.renderer.grid.template.disabled = true;
   valueAxis.renderer.labels.template.fontSize = 12;
   valueAxis.title.text = "Shap Value";
   valueAxis.title.fontSize = 12;


    var series = chartRef.current.series.push(new am4charts.ColumnSeries());
    // series.columns.template.width = am4core.percent(80);
    series.dataFields.categoryY = "Features:";
    series.dataFields.valueX = "LiftChg";
    // series.tooltipText = "{valueX.value}"
    
    series.columns.template.strokeOpacity = 0;
    series.columns.template.tooltipText = "[#fff font-size: 15px] {categoryY}:\n[/][#fff font-size: 15px]{valueX}[/] [#fff]{additional}[/]";

    // series.columns.template.column.cornerRadiusBottomRight = 5;
    // series.columns.template.column.cornerRadiusBottomRight = 5;
    // series.columns.template.column.cornerRadiusTopRight = 5;
    
    var labelBullet = series.bullets.push(new am4charts.LabelBullet())
    // labelBullet.label.horizontalCenter = "left";
    // labelBullet.label.dx = 10;
    // labelBullet.label.text = "{values.valueX.workingValue}";
    // labelBullet.label.fontSize = 10;
    // labelBullet.locationX = 1;
    
    // as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
    // series.columns.template.adapter.add("fill", function(fill, target){
    //   return chartRef.current.colors.getIndex(target.dataItem.index);
    // });
    
    categoryAxis.sortBySeries = series;
    chartRef.current.exporting.menu = new am4core.ExportMenu();
          chartRef.current.exporting.menu.align = "right";
          chartRef.current.exporting.menu.verticalAlign = "top";

})
  return (
    <div
      id={CHART_ID}
      style={{ width: "100%", height: "300px", margin: "50px 0" }}
    >
      {" "}
    </div>
  );
}

export default ShapeFeatureImportance;
