import React, { useRef, useState, useEffect } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { makeStyles, withStyles } from "@material-ui/core/styles";
import DeleteIcon from '@mui/icons-material/Delete';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import TablePagination from '@mui/material/TablePagination';

import './RuleBank.css'

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  // opacity: 0.5,
  // bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

var updatedData;
var getname = {};
var ruledescriptionvalue = '';
function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
];



export default function RuleBankPage() {

  const [data,setData]=useState([]);
     const [data1,setData1]=useState([]);
     const [Comment, setComment] = useState("No");
    
//     console.log(data1.data)
    
    
        useEffect(() => {
    fetch('/rule_bank').then(res => res.json()).then((data) => setData(data.data));
    },[Comment]);
    
    console.log(data)
    
//   useEffect(() => {
//    setData(rows);
//     },[]);
    const [open, setOpen] = React.useState(false);
  const handleOpen = (ruledescription,index) => 
  {setOpen(true);
    ruledescriptionvalue = ruledescription;
    getname = {"rule_description":ruledescription}

  }
  const DeleteRow = () =>{
    // updatedData = data.filter(row1 => !row1.RuleNumber.includes(getname))



    // setData(updatedData)
    setOpen(false);

    fetch('/delete_rules', {  // Enter your IP address here

  method: 'POST', 
  headers: { "Content-Type": "application/json" },
//   mode: 'cors', 
  body: JSON.stringify(getname) // body data type must match "Content-Type" header

}

)

setComment(getname)

    // console.log(data.data.filter(row => console.log(row.RuleNumber)))
  //   console.log(getname)
  //  console.log(updatedData)
  }
  const handleClose = () => {
    setOpen(false);
  };

    const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
 

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
    
  return (
    <div>
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Rule Number</TableCell>
            <TableCell align="center">Rule Description</TableCell>
            <TableCell align="center">Date created</TableCell>
            <TableCell align="center">Alert</TableCell>
            <TableCell align="center">Investigation</TableCell>
            <TableCell align="center">Fraud</TableCell>
            <TableCell align="center">$ Savings</TableCell>
             <TableCell align="left">Delete</TableCell>

          </TableRow>
        </TableHead>
        <TableBody>
          {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row,index) => (

            
            <TableRow
              key={index}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
             
            
              <TableCell component="th" scope="row">
                {row.RuleNumber}
              </TableCell>
              <TableCell align="center">{row.RuleDescription}</TableCell>
              <TableCell align="center">{row.DateCreated}</TableCell>
              <TableCell align="center">{row.Alert}</TableCell>
              <TableCell align="center">{row.Investigation}</TableCell>
              <TableCell align="center">{row.Frauds}</TableCell>
              <TableCell align="center">{row.Savings.toFixed(2)}</TableCell>

              <TableCell align="left"><DeleteIcon className='icon' onClick={(e) => {handleOpen(row.RuleDescription,index);}} /></TableCell>
              <Dialog 
              // style={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
  // overlayStyle={{backgroundColor: 'transparent'}}
  BackdropProps={{ style: { backgroundColor: "transparent" } }}
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        {/* <DialogTitle id="alert-dialog-title">
          {"Use Google's location service?"}
        </DialogTitle> */}
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
           Are u want to delete the rule?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={(e) => {DeleteRow();}} >Yes</Button>
          <Button onClick={handleClose} autoFocus>
            No
          </Button>
        </DialogActions>
      </Dialog>
              {/* <TableCell align="right"><DeleteIcon onClick={(e) => {DeleteRow(row.name);}}  /></TableCell> */}
            </TableRow>
            
          ))}
        </TableBody>
      </Table>
  <TablePagination
        rowsPerPageOptions={[5,10, 25, 100]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </TableContainer>
    
     </div>
  );
}
