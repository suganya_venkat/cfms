import React, { useRef, useState, useEffect } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import MaterialTableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormHelperText from '@mui/material/FormHelperText';
import Rule_navbar from '../../Components/Rule_navbar'
import RuleFilters from './RuleFilters'
import { makeStyles, withStyles } from "@material-ui/core/styles";
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Alert from '@mui/material/Alert';
import RuleBank from '../RuleBank/RuleBank'
import { Link } from 'react-router-dom';
import './Rules.css'

import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
const TableCell = withStyles({
  root: {
    borderBottom: "none" 
  }
})(MaterialTableCell);

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto"
  },
  table: {
    minWidth: 650
  }
}));



var RuleVariable1=[]
var RuleVariable2= []
var RuleVariable3= []
var RuleOperator1=[]
var RuleOperator2=[]
var RuleOperator3=[]
var RuleValue1=[]
var RuleValue2=[]
var RuleValue3=[]
// var FinalData =[]
var FinalDataToBePassed1=[]
var FinalDataToBePassed
var error_message = "Please Enter All values"
export default function CreateRule() {

   
    const [variable, setVariable] = React.useState('');
    const [operator, setOperator] = React.useState('');
    const [Relation, setRelation] = React.useState('');
    const [operator1, setOperator1] = React.useState('');
    const [variable1, setVariable1] = React.useState('');
    const [operator2, setOperator2] = React.useState('');
    const [variable2, setVariable2] = React.useState('');
    const [value, setvalue] = React.useState('');
    const [value1, setvalue1] = React.useState('');
    const [value2, setvalue2] = React.useState('');
      const [save, setSave] =  React.useState("");
      const [FinalData, setFinalData] = React.useState([])
      const [buttonDisabled, setbuttonDisabled] = React.useState(false);
      const [isStart, setIsStart] = useState(false);
    





  const variableSelected = (event) => {
    setVariable(event.target.value);
    RuleVariable1.push(event.target.value);
    event.preventDefault();
    // FinalData.push({"rule1":{"Variable": RuleVariable1.toString()}})
  };
  
  // console.log(variable)
  

  const variableSelected1 = (event) => {
    setVariable1(event.target.value);
    RuleVariable2.push(event.target.value);
    event.preventDefault();
    // FinalData.push({"rule2":{"Variable": RuleVariable2.toString()}})
  };

  const variableSelected2 = (event) => {
    setVariable2(event.target.value);
    RuleVariable3.push(event.target.value);
    event.preventDefault();
    // FinalData.push({"rule2":{"Variable": RuleVariable2.toString()}})
  };

  

  const operatorSelected = (event) => {
    setOperator(event.target.value);
    RuleOperator1.push(event.target.value);
    event.preventDefault();
    // FinalData.push({"rule1":{"Operator": RuleOperator1.toString()}})
  };

  
  

  const operatorSelected1 = (event) => {
    setOperator1(event.target.value);
    RuleOperator2.push(event.target.value);
    event.preventDefault();
  };

  const operatorSelected2 = (event) => {
    setOperator2(event.target.value);
    RuleOperator3.push(event.target.value);
    event.preventDefault();
  };

  const relationSelected = (event) => {
    setRelation(event.target.value);
  };

  const valueSelected = (event) => {
    setvalue(event.target.value);
    RuleValue1.push(event.target.value)
      
      ;
      event.preventDefault();
    // FinalData.push({"rule1":{"Value": RuleValue1.toString()}})

  };
 
  const valueSelected1 = (event) => {
    setvalue1(event.target.value);
    RuleValue2.push(event.target.value)
    event.preventDefault();
  };

  const valueSelected2 = (event) => {
    setvalue2(event.target.value);
    RuleValue3.push(event.target.value)
    event.preventDefault();
  };
  // console.log(RuleValue1[RuleValue1.length -1])



  useEffect(() => {
    if(isStart)
    {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(FinalData),
    };

    fetch('/add_rules', requestOptions).then((res) =>
      setSave(res.status),
       setbuttonDisabled(true)  , 
      
                
    );}
    },[isStart],[]);

  const passingData = () =>{

    

    if(variable, operator, value){
      
    FinalData.push({"rule1":{"Operator": RuleOperator1.toString(),
    "Variable": RuleVariable1.toString(), "Value": RuleValue1[RuleValue1.length-1].toString()
    
  }
  
  }
  
  )
  RuleOperator1 = []
  RuleVariable1=[]
  RuleValue1=[]
}
  if(variable1, operator1, value1){
    FinalData.push({"rule2":{"Operator": RuleOperator2.toString(),
  "Variable": RuleVariable2.toString(), "Value": RuleValue2[RuleValue2.length-1].toString()
}} )
  
RuleOperator2=[]
RuleVariable2=[]
RuleValue2=[]
  
}

if(variable2, operator2, value2){
  FinalData.push({"rule3":{"Operator": RuleOperator3.toString(),
"Variable": RuleVariable3.toString(), "Value": RuleValue3[RuleValue3.length-1].toString()
}} )

RuleOperator3=[]
RuleVariable3=[]
RuleValue3=[]
}

console.log(FinalData)
// fetch('/add_rules', {  // Enter your IP address here

//   method: 'POST', 
//   headers: { "Content-Type": "application/json" },
// //   mode: 'cors', 
//   body: JSON.stringify(FinalData) // body data type must match "Content-Type" header

// })

setIsStart(true)
    
   
//       save === 200 ? setbuttonDisabled(true) : setbuttonDisabled(false)
 
  };
      
  
  
  



  // console.log(RuleValue2[RuleValue2.length -1])
   
//   if( variable, operator, value){
//   FinalData.push({"rule1":{"Operator": RuleOperator1.toString(),
//   "Variable": RuleVariable1.toString(), "Value": RuleValue1[RuleValue1.length-1].toString()
  
// }

// }

// )
// // FinalDataToBePassed1.push(FinalData)
//   }

//   if( variable1, operator1, value1){
//     FinalData.push( {"rule2":{"Operator": RuleOperator2.toString(),
//   "Variable": RuleVariable2.toString(), "Value": RuleValue2[RuleValue2.length-1].toString()
// }})
// // FinalDataToBePassed1.push(FinalData)
// }


// console.log(FinalData)

// if( variable, operator, value){
// const uniq = new Set(FinalData.map(e => JSON.stringify(e)));
// FinalDataToBePassed = Array.from(uniq).map(e => JSON.parse(e));
// }
// if( variable1, operator1, value1){
//   const uniq = new Set(FinalData.map(e => JSON.stringify(e)));
//   FinalDataToBePassed = Array.from(uniq).map(e => JSON.parse(e));
//   }


// if(FinalDataToBePassed){
// console.log(FinalDataToBePassed[FinalDataToBePassed.length-1])
// FinalDataToBePassed1.push(FinalData)}
 
  return (
      <div>
           <Rule_navbar/>
<br/>

    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow  style={{
                    height: (20) 
                  }}>
            <TableCell style={{borderBottom: "none"}}>Variables</TableCell>
            <TableCell align="center" style={{borderBottom: "none"}}>Operator</TableCell>
            <TableCell align="center" style={{borderBottom: "none"}}>Value</TableCell>
            <TableCell align="center" style={{borderBottom: "none"}}>Relation</TableCell>
            {/* <TableCell align="center" style={{borderBottom: "none"}}>Actions</TableCell> */}
         
          </TableRow>
        </TableHead>
        <TableBody>
         
            <TableRow
              // key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row" style={{borderBottom: "none"}}>
              <FormControl sx={{ m: 1, minWidth: 120 }}>
        <InputLabel id="Variables">Variables</InputLabel>
        <Select
          labelId="Variables"
          id="Variable"
          required
          value={variable}
          label="Variables"
          onChange={variableSelected}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={"Claims Amount"}>Claims Amount</MenuItem>
          <MenuItem value={"Claims Age"}>Claims Age</MenuItem>
          
        </Select>
        {/* <FormHelperText>With label + helper text</FormHelperText> */}
      </FormControl>
              </TableCell>
              <TableCell align="center" style={{borderBottom: "none"}}>
              <FormControl sx={{ m: 1, minWidth: 120 }}>
        <InputLabel id="Operator">Operator</InputLabel>
        <Select
          required
          labelId="Operator"
          id="Operator"
          value={operator}
          label="Operator"
          onChange={operatorSelected}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={">"}>Greater than</MenuItem>
          <MenuItem value={"<"}>Lesser Than</MenuItem>
          <MenuItem value={">="}>Greater than or equal to</MenuItem>
          <MenuItem value={"<="}>Lesser than or equal to</MenuItem>
          <MenuItem value={"="}>Equal to</MenuItem>

        </Select>
        {/* <FormHelperText>With label + helper text</FormHelperText> */}
      </FormControl>
      
                  
                  </TableCell>
              <TableCell align="center" style={{borderBottom: "none"}}>
              <Box
      component="form"
      sx={{
        '& > :not(style)': { m: 1, width: '25ch' },
      }}
      noValidate
      autoComplete="off"
    >
              <TextField id="Value" label="Enter Value"
              variant="outlined"
              required
              name="Value"
              label="Value"
              type="Value"
              // autoFocus
              value={value}
              // onChange={(event) => {setPassword(event.target.value)}} //whenever the text field change, you save the value in state
              onChange={valueSelected}
              />

     </Box>
                  
                  </TableCell >
              <TableCell align="center" style={{borderBottom: "none"}} rowSpan={2} >
              <FormControl sx={{ m: 1, minWidth: 120 }}>
        <InputLabel id="Relation">Relation</InputLabel>
        <Select
          labelId="Relation"
          id="Relation"
          value={Relation}
          label="Relation"
          onChange={relationSelected}
          // required
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={10}>AND</MenuItem>
          <MenuItem value={20}>OR</MenuItem>
          
        </Select>
        {/* <FormHelperText>With label + helper text</FormHelperText> */}
      </FormControl>
                  </TableCell>
                  {/* <TableCell align="center" style={{borderBottom: "none"}} rowSpan={2}>
              <DeleteOutlineIcon/>
                  </TableCell> */}
            
            </TableRow>


           
      
         
            <TableRow
              // key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row" style={{borderBottom: "none"}}>
              <FormControl sx={{ m: 1, minWidth: 120 }}>
        <InputLabel id="Variables1">Variables</InputLabel>
        <Select
          required
          labelId="Variables1"
          id="Variable1"
          value={variable1}
          label="Variables1"
          onChange={variableSelected1}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={"Claims Amount"}>Claims Amount</MenuItem>
          <MenuItem value={"Claims Age"}>Claims Age</MenuItem>
          
        </Select>
        {/* <FormHelperText>With label + helper text</FormHelperText> */}
      </FormControl>
              </TableCell>
              <TableCell align="center" style={{borderBottom: "none"}}>
              <FormControl sx={{ m: 1, minWidth: 120 }}>
        <InputLabel id="Operator1">Operator</InputLabel>
        <Select
          required
          labelId="Operator1"
          id="Operator1"
          value={operator1}
          label="Operator1"
          onChange={operatorSelected1}
         
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={">"}>Greater than</MenuItem>
          <MenuItem value={"<"}>Lesser Than</MenuItem>
          <MenuItem value={">="}>Greater than or equal to</MenuItem>
          <MenuItem value={"<="}>Lesser than or equal to</MenuItem>
          <MenuItem value={"="}>Equal to</MenuItem>

        </Select>
        {/* <FormHelperText>With label + helper text</FormHelperText> */}
      </FormControl>
      
                  
                  </TableCell>
              <TableCell align="center" style={{borderBottom: "none"}}>
              <Box
      component="form"
      sx={{
        '& > :not(style)': { m: 1, width: '25ch' },
      }}
      noValidate
      autoComplete="off"
    >
              <TextField id="Value" label="Enter Value"
              variant="outlined"
              required
              name="Value"
              label="Value"
              type="Value"
              // autoFocus
              value={value1}
              // onChange={(event) => {setPassword(event.target.value)}} //whenever the text field change, you save the value in state
              onChange={valueSelected1}
              // error={value1 === ""}
              // helperText={value1 === "" ? 'Required!' : ' '}
              />

     </Box>
                  
                  </TableCell >
              <TableCell align="center" style={{borderBottom: "none"}} >
             
                  </TableCell>
                  <TableCell align="center" style={{borderBottom: "none"}} >
             
                  </TableCell>
            
            </TableRow>
         <TableRow
              // key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
              <FormControl sx={{ m: 1, minWidth: 120 }}>
        <InputLabel id="Variables2">Variables</InputLabel>
        <Select
          required
          labelId="Variables2"
          id="Variable2"
          value={variable2}
          label="Variables2"
          onChange={variableSelected2}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={"Claims Amount"}>Claims Amount</MenuItem>
          <MenuItem value={"Claims Age"}>Claims Age</MenuItem>
          
        </Select>
        {/* <FormHelperText>With label + helper text</FormHelperText> */}
      </FormControl>
              </TableCell>
              <TableCell align="center" style={{borderBottom: "none"}}>
              <FormControl sx={{ m: 1, minWidth: 120 }}>
        <InputLabel id="Operator2">Operator</InputLabel>
        <Select
          required
          labelId="Operator2"
          id="Operator2"
          value={operator2}
          label="Operator2"
          onChange={operatorSelected2}
         
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
         <MenuItem value={">"}>Greater than</MenuItem>
          <MenuItem value={"<"}>Lesser Than</MenuItem>
          <MenuItem value={">="}>Greater than or equal to</MenuItem>
          <MenuItem value={"<="}>Lesser than or equal to</MenuItem>
          <MenuItem value={"="}>Equal to</MenuItem>

        </Select>
        {/* <FormHelperText>With label + helper text</FormHelperText> */}
      </FormControl>
      
                  
                  </TableCell>
              <TableCell align="center" style={{borderBottom: "none"}}>
              <Box
      component="form"
      sx={{
        '& > :not(style)': { m: 1, width: '25ch' },
      }}
      noValidate
      autoComplete="off"
    >
              <TextField id="Value" label="Enter Value"
              variant="outlined"
              required
              name="Value"
              label="Value"
              type="Value"
              // autoFocus
              value={value2}
              // onChange={(event) => {setPassword(event.target.value)}} //whenever the text field change, you save the value in state
              onChange={valueSelected2}
              // error={value1 === ""}
              // helperText={value1 === "" ? 'Required!' : ' '}
              />

     </Box>
                  
                  </TableCell >
              <TableCell align="center" style={{borderBottom: "none"}} >
             
                  </TableCell>
                  <TableCell align="center" style={{borderBottom: "none"}} >
             
                  </TableCell>
            
            </TableRow>
        </TableBody>
       
      </Table>
      <br/>
    {save === 200 ? <Alert onClose={() => <RuleBank/>} component={Link} to='/RuleBank'>Successfully created the Rule!</Alert>
    : ""}
                      {save != 200 && save ? <Alert severity="error">Rules are not creates!</Alert>
    : ""}
      <div className='button'>
        
   <Button variant="outlined">Test & Save</Button>
<Button variant="contained" variant="outlined" disabled = {buttonDisabled} onClick={passingData}>Move To Rule Bank</Button>

        </div>
        <br/>
    </TableContainer>
   
        </div>
  );
}
