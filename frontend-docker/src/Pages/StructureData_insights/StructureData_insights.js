import React, { useState } from 'react'
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { Typography } from "@material-ui/core";
import Lift from './Lift';
import ShapeMean from './ShapeMean';
import Lift_positive from './Lift_positive';
import ModelDetailsjs from './ModelDetails';
import Filters from '../LandingPage/Filters';
import './ModelDetails.css';
import NavBar from '../../Components/Navbar_configuration'
import MainNavBar from '../../Components/MainNavBar';
// import Configuration_navbar from '../../Components/Configuration_navbar'

// import Rule_navbar from '../../Components/Rule_navbar'
import Configuration_navbar from '../../Components/Configuration_navbar';


// import Filters from './Filters';
function StructureData_insights() {
  const [model_value, setmodel_value] = useState('Model1');

  return (
    <div>
      <MainNavBar/>
      <Configuration_navbar/>
     <NavBar/>
     <br/>
     {/* <Configuration_navbar/> */}
      <p className='st-box'><ModelDetailsjs setmodel_value={setmodel_value} model_value={model_value}/> </p>
      <p className='nd-box'><Filters setmodel_value={setmodel_value} model_value={model_value}/> </p>
    <Grid container>
    <Grid item xs = {6}>
    <Paper elevation={3} className="fraudratio">
    <CardActions>
      <Typography variant="h6" color="primary">
      
      Model Lift Chart
      </Typography>
    </CardActions>
    <CardContent>
    
    <Lift_positive setmodel_value={setmodel_value} model_value={model_value}/>
    </CardContent>
    </Paper>
    </Grid>
    
   
    <Grid item xs = {6} width ="50px"> 
    <Paper elevation={3} className="a" >
    <CardActions>
      <Typography variant="h6" color="primary">
      Shap Mean Feature Importance
      </Typography>
    </CardActions>
    <CardContent>
    <ShapeMean setmodel_value={setmodel_value} model_value={model_value}/>
    </CardContent>
    </Paper>
    </Grid>
    </Grid>
    <Paper elevation={3}>
    <CardActions>
      <Typography variant="h6" color="primary">
      Model Lift Chart with claims data
      </Typography>
    </CardActions>
    <CardContent>
    <Lift setmodel_value={setmodel_value} model_value={model_value}/>
    </CardContent>
    </Paper>

    </div>
    
  )
}

export default StructureData_insights



