import React, { useRef, useState, useEffect } from "react";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import Paper from "@material-ui/core/Paper";
import ShapeData from './ShapeData.json';
import Model2ShapData from './Model2ShapData.json';
import Model3ShapData from './Model3ShapData.json';
import { values } from "lodash";

// import "./linechart_paper.css";

am4core.useTheme(am4themes_animated);

const CHART_ID = "shape_chart";

// const fetchURL = "http://localhost:5000/insights/actualforecast_chart";

function ShapeMean(props) {
  const chartRef = useRef(null);
  const [items, setItems] = useState([]);
  const model_type = props.model_value;

//   const datalevel = props.selectedForecast;
//   const timeframe = props.timeframe;
//   const selectedFilterValues = props.selectedFilterValues;

//   console.log("filters ==> ",selectedFilterValues)

//   useEffect(() => {
  
//     const requestOptions = {
//       method: "POST",
//       headers: { "Content-Type": "application/json" },
//       body: JSON.stringify({
//         markets: ["Market Europe"],
//         technology: ["Microwave"],
//         customers: ["All"],
//         country: ["Spain"],
//         gsd_name: ["All"],
//         customer_team: ["All"],
//         datalevel: datalevel,
//       }),
//     };
//     fetch(fetchURL, requestOptions)
//       .then((res) => res.json())
//       .then((data) => setItems(data.actualforecast_chart_data));
//   }, [datalevel]);


//   items.map((a) => {
//   a.lineDash = a.forecast === false ? " " : "5,5";
//     // a.lineColor = a.forecast === false ? "#52C1C8" : "#FC9D5A";
//   });

//   items.map((fy) => {
//     var date = new Date(fy.date);
//     var year = date.getFullYear();
//     fy.fiscalyear_quarter = fy.quarter === 4 ? year - 1 + "-Q"+fy.quarter : year+ "-Q"+fy.quarter; 
//     fy.fiscalyear = fy.quarter === 4 ? year - 1  : year;
//    });

//   var helper = {};
//   var result;
//   var counter = 0;
//   var count = [];

//   if (items) {
//     result = items.reduce(function (r, o) {

//       if (timeframe == "Monthly") {
//         var key = o.date;
//       }
//       else if (timeframe == "Quaterly") {
//         var key = o.fiscalyear_quarter;
//       } else {
//         var key = o.fiscalyear;
//       }
//       if (!helper[key]) {
//         count.push(counter)
//         counter = 1;
//         helper[key] = Object.assign({}, o); // create a copy of o
//         r.push(helper[key]);
//       } 
//       else {
//         helper[key].case_volume += o.case_volume;
//         counter = counter + 1;
//       }

//       return r;
//     }, []);

//     count.push(counter)

//     result.map((res, index) => {
//      res.case_volume = res.case_volume/(count[index+1]);
//     })
//     // console.log(result);

//   } else {
//     result = null;
//   }

  useEffect(() => {
    chartRef.current = am4core.create(CHART_ID, am4charts.XYChart);
    chartRef.current.data = model_type === "Model1" ? ShapeData : model_type === "Model2" ?  Model2ShapData : Model3ShapData;

    // chartRef.current.padding(60, 0, 60, 0);

    var categoryAxis = chartRef.current.yAxes.push(new am4charts.CategoryAxis());
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.dataFields.category = "index";
    // categoryAxis.renderer.minGridDistance = 1;
    categoryAxis.renderer.inversed = true;
    categoryAxis.renderer.labels.template.fontSize = 12;
    categoryAxis.renderer.minGridDistance = 20;

categoryAxis.renderer.cellStartLocation = 0;
categoryAxis.renderer.cellEndLocation = 1;
categoryAxis.title.text = "Shap Variable";
categoryAxis.title.fontSize = 12;


    // categoryAxis.renderer.grid.template.disabled = true;
    
    var valueAxis = chartRef.current.xAxes.push(new am4charts.ValueAxis());
    valueAxis.min = 0;
     valueAxis.renderer.grid.template.disabled = true;
   valueAxis.renderer.labels.template.fontSize = 12;
   valueAxis.title.text = "Shap Value";
   valueAxis.title.fontSize = 12;


    var series = chartRef.current.series.push(new am4charts.ColumnSeries());
    // series.columns.template.width = am4core.percent(80);
    series.dataFields.categoryY = "index";
    series.dataFields.valueX = "values";
    // series.tooltipText = "{valueX.value}"
    
    series.columns.template.strokeOpacity = 0;
    series.columns.template.tooltipText = "[#fff font-size: 15px] {categoryY}:\n[/][#fff font-size: 15px]{valueX}[/] [#fff]{additional}[/]";

    // series.columns.template.column.cornerRadiusBottomRight = 5;
    // series.columns.template.column.cornerRadiusBottomRight = 5;
    // series.columns.template.column.cornerRadiusTopRight = 5;
    
    var labelBullet = series.bullets.push(new am4charts.LabelBullet())
    // labelBullet.label.horizontalCenter = "left";
    // labelBullet.label.dx = 10;
    // labelBullet.label.text = "{values.valueX.workingValue}";
    // labelBullet.label.fontSize = 10;
    // labelBullet.locationX = 1;
    
    // as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
    // series.columns.template.adapter.add("fill", function(fill, target){
    //   return chartRef.current.colors.getIndex(target.dataItem.index);
    // });
    
    categoryAxis.sortBySeries = series;
    chartRef.current.exporting.menu = new am4core.ExportMenu();
          chartRef.current.exporting.menu.align = "right";
          chartRef.current.exporting.menu.verticalAlign = "top";

})
  return (
    <div
      id={CHART_ID}
      style={{ width: "100%", height: "300px", margin: "50px 0" }}
    >
      {" "}
    </div>
  );
}

export default ShapeMean;
