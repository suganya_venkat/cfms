import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import ProductionModelDetails from './ProductionModelDetails.json';
import Model2Details from './Model2Details.json';
import Model3Details from './Model3Details.json';
import  './ModelDetails.css'
import Filters from '../LandingPage/Filters';



const bull = (
  <Box
    component="span"
    sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
  >
    
  </Box>
);

export default function BasicCard(props) {
    // console.log(ProductionModelDetails[0].model_type);
    const model_type = props.model_value;

  return (
     
    <Card sx={{ Width: 20 }}>
      <CardContent>
        <Typography sx={{ fontSize: 14, fontWeight:"bold" }} color="text.secondary" gutterBottom>  
       Model Number: {model_type === "Model1" ? ProductionModelDetails[0].model_number :
       model_type === "Model2" ? Model2Details[0].model_number : Model3Details[0].model_number  } Model Type: {model_type === "Model1" ? ProductionModelDetails[0].model_type :
       model_type === "Model2" ? Model2Details[0].model_type : Model3Details[0].model_type  }

       <br/>Tree Depth: {model_type === "Model1" ? ProductionModelDetails[0].max_depth :
       model_type === "Model2" ? Model2Details[0].max_depth : Model3Details[0].max_depth  }  Learning Rate: {model_type === "Model1" ? ProductionModelDetails[0].learning_rate:
       model_type === "Model2" ? Model2Details[0].learning_rate: Model3Details[0].learning_rate } n_estimators: {model_type === "Model1" ? ProductionModelDetails[0].n_estimates:
       model_type === "Model2" ? Model2Details[0].n_estimates: Model3Details[0].n_estimates } Model Lift: {model_type === "Model1" ? ProductionModelDetails[0].model_lift:
       model_type === "Model2" ? Model2Details[0].model_lift: Model3Details[0].model_lift }  
       
        </Typography>
       
      </CardContent>
      
    </Card>
    
  );
}
