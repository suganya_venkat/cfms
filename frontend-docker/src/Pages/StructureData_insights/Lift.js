import React, { useRef, useState, useEffect } from "react";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import Paper from "@material-ui/core/Paper";
import LiftData from './LiftData.json';
import LiftDataModel2 from './LiftDataModel2.json';
import LiftDataModel3 from './LiftDataModel3.json';


// import "./linechart_paper.css";

am4core.useTheme(am4themes_animated);

const CHART_ID = "lift_chart";

// const fetchURL = "http://localhost:5000/insights/actualforecast_chart";

function Lift(props) {
  const chartRef = useRef(null);
  const [items, setItems] = useState([]);
  const model_type = props.model_value;
  console.log(model_type)
//   const datalevel = props.selectedForecast;
//   const timeframe = props.timeframe;
//   const selectedFilterValues = props.selectedFilterValues;

//   console.log("filters ==> ",selectedFilterValues)

//   useEffect(() => {
  
//     const requestOptions = {
//       method: "POST",
//       headers: { "Content-Type": "application/json" },
//       body: JSON.stringify({
//         markets: ["Market Europe"],
//         technology: ["Microwave"],
//         customers: ["All"],
//         country: ["Spain"],
//         gsd_name: ["All"],
//         customer_team: ["All"],
//         datalevel: datalevel,
//       }),
//     };
//     fetch(fetchURL, requestOptions)
//       .then((res) => res.json())
//       .then((data) => setItems(data.actualforecast_chart_data));
//   }, [datalevel]);


//   items.map((a) => {
//   a.lineDash = a.forecast === false ? " " : "5,5";
//     // a.lineColor = a.forecast === false ? "#52C1C8" : "#FC9D5A";
//   });

//   items.map((fy) => {
//     var date = new Date(fy.date);
//     var year = date.getFullYear();
//     fy.fiscalyear_quarter = fy.quarter === 4 ? year - 1 + "-Q"+fy.quarter : year+ "-Q"+fy.quarter; 
//     fy.fiscalyear = fy.quarter === 4 ? year - 1  : year;
//    });

//   var helper = {};
//   var result;
//   var counter = 0;
//   var count = [];

//   if (items) {
//     result = items.reduce(function (r, o) {

//       if (timeframe == "Monthly") {
//         var key = o.date;
//       }
//       else if (timeframe == "Quaterly") {
//         var key = o.fiscalyear_quarter;
//       } else {
//         var key = o.fiscalyear;
//       }
//       if (!helper[key]) {
//         count.push(counter)
//         counter = 1;
//         helper[key] = Object.assign({}, o); // create a copy of o
//         r.push(helper[key]);
//       } 
//       else {
//         helper[key].case_volume += o.case_volume;
//         counter = counter + 1;
//       }

//       return r;
//     }, []);

//     count.push(counter)

//     result.map((res, index) => {
//      res.case_volume = res.case_volume/(count[index+1]);
//     })
//     // console.log(result);

//   } else {
//     result = null;
//   }

  useEffect(() => {
    chartRef.current = am4core.create(CHART_ID, am4charts.XYChart);
    chartRef.current.data = model_type === "Model1" ? LiftData : model_type === "Model2" ?  LiftDataModel2 : LiftDataModel3;

    // chartRef.current.colors.list = [
    //   am4core.color("#52C1C8"),
    //   am4core.color("#A0CCEC"),
    // ];

    chartRef.current.colors.step = 2;
    chartRef.current.legend = new am4charts.Legend()
// chartRef.current.legend.position = 'top'
// chartRef.current.legend.paddingBottom = 20
 chartRef.current.legend.fontSize = 12
// chartRef.current.legend.labels.template.maxWidth = 95

var xAxis = chartRef.current.xAxes.push(new am4charts.CategoryAxis())
xAxis.dataFields.category = 'index'
xAxis.renderer.cellStartLocation = 0.1
xAxis.renderer.cellEndLocation = 0.9
xAxis.renderer.grid.template.location = 0;
xAxis.renderer.labels.template.fontSize = 12;
xAxis.tooltip.label.textAlign = "middle";
xAxis.title.text = "QT"
xAxis.title.fontSize = 12;




var yAxis = chartRef.current.yAxes.push(new am4charts.ValueAxis());
yAxis.min = 0;
yAxis.renderer.labels.template.fontSize = 12;
yAxis.tooltip.label.textAlign = "middle";
yAxis.title.text = "Suspicious/Claim value";
yAxis.title.fontSize = 12;




function createSeries(value, name) {
    var series = chartRef.current.series.push(new am4charts.ColumnSeries())
    series.dataFields.valueY = value
    series.dataFields.categoryX = 'index'
    series.name = name
    series.columns.template.tooltipText = "[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 15px]{valueY}[/] [#fff]{additional}[/]";

    // series.events.on("hidden", arrangeColumns);
    // series.events.on("shown", arrangeColumns);

    var bullet = series.bullets.push(new am4charts.LabelBullet())
    // bullet.tooltipText = "[#fff font-size: 15px]{index} in {categoryX}:\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]"

    // bullet.interactionsEnabled = false
    // bullet.dy = 30;
    // bullet.label.text = '{valueY}'
    // bullet.label.fontSize = 12;
    // bullet.label.fill = am4core.color('black')

    return series;
}

// chart.data = [
//     {
//         category: 'Place #1',
//         first: 40,
//         second: 55,
//         third: 60
//     },
//     {
//         category: 'Place #2',
//         first: 30,
//         second: 78,
//         third: 69
//     },
//     {
//         category: 'Place #3',
//         first: 27,
//         second: 40,
//         third: 45
//     },
//     {
//         category: 'Place #4',
//         first: 50,
//         second: 33,
//         third: 22
//     }
// ]


createSeries('Suspicious', 'Suspicious');
createSeries('Non Suspicious', 'Non Suspicious');
// createSeries('third', 'The Third');

    // let dateAxis = chartRef.current.xAxes.push(new am4charts.DateAxis());
    // dateAxis.dataFields.category = "date";
    // dateAxis.dateFormats.setKey("month", "yyyy-MM");
    // dateAxis.periodChangeDateFormats.setKey("month", "yyyy-MM");
    // dateAxis.renderer.grid.template.disabled = true;
    // dateAxis.renderer.labels.template.fill = am4core.color("#949FAF");
    // dateAxis.tooltip.disabled = true;
    // dateAxis.groupData = true;

    // // For aggregating the data
    // if (timeframe === "Yearly") {
    //   dateAxis.groupCount = 12; //for yearly
    //   dateAxis.groupIntervals.setAll([
    //     { timeUnit: "month", count: 1 },
    //     { timeUnit: "year", count: 1 },
    //   ]);
    // }
    // if (timeframe === "Quaterly") {
    //   dateAxis.groupCount = 14; //for quaterly
    //   dateAxis.groupIntervals.setAll([
    //     { timeUnit: "month", count: 1 },
    //     { timeUnit: "month", count: 3 },
    //     { timeUnit: "year", count: 1 },
    //   ]);
    // }

    // dateAxis.gridIntervals.setAll([
    //   { timeUnit: "month", count: 1 },
    //   { timeUnit: "month", count: 3 },
    // ]);

    // let valueAxis = chartRef.current.yAxes.push(new am4charts.ValueAxis());
    // valueAxis.title.text = "Case Vol.";
    // valueAxis.tooltip.disabled = true;
    // valueAxis.renderer.labels.template.fill = am4core.color("#BCC3CE");
    // valueAxis.renderer.grid.template.stroke = am4core.color("#e7eff3");
    // valueAxis.renderer.grid.template.strokeOpacity = 1;
    // // valueAxis.min = 0;
    // // valueAxis.extraMin = 0.2;
    // // valueAxis.extraMax = 0.2;

    // let series = chartRef.current.series.push(new am4charts.LineSeries());
    // series.dataFields.dateX = "date";
    // series.dataFields.valueY = "case_volume";
    // series.strokeWidth = 3;
    // series.name = "Actual";
    // series.tooltipText = "{valueY}";
    // series.propertyFields.strokeDasharray = "lineDash";
    // // series.propertyFields.stroke = "lineColor";
    // // series.propertyFields.fill = "lineColor";

    // let circleBullet = series.bullets.push(new am4charts.CircleBullet());
    // circleBullet.circle.stroke = am4core.color("#fff");
    // circleBullet.circle.strokeWidth = 0;

    // // Add cursor
    // chartRef.current.cursor = new am4charts.XYCursor();
    // chartRef.current.cursor.xAxis = dateAxis;

    // // Add legend
    // chartRef.current.legend = new am4charts.Legend();
    // chartRef.current.legend.position = "bottom";

    // // Add scrollbar
    // chartRef.current.scrollbarX = new am4core.Scrollbar();
    // chartRef.current.scrollbarX.thumb.minWidth = 30;
    // chartRef.current.scrollbarX.parent = chartRef.current.bottomAxesContainer;
    // chartRef.current.scrollbarX.startGrip.disabled = false;
    // chartRef.current.scrollbarX.endGrip.disabled = false;

    // chartRef.current.exporting.menu = new am4core.ExportMenu();
    // chartRef.current.exporting.menu.align = "right";
    // chartRef.current.exporting.menu.verticalAlign = "top";
    chartRef.current.exporting.menu = new am4core.ExportMenu();
    chartRef.current.exporting.menu.align = "right";
    chartRef.current.exporting.menu.verticalAlign = "top";
    return () => {
      chartRef.current && chartRef.current.dispose();
    };
  });

  // Load data into chart

//   useEffect(() => {
//     if (chartRef.current) {
//       chartRef.current.data = result;
//     }
//   }, [items]);

  return (
    <div
      id={CHART_ID}
      style={{ width: "100%", height: "300px", margin: "50px 0" }}
    >
      {" "}
    </div>
  );
}

export default Lift;
