import React from 'react'
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import { Typography } from "@material-ui/core";
import ClaimsAmountTable from './ClaimsAmountTable';
import ClaimsReported from './ClaimsReported';
import Filters from './Filters';
import ClaimsbassedonMake from './ClaimsbasedonMake';
import ClaimsTrend from './ClaimsTrend';
import ClaimsDataByStateTable from './ClaimsDataByStateTable';
import ClaimsCountLast12Month from './ClaimsCountLast12Month'
import FraudRatio from '../Insights/FraudRatio';
import ClaimsByClaimsType from './ClaimsByClaimsType';
import ClaimsAmtByClaimType from './ClaimsAmtByClaimType';
import ClaimsAmtByState from './ClaimsAmtByState';
import ClaimsByState from './ClaimsByState';
import Grid from "@material-ui/core/Grid";
import ClaimsSeries from './ClaimsSeries';
import KpiCards from './KpiCards'
import './Filters.css'
import IncidentBasedClaims from '../Insights/IncidentBasedClaims';
import NavBar from '../../Components/Navbar'
import ManinNavBar from '../../Components/MainNavBar'

function Summary() {
  return (
  
    <div className='divalign'>
      <ManinNavBar/>
        <NavBar/>
      <CardContent>
      <KpiCards/>
      </CardContent>
      {/* <div>
        <br/>
      <Filters/>
      </div>
      <br/> */}
      <Grid  container spacing={1}>
        <Grid item xs = {3}>
        <Paper elevation={3} className="fraudratio">
        <CardActions className="table">
          
          <ClaimsCountLast12Month/>
          {/* <Typography variant="h6" color="primary">
          Claims By Claims Type
          </Typography> */}
        </CardActions>
        <CardContent>
        {/* <Filters/> */}
        
       
        </CardContent>
        </Paper>
        </Grid>
        
       
        <Grid item xs = {3} > 
        <Paper elevation={3} className="a" >
        <CardActions>
          <Typography variant="h6" color="primary">
           Claims By Claims Type
          </Typography>
        </CardActions>
        <CardContent>
        <ClaimsByClaimsType />
        <ClaimsAmtByClaimType /> 
        </CardContent>
        </Paper>
        </Grid>
        <Grid item xs = {3} width ="50px"> 
        <Paper elevation={3} className="a" >
        <CardActions>
          <Typography variant="h6" color="primary">
          Claims By State
          </Typography>
        </CardActions>
        <CardContent>
        <ClaimsByState />
  
  <ClaimsAmtByState />
        </CardContent>
        </Paper>
        </Grid>
        <Grid item xs = {3} width ="50px"> 
        <Paper elevation={3} className="a" >
        {<CardActions className="table"> 
          {/* <Typography variant="h6" color="primary">
           Claimed Amount By Claim Type
          </Typography> */}
           <ClaimsDataByStateTable/>
         </CardActions>}
        <CardContent>
       
       
        </CardContent>
        </Paper>
        </Grid>
        </Grid>
      
      
      
        <Grid container  spacing={1}>
        <Grid item xs = {12}>
        <Paper elevation={3} >
      <CardActions>
        <Typography variant="h6" color="primary">
          Daywise Claim Report
        </Typography>
      </CardActions>
      <CardContent>
      <ClaimsSeries/>
      </CardContent>
      </Paper>
      </Grid>
      </Grid>
      
      
      
      <Grid container  spacing={1}>
        <Grid item xs = {6}>
        <Paper elevation={3} className="fraudratio">
        <CardActions>
          <Typography variant="h6" color="primary">
           Total fraud claims ratio
          </Typography>
        </CardActions>
        <CardContent>
        {/* <Filters/> */}
        <FraudRatio />
       
        </CardContent>
        </Paper>
        </Grid>
        
       
        <Grid item xs = {6} width ="50px"> 
        <Paper elevation={3} className="a" >
        <CardActions>
          <Typography variant="h6" color="primary">
           Claims based on incident type
          </Typography>
        </CardActions>
        <CardContent>
        <IncidentBasedClaims />
        </CardContent>
        </Paper>
        </Grid>
        </Grid>
      </div>
    

    
  )
}

export default Summary
