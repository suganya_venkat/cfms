import  React , {useEffect , useRef} from 'react';
import claimsdata from './ClaimsCountSeries.json';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

am4core.useTheme(am4themes_animated);

const CHART_ID = "ClaimsReport_chart";

function ClaimsTrend(){
    const chartRef = useRef(null);
    useEffect(() => {

      var sample1 = claimsdata
      ? claimsdata.map((fy) => {
        var date = new Date(fy.CLAIMREPORTDATE_);
        var year = date.getFullYear();
        var month = date.getMonth();
        // var datewithtime1 = date.getMinutes
        // fy.fiscalyear_quarter = year + "-Q" + fy.quarter;
        fy.YearMonth = year +"-"+ month;
        fy.Month = month;
        fy.date = date
      })
      : null;

  var helper = {};
  var result;
  var counter = 0;
  var count = [];

  // if (claimsdata) {
  //   result = claimsdata.reduce(function (r, o) {
      
  //       var key = o.YearMonth;
      
  //     if (!helper[key]) {
  //       count.push(counter);
  //       counter = 1;
  //       helper[key] = Object.assign({}, o); // create a copy of o
  //       r.push(helper[key]);
  //     } else {
  //       helper[key].CLAIMREPORTDATE_count += o.CLAIMREPORTDATE_count;
  //       counter = counter + 1;
  //     }
  //     return r;
  //   }, []);

  //   count.push(counter);

  //   // result.map((res, index) => {
  //   //   res.CLAIMREPORTDATE_count = res.CLAIMREPORTDATE_count / count[index + 1];
  //   // });
  // } else {
  //   result = null;
  // }


  console.log(claimsdata[0].date)

        chartRef.current = am4core.create(CHART_ID, am4charts.XYChart);
        chartRef.current.data = claimsdata;
    
        chartRef.current.colors.list = [
          am4core.color("#52C1C8"),
          am4core.color("#A0CCEC"),
        ];

        let dateAxis = chartRef.current.xAxes.push(new am4charts.DateAxis());
        dateAxis.dataFields.category = "date";
        dateAxis.dateFormats.setKey("year", "yyyy");
        dateAxis.periodChangeDateFormats.setKey("year", "yyyy");
        dateAxis.renderer.grid.template.disabled = true;
        dateAxis.renderer.labels.template.fill = am4core.color("black");
        dateAxis.tooltip.disabled = true;
        dateAxis.renderer.labels.template.fontSize = 12;
        // dateAxis.renderer.labels.color = "black";


        // dateAxis.gridIntervals.setAll([
        //     { timeUnit: "month", count: 1 },
        //     { timeUnit: "month", count: 3 },
        //   ]);
      
          let valueAxis = chartRef.current.yAxes.push(new am4charts.ValueAxis());
          valueAxis.title.text = "Claims Reported";
          // valueAxis.title.text.fontSize=12
          valueAxis.tooltip.disabled = true;
          valueAxis.renderer.labels.template.fill = am4core.color("black");
          valueAxis.renderer.grid.template.stroke = am4core.color("#e7eff3");
          valueAxis.renderer.grid.template.strokeOpacity = 1;
          valueAxis.renderer.labels.template.fontSize = 12;



          // valueAxis.min = 0;
          // valueAxis.extraMin = 0.2;
          // valueAxis.extraMax = 0.2;
      
          let series = chartRef.current.series.push(new am4charts.LineSeries());
          series.dataFields.dateX = "date";
          series.dataFields.valueY = "CLAIMREPORTDATE_count";
          series.strokeWidth = 3;
          series.name = "Claims Date";
          // series.name.fontSize=12

          series.tooltipText = "[#fff font-size: 15px] {dateX}:\n[/][#fff font-size: 15px]{valueY}[/] [#fff]{additional}[/]";
          // series.propertyFields.strokeDasharray = "lineDash";
          // series.propertyFields.stroke = "lineColor";
          // series.propertyFields.fill = "lineColor";
          // series.columns.template.tooltipText = "[#fff font-size: 15px] {valueY}:\n[/][#fff font-size: 15px]{dateX}[/] [#fff]{additional}[/]";

      
          let bullet = series.bullets.push(new am4charts.CircleBullet());
          bullet.circle.radius = 4;
          bullet.circle.strokeWidth = 2;
      
          // bullet.adapter.add("fill", function (fill, target) {
          //   if (!target.dataItem) {
          //     return fill;
          //   }
          //   var values = target.dataItem.properties;
      
          //   return values.strokeDasharray === ""
          //     ? am4core.color("#52C1C8")
          //     : am4core.color("#FC9D5A");
          // });
      
          // bullet.adapter.add("stroke", function (stroke, target) {
          //   if (!target.dataItem) {
          //     return stroke;
          //   }
          //   var values = target.dataItem.properties;
      
          //   return values.strokeDasharray === ""
          //     ? am4core.color("#52C1C8")
          //     : am4core.color("#FC9D5A");
          // });
      
      
          // Add cursor
          chartRef.current.cursor = new am4charts.XYCursor();
          chartRef.current.cursor.xAxis = dateAxis;
      
          // Add legend
          chartRef.current.legend = new am4charts.Legend();
          chartRef.current.legend.position = "bottom";
      
          // Add scrollbar
          chartRef.current.scrollbarX = new am4core.Scrollbar();
          chartRef.current.scrollbarX.thumb.minWidth = 30;
          chartRef.current.scrollbarX.parent = chartRef.current.bottomAxesContainer;
          chartRef.current.scrollbarX.startGrip.disabled = false;
          chartRef.current.scrollbarX.endGrip.disabled = false;
      
          chartRef.current.exporting.menu = new am4core.ExportMenu();
          chartRef.current.exporting.menu.align = "right";
          chartRef.current.exporting.menu.verticalAlign = "top";
      
          chartRef.current.events.on("beforedatavalidated", function(ev) {
            chartRef.current.data.sort(function(a, b) {
              return (new Date(a.date)) - (new Date(b.date));
            });
          })
        //   return () => {
        //     chartRef.current && chartRef.current.dispose();
        //   };
        // }, [timeframe]);
      
        // Load data into chart
      
      })
      
        return (
          <div
            id={CHART_ID}
            style={{ width: "100%", height: "400px", margin: "50px 0" }}
          >
            {" "}
          </div>
        );
  
}
export default ClaimsTrend


