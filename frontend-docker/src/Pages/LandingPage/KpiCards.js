import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Data from './KpiCards_histogramData.json'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    border: `1px solid #EEF3F5`,
    borderRadius: 4,
    color: "#949FAF",
    minWidth: 275,
    minHeight: 70,
    marginTop: 12,
    marginLeft: 12,
    marginRight: 12,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    paddingBlock: 20,
    borderRadius: 0.5,
  },
  card: {
    color: "#16448F",
    fontWeight: 700,
    display: "inline-block"
  },
  cardsmall:{
    color: '#5DC9A2',
    fomtWeight: 500,
    display: "inline-block",
    fontSize: 12,
    marginInline: 4 
  }
}));


const fetchURL_data = "http://localhost:5001/kpicards";


export default function KpiCards(props) {
    const classes = useStyles();

    const [data,setData]=useState({});
    
   useEffect(() => {
  fetch('/kpicards').then(res => res.json()).then((data) => setData(data));
  },[]);

    
//     useEffect(() => {
//   fetch(fetchURL_data

//        )
    
// .then((res) => res.json())
//     .then((data) => setData(data));
//     // setLoading(false)
// },[]);



  return (
      
    <div className={classes.root}>
        {}
      {}
{Object.keys(data).length > 0 ? (
         <div>
          <Grid container alignItems="center" spacing={1}>
            <Grid item xs={12} sm={3}>
              <Paper className={classes.paper}>
                {" "}
                Claims Initiated <br /><br />{" "}
                <div className={classes.card}>{data["Claims Initiated"]}</div>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Paper className={classes.paper}>
                {" "}
                Claims in progress <br /><br />{" "}
               
                <div className={classes.card}>
                {data["Claims in Progress"]}
                </div>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Paper className={classes.paper}>
                {" "}
               
                Claims closed <br /><br />{" "}
                <div className={classes.card}> {data["Claims Closed"]}</div>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Paper className={classes.paper}>
                {" "}
               
                Claims paid count <br /><br />{" "}
                <div className={classes.card}>
                  {data["Claims Paid"]["counts"]["Yes"]}
                </div>
              </Paper>
            </Grid>
            </Grid>
            
            <Grid container alignItems="center" spacing={1}>
            <Grid item xs={12} sm={4}>
              <Paper className={classes.paper}>
                {" "}
              
                Average claims amount <br /><br />{" "}
                <div className={classes.card}>
                   
                ${ data["Average Claim Amount"].toFixed(2)}
                </div>
              </Paper>
            </Grid>
           
            
            <Grid item xs={12} sm={4}>
              <Paper className={classes.paper}>
                {" "}
              
               Average paid amount <br /><br />{" "}
                <div className={classes.card}>
                ${data["Average Paid Amount"].toFixed(2)}
                </div>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Paper className={classes.paper}>
                {" "}
               
                Closure ratio <br /><br />{" "}
                <div className={classes.card}>
                {data["Closure ratio"].toFixed(2)}
                </div>
              </Paper>
            </Grid>
          </Grid>
         </div> 
     
   
): "loading"} 
 </div>
  );
}
