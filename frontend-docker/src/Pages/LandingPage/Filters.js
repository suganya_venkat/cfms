import * as React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

export default function Filters(props) {
 
  // const [Model, setModel] = React.useState('');
  const handleChange = (event) => {
    props.setmodel_value(event.target.value);
    
  };
  // const model_value = Model
  // console.log(model_value);
  
  return (
    <Box sx={{ minWidth: 158 }}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">Model</InputLabel>
        <Select 
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          displayEmpty
          value={props.model_value}
          label="Model"
          onChange={handleChange}
        >
          {/* <MenuItem value=""><Put any default Value which you want to show/></MenuItem> */}
          <MenuItem value={"Model1"}>Model 1</MenuItem>
          <MenuItem value={"Model2"}>Model 2</MenuItem>
          <MenuItem value={"Model3"}>Model 3</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
 
}
