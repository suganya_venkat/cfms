//expire_loss_cr_Low
import React, { useRef, useState, useEffect } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import MaterialTableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {Link} from 'react-router-dom';

// import Data from './Alert_Management.json';
import TablePagination from '@mui/material/TablePagination';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import RemoveRedEyeOutlinedIcon from '@mui/icons-material/RemoveRedEyeOutlined';
import { makeStyles, withStyles } from "@material-ui/core/styles";



import './Alert_Management.css'

const TableCell = withStyles({
    root: {
      borderBottom: "none" ,
      borderRightStyle: "solid",
      borderRightColor:"azure",
      borderRightWidth: "thin",
  
    }
  })(MaterialTableCell);

  var tempData = {};
var getIndex = "";

export default function FraudType_expire_loss_cr_Low() {
   const [newdata,newsetData]=useState([]);
   const[check,setCheck] = React.useState(false)
   const [save, setSave] =  React.useState("");

     useEffect(() => {
  fetch('/alert_management_frdtyp').then(res => res.json()).then((newdata) => newsetData(newdata));
  },[]);
  

  var sample =  newdata.expire_loss_cr_Low === undefined ||  newdata.expire_loss_cr_Low === null || newdata.expire_loss_cr_Low.length <=0 
  ?  null : newdata.expire_loss_cr_Low.map((a) => {
    a.FraudTpe = a.rc_bucket === 'expire_loss_cr_Low' ? 'Claim within close proximity of expiration date' :
    
    a.rc_bucket
  })
  


const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [dense, setDense] = React.useState(false);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleChangeDense = (event) => {
    setDense(event.target.checked);
  };

//   const gettingIndex  = (index) =>{
//     window.location.href = '/ClaimsDetails';
//     setCheck(true)
// console.log(index)
//   }

// console.log(Data.expire_loss_cr_Low)




  
  return (
    <>
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table"
      size= 'small'
      >
        <TableHead>
          <TableRow  style={{ backgroundColor: "#E8E8E8" }}>
          <TableCell>Fraud Type</TableCell>
            <TableCell align="center"  style={{borderBottom: "none"}}>Claim Number</TableCell>
            <TableCell align="center"  style={{borderBottom: "none"}}>Severity</TableCell>
            <TableCell align="center"  style={{borderBottom: "none"}}>Report Date</TableCell>
            <TableCell align="center"  style={{borderBottom: "none"}}>$ Value</TableCell>
           
            <TableCell align="center"  style={{borderBottom: "none"}}>Assign Resource</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
                
        {  newdata.expire_loss_cr_Low === undefined ||  newdata.expire_loss_cr_Low === null || newdata.expire_loss_cr_Low.length <=0 ? "Loading" :
                newdata.expire_loss_cr_Low.sort((a, b) => b.severity - a.severity).slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row,index) => ( 
  
            <TableRow
            key={index}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row"  style={{borderBottom: "none"}} >
                {index === 0 ? row.FraudTpe: ""}
              </TableCell>
              {/* <TableCell align="center"  style={{borderBottom: "none"}}><RemoveRedEyeOutlinedIcon className='icon' onClick={(e) => {gettingIndex(row.claim_number); }}fontSize='small'/>{row.claim_number}</TableCell> */}
              <TableCell align="center"  style={{borderBottom: "none"}}> 
              <Link to={{
  pathname: '/ClaimsDetails',
  state:  row.claim_number
}} ><RemoveRedEyeOutlinedIcon className='icon' fontSize='small'/></Link>  {row.claim_number}              
              </TableCell>
              {row.severity === 1 ? <TableCell align="center"  style={{borderBottom: "none"}}><span className='Severitydot1'></span> </TableCell> :  
      row.severity === 2 ? <TableCell align="center"  style={{borderBottom: "none"}}><span className='Severitydot2'></span> </TableCell>:
      row.severity === 3 ? <TableCell align="center"  style={{borderBottom: "none"}}><span className='Severitydot3'></span></TableCell>:
      row.severity === 4 ? <TableCell align="center"  style={{borderBottom: "none"}}><span className='Severitydot4'></span></TableCell>:
      row.severity === 5 ? <TableCell align="center"  style={{borderBottom: "none"}}><span className='Severitydot5'></span></TableCell>:
      row.severity === 6 ? <TableCell align="center"  style={{borderBottom: "none"}}><span className='Severitydot6'></span></TableCell>:
      row.severity === 7 ? <TableCell align="center"  style={{borderBottom: "none"}}><span className='Severitydot7'></span></TableCell>:
      row.severity === 8 ? <TableCell align="center"  style={{borderBottom: "none"}}><span className='Severitydot8'></span></TableCell>:
      row.severity === 9 ? <TableCell align="center"  style={{borderBottom: "none"}}><span className='Severitydot9'></span></TableCell>:
      row.severity === 10 ? <TableCell align="center"  style={{borderBottom: "none"}}><span className='Severitydot10'></span></TableCell>:
    
      <TableCell align="center"  style={{borderBottom: "none"}}>{row.severity}</TableCell>}
              <TableCell align="center"  style={{borderBottom: "none"}}>{row.claim_report_date}</TableCell>
              <TableCell align="center"  style={{borderBottom: "none"}}>{row.claim_amt_usd.toFixed(2)}</TableCell>
              <TableCell align="center"  style={{borderBottom: "none"}}>{row.assigned_resource}</TableCell>

            </TableRow>
         

          ))}
        </TableBody>
      </Table>
      {/* <FormControlLabel
        control={<Switch checked={dense} onChange={handleChangeDense} />}
        label="Dense padding"
      /> */}
      <TablePagination
        rowsPerPageOptions={[5,10, 25, 100]}
        component="div"
        count={newdata.expire_loss_cr_Low ? newdata.expire_loss_cr_Low.length : ""}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
     
    </TableContainer>
    { check ?
      <Link to={{
        pathname: '/ClaimsDetails',
        state: {getIndex}
      }} ></Link> : "" }
      </>
  );
}
