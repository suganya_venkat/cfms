import React from 'react'
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Configuration_navbar from '../../Components/Configuration_navbar'
import MainNavBar from '../../Components/MainNavBar';
import Alert_Management from './Alert_Management_Page'
import FraudType_is_self_accident from './FraudType_is_self_accident'
import FraudType_expire_loss_cr_Low from './FraudType_expire_loss_cr_Low'
import FraudType_week_time_3 from './FraudType_week_time_3'

function AlertManagement() {
    return (
        <div>
  <MainNavBar/>

   
      
    
  
      
      <CardContent>
    <Alert_Management/>
    </CardContent>
    <CardContent>
     <FraudType_is_self_accident/>
     </CardContent>
     <CardContent>
     <FraudType_expire_loss_cr_Low/>
     </CardContent>
     <CardContent>
     <FraudType_week_time_3/>
     </CardContent>
     </div>
    )
}

export default AlertManagement
