import React, { useRef, useState, useEffect } from "react";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import Paper from "@material-ui/core/Paper";
import claimsdata from './InvestigationClaimsByStateData.json';

// import "./linechart_paper.css";

am4core.useTheme(am4themes_animated);

const CHART_ID = "InvestChart1";

// const fetchURL = "http://localhost:5000/insights/actualforecast_chart";

function InvestigationClaimsByState() {
  const chartRef = useRef(null);

 const [data,setData]=useState([]);

     useEffect(() => {
  fetch('/invesigation_overview').then(res => res.json()).then((data) => setData(data));
  },[]);
    
  useEffect(() => {
    chartRef.current = am4core.create(CHART_ID, am4charts.XYChart);
    chartRef.current.data = Object.keys(data).length > 0 ? data.Investigated_status_by_states_perc : "Loading";

    var categoryAxis = chartRef.current.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "STATE";
categoryAxis.renderer.grid.template.disabled = true;
categoryAxis.title.text = "State";
categoryAxis.title.fontSize = 10;
categoryAxis.renderer.labels.template.fontSize = 10;
categoryAxis.renderer.labels.template.disabled=false;

categoryAxis.renderer.minGridDistance = 20;

// categoryAxis.renderer.grid.template.location = 1;
// categoryAxis.startLocation = 1;
// categoryAxis.endLocation = 1;

// Setting up label rotation
// categoryAxis.renderer.labels.template.rotation = 45;

// categoryAxis.renderer.grid.template.location = 0;
// categoryAxis.renderer.minGridDistance = 30;

// categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
//   if (target.dataItem && target.dataItem.index & 2 == 2) {
//     return dy + 25;
//   }
//   return dy;
// });

var valueAxis = chartRef.current.yAxes.push(new am4charts.ValueAxis());
valueAxis.title.text = "Claims(#)";
valueAxis.title.fontSize = 10;
valueAxis.renderer.labels.template.fontSize = 10;

// Create series
var series = chartRef.current.series.push(new am4charts.ColumnSeries());
series.dataFields.valueY = "Investigated_status";
series.dataFields.categoryX = "STATE";
series.name = "Investigated Status";
series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
series.columns.template.fillOpacity = .8;

valueAxis.renderer.grid.template.disabled = true;

var columnTemplate = series.columns.template;
columnTemplate.strokeWidth = 2;
columnTemplate.strokeOpacity = 1;

chartRef.current.exporting.menu = new am4core.ExportMenu();
chartRef.current.exporting.menu.align = "right";
chartRef.current.exporting.menu.verticalAlign = "top";

});
   

  return (
    <div
      id={CHART_ID}
      style={{ width: "100%", height: "200px", margin: "50px 0" }}
    >
      {" "}
    </div>
  );
}

export default InvestigationClaimsByState;
