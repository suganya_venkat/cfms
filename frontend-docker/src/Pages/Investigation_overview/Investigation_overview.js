import React from 'react'
import Grid from "@material-ui/core/Grid";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import { Typography } from "@material-ui/core";
import NavBar from '../../Components/Navbar'
import MainNavBar from '../../Components/MainNavBar'
import InvestigationKPICard from './InvestigationKPICard'
import InvestidationClaimsByState from './InvestigationClaimsByState'
import InvestigationComboChart from './InvestigationComboChart'
import './InvestigatedPage.css'

function Investigation_overview() {
  
    return (
        <div className='divalign'>
          <MainNavBar/>
        <NavBar/>
       <InvestigationKPICard/>
       <Grid  container spacing={1}>
        <Grid item xs = {6}>
        <Paper elevation={3} className="fraudratio">
        <CardActions className="table">
        <Typography variant="h6" color="primary">
       Investigation by State
       </Typography>
      </CardActions>
          <InvestidationClaimsByState/>
          {/* <Typography variant="h6" color="primary">
          Claims By Claims Type
          </Typography> */}
        
        <CardContent>
        {/* <Filters/> */}
        
       
        </CardContent>
        </Paper>
        </Grid>
        
       
        <Grid item xs = {6} > 
        <Paper elevation={3} className="a" >
        <CardActions>
          <Typography variant="h6" color="primary">
         Payment Status
          </Typography>
        </CardActions>
        <InvestigationComboChart/>
        <CardContent>
      
        </CardContent>
        </Paper>
        </Grid>
        </Grid>
        </div>
    )
}

export default Investigation_overview
