import  React , {useEffect , useRef} from 'react';
import claimsdata from '../LandingPage/FraudRatio.json';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

am4core.useTheme(am4themes_animated);

const CHART_ID = "fraudratio_chart";

function FraudRatio() {
    const chartRef = useRef(null);
    useEffect(() => {
        var chart = am4core.create(CHART_ID, am4charts.PieChart);

        // chartRef.current = am4core.create(CHART_ID, am4charts.PieSeries);
        chart.data = claimsdata;
        // chartRef.current.colors.list = [
        //   am4core.color("#52C1C8"),
        //   am4core.color("#A0CCEC"),
        // ];
        // chart.innerRadius = am4core.percent(40);

        let pieSeries = chart.series.push(new am4charts.PieSeries());
         pieSeries.dataFields.category = "index";
        pieSeries.dataFields.value = "target";
        pieSeries.labels.template.fontSize=12
        // pieSeries.slices.template.stroke = am4core.color("yellow");
        // pieSeries.slices.template.strokeWidth = 2;
        // pieSeries.slices.template.strokeOpacity = 1;

        // dateAxis.renderer.grid.template.disabled = true;
        // dateAxis.renderer.labels.template.fill = am4core.color("#949FAF");
        pieSeries.tooltip.disabled = false;
        pieSeries.ticks.template.disabled = true;
        // pieSeries.slices.template.propertyFields.fill = "color";

        // dateAxis.gridIntervals.setAll([
        //     { timeUnit: "month", count: 1 },
        //     { timeUnit: "month", count: 3 },
        //   ]);
      
        //   let valueAxis = chartRef.current.yAxes.push(new am4charts.ValueAxis());
        //   valueAxis.title.text = "Case Vol.";
        //   valueAxis.dataFields.value = "total_claim_amount";

        //   valueAxis.tooltip.disabled = true;
        //   valueAxis.renderer.labels.template.fill = am4core.color("#BCC3CE");
        //   valueAxis.renderer.grid.template.stroke = am4core.color("#e7eff3");
        //   valueAxis.renderer.grid.template.strokeOpacity = 1;
          // valueAxis.min = 0;
          // valueAxis.extraMin = 0.2;
          // valueAxis.extraMax = 0.2;
      
        //   let series = chartRef.current.series.push(new am4charts.LineSeries());
        //   series.dataFields.dateX = "incident_date";
        //   series.dataFields.valueY = "age";
        //   series.strokeWidth = 3;
        //   series.name = "Actual";
        //   series.tooltipText = "{dateX}{valueY}";
        //   series.propertyFields.strokeDasharray = "lineDash";
        //   series.propertyFields.stroke = "lineColor";
          // series.propertyFields.fill = "lineColor";
      
        //   let bullet = series.bullets.push(new am4charts.CircleBullet());
        //   bullet.circle.radius = 4;
        //   bullet.circle.strokeWidth = 2;
      
          // bullet.adapter.add("fill", function (fill, target) {
          //   if (!target.dataItem) {
          //     return fill;
          //   }
          //   var values = target.dataItem.properties;
      
          //   return values.strokeDasharray === ""
          //     ? am4core.color("#52C1C8")
          //     : am4core.color("#FC9D5A");
          // });
      
          // bullet.adapter.add("stroke", function (stroke, target) {
          //   if (!target.dataItem) {
          //     return stroke;
          //   }
          //   var values = target.dataItem.properties;
      
          //   return values.strokeDasharray === ""
          //     ? am4core.color("#52C1C8")
          //     : am4core.color("#FC9D5A");
          // });
      
      
          // Add cursor
        //   chartRef.current.cursor = new am4charts.XYCursor();
        //   chartRef.current.cursor.xAxis = dateAxis;
      
          // Add legend
        //   chartRef.current.legend = new am4charts.Legend();
        //   chartRef.current.legend.position = "bottom";
      
          // Add scrollbar
        //   chartRef.current.scrollbarX = new am4core.Scrollbar();
        //   chartRef.current.scrollbarX.thumb.minWidth = 30;
        //   chartRef.current.scrollbarX.parent = chartRef.current.bottomAxesContainer;
        //   chartRef.current.scrollbarX.startGrip.disabled = false;
        //   chartRef.current.scrollbarX.endGrip.disabled = false;
      
         chart.exporting.menu = new am4core.ExportMenu();
         chart.exporting.menu.align = "right";
         chart.exporting.menu.verticalAlign = "top";
      
        //   return () => {
        //    chart &&chart.dispose();
        //   };
        
      
        // Load data into chart
    
      })
      
        return (
          <div
            id={CHART_ID}
            style={{ width: "100%", height: "200px", margin: "50px 0" }}
          >
            {" "}
          </div>
        );
  
}


export default FraudRatio
