import React, { useRef, useState, useEffect } from "react";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import Paper from "@material-ui/core/Paper";
import claimsdata from '../LandingPage/ClaimsData.json';

// import "./linechart_paper.css";

am4core.useTheme(am4themes_animated);

const CHART_ID = "shape_chart";

// const fetchURL = "http://localhost:5000/insights/actualforecast_chart";

function IncidentBasedClaims() {
  const chartRef = useRef(null);
  const [items, setItems] = useState([]);

//   const datalevel = props.selectedForecast;
//   const timeframe = props.timeframe;
//   const selectedFilterValues = props.selectedFilterValues;

//   console.log("filters ==> ",selectedFilterValues)

//   useEffect(() => {
  
//     const requestOptions = {
//       method: "POST",
//       headers: { "Content-Type": "application/json" },
//       body: JSON.stringify({
//         markets: ["Market Europe"],
//         technology: ["Microwave"],
//         customers: ["All"],
//         country: ["Spain"],
//         gsd_name: ["All"],
//         customer_team: ["All"],
//         datalevel: datalevel,
//       }),
//     };
//     fetch(fetchURL, requestOptions)
//       .then((res) => res.json())
//       .then((data) => setItems(data.actualforecast_chart_data));
//   }, [datalevel]);


//   items.map((a) => {
//   a.lineDash = a.forecast === false ? " " : "5,5";
//     // a.lineColor = a.forecast === false ? "#52C1C8" : "#FC9D5A";
//   });

//   items.map((fy) => {
//     var date = new Date(fy.date);
//     var year = date.getFullYear();
//     fy.fiscalyear_quarter = fy.quarter === 4 ? year - 1 + "-Q"+fy.quarter : year+ "-Q"+fy.quarter; 
//     fy.fiscalyear = fy.quarter === 4 ? year - 1  : year;
//    });

//   var helper = {};
//   var result;
//   var counter = 0;
//   var count = [];

//   if (items) {
//     result = items.reduce(function (r, o) {

//       if (timeframe == "Monthly") {
//         var key = o.date;
//       }
//       else if (timeframe == "Quaterly") {
//         var key = o.fiscalyear_quarter;
//       } else {
//         var key = o.fiscalyear;
//       }
//       if (!helper[key]) {
//         count.push(counter)
//         counter = 1;
//         helper[key] = Object.assign({}, o); // create a copy of o
//         r.push(helper[key]);
//       } 
//       else {
//         helper[key].case_volume += o.case_volume;
//         counter = counter + 1;
//       }

//       return r;
//     }, []);

//     count.push(counter)

//     result.map((res, index) => {
//      res.case_volume = res.case_volume/(count[index+1]);
//     })
//     // console.log(result);

//   } else {
//     result = null;
//   }

  useEffect(() => {
    chartRef.current = am4core.create(CHART_ID, am4charts.XYChart);
    chartRef.current.data = claimsdata;

    var categoryAxis = chartRef.current.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "incident_type";
categoryAxis.renderer.grid.template.disabled = true;
categoryAxis.title.text = "Incident Type";
categoryAxis.title.fontSize = 12;
categoryAxis.renderer.labels.template.fontSize = 12;

categoryAxis.renderer.minGridDistance = 20;

// categoryAxis.renderer.grid.template.location = 0;
// categoryAxis.renderer.minGridDistance = 30;

// categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
//   if (target.dataItem && target.dataItem.index & 2 == 2) {
//     return dy + 25;
//   }
//   return dy;
// });

var valueAxis = chartRef.current.yAxes.push(new am4charts.ValueAxis());
valueAxis.title.text = "claim_counts";
valueAxis.title.fontSize = 12;
valueAxis.renderer.labels.template.fontSize = 12;

// Create series
var series = chartRef.current.series.push(new am4charts.ColumnSeries());
series.dataFields.valueY = "claim_counts";
series.dataFields.categoryX = "incident_type";
series.name = "claim_counts";
series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
series.columns.template.fillOpacity = .8;

valueAxis.renderer.grid.template.disabled = true;

var columnTemplate = series.columns.template;
columnTemplate.strokeWidth = 2;
columnTemplate.strokeOpacity = 1;

chartRef.current.exporting.menu = new am4core.ExportMenu();
chartRef.current.exporting.menu.align = "right";
chartRef.current.exporting.menu.verticalAlign = "top";

});
   

  return (
    <div
      id={CHART_ID}
      style={{ width: "100%", height: "200px", margin: "50px 0" }}
    >
      {" "}
    </div>
  );
}

export default IncidentBasedClaims;
