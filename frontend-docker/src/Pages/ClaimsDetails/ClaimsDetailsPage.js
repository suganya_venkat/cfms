
import React, { useRef, useState, useEffect } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import data from './ClaimsDetails.json';
import Button from '@mui/material/Button';
import Alert from '@mui/material/Alert';


import { makeStyles, TextField } from "@material-ui/core";

import { Divider, Avatar, Grid } from "@material-ui/core";

import './ClaimsDetails.css'

const imgLink =
  "https://images.pexels.com/photos/1681010/pexels-photo-1681010.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260";

  var claim_number_selected = {};
var newComment = {};

 const useStyles = makeStyles(() => ({
    textField: {
      width: "90%",
      marginLeft: "auto",
      marginRight: "auto",
      paddingBottom: 0,
      marginTop: 0,
      fontWeight: 500
    },
    input: {
      color: "white"
    }
  }));



// console.log(data['claim specific details'])

export default function ClaimsDetailsPage(props) {

  const claim_data = props.claimDetails;
  // console.log(claim_data)
     const [newdata,newsetData]=useState([]);
     const [Comment, setComment] = useState("");


claim_number_selected = {"claim_number" : claim_data}

  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(claim_number_selected),
  };

  useEffect(() => {
  fetch('/claim_level_data', requestOptions).then((res) => res.json()).then((newdata) => newsetData(newdata)
                                
  )},[Comment]);
//  console.log(claimDetails)       
  //    useEffect(() => {
  // fetch('/claim_level_data').then(res => res.json()).then((newdata) => newsetData(newdata));
  // },[]);
    
     console.log(newdata['comments'])
    
     const classes = useStyles();

  const [save, setSave] =  React.useState("");

  



const getComment = () =>{
  // console.log(Comment)
  newComment = {"user_id": 1, "claim_number": claim_data, "comment": Comment}
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(newComment),
  };

  fetch('/add_comments', requestOptions).then((res) =>
    setSave(res.status),
    //  setbuttonDisabled(true)   
                                     
  );
  setComment("")
}

console.log(newComment)
console.log(save)
    
  return (
      <div>
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 1124 }} aria-label="simple table">
      {/* {data['claim specific details'].map((row) => ( */}
      {newdata === undefined || newdata === null || Object.keys(newdata).length <= 0 ? "loading":
        <TableHead>
     
          <TableRow>
            <TableCell  align="center">Claim no<br/> <div>{newdata['claim specific details'].claim_number}</div></TableCell>
            <TableCell align="center">Rported Date <div>{newdata['claim specific details'].CLAIMREPORTDATE}</div></TableCell>
            <TableCell align="center">Date Of Loss <div>{newdata['claim specific details'].DATEOFLOSS}</div></TableCell>
            
          </TableRow>
          <TableRow>
            <TableCell align="center">Claim Type Description <div>{newdata['claim specific details'].CLAIMTYPEDESC}</div></TableCell>
            <TableCell align="center">Address <div>{newdata['claim specific details'].address}</div></TableCell>
            <TableCell  align="center">Pin Code <br/> <div>{newdata['claim specific details'].pin_code}</div></TableCell>
            
          </TableRow>

        </TableHead>
}
          {/* ))} */}
        {/* <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.calories}</TableCell>
              <TableCell align="right">{row.fat}</TableCell>
              <TableCell align="right">{row.carbs}</TableCell>
              <TableCell align="right">{row.protein}</TableCell>
            </TableRow>
          ))}
        </TableBody> */}
      </Table>
    </TableContainer>
  <h2>Comments</h2>
       <div  className="scroll">
     
       {newdata['comments'] === null || newdata['comments'] === undefined || newdata['comments'].length <=0 ? <center> no comments</center> :
       newdata['comments'].map((row) => (
       <Paper style={{ padding: "40px 20px" }}>
         <Grid container wrap="nowrap" spacing={2}>
           <Grid item>
             <Avatar />
           </Grid>
           <Grid justifyContent="left" item xs zeroMinWidth>
             <h4 style={{ margin: 0, textAlign: "left" }}>{row.user_name}</h4>
             <p style={{ textAlign: "left" }}>
             {row.comment}.{" "}
             </p>
             <p style={{ textAlign: "left", color: "gray" }}>
             {row.insert_ts}
             </p>
           </Grid>
         </Grid>
         {/* <Divider variant="fullWidth" style={{ margin: "30px 0" }} />
         <Grid container wrap="nowrap" spacing={2}>
           <Grid item>
             <Avatar alt="Remy Sharp" src={imgLink} />
           </Grid>
           <Grid justifyContent="left" item xs zeroMinWidth>
             <h4 style={{ margin: 0, textAlign: "left" }}>Michel Michel</h4>
             <p style={{ textAlign: "left" }}>
               Lorem ipsum dolor sit amet, consectetur adipiscing elit. .{" "}
             </p>
             <p style={{ textAlign: "left", color: "gray" }}>
               posted 1 minute ago
             </p>
           </Grid>
         </Grid> */}
       </Paper>
       ))}
       </div>

   <br/>
       <TextField
        id="Comment"
        label="Add Comments"
        variant = "outlined"
        // placeholder="Add Comment"
        multiline
        className={classes.textField}
        value={Comment}
        rows = {3}
        maxRows={10}
        onChange={(e) => setComment(e.target.value)}
        
        
        ></TextField>
        <Button onClick={getComment} className="commentbutton" variant="outlined">comment</Button>
        {save === 200 ? <Alert>Comment Inserted</Alert>
    : ""}
                      {save != 200 && save ? <Alert severity="error">Something Wrong!</Alert>
    : ""}
       </div>
  );
}
