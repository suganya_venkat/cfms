import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import ClaimsDetailsPage from './ClaimsDetailsPage';
import './ClaimsDetails'

// var claim_number_selected = {};

function TabPanel(props) {
   const { children, value, index, ...other } = props;


  return (
    <div
      // role="tabpanel"
      // hidden={value !== index}
      // id={`vertical-tabpanel-${index}`}
      // aria-labelledby={`vertical-tab-${index}`}
      // {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

// TabPanel.propTypes = {
//   children: PropTypes.node,
//   index: PropTypes.number.isRequired,
//   value: PropTypes.number.isRequired,
// };

// function a11yProps(index) {
//   return {
//     id: `vertical-tab-${index}`,
//     'aria-controls': `vertical-tabpanel-${index}`,
//   };
// }

export default function ClaimsDetailsNavBar(props) {
  const [value, setValue] = React.useState(0);
 
  const claim_data = props.claimData;
  // console.log(claim_data)
  const[claimDetails,setclaimDetails] = React.useState(claim_data)
  // claim_number_selected = {"claim_number" : claim_data}

  // const requestOptions = {
  //   method: "POST",
  //   headers: { "Content-Type": "application/json" },
  //   body: JSON.stringify(claim_number_selected),
  // };

  // fetch('/claim_level_data', requestOptions).then((res) => res.json()).then((claimDetails) => setclaimDetails(claimDetails),
  // // console.log(claimDetails)                                     
  // );

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box
      sx={{ flexGrow: 1, bgcolor: 'background.paper', display: 'flex', height: 550}}
    >
      <Tabs
        orientation="vertical"
        // variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        sx={{ borderRight: 1, borderColor: 'divider' }}
      >
        <Tab label="Basic Info"/>
        <Tab label="Raw Data" />
        <Tab label="Documents"  />
        <Tab label="Rules"  />
        <Tab label="Model" />
        <Tab label="Nexus"  />
        <Tab label="Forensic" />
        <Tab label="Flow" />
      </Tabs>
      <TabPanel >
    <  ClaimsDetailsPage setclaimDetails={setclaimDetails} claimDetails={claimDetails}/>
      </TabPanel>
      <TabPanel value={value} index={1}>
       
      </TabPanel>
      <TabPanel value={value} index={2}>
     
      </TabPanel>
      <TabPanel value={value} index={3}>
       
      </TabPanel>
      <TabPanel value={value} index={4}>
        
      </TabPanel>
      <TabPanel value={value} index={5}>
       
      </TabPanel>
      <TabPanel value={value} index={6}>
      
      </TabPanel>
    </Box>
  );
}
