import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from 'react-router-dom';
import Data from './DocumentDetails.json';
import Button from '@material-ui/core/Button';
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TablePagination from '@material-ui/core/TablePagination';
import SearchBar from "material-ui-search-bar";
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Upload_Document from './Upload_Document';
import  './UploadDocument.css'
import { color } from "@amcharts/amcharts4/core";
import ViewDocument from "./ViewDocument";

// import SpinnerPage from "../../utils/loader";

/*const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.white,
    color: theme.palette.common.Grey,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {npm
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);*/

const useStyles = makeStyles({
  table1: {
    minWidth: 450,
  },
//   root: {
//     "& .MuiTableCell-head": {
//         color: "white",
//         backgroundColor: "#0a58cac4"
//     },
// },

  container1: {
    maxHeight: 450,
    marginTop: 5,
  },
  banner: {
    display: "flex",
    justifycontent: "space-between",
    background: "#EDF2F5",
    height: "48px",
  },
  right_align: {
    position: "absolute",
    right: "55px",
    margintop: "10px",
    color: "grey",
    top: "70px",
    fontSize: 13,
  },
});

const fetchURL =
  "http://makeup-api.herokuapp.com/api/v1/products.json?brand=maybelline";
const getItems = () => fetch(fetchURL).then((res) => res.json());

function DocumentProcessed_Table(props) {
  const classes = useStyles();

  const [items, setItems] = useState();
  const [sample, setSample] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searched, setSearched] = useState("");

  // const tableData = props.Data;
   console.log(Data[0].Owner)

  useEffect(() => {
    getItems().then((data) => setItems(data));
    getItems().then((data) => setSample(data));
  }, []);

  // console.log(items);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const requestSearch = (searchedVal) => {
    const filteredRows = sample.filter((item) => {
      if (searchedVal === "") {
        return item;
      } else if (item.product_type.toLowerCase().includes(searchedVal.toLowerCase())) {
        return item;
      }
    })
    setItems(filteredRows);
    console.log(filteredRows)
  };

 
  const cancelSearch = () => {
    setSearched("");
    requestSearch(searched);
  };


  return (
    <div>
             {items === undefined ? (
        <div>
          {" "}
          {" "}
        </div>
      ) : (
        <div>

<SearchBar
          value={searched}
          onChange={(searchVal) => requestSearch(searchVal)}
          onCancelSearch={() => cancelSearch()}
        />
          <TableContainer  className={classes.container1}>
            <Table
              className={classes.table1}
              stickyHeader
              aria-label="sticky table"
            >
              <TableHead>
                <TableRow style={{ backgroundcolor: "yellow" }}> 
                  {/* className={classes.root} */}
                  <TableCell align="center" >
                    Document Name
                  </TableCell>
                  <TableCell align="center" >
                    Created On
                  </TableCell>
                  <TableCell align="center" >
                    Owner
                  </TableCell>
                  <TableCell align="center">
                   Status
                  </TableCell>
                  <TableCell align="center">
                   
                  </TableCell>
                  {/* <TableCell align="center" style={{ color: "grey" }}>
                    Rating
                  </TableCell> */}
                </TableRow>
              </TableHead>
             <TableBody>
                
            { Data.map((d) => {
                    
                       return ( 
                         
                        <TableRow
                          // hover
                          // role="checkbox"
                          // tabIndex={-1}
                        //   key={item.id}
                        >
                          <TableCell align="center"
                            component="th"
                            scope="row"
                            style={{ color: "Midnightblue" }}
                          >
                            {d.Document_Name}
                          </TableCell>
                          <TableCell align="center">
                              {d.Created_on}
                              </TableCell>
                          <TableCell align="center">
                            {d.Owner}
                          </TableCell>
                          <TableCell align="center">
                              {d.Status}
                              </TableCell>
                          {d.Status === "Completed" ? 
                          (
                          <TableCell align="center">
<Button onClick={() => <ViewDocument/>} component={Link} to='/ViewDocument'>View Document</Button>                         
                           </TableCell>
                          
                          ) : "" 
                        }

                        </TableRow>
                      
                       )}
            )}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      )}
    </div>
  );
}

export default DocumentProcessed_Table;
