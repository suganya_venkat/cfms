import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from 'react-router-dom';
import Data from './ViewDocumentDetailsData.json';
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";




const useStyles = makeStyles({


  container1: {
    maxHeight: 580,
    // marginTop: 5,
  },

});

// const fetchURL =
//   "http://makeup-api.herokuapp.com/api/v1/products.json?brand=maybelline";
// const getItems = () => fetch(fetchURL).then((res) => res.json());

function ViewDocumentDetailsData() {
  const classes = useStyles();
// console.log(claimsdata[0].Tabledata_statewise[0])


//   const [items, setItems] = useState();
//   const [sample, setSample] = useState([]);
//   const [page, setPage] = useState(0);
//   const [rowsPerPage, setRowsPerPage] = useState(5);
//   const [searched, setSearched] = useState("");


//   useEffect(() => {
//     getItems().then((data) => setItems(data));
//     getItems().then((data) => setSample(data));
//   }, []);

  // console.log(items);

//   const handleChangePage = (event, newPage) => {
//     setPage(newPage);
//   };

//   const handleChangeRowsPerPage = (event) => {
//     setRowsPerPage(parseInt(event.target.value, 10));
//     setPage(0);
//   };

//   const requestSearch = (searchedVal) => {
//     const filteredRows = sample.filter((item) => {
//       if (searchedVal === "") {
//         return item;
//       } else if (item.product_type.toLowerCase().includes(searchedVal.toLowerCase())) {
//         return item;
//       }
//     })
//     setItems(filteredRows);
//     console.log(filteredRows)
//   };

 
//   const cancelSearch = () => {
//     setSearched("");
//     requestSearch(searched);
//   };


  return (
    <div>
        
        {/* <Breadcrumbs aria-label="breadcrumb">
  <Link underline="hover" color="inherit" href="/">
    Home
  </Link> */}
  {/* <Link
    underline="hover"
    color="inherit"
    href="/getting-started/installation/"
  >
    Core
  </Link>
  <Typography color="text.primary">Breadcrumbs</Typography> */}
{/* </Breadcrumbs> */}
      {/* <div className={classes.root}>
        <form className={classes.right_align}>
          <input type="radio" name="choice" value="yes" /> Monthly
          <input type="radio" name="choice" value="no" /> Quaterly
          <input type="radio" name="choice" value="no" /> Yearly
        </form>
      </div> */}
      {Data === undefined ? (
        <div>
          {" "}
          {" "}
        </div>
      ) : (
        <div>

{/* <SearchBar
          value={searched}
          onChange={(searchVal) => requestSearch(searchVal)}
          onCancelSearch={() => cancelSearch()}
        /> */}
          <TableContainer  className={classes.container1}>
            <Table
            //   className={classes.table1}
              stickyHeader
              aria-label="sticky table"
            >
              <TableHead>
                <TableRow > 
                  {/* className={classes.root} */}
                  <TableCell align="center" >
                 Feature Name
                  </TableCell>
                  <TableCell align="center" >
                 Output
                  </TableCell>
                
                 
                  {/* <TableCell align="center" style={{ color: "grey" }}>
                    Rating
                  </TableCell> */}
                </TableRow>
              </TableHead>
             <TableBody>
                  {Data.map((a) => {
                    
                       return ( 
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                        //   key={item.id}
                        >
                          
                          <TableCell align="center">
                              {a.Feature_Name}
                              </TableCell>
                          <TableCell align="center">
                            {a.Output}
                          </TableCell>
                          
                         
                        </TableRow>
                      ); 
                     })
                    }
              </TableBody>
            </Table>
            {/* <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={items.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        /> */}
          </TableContainer>
        </div>
      )}
    </div>
  );
}

export default ViewDocumentDetailsData;
