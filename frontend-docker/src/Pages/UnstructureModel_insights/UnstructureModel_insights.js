import React, { useState } from 'react'
import UploadDocument from './UploadDocument'
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import { Typography } from "@material-ui/core";
import  './UploadDocument.css'
 import DocumentProcessed_Table from './DocumentProcessed_Table';
 import NavBar from '../../Components/Navbar_configuration'
 
import MainNavBar from '../../Components/MainNavBar';
// import Configuration_navbar from '../../Components/Configuration_navbar'

// import Rule_navbar from '../../Components/Rule_navbar'
import Configuration_navbar from '../../Components/Configuration_navbar';

 


function UnstructureModel_insights() {
    const [setData, Data] = useState('');
    return (
        <div>
             <MainNavBar/>
      <Configuration_navbar/>
     
            <NavBar/>
            <br/>
        <UploadDocument/>
        <Paper elevation={3}>
        <CardActions>
          <Typography variant="h6" color="primary">
              Documents Processed
          </Typography>
        </CardActions>
        <CardContent className='card'>
        <DocumentProcessed_Table setData={setData} Data={Data}/>
        </CardContent>
        </Paper>
        </div>
    )
}

export default UnstructureModel_insights
