import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import { Link } from 'react-router-dom';
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Checkbox from '@mui/material/Checkbox';
import ListItemText from '@mui/material/ListItemText';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import UnstructureModel_insights from './UnstructureModel_insights';
import TextField from '@material-ui/core/TextField';

import { Document, Page } from "react-pdf";
//import samplePDF from './FCL_Loss_Run.pdf';
import  './UploadDocument.css'


// Import Worker
import { Worker } from '@react-pdf-viewer/core';
// Import the main Viewer component
import { Viewer } from '@react-pdf-viewer/core';
// Import the styles
import '@react-pdf-viewer/core/lib/styles/index.css';

const useStyles = makeStyles({
  table1: {
    minWidth: 450,
  },

  container1: {
    maxHeight: 450,
    marginTop: 5,
  },
  banner: {
    display: "flex",
    justifycontent: "space-between",
    background: "#EDF2F5",
    height: "48px",
  },
  right_align: {
    position: "absolute",
    right: "55px",
    margintop: "10px",
    color: "grey",
    top: "70px",
    fontSize: 13,
  },
  TextField_align:{
    width: "1280px"
  }
});

const Input = styled('input')({
  display: 'none',
});
const names = [
  'color',
  'Specific Gravity',
  'PH',
  'RBC',
  'Pus Cells',
  'Report',
  'Nitrite',
];

var data_to_be_passed = []
var FielsSelected =[]
var FileName = []
var FinalData =[]
var FieldsMerged
function Upload_Document(props) {
  const classes = useStyles();

  // const [items, setItems] = useState();
  const [sample, setSample] = useState([]);
  const [page, setPage] = useState(0);
  const [hasvalue, setHasvalue] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searched, setSearched] = useState("");
  const [Encrypt_option, setEncrypt_option] = useState("");
  const [Documenttype_option, setDocumenttype_option] = useState("");
  const [Fieldvalues, setFieldvalues] = useState([]);
  const [personName, setPersonName] = useState([]) ;
  const [selectedFile, setSelectedFile] = useState();
	const [isFilePicked, setIsFilePicked] = useState(false);
  const [pdfFile, setPdfFile]=useState(null);
  const [imageUrl, setImageUrl] = useState(null);
  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);
  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };
  const handleChange = (event) => {
    setHasvalue(1)
    const {
      target: { value },
    } = event;
    setPersonName(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value,
    );
    // var b = personName? data_to_be_passed.push(personName):''
   
    FielsSelected.splice(0,1,typeof value === 'string' ? value.split(',') : value)
    // FieldsMerged = [].concat.apply([], FielsSelected);
    FinalData.push({"FieldsSelected":FielsSelected})
  };
  
 
// console.log(FinalData)
  
  const handleChange_encrypt = (event) => {
    setEncrypt_option(event.target.value);
   
  };

  

  const handleChange_documenttype = (event) => {
   setDocumenttype_option(event.target.value);
   data_to_be_passed.splice(0,1,event.target.value);
    FinalData.push({"Document_Type":data_to_be_passed})

   
  };


  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
  }
  const changeHandler = (event) => {
		setSelectedFile(event.target.files[0]);
    FileName.splice(0,1,event.target.files[0].name)
    FinalData.push({"FileName":FileName})
		setIsFilePicked(true);
	};

  if(selectedFile){
    let reader = new FileReader();
        reader.readAsDataURL(selectedFile);
        reader.onloadend=(e)=>{
          // setPdfError('');
          setPdfFile(e.target.result);
          // console.log(pdfFile)
        } 
  }

	const handleSubmission = () => {
	};

  useEffect(() => {
    if (selectedFile) {
      setImageUrl(URL.createObjectURL(selectedFile));
    }
  }, [selectedFile]);

// FinalData.push(FileName,FielsSelected,data_to_be_passed)
console.log(FinalData)
const uniq = new Set(FinalData.map(e => JSON.stringify(e)));
const FinalDataToBePassed = Array.from(uniq).map(e => JSON.parse(e));
//To remove dulpicated from an array
// let FinalDataToBePassed = [...new Set(FinalData)];
// props.setData(FinalDataToBePassed);
 console.log(FinalDataToBePassed);
//  const arr = [['a'],['b','l'],['c']];
//  const res = arr.reduce((a,b)=> (a[b]='',a),{});
//  console.log(res)

  return (
    <div>
   
        <div>
       
<Paper elevation={3}>
        <CardActions>
          {/* <Typography variant="h6" color="primary">
              Documents Processed
          </Typography> */}
        </CardActions>
        <CardContent>
        
        
          <TableContainer  className={classes.container1}>
            <Table
              className={classes.table1}
              stickyHeader
              aria-label="sticky table"
            >
              <TableHead>
                <TableRow>
                  <TableCell align="center" >
                    Encrypted
                  </TableCell>
                  <TableCell align="center" >
                    Select Document
                  </TableCell>
                  <TableCell align="center" >
                    Document Type
                  </TableCell>
                  <TableCell align="center" >
                   Fields
                  </TableCell>
                  <TableCell align="center" >
                   
                  </TableCell>
                  {/* <TableCell align="center" >
                    Rating
                  </TableCell> */}
                </TableRow>
              </TableHead>
            
            
              <TableBody>
                 
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                        //   key={item.id}
                        >
                          <TableCell
                            component="th"
                            scope="row"
                            style={{ color: "Midnightblue" }}
                          >
                            <FormControl fullWidth>
        {/* <InputLabel id="demo-simple-select-label"></InputLabel> */}
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
         
          // label="Age"
           onChange={handleChange_encrypt}
        >
          <MenuItem value={"Yes"}>Yes</MenuItem>
          <MenuItem value={"No"}>No</MenuItem>
         
        </Select>
      </FormControl>
                          </TableCell>
                          <TableCell align="center">
                          <Box  sx={{ p: 2, border: '1px dashed grey' }}>
                          <Stack direction="row" alignItems="left" spacing={1}>
      
      <label htmlFor="icon-button-file">
      
       {isFilePicked ? (
				
					<p> {selectedFile.name}</p>
					
				
			) : (
				<p> Choose Document</p>
			)}
        {/* <Input accept="image/*" id="icon-button-file" type="file" />
        <IconButton color="primary" aria-label="upload picture" component="span">
          <PhotoCamera />
        </IconButton> */}
      </label>
      <label htmlFor="contained-button-file">
        <Input accept="image/*" id="contained-button-file" multiple type="file" onChange={changeHandler} />
        <Button style={{
      
	  backgroundColor: "#00965f",
	  color:"white"
  }} component="span" onClick={handleSubmission}>
          Upload
        </Button>
      </label>
    </Stack>
    </Box>
                              </TableCell>
                          <TableCell align="center">
                          <FormControl fullWidth>
        {/* <InputLabel id="demo-simple-select-label">Age</InputLabel> */}
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          // value={age}
          // label="Age"
           onChange={handleChange_documenttype}
        >
          <MenuItem value={"Medical Invoices"}>Medical Invoices</MenuItem>
          <MenuItem value={"Medical Report"}>Medical Report</MenuItem>
          <MenuItem value={"Invoices"}>Invoices</MenuItem>
          <MenuItem value={"Loss Runs"}>Loss Runs</MenuItem>
          <MenuItem value={"Risk Engg Report"}>Risk Engg Report</MenuItem>
          <MenuItem value={"BlueStar"}>BlueStar</MenuItem>

        </Select>
      </FormControl>
                          </TableCell>

                          <TableCell align="center">
                          <FormControl sx={{ m: 1, width: 300 }}>
         {/* <InputLabel id="demo-multiple-checkbox-label"></InputLabel>  */}
        <Select
          
          multiple
          value={personName}
          onChange={handleChange}
          // input={<OutlinedInput label="Tag" />}
          renderValue={(selected) => selected.join(', ')}
          MenuProps={MenuProps}
        >
          {names.map((name) => (
            <MenuItem key={name} value={name}>
              <Checkbox checked={personName.indexOf(name) > -1} />
               <ListItemText primary={name} /> 
            </MenuItem>
          ))}
        </Select>
      </FormControl>
                          </TableCell>
                          <TableCell>
                          <Button style={{
      
      backgroundColor: "#00965f",
      color:"white"
    }} component="span" onClick={() => <UnstructureModel_insights/>} component={Link} to='/UnstructureModel_insights'>
          Process
        </Button>
                          </TableCell>
                        
                        </TableRow>
                      
              </TableBody> 
            
            </Table>
            {/* <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={items.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        /> */}
 </TableContainer>
 {Encrypt_option === "Yes"?
         <TextField
         required
         className={classes.TextField_align}
         id="outlined-required"
         label="Password"
         // className={classes.textField}
         margin="normal"
         variant="outlined"
       />
     : "" }
          {/* {selectedFile && (
            <div>
       <Document
      file={selectedFile.name}
      onLoadSuccess={onDocumentLoadSuccess}
    >
      {Array.from(
        new Array(numPages),
        (el, index) => (
          <Page
            key={`page_${index + 1}`}
            pageNumber={index + 1}
          />
        ),
      )}
    </Document>

      </div>
          )} */}

{pdfFile&&(
          <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.12.313/build/pdf.worker.min.js">
            <Viewer fileUrl={pdfFile}>
          
              </Viewer>
          </Worker>
        )}

          {/* {imageUrl && selectedFile && (
        <Box mt={2} textAlign="center">
          <h1>Preview</h1>
          <img src={imageUrl} alt={selectedFile.name}  />
        </Box>
      )} */}
          </CardContent>
        </Paper>
        </div>
      
    </div>
  );
}

export default Upload_Document;
