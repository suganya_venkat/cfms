import './Navbar.css'
import React, {useState} from 'react'
import { useLocation } from 'react-router-dom';


import {NavLink} from 'react-router-dom'

import HomeIcon from '@mui/icons-material/Home';
import Configuration_icon from '../Pages/Configuration/Configuration_icon'

function MainNavBar() {
    // const [icon , setIcon] =useState(false)
    // const handleClick = () =>{
    //     setIcon (!icon)
    // }
    // const closeSideDrawer = () =>{
    //     setIcon(false)
    // }

    const { pathname } = useLocation();
    return (
       <>
       <nav className='navbar'>
       {/* <NavLink to = '/Configuration_icon' className='nav-links' >
                <HomeIcon/>
                </NavLink> */}
           <NavLink to = '/' className = 'nav-logo-landing'>
           {/* <img src={bi2ilogo} alt="bi2i" width="500" height="600"/>  */}
           Logo
           </NavLink>
           
           {/* <div className='menu-icon' onClick={handleClick}>
                {
                    icon ? <FaTimes className = 'fa-times' ></FaTimes> :
                    <FaBars className = 'fa-bars'></FaBars>
                }
           </div> */}
           <ul className= 
                'nav-menu'>
            
           
           
           <li>
                <NavLink to = '/Summary'   className='nav-links' activeClassName="is-active" isActive={() => ['/Summary', '/Investigation_overview',"/"].includes(pathname)} > 
                Reporting
                </NavLink>
           </li>
           <li>
                <NavLink to = '/Rules'   className='nav-links' activeClassName="is-active" isActive={() => ['/Rules', '/StructureData_insights','/Strategy','/RuleBank','/StructureData_insights', '/UnstructureModel_insights','/AnamolyPage'].includes(pathname)} >
                   Configuration
                </NavLink>
           </li>
           <li>
                <NavLink to = '/AlertManagement'  className='nav-links' activeClassName="is-active" >
               Alert Management
                </NavLink>
           </li>
           <li>
                <NavLink to = '/Notfound'  className='nav-links' activeClassName="is-active" >
               Forensic
                </NavLink>
           </li>
           
           </ul>
          
       </nav>
       </>
    )
}

export default MainNavBar
