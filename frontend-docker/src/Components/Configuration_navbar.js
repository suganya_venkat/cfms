import './Navbar.css'
import React, {useState} from 'react'
import { useLocation } from 'react-router-dom';

import bi2ilogo from '../Components/bi2i.png'
import {NavLink} from 'react-router-dom'
import {FaBars, FaTimes} from 'react-icons/fa'
import HomeIcon from '@mui/icons-material/Home';

function Configuration_navbar() {
    // const [icon , setIcon] =useState(false)
    // const handleClick = () =>{
    //     setIcon (!icon)
    // }
    // const closeSideDrawer = () =>{
    //     setIcon(false)
    // }
    const { pathname } = useLocation();

    return (
       <div>
       <nav className='rule_navbar'>
      
           <ul className= 
                'rulebank_nav-menu'
           >
              <li>
                <NavLink to = '/Rules' className='rule_nav-links' activeClassName="rule_is-active" isActive={() => ['/Rules', '/RuleBank'].includes(pathname)}>
                    Rules
                </NavLink>
            </li>
            <li>
                <NavLink to = '/StructureData_insights' className='rule_nav-links' activeClassName="rule_is-active" isActive={() => ['/StructureData_insights', '/UnstructureModel_insights','/AnamolyPage'].includes(pathname)}>
                   Models
                </NavLink>
            </li>
            <li>
                <NavLink to = '/Strategy' className='rule_nav-links' activeClassName="rule_is-active" >
                   Strategy
                </NavLink>
            </li>
            <li>
                <NavLink to = '/Notfound' className='rule_nav-links' activeClassName="rule_is-active" >
                   Business Segment
                </NavLink>
            </li>
            {/* <li>
                <NavLink to = ' ' className='nav-links' activeClassName="is-active" onClick={closeSideDrawer}>
                <HomeIcon/>
                </NavLink>
            </li> */}
            
           {/* <li>
                <NavLink to = '/Alertmanage' onClick={closeSideDrawer} className='nav-links' activeClassName="is-active" >
                    Insights
                </NavLink>
            </li> */}
            {/* <li>
                <NavLink to = '/StructureData_insights' onClick={closeSideDrawer} className='nav-links' activeClassName="is-active" >
                    Structured Data Model Insights
                </NavLink>
           </li>
           
           <li>
                <NavLink to = '/UnstructureModel_insights' onClick={closeSideDrawer} className='nav-links' activeClassName="is-active" >
                Unstructured(OCR) Data Insights
                </NavLink>
           </li>
           <li>
                <NavLink to = '/SignIn' onClick={closeSideDrawer} className='nav-links' activeClassName="is-active">
                    Sign in
                </NavLink>
           </li> */}
           </ul>
          
       </nav>
       </div>
    )
}

export default Configuration_navbar
