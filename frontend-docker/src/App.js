import React from 'react'
import './App.css'
import {BrowserRouter as Router, Switch , Route} from 'react-router-dom'
import Navbar from './Components/Navbar'
import Summary from './Pages/LandingPage/Summary'
import Alertmanage from './Pages/Insights/Alertmanage'
import About from './Pages/About'

import StructureData_insights from './Pages/StructureData_insights/StructureData_insights'
import UnstructureModel_insights from './Pages/UnstructureModel_insights/UnstructureModel_insights'
import SignIn from './Pages/SignIn/SignIn'
import Upload_Document from './Pages/UnstructureModel_insights/Upload_Document'
import ViewDocument from './Pages/UnstructureModel_insights/ViewDocument'
import LandingIcon from './Pages/LandingPage_icon/LandingIcons'
import Investigation_overview from './Pages/Investigation_overview/Investigation_overview'
import Configuration_icon from './Pages/Configuration/Configuration_icon'
import Rules from './Pages/Rules/Rules'
import RuleBank from './Pages/RuleBank/RuleBank'
import AlertManagement from './Pages/Alert_Management/AlertManagement'
import ClaimsDetails from './Pages/ClaimsDetails/ClaimsDetails'
import AnamolyPage from './Pages/AnamolyPage/AnamolyPage'
import StrategyPage from './Pages/StrategyPage/StrategyPage'
import Notfound from './Pages/Notfound'
import login from './Pages/Login/Login'
function App() {
 
  return (
    <Router>
    
      <Switch>
     
      
     
      
     {/* { window.location.pathname !== "/"  ? 
     
    : null} */}
     <Route path = '/summary' component = {Summary}/>
     <Route path = '/StructureData_insights' component = {StructureData_insights}/>
     <Route path = '/UnstructureModel_insights' component = {UnstructureModel_insights}/>
     <Route path = '/SignIn' component = {SignIn}/> 
     <Route path = '/Upload_Document' component = {Upload_Document}/> 
     <Route path = '/Investigation_overview' component = {Investigation_overview}/> 
     <Route path = '/ViewDocument' component = {ViewDocument}/> 
     <Route path = '/Configuration_icon' component = {Configuration_icon}/> 
     <Route path = '/Rules' component = {Rules}/> 
     <Route path = '/RuleBank' component = {RuleBank}/> 
     <Route path = '/AlertManagement' component = {AlertManagement}/> 
     <Route path = '/ClaimsDetails' component = {ClaimsDetails}/> 
     <Route path = '/AnamolyPage' component = {AnamolyPage}/> 
     <Route path = '/Strategy' component = {StrategyPage}/> 
     <Route path = '/Notfound' component={Notfound}/>
     <Route path = '/login' component={login}/>

     
     <Route path = '/' component = {login}/>
    
     
    
        
      

        
      </Switch>
      </Router>
  )
}

export default App

