# README #

A Capability demonstration app to demonstrate various AI/ML capabilities developed in insurance BU.

### About Repository ###

* A demo web UI using React and backend Flask framework
* Version 1.0.0

### Setup ###

* Frontend setup: 
    - Install all the dependency of React using `npm install` from frontend folder
* Backend setup:
    - Use `requirements.txt` inside backend folder (backend/flask_app) to install the dependencies - `conda install --file requirements.txt`
* Port setup
    - In frontend folder locate package.json and line `"proxy": "http://localhost:5001"` make sure the port number here is same as port number given in the backend `api.py` file at line `app.run(port=5001, debug=True)`
    - Inside frontend folder create a `.env` file and specify the desired port eg. `PORT=3006`  to run the react frontend app, save this file
* Running the App
    - Run the flask `api.py` from backend folder (backend/flask_app)
    - Run `npm start` from front end folder (frontend/)
    
##### NOTE: Make sure the ports are specified correctly so that the requests are sent properly from frontend to backend and vis-a-vis


### Usage ###

* On UI we can see the views as Reporting, Alert Management etc
* Each view demonstrates some kind of AI/ML/Analytics capability  

### Contact ###

* arun.john@bridgei2i.com
* rohit.bhagwat@bridgei2i.com
* suganya.v@bridgei2i.com