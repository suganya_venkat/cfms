import React, { useRef, useState, useEffect } from "react";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import SpinnerPage from '../../utils/loader';
import ForecastData from './Forecastdata.json'




// import "./linechart_paper.css";

am4core.useTheme(am4themes_animated);

const CHART_ID = "ClaimsLineSeries";

// const fetchURL = "http://localhost:5000/insights/actualforecast_chart";

function ClaimsLineSeries(props) {
  const chartRef = useRef(null);
  // const [items, setItems] = useState([]);
  const selectedState = props.getState;
  const selectedWeek = props.getWeek
  // console.log(selectedState)
  const [items, setItems] = useState([]);
    
  const [data,setData]=useState(ForecastData);



// useEffect(() => {
// fetch('/get_forecast').then(res => res.json()).then((data) => setData(data));
// },[]);


useEffect(() => {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      "state": selectedState,
      "weeks": selectedWeek

    })
  };
  fetch('/get_forecast', requestOptions)
  .then(res => res.json()).then((data) => setData(data));
},[selectedState,selectedWeek]);

//  console.log(data)
// console.log(data.Data)
//   const model_type = props.model_value;
//   console.log(model_type)

  useEffect(() => {
   
    chartRef.current = am4core.create(CHART_ID, am4charts.XYChart);
    chartRef.current.data = data.Data_Claim_Counts 
    // chartRef.current.dateFormatter.inputDateFormat = "yyyy-MM-dd";
    chartRef.current.dateFormatter.dateFormat = "dd-MM-yyyy";
    chartRef.current.numberFormatter.numberFormat = "#.a";

    chartRef.current.events.on("beforedatavalidated", function(ev) {
      chartRef.current.data.sort(function(a, b) {
        return (new Date(a.date)) - (new Date(b.date));
      });
    });




// Create axes
var dateAxis = chartRef.current.xAxes.push(new am4charts.DateAxis());
dateAxis.renderer.grid.template.location = 0;
dateAxis.renderer.minGridDistance = 50;

var valueAxis = chartRef.current.yAxes.push(new am4charts.ValueAxis());
valueAxis.renderer.inside = true;
valueAxis.renderer.labels.template.verticalCenter = "bottom";
valueAxis.renderer.labels.template.dx = -5;
valueAxis.renderer.labels.template.dy = 10;
valueAxis.renderer.maxLabelPosition = 0.95;
valueAxis.title.text = "Claim count/week";
valueAxis.title.marginRight = 5;

// Create vertical scrollbar and place it before the value axis
          chartRef.current.scrollbarY = new am4core.Scrollbar();
          chartRef.current.scrollbarY.parent = chartRef.current.leftAxesContainer;
          chartRef.current.scrollbarY.toBack();
// Create series
function createSeries(field, name, color, dashed) {
  var series = chartRef.current.series.push(new am4charts.LineSeries());
  series.dataFields.valueY = field;
  series.dataFields.dateX = "date";
  series.name = name;
  series.tooltipText = "[bold]{name}[/]\n{dateX}: [b]{valueY}[/]";
  series.strokeWidth = 2;
//   series.smoothing = "monotoneX";
  series.stroke = color;
  
  if (dashed) {
    series.strokeDasharray = "5 3";
  }
  
  return series;
}

createSeries("actual_value", "Actual Value", am4core.color("#0a54c3")); //#B1B106
createSeries("mean_forecast_value", "Expected", am4core.color("#D68C45"), true);
createSeries("upper_bound", "Maximum Expected", am4core.color("#2C6E49"), true);
createSeries("lower_bound", "Mininum Expected", am4core.color("#B1B106"), true);

chartRef.current.legend = new am4charts.Legend();
chartRef.current.cursor = new am4charts.XYCursor();


// chartRef.current.data = data;

chartRef.current.exporting.menu = new am4core.ExportMenu();
    chartRef.current.exporting.menu.align = "right";
    chartRef.current.exporting.menu.verticalAlign = "top";
    return () => {
      chartRef.current && chartRef.current.dispose();
    };
    
  });
 
  
  return (
    <>
    { data === undefined || data === null || data.length === 0 ? (
     " "
    ) :
    <div
      id={CHART_ID}
      style={{ width: "100%", height: "300px" }}
    >
      
    </div>
   
  
    }
  </>
  );
}

export default ClaimsLineSeries;
