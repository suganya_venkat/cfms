import React, { useEffect, useState } from "react";
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@mui/material/Button';

export default function ForecastFilter(props) {

  const[getFilterState,setFilterState]=useState('all')
  const[getFilterWeek,setFilterWeek]=useState(8)
  // const [getState, setState] = React.useState('');
  const handleChange = (event) => {
    // setState(event.target.value);
    // props.setState(event.target.value);
    setFilterState(event.target.value)
    
  };
  const handleChangeWeek = (event) => {
    // setState(event.target.value);
    // props.setWeek(event.target.value);
    setFilterWeek(event.target.value)
    
  };

  const passFilterValues = () =>{
    props.setState(getFilterState)
    props.setWeek(getFilterWeek)
  
  }

  
  // const model_value = Model
  // console.log(model_value);
  const state_names = ['all','Selangor', 'Negeri', 'Wilayah', 'Melaka' ,'Johor', 'Jalan' ,'Sarawak', 'Perak',
  'Pulau', 'Sabah' ,'Kedah', 'Senai' ,'Taman', 'Penang', 'Terengganu',
  'Kuala', 'Kelantan' ,'Malacca','Bandar' ,'Perlis', 'Pahang']
const week = [4,6,8]
  return (
    <div style ={{marginTop:14}}className='filter_div'>
    <Box sx={{ minWidth: 158 }}>
      <FormControl fullWidth className='filter_flex'>
        <InputLabel id="demo-simple-select-label">Select State</InputLabel>
        <Select 
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          displayEmpty
          value={getFilterState}
          label="Select State"
          onChange={handleChange}
        >
          {/* <MenuItem value=""><Put any default Value which you want to show/></MenuItem> */}
          {
            state_names.map((d) => {
             
                return ( 
            <MenuItem value={d}>{d}</MenuItem>
                )
              })
              
               
}
        </Select>
      </FormControl>
    </Box>
    <Box sx={{ minWidth: 158 }}>
      <FormControl fullWidth className='filter_flex'>
        <InputLabel id="demo-simple-select-label">Select week</InputLabel>
        <Select 
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          displayEmpty
          value={getFilterWeek}
          label="Select week"
          onChange={handleChangeWeek}
        >
          {/* <MenuItem value=""><Put any default Value which you want to show/></MenuItem> */}
          {
            week.map((d) => {
             
                return ( 
            <MenuItem value={d}>{d}</MenuItem>
                )
              })
              
               
}
        </Select>
      </FormControl>
    </Box>
 
    <Button onClick={passFilterValues}   style={{
      
      backgroundColor: "black",
      color:"white",
      height: 32,
    fontSize: 12,
    marginTop: 16,
    marginLeft: 12
    }}   > Filter</Button>
    </div>
  );
 
}
