import React,{useState} from 'react'
import Grid from "@material-ui/core/Grid";

import Paper from "@material-ui/core/Paper";
import { Typography } from "@material-ui/core";
import ClaimsLineSeries from './ClaimsLineSeries'
import NavBar from '../../Components/Navbar'
import MainNavBar from '../../Components/MainNavBar'
import ForecastFilter from './ForecastFilter'
import ClaimsForecastKPI from './ClaimsForecastKPI';
import ClaimForecastAmountSeries from './ClaimsForecastAmountSeries';
import ClaimForecastAmtKPI from './ClaimForecastAmtKPI';
import AutoGraphIcon from '@mui/icons-material/AutoGraph';
import AnalyticsandReportNavBar from '../../Components/AnalyticsandReportNavBar'
import Popover from '@mui/material/Popover';
import CardMedia from "@material-ui/core/CardMedia"
import ReportingBe from '../../assets/images/ReportingBe.png'
import ProgressBarTop from '../../assets/images/ProgressBarTop.PNG'
import NewTheme from '../../Components/newTheme.js';
import './claimsForecastStyle.css'
import { styled } from '@mui/material/styles';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import InfoIcon from '@mui/icons-material/Info';
import '../CommonStyling/Style.css'

const HtmlTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#e6e6fa',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 225,
    height:50,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
    padding:"10px"
  },
}));

function ClaimsForecast() {

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;


  const [getState, setState] = useState('all');
  const [getWeek, setWeek] = useState(8);

  
    return (
        <div>
       
          <AnalyticsandReportNavBar/>
          {/* <CardMedia
     className='ProgressBarTopClaimsForecast'   
          image={ProgressBarTop}

 /> */}

          {/* <NewTheme/>
          <div className='activeLineSummary'></div><br/> */}

        <NavBar/>
        
        <div  className="infoHeadingContainer" style={{ marginTop: 13}}>
        {/* <AutoGraphIcon style={{fontSize: 47,cursor: "pointer",marginLeft:8}}  onClick={handleClick} />   */}
         <h3 style={{color:"black",marginTop:15,marginLeft: 32}}> Forecast Intelligence</h3><br/>
        <span className="forecastInfo">
       <HtmlTooltip  placement="right-start"
        title={
          <React.Fragment>
               <p style={{fontSize:12}}><i>Estimation of future claims and amounts</i></p>
               </React.Fragment>
        }
      >
        <InfoIcon className="infoIcon" />
      </HtmlTooltip>
     
      </span>
      
     
     </div>
     
        <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
         <CardMedia
          className='cardMediaStylepopup'   
              image={ReportingBe}

/>
        {/* <Typography sx={{ p: 2 }}>The content of the Popover.</Typography> */}
      </Popover>
        <br/>
   <ForecastFilter setState={setState} getState={getState} setWeek={setWeek} getWeek={getWeek}/>
   <br/>
   {/* style={{marginLeft: 43,
    marginRight: 25}}
style={{marginLeft: 43,
    marginRight: 25}} */}
   <div style={{margin:28}}>
       <Grid  container spacing={1}>
        <Grid  item xs = {12}>
        <Paper elevation={3} className="fraudratio">
      
        <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}><center>Claims Forecast Count </center>
       </p>
    
          
          {/* <Typography variant="h6" color="primary">
          Claims By Claims Type
          </Typography> */}
        
        <ClaimsLineSeries setState={setState} getState={getState} setWeek={setWeek} getWeek={getWeek}/>
        {/* <ClaimsForecastKPI/> */}
       
        </Paper>
        </Grid>
        
       
        
        </Grid>
      <br/>
        <Grid  item xs = {12}>
        <Paper elevation={3} className="fraudratio">
      
        <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}><center>
       Claims forecast Amount </center>
       </p>
      
       <ClaimForecastAmountSeries setState={setState} getState={getState} setWeek={setWeek} getWeek={getWeek}/>

          {/* <Typography variant="h6" color="primary">
          Claims By Claims Type
          </Typography> */}
       
       
        </Paper>
        </Grid>
        <br/>
        <ClaimForecastAmtKPI setState={setState} getState={getState} setWeek={setWeek} getWeek={getWeek}/>

        </div>
        
       
        </div>
    )
}

export default ClaimsForecast
