import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import "./claimsForecastStyle.css"
// import Data from './KpiCards_histogramData.json'
import ForecastData from './Forecastdata.json'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    border: `1px solid #EEF3F5`,
    borderRadius: 4,
    color: "#949FAF",
    minWidth: 275,
    minHeight: 70,
    marginTop: 12,
    marginLeft: 12,
    marginRight: 12,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    paddingBlock: 3,
    borderRadius: 5,
  },
  card: {
    color: "#16448F",
    fontWeight: 700,
    display: "inline-block"
  },
  cardsmall:{
    color: '#5DC9A2',
    fomtWeight: 500,
    display: "inline-block",
    fontSize: 12,
    marginInline: 4 
  }
}));


// const fetchURL_data = "http://localhost:5001/kpicards";


export default function ClaimForecastAmtKPI(props) {

    
  // const [items, setItems] = useState([]);
  const selectedState = props.getState;
  const selectedWeek = props.getWeek
  // console.log(selectedState)
  const [items, setItems] = useState([]);
    
  const [data,setData]=useState(ForecastData);



// useEffect(() => {
// fetch('/get_forecast').then(res => res.json()).then((data) => setData(data));
// },[]);


useEffect(() => {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      "state": selectedState,
      "weeks": selectedWeek

    })
  };
  fetch('/get_forecast', requestOptions)
  .then(res => res.json()).then((data) => setData(data));
},[selectedState,selectedWeek]);

//  console.log(data)
    const classes = useStyles();

    // const [data,setData]=useState({"Claim amount expected ($)": 2380341.8752253754, "Expected  total payment ($)": 1717520.6199729675});
    

    // const selectedState = props.getState;
    // const selectedToRptDate=props.getToRptDate;
    // const selectedClaimType=props.getClaimType;
    // const selectedClaimAmount=props.getClaimAmount;
    // const selectedFromRptDate = props.getFromRptDate;
  // console.log(selectedState,selectedFromRptDate,selectedToRptDate,selectedClaimType,selectedClaimAmount)
  //  useEffect(() => {
  // fetch('/kpicards').then(res => res.json()).then((data) => setData(data));
  // },[]);

//   useEffect(() => {
//     const requestOptions = {
//       method: 'POST',
//       headers: { 'Content-Type': 'application/json' },
//       body: JSON.stringify({"reporting_view_filters" :  

//       {"CLAIMREPORTDATE" : [{"from":  selectedFromRptDate,  

//                           "to": selectedToRptDate}], 

//       "STATE" : selectedState,  

//       "CLAIM_TYPE": selectedClaimType,  

//       "CLAIM_AMT_USD": selectedClaimAmount

//     } 

//     } )
//     };
//     fetch('/reporting_overview_claims', requestOptions)
//     .then(res => res.json()).then((data) => setData(data));
//   },[selectedState,selectedFromRptDate,selectedToRptDate,selectedClaimType,selectedClaimAmount]);
//     console.log(data)
//     useEffect(() => {
//   fetch(fetchURL_data

//        )
    
// .then((res) => res.json())
//     .then((data) => setData(data));
//     // setLoading(false)
// },[]);



  return (
      
    <div >
        {}
      {}
{Object.keys(data).length > 0 ? (
         <div >
          <Grid container alignItems="center" spacing={1}>
          <Grid item xs={12} sm={4}>
              <Paper className={classes.paper}>
                {" "}
                Estimated Claim Count <br /><br />{" "}
               
                <div className={classes.card}>
                {(data["Estimated claim count"]).toFixed(0)}
                </div>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Paper className={classes.paper}>
                {" "}
                Forecasted Total Claim Amount $(US) for {selectedWeek} week <br /><br />{" "}
                <div className={classes.card}>
                {(data["Forecasted Total Claim Amount $(US)"]).toLocaleString('en-US', {minimumFractionDigits: 0, maximumFractionDigits: 0})} ({(data["Forecasted Total Claim Amount $(US)"]/1000000).toFixed(2)}M)</div>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Paper className={classes.paper}>
                {" "}
                Forecasted Total Payout $(US) for {selectedWeek} week <br /><br />{" "}
               
                <div className={classes.card}>
                {(data["Forecasted Total Payout $(US)"]).toLocaleString('en-US', {minimumFractionDigits: 0, maximumFractionDigits: 0})} ({(data["Forecasted Total Payout $(US)"]/1000000).toFixed(2)}M)
              
                </div>
              </Paper>
            </Grid>
                     </Grid>
         </div> 
     
   
): "loading"} 
 </div>
  );
}
