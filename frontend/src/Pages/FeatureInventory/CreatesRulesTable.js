import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import CreatedRulesTableData from './CreatedRulesTableData.json'
import { styled } from '@mui/material/styles';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import InfoIcon from '@mui/icons-material/Info';


const HtmlTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#e6e6fa',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 220,
    height:90,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
    padding:"10px"
  },
}));

export default function CreatedRulesTable() {
  return (
    <TableContainer  style={{marginLeft:10,height:318}} component={Paper}>
      <Table sx={{ Width: 400 }} size="small" className='s' aria-label="a dense table sticky table" 
           //   className={classes.table1}
           // style={{ padding: "6px 23px 6px 16px" }}
        stickyHeader
           >
        <TableHead>
          <TableRow>
            <TableCell >Feature Name</TableCell>
            <TableCell align="center" style={{whiteSpace: "nowrap"}}>Feature Type</TableCell>
            <TableCell  align="center" style={{whiteSpace: "nowrap"}}>Feature Predictiveness
            <HtmlTooltip  placement="right-start"
        title={
          <React.Fragment>
           
           
       <div  style={{ fontSize:10}}><b>Feature productiveness is captured through statistical measure of Information Value (IV). It captures the relative frequency of % of fraud in feature, weighted by Weight of Evidence</b></div> 


          </React.Fragment>
        }
      >
        <InfoIcon style={{fontSize:13,color:"#8d848d"}}/>
      </HtmlTooltip>
            </TableCell>
           </TableRow>
        </TableHead>
        <TableBody>
          {CreatedRulesTableData.map((row) => (
            <TableRow
              key={row.index}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.index}
              </TableCell>
              <TableCell align="center">Binary</TableCell>
              <TableCell align="center">{row.percent_ft_importance}</TableCell>
            
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
