import React from 'react'
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia"
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { Typography } from "@material-ui/core";
import Configuration_navbar from '../../Components/Configuration_navbar'
// import MainNavBar from '../../Components/MainNavBar'
// import CreateRule from './CreateRule'
// import RuleBankPage from './RuleBankPage'
// import NewTheme from '../../Components/newTheme.js';
import DomainAddIcon from '@mui/icons-material/DomainAdd';
import AnalyticsNavBar from '../../Components/AnalyticsNavBar'
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import { makeStyles } from "@material-ui/core/styles";

import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import InfoIcon from '@mui/icons-material/Info';
import BuiltInFeatureTable from './BuildInFeatureTable';
import CreatesRulesTable from './CreatesRulesTable';
import BuildFeatureLineChart from './BuildFeatureLineChart';
import CreateFeatureLineChart from './CreateFeatureLineChart';
import BuildInFeatureLineChartPic from '../../assets/images/BuildInFeatureLineChart.png'
import CreatedRulesLineChartPic from '../../assets/images/CreatedRulesLineChart.png'
// import '../CommonStyling/Style.css'
// import '../CommonStyling/Style.css'
import './FeatureInventory.css'
// import RuleSummary from './RuleSummary';

const HtmlTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#e6e6fa', //e0ffe0
    color: 'rgba(0, 0, 0, 0.87)',
    width: 400,
    height:100,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
    padding:"10px"
  },
}));

function FeatureInventory() {

  const useStyles = makeStyles(theme => ({
   
    cardMediaStyle: {
      paddingTop: "50.5%",
      // width:'250px'
    }
  }));
  const styles = useStyles();

  
    return (
        <div style={{margin:0}}>
            
            <AnalyticsNavBar/>
        

        
     <Configuration_navbar/>
    
     <div style={{margin: 10}}>
    
    
     
     {/* <p style={{fontSize:11,marginLeft: 53}}> Incorporating the domain knowledge to identify suspicious claims</p>
<br/>

      <p style={{fontSize:11,marginLeft: 53}}><i>Example: A very high Report Lag can possibly mean high chances of fraud.</i></p> */}
     
      

         
       
     <Grid  container spacing={1}>     <Grid item xs = {6}>
    
   
      <p className='Header'>
      
   Bivariate Analysis - Built In Features
      </p>
<br/>
   <BuiltInFeatureTable/>

   
   
    </Grid>
    <Grid item xs = {6}>

    <Paper style={{marginLeft:8}} >
   <div style={{display:"flex"}}>
   
      <p className='Feature' style={{flex:1,marginRight: 10,fontSize: 12}}>
      
   Self Accident
      </p>
      <Box sx={{ minWidth: 158 }}  >
      <FormControl fullWidth style={{marginTop:-9}}>
        <InputLabel id="demo-simple-select-label" style={{fontSize:13}}>Select Feature</InputLabel>
        <Select 
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          displayEmpty
        //   value={props.model_value}
          label="Model"
        //   onChange={handleChange}
        >
          {/* <MenuItem value=""><Put any default Value which you want to show/></MenuItem> */}
          <MenuItem value={"Model1"}>Self Accident</MenuItem>
          <MenuItem value={"Model2"}>Accident Occurs On Weekend</MenuItem>
          <MenuItem value={"Model3"}>Claim within close proximity of inception date	</MenuItem>
          <MenuItem value={"Model4"}>Notification of claim long after claim loss date	</MenuItem>

        </Select>
      </FormControl>
    </Box>
   
      </div>
      <CardContent>
        <CardMedia
           className={styles.cardMediaStyle}    
              image={BuildInFeatureLineChartPic}

/>
        </CardContent>
{/* <BuildInFeatureLineChartPic/> */}
  
   
    </Paper>
    
   
    {/* <Grid item xs = {12}>
    <Paper style={{marginLeft:8}} > */}
{/*    
      <p className='Header'>
      
   Rule Summary
      </p> */}

   
      {/* <CreateFeatureLineChart/> */}

    {/* <RuleSummary  getFeature={getFeature} setFeature={setFeature}/> */}
   
    {/* </Paper>
    </Grid> */}
    </Grid>
    </Grid>
    {/* </Grid> */}
<br/>

<Grid  container spacing={1}>     <Grid item xs = {6}>
   
   
      <p className='Header'>
      
   Bivariate Analysis - Created Rules
      </p>
<br/>
   <CreatesRulesTable/>

   
   
    </Grid>
    <Grid item xs = {6}>

    <Paper style={{marginLeft:8}} >
   <div style={{display:"flex"}}>
   
      <p className='Feature' style={{flex:1,marginRight: 10,fontSize: 12}}>
      
   Claim Amount Greater than $5000
      </p>
      <Box sx={{ minWidth: 158 }}  >
      <FormControl fullWidth style={{marginTop:-9}}>
        <InputLabel id="demo-simple-select-label" style={{fontSize:13}}>Select Feature</InputLabel>
        <Select 
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          displayEmpty
        //   value={props.model_value}
          label="Model"
        //   onChange={handleChange}
        >
          {/* <MenuItem value=""><Put any default Value which you want to show/></MenuItem> */}
          <MenuItem value={"Model1"}>Claim Amount Greater than $5000</MenuItem>
          <MenuItem value={"Model2"}>Has Report Lag Greater than 14 days</MenuItem>
          {/* <MenuItem value={"Model3"}>Notification of claim after claim expiry</MenuItem>
          <MenuItem value={"Model4"}>Average claim amount in the last 36 months greater than 10K</MenuItem> */}

        </Select>
      </FormControl>
    </Box>
   
      </div>
      <CardContent>
        <CardMedia
           className={styles.cardMediaStyle}    
              image={CreatedRulesLineChartPic}

/>
        </CardContent>
{/* <BuildInFeatureLineChartPic/> */}
  
   
    </Paper>
    
   
    {/* <Grid item xs = {12}>
    <Paper style={{marginLeft:8}} > */}
{/*    
      <p className='Header'>
      
   Rule Summary
      </p> */}

   
      {/* <CreateFeatureLineChart/> */}

    {/* <RuleSummary  getFeature={getFeature} setFeature={setFeature}/> */}
   
    {/* </Paper>
    </Grid> */}
    </Grid>
    </Grid>
    {/* </Grid> */}

    </div>
     </div>
    )
}

export default FeatureInventory
