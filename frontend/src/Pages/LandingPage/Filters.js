import * as React from 'react';
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@mui/material/Button';

import './Filters.css'

export default function Filters(props) {
 
  // const [Model, setModel] = React.useState('');
  const handleChange = (event) => {
    props.setmodel_value(event.target.value);
    
  };
  // const model_value = Model
  // console.log(model_value);
  
  return (
    <div className='modelButton'>
    <Box sx={{ minWidth: 158 }} className='filter_flex'>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">Select Model</InputLabel>
        <Select 
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          displayEmpty
          value={props.model_value}
          label="Model"
          onChange={handleChange}
        >
          {/* <MenuItem value=""><Put any default Value which you want to show/></MenuItem> */}
          <MenuItem value={"Model1"}>Model 1</MenuItem>
          <MenuItem value={"Model2"}>Model 2</MenuItem>
          <MenuItem value={"Model3"}>Model 3</MenuItem>
        </Select>
      </FormControl>
    </Box>
    {/* <Button className='filter_flex'   style={{marginLeft:511, backgroundColor: "#00965f",color:"white"}}>Start Model Training</Button> */}

    </div>
  );
 
}
