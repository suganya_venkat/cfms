import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Data from './KpiCards_histogramData.json'
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import KpiCards_data from './claims_overview_summary_dict.json'

import SpinnerPage from "../../utils/loader";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    border: `1px solid #EEF3F5`,
    borderRadius: 4,
    color: "#949FAF",
    minWidth: 275,
    minHeight: 70,
    marginTop: 12,
    marginLeft: 12,
    marginRight: 12,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    paddingBlock: 5,
    borderRadius: 5,
  },
  card: {
    color: "#16448F",
    fontWeight: 700,
    display: "inline-block"
  },
  cardsmall:{
    color: '#5DC9A2',
    fomtWeight: 500,
    display: "inline-block",
    fontSize: 12,
    marginInline: 4 
  }
}));


const fetchURL_data = "http://localhost:5001/kpicards";


export default function KpiCards(props) {
    const classes = useStyles();

    const [data,setData]=useState(KpiCards_data);
    const [open, setOpen] = useState(true);

    const selectedState = props.getState;
    const selectedToRptDate=props.getToRptDate;
    const selectedClaimType=props.getClaimType;
    const selectedClaimAmount=props.getClaimAmount;
    const selectedFromRptDate = props.getFromRptDate;
  // console.log(selectedState,selectedFromRptDate,selectedToRptDate,selectedClaimType,selectedClaimAmount)
  //  useEffect(() => {
  // fetch('/kpicards').then(res => res.json()).then((data) => setData(data));
  // },[]);

  useEffect(() => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({"reporting_view_filters" :  

      {"CLAIMREPORTDATE" : [{"from":  selectedFromRptDate,  

                          "to": selectedToRptDate}], 

      "STATE" : selectedState,  

      "CLAIM_TYPE": selectedClaimType,  

      "CLAIM_AMT_USD": selectedClaimAmount

    } 

    } )
    };
    fetch('/reporting_overview_claims', requestOptions)
    .then(res => res.json()).then((data) => setData(data));
  },[selectedState,selectedFromRptDate,selectedToRptDate,selectedClaimType,selectedClaimAmount]);
    console.log(data)
//     useEffect(() => {
//   fetch(fetchURL_data

//        )
    
// .then((res) => res.json())
//     .then((data) => setData(data));
//     // setLoading(false)
// },[]);



  return (
      
    <div className={classes.root}>
        {}
      {}
{Object.keys(data).length > 0 ? (
         <div>
          <Grid container alignItems="center" spacing={1}>
            <Grid item xs={12} sm={3}>
              <Paper className={classes.paper}>
                {" "}
                Claims Initiated <br /><br />{" "}
                <div className={classes.card}>{data["Claims Initiated"].toLocaleString("en", {   
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
})}</div>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Paper className={classes.paper}>
                {" "}
                Claims in progress <br /><br />{" "}
               
                <div className={classes.card}>
                {data["Claims in Progress"].toLocaleString("en", {   
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
})}
                </div>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Paper className={classes.paper}>
                {" "}
               
                Claims closed <br /><br />{" "}
                <div className={classes.card}> {data["Claims Closed"].toLocaleString("en", {   
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
})}</div>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Paper className={classes.paper}>
                {" "}
               
                Claims paid count <br /><br />{" "}
                <div className={classes.card}>
                  {data["Claims Paid"]["counts"]["Yes"].toLocaleString("en", {   
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
})}
                </div>
              </Paper>
            </Grid>
            </Grid>
            
            <Grid container alignItems="center" spacing={1}>
            <Grid item xs={12} sm={4}>
              <Paper className={classes.paper}>
                {" "}
              
                Average claims amount <br /><br />{" "}
                <div className={classes.card}>
                   
                ${ data["Average Claim Amount"].toLocaleString("en", {   
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
})}
                </div>
              </Paper>
            </Grid>
           
            
            <Grid item xs={12} sm={4}>
              <Paper className={classes.paper}>
                {" "}
              
               Average paid amount <br /><br />{" "}
                <div className={classes.card}>
                ${data["Average Paid Amount"].toLocaleString("en", {   
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
})}
                </div>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Paper className={classes.paper}>
                {" "}
               
                Closure ratio <br /><br />{" "}
                <div className={classes.card}>
                {(data["Closure ratio"]*100).toFixed()}%
                </div>
              </Paper>
            </Grid>
          </Grid>
         </div> 
     
   
):  
"Loading"
} 
 </div>
  );
}
