import React,{useState} from 'react'
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import { Typography } from "@material-ui/core";
import ClaimsAmountTable from './ClaimsAmountTable';
import ClaimsReported from './ClaimsReported';
import Filters from './Filters';
import ClaimsbassedonMake from './ClaimsbasedonMake';
import ClaimsTrend from './ClaimsTrend';
import ClaimsDataByStateTable from './ClaimsDataByStateTable';
import ClaimsCountLast12Month from './ClaimsCountLast12Month'
import FraudRatio from '../Insights/FraudRatio';
import ClaimsByClaimsType from './ClaimsByClaimsType';
import ClaimsAmtByClaimType from './ClaimsAmtByClaimType';
import ClaimsAmtByState from './ClaimsAmtByState';
import ClaimsByState from './ClaimsByState';
import Grid from "@material-ui/core/Grid";
import ClaimsSeries from './ClaimsSeries';
import KpiCards from './KpiCards'
import './Filters.css'
import IncidentBasedClaims from '../Insights/IncidentBasedClaims';
import NavBar from '../../Components/Navbar'
import ManinNavBar from '../../Components/MainNavBar'
import PageFilter from './PageFilter';
import ClaimsInvestigatiion from './ClaimsInvestigation'
import MonthlyAmount from './MonthlyAmount';
import AmountKPICards from './AmountKPICards'
import AmountByClaimType from './AmountsByClaimType';
import AmountByState from './AmountByState';
import AssessmentIcon from '@mui/icons-material/Assessment';
import AnalyticsandReportNavBar from '../../Components/AnalyticsandReportNavBar'
import ReportingBe from '../../assets/images/ReportingBe.png'
import Popover from '@mui/material/Popover';
import CardMedia from "@material-ui/core/CardMedia"
import NewTheme from '../../Components/newTheme.js';
import ProgressBarTop from '../../assets/images/ProgressBarTop.PNG'
import { styled } from '@mui/material/styles';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import InfoIcon from '@mui/icons-material/Info';
import '../CommonStyling/Style.css'

const HtmlTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#e6e6fa',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 225,
    height:50,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
    padding:"10px"
  },
}));

function Summary() {

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;



  const [getState, setState] = useState(["all"]);
  const[getFromRptDate,setFromRptDate] = useState("2020-01-01 00:00:00");
  const[getToRptDate,setToRptDate] = useState("2021-12-31 00:00:00");
  const[getClaimType,setClaimType] = useState(["all"]);
  const[getClaimAmount,setClaimAmount] = useState(["all"]);
 



  return (
  
    <div className='divalign'>
      
          
      <AnalyticsandReportNavBar/>
     
    
        {/* <CardMedia
     className='progressTopReporting'   
          image={ProgressBarTop}

 /> */}
      {/* <NewTheme/>
    
      <div className='activeLineSummary'></div><br/> */}
      <NavBar/>
        <br/>
        <div  className="infoHeadingContainer">
        {/* <AssessmentIcon style={{fontSize: 47,cursor: "pointer"}}  onClick={handleClick}/>    */}
        <h3 style={{color:"black",marginLeft:33,marginTop:15}}>  Claims Summary Dashboard</h3><br/>
        <span className="summaryInfo">
       <HtmlTooltip  placement="right-start"
        title={
          <React.Fragment>
             <p style={{fontSize:12,padding:5}}><i>A holistic view around claims</i></p>
               </React.Fragment>
        }
      >
        <InfoIcon className="infoIcon" />
      </HtmlTooltip>
     
      </span>
      
     
     </div>
        
        <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
         <CardMedia
          className='cardMediaStylepopup'   
              image={ReportingBe}

/>
        {/* <Typography sx={{ p: 2 }}>The content of the Popover.</Typography> */}
      </Popover>

        <br/><br/>
<PageFilter setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/>
<br/>
      <CardContent>
      <KpiCards setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/>
      </CardContent>
      {/* <div>
        <br/>
      <Filters/>
      </div>
      <br/> */}
      <div style={{margin:10}}>
      <Grid  container spacing={1} >
      {/* style={{marginLeft: 3}} */}
        <Grid item xs = {3}>
        <Paper elevation={3} className="fraudratio">
       
          
          <ClaimsCountLast12Month setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/>
          {/* <Typography variant="h6" color="primary">
          Claims By Claims Type
          </Typography> */}
       
        {/* <CardContent>
     
        
       
        </CardContent> */}
        </Paper>
        </Grid>
        
       
        <Grid item xs = {3} > 
        <Paper elevation={3}  >
      
        <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}>
          <center> Claims By Claims Type</center>
          </p>
       
       <br/>
        <ClaimsByClaimsType setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/>
        {/* <ClaimsAmtByClaimType />  */}
       
        </Paper>
        </Grid>
        <Grid item xs = {3} width ="50px"> 
        <Paper elevation={3} className="a" >
     
         
        <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}>
       <center> Claims By State </center>  
          </p>
        <br/>
       
        <ClaimsByState setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/>
  
  {/* <ClaimsAmtByState /> */}
        </Paper>
        </Grid>
        <Grid item xs = {3} width ="50px"> 
        <Paper elevation={3} className="a" >
       
        <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}>
       <center>  Claims By Final Status </center>
          </p>
        <br/>
          {/* <Typography variant="h6" color="primary">
           Claimed Amount By Claim Type
          </Typography> */}
           <ClaimsInvestigatiion setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/>
       
        
        </Paper>
        </Grid>
        </Grid>
        <br/>

        <Grid  container spacing={1} >
          {/* style={{marginLeft: 3}} */}
        <Grid item xs = {3}>
        <Paper elevation={3} className="fraudratio">
       
         
          <MonthlyAmount setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/>
          {/* <Typography variant="h6" color="primary">
          Claims By Claims Type
          </Typography> */}
      
        </Paper>
        </Grid>
        
       
        <Grid item xs = {3} > 
        <Paper elevation={3} className="a" >
       
        <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}>
       <center> Amount($) By Claim Type </center>
          </p>
        
        <br/>
        {/* <ClaimsByClaimsType /> */}
        <AmountByClaimType setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/> 
        
        </Paper>
        </Grid>
        <Grid item xs = {3} > 
        <Paper elevation={3} className="a" >
     
        <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}><center> Amount($) By State </center> 
          </p>
        
      <br/>
        {/* <ClaimsByState /> */}
  
  <AmountByState setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate} />
      
        </Paper>
        </Grid>
        <Grid item xs = {3} > 
       
       
           <AmountKPICards setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/>
         
       
        
        </Grid>
        </Grid>
      
      <br/>
        <Grid container  spacing={1} >
          {/* style={{marginLeft: 3}} */}
        <Grid item xs = {12}>
        <Paper elevation={3} >
      
      <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}><center> Daywise Claim Report </center>
        </p>
     
      <br/>
      <ClaimsSeries setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/>
             <p style={{color:"black",fontSize:12,marginLeft:600,marginTop:-39}}><i> Adjust the Time window</i></p>

      </Paper>
      </Grid>
     
      </Grid>
      </div>
      </div>
    

    
  )
}

export default Summary
