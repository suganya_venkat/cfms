import React, { useRef, useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from 'react-router-dom';
import claimsdata from './KpiCards_histogramData.json';
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TablePagination from '@material-ui/core/TablePagination';


import './Filters.css'


// import SpinnerPage from "../../utils/loader";

/*const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.white,
    color: theme.palette.common.Grey,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {npm
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);*/

const useStyles = makeStyles({
//   table1: {
//     minWidth: 450,
//   },
//   root: {
//     "& .MuiTableCell-head": {
//         color: "white",
//         backgroundColor: "#0a58cac4"
//     },
// },

  container1: {
    maxHeight: 300,
    // marginTop: 5,
  },
//   banner: {
//     display: "flex",
//     justifycontent: "space-between",
//     background: "#EDF2F5",
//     height: "48px",
//   },
//   right_align: {
//     position: "absolute",
//     right: "55px",
//     margintop: "10px",
//     color: "grey",
//     top: "70px",
//     fontSize: 13,
//   },
});

// const fetchURL =
//   "http://makeup-api.herokuapp.com/api/v1/products.json?brand=maybelline";
// const getItems = () => fetch(fetchURL).then((res) => res.json());

function ClaimsDataByStateTable() {


  const classes = useStyles();
// console.log(claimsdata[0].Tabledata_statewise[0])
    
    const [data,setData]=useState([]);

     useEffect(() => {
  fetch('/kpicards').then(res => res.json()).then((data) => setData(data));
  },[]);

// var a =data.Tabledata_statewise
//     ? data.Tabledata_statewise.map((a) => {
// // console.log(a.STATE)
//     }) : ""

const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <div>
        
       
      {data.Tabledata_statewise === undefined ? (
        <div>
          {" "}
          {" "}
        </div>
      ) : (
        <div>

{/* <SearchBar
          value={searched}
          onChange={(searchVal) => requestSearch(searchVal)}
          onCancelSearch={() => cancelSearch()}
        /> */}
          <TableContainer  className={classes.container1}>
            <Table
               aria-label="sticky table"
           //   className={classes.table1}
           // style={{ padding: "6px 23px 6px 16px" }}
        
           stickyHeader

            >
              <TableHead>
                <TableRow > 
                  {/* className={classes.root} */}
                  <TableCell align="center" style={{backgroundColor: "#00965f", color:"white"}}>
                  State
                  </TableCell>
                  <TableCell align="center" style={{backgroundColor: "#00965f", color:"white"}} >
                  Total Approved Amount($)
                  </TableCell>
                  <TableCell align="center" style={{backgroundColor: "#00965f", color:"white"}} >
                  Claim count
                  </TableCell>
                 
                  {/* <TableCell align="center" style={{ color: "grey" }}>
                    Rating
                  </TableCell> */}
                </TableRow>
              </TableHead>
             <TableBody>
                  {data.Tabledata_statewise[0]
    ? data.Tabledata_statewise.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((a) => {
                    
                       return ( 
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                        //   key={item.id}
                        >
                          
                          <TableCell align="center">
                              {a.STATE}
                              </TableCell>
                          <TableCell align="center">
                            {a.APPRV_AMT_USD.toFixed(2)}
                          </TableCell>
                          <TableCell align="center">
                              {a.Claim_count}
                              </TableCell>
                         
                        </TableRow>
                      ); 
                     })
                  : "Loading.."} 
              </TableBody>
            </Table>
           
          </TableContainer>
          <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={data.Tabledata_statewise ? data.Tabledata_statewise.length : ""}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
        </div>
      )}
    </div>
  );
}

export default ClaimsDataByStateTable;
