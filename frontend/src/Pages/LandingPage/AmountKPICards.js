import * as React from 'react';
import {useEffect, useState} from "react";
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import KpiCards_histogramData from './KpiCards_histogramData.json'
import SpinnerPage from '../../utils/loader';
import KpiCards_data from './claims_overview_summary_dict.json'



const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

export default function AmountKPICards(props) {

    const [data,setData]=useState(KpiCards_data);
    const selectedState = props.getState;
    const selectedToRptDate=props.getToRptDate;
    const selectedClaimType=props.getClaimType;
    const selectedClaimAmount=props.getClaimAmount;
    const selectedFromRptDate = props.getFromRptDate;

    console.log(data)
  // console.log(selectedState,selectedFromRptDate,selectedToRptDate,selectedClaimType,selectedClaimAmount)
  //  useEffect(() => {
  // fetch('/kpicards').then(res => res.json()).then((data) => setData(data));
  // },[]);

  useEffect(() => {
    // setData(KpiCards_data)
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({"reporting_view_filters" :  

      {"CLAIMREPORTDATE" : [{"from":  selectedFromRptDate,  

                          "to": selectedToRptDate}], 

      "STATE" : selectedState,  

      "CLAIM_TYPE": selectedClaimType,  

      "CLAIM_AMT_USD": selectedClaimAmount

    } 

    } )
    };
    fetch('/reporting_overview_claims', requestOptions)
    .then(res => res.json()).then((data) => setData(data));
  },[selectedState,selectedFromRptDate,selectedToRptDate,selectedClaimType,selectedClaimAmount]);

  return (
    <div>
      {Object.keys(data).length > 0 ? (
    <Stack gap={3} style={{ marginTop:"54px"}}>
      <Stack spacing={5} alignItems="center">
        <Item style={{"width": "257px" , padding:"15px"}}>
            <div>Approved Amount Ratio</div> 
            {(((data.ApprovedAmountRatio)*100).toFixed(0))}%
            
            </Item>
        <Item style={{"width": "257px"  , padding:"15px"}}>
        <div>Total Approved Amount($)</div> 
            {(data.TotalApprovedAmount/1000000).toFixed(0)}M</Item>
        <Item style={{"width": "257px"  , padding:"15px"}}>
        <div>Total Claimed Amount($)</div>
            {(data.TotalClaimedAmount/1000000).toFixed(0)}M</Item>
      </Stack>
     
    </Stack>
    ):" "} 
    </div>
  );
}
