import React, { useRef, useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from 'react-router-dom';
import claimsdata from './KpiCards_histogramData.json';
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TablePagination from '@material-ui/core/TablePagination';
import MonthlyAmountData from './MontlyAmount.json'
import KpiCards_data from './claims_overview_summary_dict.json'

import './Filters.css'


import { color } from "@amcharts/amcharts4/core";

// import SpinnerPage from "../../utils/loader";

/*const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.white,
    color: theme.palette.common.Grey,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {npm
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);*/

const useStyles = makeStyles({
//   table1: {
//     minWidth: 450,
//   },
//   root: {
//     "& .MuiTableCell-head": {
//         color: "white",
//         backgroundColor: "#0a58cac4"
//     },
// },

  container1: {
    maxHeight: 350,
    // marginTop: 5,
  },

});
function MonthlyAmount(props) {
  const classes = useStyles();
// console.log(claimsdata[0].Last12MonthCount)
    const [data,setData]=useState(KpiCards_data);

    
    const selectedState = props.getState;
    const selectedToRptDate=props.getToRptDate;
    const selectedClaimType=props.getClaimType;
    const selectedClaimAmount=props.getClaimAmount;
    const selectedFromRptDate = props.getFromRptDate;
  // console.log(selectedState,selectedFromRptDate,selectedToRptDate,selectedClaimType,selectedClaimAmount)

  //    useEffect(() => {
  // fetch('/kpicards').then(res => res.json()).then((data) => setData(data));
  // },[]);

  useEffect(() => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({"reporting_view_filters" :  

      {"CLAIMREPORTDATE" : [{"from":  selectedFromRptDate,  

                          "to": selectedToRptDate}], 

      "STATE" : selectedState,  

      "CLAIM_TYPE": selectedClaimType,  

      "CLAIM_AMT_USD": selectedClaimAmount

    } 

    } )
    };
    fetch('/reporting_overview_claims', requestOptions)
    .then(res => res.json()).then((data) => setData(data));
  },[selectedState,selectedFromRptDate,selectedToRptDate,selectedClaimType,selectedClaimAmount]);
    
var g = MonthlyAmountData
? MonthlyAmountData.map((fy) => {
  const monthname = ["January","February","March","April","May","June","July","August","September","October","November","December"];

  var date = new Date(fy.Month);
  var year = date.getFullYear();
  var month_ = monthname[date.getMonth()]
  // var datewithtime1 = date.getMinutes
  // fy.fiscalyear_quarter = year + "-Q" + fy.quarter;
  
  fy.YearMonth = month_ +"-"+ year;
  fy.MonthFullName = month_;
  fy.date = date
}) : ""

// MonthlyAmountData.map((fy) => {
//     console.log(fy.MonthFullName)
// })
// console.log(claimsdata[0].Last12MonthCount)
// var a =claimsdata[0].Tabledata_statewise[0]
//     ? claimsdata[0].Tabledata_statewise.map((a) => {
// console.log(a.STATE)
//     }) : ""
//   const [items, setItems] = useState();
//   const [sample, setSample] = useState([]);
//   const [page, setPage] = useState(0);
//   const [rowsPerPage, setRowsPerPage] = useState(5);
//   const [searched, setSearched] = useState("");


//   useEffect(() => {
//     getItems().then((data) => setItems(data));
//     getItems().then((data) => setSample(data));
//   }, []);

  // console.log(items);

//   const handleChangePage = (event, newPage) => {
//     setPage(newPage);
//   };

//   const handleChangeRowsPerPage = (event) => {
//     setRowsPerPage(parseInt(event.target.value, 10));
//     setPage(0);
//   };

//   const requestSearch = (searchedVal) => {
//     const filteredRows = sample.filter((item) => {
//       if (searchedVal === "") {
//         return item;
//       } else if (item.product_type.toLowerCase().includes(searchedVal.toLowerCase())) {
//         return item;
//       }
//     })
//     setItems(filteredRows);
//     console.log(filteredRows)
//   };

 
//   const cancelSearch = () => {
//     setSearched("");
//     requestSearch(searched);
//   };

const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  return (
    <div>
        
        {/* <Breadcrumbs aria-label="breadcrumb">
  <Link underline="hover" color="inherit" href="/">
    Home
  </Link> */}
  {/* <Link
    underline="hover"
    color="inherit"
    href="/getting-started/installation/"
  >
    Core
  </Link>
  <Typography color="text.primary">Breadcrumbs</Typography> */}
{/* </Breadcrumbs> */}
      {/* <div className={classes.root}>
        <form className={classes.right_align}>
          <input type="radio" name="choice" value="yes" /> Monthly
          <input type="radio" name="choice" value="no" /> Quaterly
          <input type="radio" name="choice" value="no" /> Yearly
        </form>
      </div> */}
      {data.Tabledata_statewise === undefined ? (
        <div>
          {" "}
          {" "}
        </div>
      ) : (
        <div>

{/* <SearchBar
          value={searched}
          onChange={(searchVal) => requestSearch(searchVal)}
          onCancelSearch={() => cancelSearch()}
        /> */}
          <TableContainer  className={classes.container1}>
            <Table
            //   className={classes.table1}
              stickyHeader
              aria-label="sticky table"
            >
              <TableHead>
                <TableRow > 
                  {/* className={classes.root} */}
                  <TableCell align="center" style={{fontSize:12,fontWeight:"bold"}} >
                  Month 
                  </TableCell>
                  <TableCell align="center"  style={{fontSize:12,fontWeight:"bold"}}>
                  Claimed($)
                  </TableCell>
                  <TableCell align="center" style={{fontSize:12,fontWeight:"bold"}}>
                  Approved($)
                  </TableCell>
                 
                  {/* <TableCell align="center" style={{ color: "grey" }}>
                    Rating
                  </TableCell> */}
                </TableRow>
              </TableHead>
             <TableBody>
                  {MonthlyAmountData
    ? MonthlyAmountData.map((a) => {
      // .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)      
                       return ( 
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                        //   key={item.id}
                        >
                          
                          <TableCell align="center" >
                              {a.YearMonth}
                              </TableCell>
                          <TableCell align="center">
                            {a.Claimed}
                          </TableCell>
                          <TableCell align="center">
                              {a.Approved}
                              </TableCell>
                         
                        </TableRow>
                      ); 
                     })
                  : "Loading.."} 
              </TableBody>
            </Table>
          
          </TableContainer>
            {/* <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={MonthlyAmountData ? MonthlyAmountData.length : ""}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        /> */}
        </div>
      )}
    </div>
  );
}

export default MonthlyAmount;
