import React, { useEffect, useState } from "react";
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Button from '@mui/material/Button';

import Select from '@material-ui/core/Select';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import FilterData from './FilterData.json'

import './Filters.css'

export default function PageFilter(props) {

  const [data,setData]=useState(FilterData);
  const [status,setStatus]=useState([]);
  const[getFilterState,setFilterState]=useState(['all'])
  const[getFilterClaimType,setFilterClaimType]=useState(['all'])
  const[getFilterClaimAmt,setFilterClaimAmt]=useState(['all'])
  const[getFilterFromRptDate,setFilterFromRptDate] =useState("2020-01-01 00:00:00");
  const[getFilterToRptDate,setFilterToRptDate] =useState("2021-12-31 00:00:00");


  useEffect(() => {
    fetch('/unique_values').then(res => res.json()).then((data) => setData(data));
    // setStatus(data.States);
    // status.push
    },[]);

    // const requestOptions = {
    //   method: 'GET',
    //   // headers: { 'Content-Type': 'application/json' },
    //   // body: JSON.stringify({
    //   //   "state": selectedState,
    //   //   "weeks": selectedWeek
  
    //   // })
    // };
    // fetch('/unique_values', requestOptions)
    // .then(res => res.json()).then((data) => setData(data));

    // console.log(status)
  const stateHandleChange = (event) => {
    const {
      target: { value },
    } = event;
    setFilterState(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value,
    );
  };

  

  const claimTypeHandleChange = (event) => {
    const {
      target: { value },
    } = event;
    setFilterClaimType(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value,
    );
  };

  
  const claimAmountHandleChange = (event) => {
    const {
      target: { value },
    } = event;
    setFilterClaimAmt(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value,
    );
  };

 //console.log(getFilterState,getFilterClaimType,getFilterClaimAmt)
  const handleFromDateChange = (date) => {
    setFilterFromRptDate(date)
   
  };
  const handleToDateChange = (date) => {
    setFilterToRptDate(date)
   
  };

  const passFilterValues = () =>{
    props.setState(getFilterState)
    props.setClaimType(getFilterClaimType)
    props.setClaimAmount(getFilterClaimAmt)
    props.setFromRptDate(getFilterFromRptDate);
    props.setToRptDate(getFilterToRptDate);
  }
  
  const state_names = ['all','Selangor',
  'Negeri',
  'Wilayah',
  'Melaka',
  'Johor',
  'Jalan',
  'Sarawak',
  'Perak',
  'Pulau',
  'Sabah',
  'Kedah',
  'Senai',
  'Taman',
  'Penang',
  'Terengganu',
  'Kuala',
  'Kelantan',
  'Malacca',
  'Bandar',
  'Perlis',
  'Pahang']

const Claim_Types= ['all','Natural Calamities', 'Accidental', 'Vandalism', 'Fire', 'TPBI', 'Theft']
const Claim_Amount= ['all','0-1000', '1000-10000', '10000+']


  return (
    <div style ={{marginLeft: 31}} className='filter_div'>
    <Box sx={{ minWidth: 179 }} className='filter_flex'>
    <MuiPickersUtilsProvider utils={DateFnsUtils} >

<KeyboardDatePicker
  label="Reported Date From"
  value={getFilterFromRptDate}
  onChange={handleFromDateChange}
  format="dd/MM/yyyy"
/>


</MuiPickersUtilsProvider>
</Box>
<Box sx={{ minWidth: 210 }} className='filter_flex'>
    <MuiPickersUtilsProvider utils={DateFnsUtils} >


<KeyboardDatePicker
  label="Reported Date To"
  value={getFilterToRptDate}
  onChange={handleToDateChange}
  format="dd/MM/yyyy"
/>

</MuiPickersUtilsProvider>
</Box>
      <Box sx={{ minWidth: 210 }} >
      <FormControl fullWidth
   
      className='filter_flex'>
        <InputLabel id="State">State</InputLabel>
        <Select 
          labelId="State"
          id="State"
          multiple
          // displayEmpty
          value={getFilterState}
          label="State"
          onChange={stateHandleChange}
         
        >
          {/* <MenuItem value=""><Put any default Value which you want to show/></MenuItem> */}
          <MenuItem  aria-label="None" value=""/>
          { Object.keys(data).length > 0 ?
            data.States.map((d) => {
             
                return ( 
            <MenuItem 
            key={d}
            value={d}
            >
              {d}
            </MenuItem>
                )
              })
              : ""
              
               
}
        </Select>
        
      </FormControl>
      </Box>
      <Box sx={{ minWidth: 210 }} >
      <FormControl fullWidth className='filter_flex'>
        <InputLabel id="Claim Type">Claim Type</InputLabel>
        <Select 
          labelId="Claim Type"
          id="Claim Type"
          multiple
          // displayEmpty
          value={getFilterClaimType}
          label="Claim Type"
          onChange={claimTypeHandleChange}
        >
          {/* <MenuItem value=""><Put any default Value which you want to show/></MenuItem> */}
          {    Object.keys(data).length > 0 ?
            data.Claim_Types.map((d) => {
             
                return ( 
            <MenuItem
            key={d} 
            value={d}>{d}</MenuItem>
                )
              })
              : "" 
               
}
        </Select>
        
      </FormControl>
      </Box>
      <Box sx={{ minWidth: 210 }} >
      <FormControl fullWidth className='filter_flex'>
        <InputLabel id="Claim Amount">Claim Amount</InputLabel>
        <Select 
          labelId="Claim Amount"
          id="Claim Amount"
          // displayEmpty
          multiple
          value={getFilterClaimAmt}
          label="Claim Amount"
          onChange={claimAmountHandleChange}
        >
          {/* <MenuItem value=""><Put any default Value which you want to show/></MenuItem> */}
          {
            Claim_Amount.map((d) => {
             
                return ( 
            <MenuItem 
            key={d}
            value={d}
            >
              {d}
            </MenuItem>
                )
              })
              
               
}

          {/* <MenuItem value={"Less than "}>Model 3</MenuItem> */}
        </Select>
        
      </FormControl>
      </Box>
     
      {/* <Button  variant="outlined" onClick={passFilterValues}  >Filter</Button> */}
      <Button onClick={passFilterValues}  style={{
      
      backgroundColor: "Black",
      color:"white",
      height: 32,
    fontSize: 12
    }}   > Filter</Button>
     
    </div>
  );
 
}
