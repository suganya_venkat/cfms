import React, { useRef, useState, useEffect } from "react";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import Paper from "@material-ui/core/Paper";
import claimsdata from './KpiCards_histogramData.json';
import KpiCards_data from './claims_overview_summary_dict.json'

// import "./linechart_paper.css";

am4core.useTheme(am4themes_animated);

const CHART_ID = "claimsChart1";

// const fetchURL = "http://localhost:5000/insights/actualforecast_chart";

function ClaimsByClaimsType(props) {
  const chartRef = useRef(null);
  const [items, setItems] = useState([]);



const [data,setData]=useState(KpiCards_data);
const selectedState = props.getState;
const selectedToRptDate=props.getToRptDate;
const selectedClaimType=props.getClaimType;
const selectedClaimAmount=props.getClaimAmount;
const selectedFromRptDate = props.getFromRptDate;
// console.log(selectedState,selectedFromRptDate,selectedToRptDate,selectedClaimType,selectedClaimAmount)  
    
  //    useEffect(() => {
  // fetch('/kpicards').then(res => res.json()).then((data) => setData(data));
  // },[]);

  useEffect(() => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({"reporting_view_filters" :  

      {"CLAIMREPORTDATE" : [{"from":  selectedFromRptDate,  

                          "to": selectedToRptDate}], 

      "STATE" : selectedState,  

      "CLAIM_TYPE": selectedClaimType,  

      "CLAIM_AMT_USD": selectedClaimAmount

    } 

    } )
    };
    fetch('/reporting_overview_claims', requestOptions)
    .then(res => res.json()).then((data) => setData(data));
  },[selectedState,selectedFromRptDate,selectedToRptDate,selectedClaimType,selectedClaimAmount]);
    
  useEffect(() => {
    chartRef.current = am4core.create(CHART_ID, am4charts.XYChart);
    chartRef.current.data = Object.keys(data).length > 0 ? data.Claims_by_Claim_Type_Histogram : "Loading";

    var categoryAxis = chartRef.current.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "Claims_type";
categoryAxis.renderer.grid.template.disabled = true;
categoryAxis.title.text = "Claims type";
categoryAxis.title.fontSize = 10;
categoryAxis.renderer.labels.template.fontSize = 10;
categoryAxis.renderer.labels.template.disabled=false;

// categoryAxis.renderer.minGridDistance = 20;

categoryAxis.renderer.minGridDistance = 10;
categoryAxis.renderer.grid.template.location = 1;
// categoryAxis.startLocation = 1;
// categoryAxis.endLocation = 1;

// Setting up label rotation
categoryAxis.renderer.labels.template.rotation = 20;

// categoryAxis.renderer.grid.template.location = 0;
// categoryAxis.renderer.minGridDistance = 30;

// categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
//   if (target.dataItem && target.dataItem.index & 2 == 2) {
//     return dy + 25;
//   }
//   return dy;
// });

var valueAxis = chartRef.current.yAxes.push(new am4charts.ValueAxis());
valueAxis.title.text = "Claims(#)";
valueAxis.title.fontSize = 10;
valueAxis.renderer.labels.template.fontSize = 10;

// Create series
var series = chartRef.current.series.push(new am4charts.ColumnSeries());
series.dataFields.valueY = "Claims";
series.dataFields.categoryX = "Claims_type";
series.name = "Claims";
series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
series.columns.template.fillOpacity = .8;
series.columns.template.fill = am4core.color("#0a54c3"); // fill #00965f
series.stroke = am4core.color("#0a54c3");

// series.columns.template.fill = am4core.color("#00965f"); // fill
      
      

valueAxis.renderer.grid.template.disabled = true;

var columnTemplate = series.columns.template;
columnTemplate.strokeWidth = 1;
columnTemplate.strokeOpacity = 1;

// chartRef.current.exporting.menu = new am4core.ExportMenu();
// chartRef.current.exporting.menu.align = "right";
// chartRef.current.exporting.menu.verticalAlign = "top";

});
   

  return (
    <div
      id={CHART_ID}
      style={{ width: "100%", height: "300px" }}
    >
      {" "}
    </div>
  );
}

export default ClaimsByClaimsType;
