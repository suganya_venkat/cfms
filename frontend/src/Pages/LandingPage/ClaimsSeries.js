import  React , {useEffect ,useState, useRef} from 'react';
import claimsdata from './ClaimsCountSeries.json';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import KpiCards_data from './claims_overview_summary_dict.json'

am4core.useTheme(am4themes_animated);

const CHART_ID = "ClaimsReport_chart";

function ClaimsTrend(props){
    const chartRef = useRef(null);
    
     const [data,setData]=useState(KpiCards_data);
     const selectedState = props.getState;
    const selectedToRptDate=props.getToRptDate;
    const selectedClaimType=props.getClaimType;
    const selectedClaimAmount=props.getClaimAmount;
    const selectedFromRptDate = props.getFromRptDate;

  //    useEffect(() => {
  // fetch('/kpicards').then(res => res.json()).then((data) => setData(data));
  // },[]);

  useEffect(() => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({"reporting_view_filters" :  

      {"CLAIMREPORTDATE" : [{"from":  selectedFromRptDate,  

                          "to": selectedToRptDate}], 

      "STATE" : selectedState,  

      "CLAIM_TYPE": selectedClaimType,  

      "CLAIM_AMT_USD": selectedClaimAmount

    } 

    } )
    };
    fetch('/reporting_overview_claims', requestOptions)
    .then(res => res.json()).then((data) => setData(data));
  },[selectedState,selectedFromRptDate,selectedToRptDate,selectedClaimType,selectedClaimAmount]);
    
    useEffect(() => {

      var sample1 = data.Daywise_Claim_Count
      ? data.Daywise_Claim_Count.map((fy) => {
        var date = new Date(fy.CLAIMREPORTDATE);
        var year = date.getFullYear();
        var month = date.getMonth();
        // var datewithtime1 = date.getMinutes
        // fy.fiscalyear_quarter = year + "-Q" + fy.quarter;
        fy.YearMonth = year +"-"+ month;
        fy.Month = month;
        fy.date = date
      })
      : null;

  var helper = {};
  var result;
  var counter = 0;
  var count = [];

  // if (claimsdata) {
  //   result = claimsdata.reduce(function (r, o) {
      
  //       var key = o.YearMonth;
      
  //     if (!helper[key]) {
  //       count.push(counter);
  //       counter = 1;
  //       helper[key] = Object.assign({}, o); // create a copy of o
  //       r.push(helper[key]);
  //     } else {
  //       helper[key].CLAIMREPORTDATE_count += o.CLAIMREPORTDATE_count;
  //       counter = counter + 1;
  //     }
  //     return r;
  //   }, []);

  //   count.push(counter);

  //   // result.map((res, index) => {
  //   //   res.CLAIMREPORTDATE_count = res.CLAIMREPORTDATE_count / count[index + 1];
  //   // });
  // } else {
  //   result = null;
  // }


//   console.log(claimsdata[0].date)

        chartRef.current = am4core.create(CHART_ID, am4charts.XYChart);
         chartRef.current.data = data.Daywise_Claim_Count;
    
        
          
          // Set input format for the dates
          chartRef.current.dateFormatter.inputDateFormat = "yyyy-MM-dd";
          
          // Create axes
          var dateAxis = chartRef.current.xAxes.push(new am4charts.DateAxis());
          var valueAxis = chartRef.current.yAxes.push(new am4charts.ValueAxis());
          
          // Create series
          var series = chartRef.current.series.push(new am4charts.LineSeries());
          series.dataFields.valueY = "CLAIMREPORTDATE_count";
          series.dataFields.dateX = "CLAIMREPORTDATE";
          series.tooltipText = "{value}"
          series.strokeWidth = 2;
          series.minBulletDistance = 15;
          series.stroke = am4core.color("#0a54c3");
          // Drop-shaped tooltips
          series.tooltip.background.cornerRadius = 20;
          series.tooltip.background.strokeOpacity = 0;
          series.tooltip.pointerOrientation = "vertical";
          series.tooltip.label.minWidth = 40;
          series.tooltip.label.minHeight = 40;
          series.tooltip.label.textAlign = "middle";
          series.tooltip.label.textValign = "middle";
          
          // Make bullets grow on hover
          var bullet = series.bullets.push(new am4charts.CircleBullet());
          bullet.circle.strokeWidth = 2;
          bullet.circle.radius = 4;
          bullet.circle.fill = am4core.color("#fff");
          
          var bullethover = bullet.states.create("hover");
          bullethover.properties.scale = 1.3;
          
          // Make a panning cursor
          chartRef.current.cursor = new am4charts.XYCursor();
          chartRef.current.cursor.behavior = "panXY";
          chartRef.current.cursor.xAxis = dateAxis;
          chartRef.current.cursor.snapToSeries = series;
          
          // Create vertical scrollbar and place it before the value axis
          chartRef.current.scrollbarY = new am4core.Scrollbar();
          chartRef.current.scrollbarY.parent = chartRef.current.leftAxesContainer;
          chartRef.current.scrollbarY.toBack();
          
          // Create a horizontal scrollbar with previe and place it underneath the date axis
          chartRef.current.scrollbarX = new am4charts.XYChartScrollbar();
          chartRef.current.scrollbarX.series.push(series);
          chartRef.current.scrollbarX.parent = chartRef.current.bottomAxesContainer;
          
          dateAxis.start = 0.79;
          dateAxis.keepSelection = true;
          
      
          chartRef.current.exporting.menu = new am4core.ExportMenu();
          chartRef.current.exporting.menu.align = "right";
          chartRef.current.exporting.menu.verticalAlign = "top";
      
          chartRef.current.events.on("beforedatavalidated", function(ev) {
            chartRef.current.data.sort(function(a, b) {
              return (new Date(a.date)) - (new Date(b.date));
            });
          })
        //   return () => {
        //     chartRef.current && chartRef.current.dispose();
        //   };
        // }, [timeframe]);
      
        // Load data into chart
      
      })
      
        return (
          <div
            id={CHART_ID}
            style={{ width: "100%", height: "400px", margin: "50px 0" }}
          >
            {" "}
          </div>
        );
  
}
export default ClaimsTrend


