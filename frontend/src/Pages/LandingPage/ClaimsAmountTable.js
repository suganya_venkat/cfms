import  React from 'react';
import ReactTable from "react-table";
import "react-table/react-table.css";
import data from './ClaimsData.json';

import _ from 'lodash'

function ClaimsAmountTable() {
  
    const columns = [{
        Header: " ",
    
    
        columns: [
          {
    
            Header: "auto_make",
            accessor: "auto_make",
            filterable: true,
        Placeholder: "Search",
        //minWidth: 140,
        style: { 'white-space': 'unset', 'color': 'Midnightblue', "fontWeight": "bold" },
        filterMethod: (filter, row) => {
          if (row._subRows === undefined) {
            return row[filter.id].toLowerCase().includes(filter.value.toLowerCase())
          } else {
            return true;
          }
        },
        Cell: row => {
          return (<div style={{ textAlign: "center" }}>{row.value}</div>)
        },
          },
          {
            Header: "auto_model",
            accessor: "auto_model",
            filterable: true,
            Placeholder: "Search",
            //minWidth: 140,
            style: { 'white-space': 'unset', 'color': 'Midnightblue', "fontWeight": "bold" },
            filterMethod: (filter, row) => {
              if (row._subRows === undefined) {
                return row[filter.id].toLowerCase().includes(filter.value.toLowerCase())
              } else {
                return true;
              }
            },
            Cell: row => {
              return (<div style={{ textAlign: "center" }}>{row.value}</div>)
            },
          },
          {
            Header: "total_claim_amount",
            accessor: "total_claim_amount",
            //aggregate: vals => " ",
            aggregate: vals =>  _.sum(vals),
            filterable: true,
            style: { 'white-space': 'unset' },
            Cell: row => {
            //   console.log(row);
              // {console.log(row.row._subRows[0]._subRows[0]._original)}
              if (row.value !== undefined) {
      
                return (<div style={{ textAlign: "center" }}>{row.value}</div>)
              }
              else {
                return (<div style={{ textAlign: "center" }}>0</div>)
              }
            }
            // filterMethod: (filter, row) => {
            //   if (row._subRows === undefined) {
            //     return row[filter.id].toLowerCase().includes(filter.value.toLowerCase())
            //   } else {
            //     return true;
            //   }
            // }
    
          },
         
        ]
      }]
    
   
      // columns.push({
      //     Header: "total_claim_amount_new" ,
      //     // headerClassName: y.slice(0, 1) === "1" ? "forecast" : "actual",
      //     accessor: "total_claim_amount",
      //     aggregate: vals => (_.sum(vals)),
      //     Cell: row => {
    
      //       // {console.log(row.row._subRows[0]._subRows[0]._original)}
      //       if (row.value !== undefined) {
    
      //         return (<div style={{ textAlign: "center" }}>{row.value.toFixed(0)}</div>)
      //       }
      //       else {
      //         return (<div style={{ textAlign: "center" }}>0</div>)
      //       }
      //     }
      
      //   })
        
      
        
    
   
 
      return (
       
        <div className="container-fluid">
          <h1><center></center></h1>
          <ReactTable columns={columns} data={data}
            // pageSize={5}
            style={{ maxHeight: "500px", borderRadius: "10px" }}
            defaultPageSize={5}
            minRows={1}
            className="-striped -highlight"
            pivotBy={["auto_make", "auto_model"]}
          
    
    
          />
        </div>
       
      );
 
}

export default ClaimsAmountTable
