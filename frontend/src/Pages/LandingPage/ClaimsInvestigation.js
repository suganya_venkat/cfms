import React, { useRef, useState, useEffect } from "react";
import claimsdata from '../LandingPage/FraudRatio.json';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import ClaimsInvestigationData from './ClaimsInvestigation.json'
import KpiCards_data from './claims_overview_summary_dict.json'

am4core.useTheme(am4themes_animated);

const CHART_ID = "ClaimsInvestigation";

function ClaimsInvestigation(props) {
    const chartRef = useRef(null);
    
    const [data,setData]=useState(KpiCards_data);
    const selectedState = props.getState;
    const selectedToRptDate=props.getToRptDate;
    const selectedClaimType=props.getClaimType;
    const selectedClaimAmount=props.getClaimAmount;
    const selectedFromRptDate = props.getFromRptDate;
  // console.log(selectedState,selectedFromRptDate,selectedToRptDate,selectedClaimType,selectedClaimAmount)
    
    
  //    useEffect(() => {
  // fetch('/kpicards').then(res => res.json()).then((data) => setData(data));
  // },[]);

  useEffect(() => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({"reporting_view_filters" :  

      {"CLAIMREPORTDATE" : [{"from":  selectedFromRptDate,  

                          "to": selectedToRptDate}], 

      "STATE" : selectedState,  

      "CLAIM_TYPE": selectedClaimType,  

      "CLAIM_AMT_USD": selectedClaimAmount

    } 

    } )
    };
    fetch('/reporting_overview_claims', requestOptions)
    .then(res => res.json()).then((data) => setData(data));
  },[selectedState,selectedFromRptDate,selectedToRptDate,selectedClaimType,selectedClaimAmount]);
    
    var sample =  data === undefined || data === null ||  data.length <= 0
?  null : data.claims_by_claim_final_status.map((a) => {
  a.color = a["Final Claim Status"] === 'Not Investigated' ? am4core.color("#663399") : 
  a.color = a["Final Claim Status"] === 'Investigated - Found Non Fraud' ?
  am4core.color("#6a5acd") : a.color = a["Final Claim Status"] === 'Investigated - Found Fraud' ?
  am4core.color("#8878c3") : a.color = a["Final Claim Status"] === 'Historically(Pre-Model Training) Identified Frauds' ?
  am4core.color("#9370db") : am4core.color("#e0b0ff")
  
  
})

console.log(data.claims_by_claim_final_status)
    
    useEffect(() => {
        var chart = am4core.create(CHART_ID, am4charts.PieChart);

        // chartRef.current = am4core.create(CHART_ID, am4charts.PieSeries);
    // chart.data = Object.keys(data).length > 0 ? data.Fraud_Ratio : "Loading";
       
    chart.data = data.claims_by_claim_final_status


        let pieSeries = chart.series.push(new am4charts.PieSeries());
         pieSeries.dataFields.category = "Final Claim Status";
        pieSeries.dataFields.value = "Claims";
      
        pieSeries.tooltip.disabled = false;
        pieSeries.ticks.template.disabled = true;
      pieSeries.labels.template.fontSize = 8.8;
      pieSeries.alignLabels = false;
      // pieSeries.labels.template.bent = true;
      pieSeries.labels.template.text = "{value}";
      // pieSeries.labels.template.disabled = true;

      // pieSeries.labels.template.radius = am4core.percent(-40);
      // pieSeries.labels.template.padding(0, 0, 0, 0);

         chart.exporting.menu = new am4core.ExportMenu();
         chart.exporting.menu.align = "right";
         chart.exporting.menu.verticalAlign = "top";
        pieSeries.slices.template.propertyFields.fill = "color";
        // chart.legend = new am4charts.Legend();

//         pieSeries.slices.template.stroke = am4core.color("#4a2abb");

//           pieSeries.slices.template.fill = am4core.color("#00965f");
//           return () => {
//            chart &&chart.dispose();
//           };
        
      
        // Load data into chart
    
      })
      
        return (
          <div
            id={CHART_ID}
            style={{ width: "100%", height: "300px" }}
          >
            {" "}
          </div>
        );
  
}


export default ClaimsInvestigation
