import React, {useState}from 'react'
import Grid from "@material-ui/core/Grid";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import { Typography } from "@material-ui/core";
import NavBar from '../../Components/Navbar'
import MainNavBar from '../../Components/MainNavBar'
import { makeStyles } from "@material-ui/core/styles";
import { styled } from '@mui/material/styles';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import '../CommonStyling/Style.css'
import './AIimpact.css'

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));
const HtmlTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#e6e6fa',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 500,
    height:100,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
    padding:"90px"
  },
}));
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    border: `1px solid #EEF3F5`,
    borderRadius: 4,
    color: "#949FAF",
    minWidth: 275,
    minHeight: 70,
    marginTop: 12,
    marginLeft: 12,
    marginRight: 12,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    paddingBlock: 5,
    borderRadius: 5,
  },
  card: {
    color: "#16448F",
    fontWeight: 700,
    display: "inline-block"
  },
  cardsmall:{
    color: '#5DC9A2',
    fomtWeight: 500,
    display: "inline-block",
    fontSize: 12,
    marginInline: 4 
  }
}));


function AIassisted() {
  const classes = useStyles();

    return (
        <div>
        <div style={{marginLeft: 29,
            marginTop: 32,marginRight:111}}>
                <p style={{marginLeft: 53,fontSize:13,padding: 7}}>AI Predicted</p>
       <Grid container spacing={1} style={{marginLeft:50}} >
  <Grid item xs={6}  >
    <Item style={{backgroundColor:"red"}}> </Item>
  </Grid>
  <Grid item xs={6} >
    <Item style={{backgroundColor:"green"}}></Item>
  </Grid>
  </Grid>
  
  <div style={{display:"flex"}} >
  <p style={{fontSize:13,transform: "rotate(270deg)" ,transformOrigin:"right",marginBottom:16,marginTop:-64, 
  whiteSpace: "nowrap"
}}>SIU Recommended</p>
  <Grid container spacing={1}  direction="column" style={{marginLeft: -90,marginTop:5}}>
  <Grid item xs={1}   >
    <Item style={{height:75,backgroundColor:"red"}} ></Item>
  </Grid>
  <Grid item xs={1} >
    <Item style={{height:75,backgroundColor:"green"}}></Item>
  </Grid>
  </Grid>
  <br/>
  <div style={{flex:1,marginTop:10}}>
       <Grid container spacing={1}  >
  <Grid item xs={6}  >
    <Item className="matrixCell" style={{ marginLeft: -96}}> 331 (14%)</Item>
  </Grid>
  <Grid item xs={6} >
    <Item className="matrixCell" style={{ marginLeft: -18}}>61 (3%)</Item>
  </Grid>
  </Grid>
  <Grid container spacing={1}>
  <Grid item xs={6} >
    <Item className="matrixCell" style={{ marginLeft: -96}}>345 (15%)</Item>
  </Grid>
  <Grid item xs={6} >
    <Item  style={{ marginLeft: -18,padding:25, height: 77,
    width: 130}}>1,550 (68%)</Item>
  </Grid>
</Grid>
      </div>
      </div>
      
                 
               </div>
               <div style={{textAlign:'center',marginTop:37}}>
 
               <span class="legendDotred"></span>
               <span style={{fontSize:12}} > Fraud</span>
               <span style={{marginLeft:10}} class="legendDotgreen"></span>
               <span style={{marginLeft:5,fontSize:12}}>Non Fraud</span>
             </div>
             <Grid container alignItems="center" spacing={1} style={{marginLeft:125}}>
      <Grid style={{flex:1}}  item xs={12} sm={3}>
                    <Paper className={classes.paper} style={{width: 182,
          height: 61,
          fontSize: 13,padding:10,marginLeft: -15,marginTop:10,backgroundColor:'#F2F2F2'
        }}>
             {/* //,backgroundColor: '#8878c3' */}
                      {" "}
                      New Fraud Cases Found <br /><br />{" "}
                      <div  className={classes.card}>209 (+10%)</div>{" "}
                    </Paper>
                  </Grid>
             
                  </Grid>
             </div>
    )
}

export default AIassisted
