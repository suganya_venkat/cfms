import React from 'react';
import Plot from 'react-plotly.js';
import Grid from "@material-ui/core/Grid";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    border: `1px solid #EEF3F5`,
    borderRadius: 4,
    color: "#949FAF",
    minWidth: 275,
    minHeight: 70,
    marginTop: 12,
    marginLeft: 12,
    marginRight: 12,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    paddingBlock: 5,
    borderRadius: 5,
  },
  card: {
    color: "#16448F",
    fontWeight: 700,
    display: "inline-block"
  },
  cardsmall:{
    color: '#5DC9A2',
    fomtWeight: 500,
    display: "inline-block",
    fontSize: 12,
    marginInline: 4 
  }
}));


export default function CreateRule()  {
  const classes = useStyles();

    return (
      <div>
      <Plot
    //   data={[
    //     {
    //       x: [1, 2, 3],
    //       y: [2, 6, 3],
    //       type: 'scatter',
    //       mode: 'lines+markers',
    //       marker: {color: 'red'},
    //     },
    //     {type: 'bar', x: [1, 2, 3], y: [2, 5, 3]},
    //   ]}
      
      var data = {[
        {type: 'funnel', name: 'Total',
      y: ["Claims", "Investigation","Fraud"],
      x: [2316,703,358],
      textinfo: "value+percent initial",hoverinfo:'x+percent initial',texttemplate:"%{x:,}",
      marker: {color: '#8878c3'},
      textfont: {
        size:  12,
        fontWeight:500,
        color:"white"
      },
      connector: {line: { width: 0}}
    },
      {
         type: 'funnel',name: 'AI Non Recommendations',
        y: ["Claims", "Investigation","Fraud"],
        x: [1788,226,109], textposition: "inside", textinfo: "value+percent total",hoverinfo:'x+percent initial',texttemplate:"%{x:,}",
        marker: {color: '#BCADE0'},
        textfont: {
          size:  13,
          fontWeight:500,
          color:"white"
        }
      },
      {
        type: 'funnel',name: 'AI Recommendations',
        y: ["Claims", "Investigation","Fraud"],
        x: [528,477,249], textposition: "inside", textinfo: "value+percent total",hoverinfo:'x+percent initial',texttemplate:"%{x:,}",
        marker: {color: '#9370db'},
        textfont: {
            size:  13,
            fontWeight:500,
            color:"white"
          },
       
      
    }
    ]}
    layout={ {margin: {l: 80, r: 0,t:40}, width: 400,height:300, showlegend: 'false', legend: {
      "orientation": "h",
      
      // margin:{l:80}
    }} }
    config = {
     { displayModeBar: false, // this is the line that hides the bar.
    }
    }
        // layout= {margin : 500, width: 600, funnelmode: "stack", showlegend: 'true'} 
        // layout = {margin : {l: 130, r: 0}, width: 600, funnelmode: "stack", showlegend: 'true'}
    
      />
      <div style={{marginLeft:22}}>
       <Grid   item xs={12} sm={3}>
      {/* <p style={{fontSize:10,whiteSpace: "nowrap",marginLeft: 28
}}>Total Loss Cost Savings</p> */}
                    <Paper className={classes.paper} style={{width: 200,
          height: 54,
          fontSize: 13,padding:5,marginLeft: 111,marginTop:11,
          backgroundColor:'#F2F2F2'}}>
             {/* //,backgroundColor: '#8878c3', marginBottom: 8,marginTop:17, */}
                      {" "}
                    
                   Total Loss cost Savings <br /><br />{" "}
                      <div  className={classes.card}>23M $</div>{" "}
                    </Paper>
                  </Grid>
      <div style={{display:"flex"}}>
      <Grid container alignItems="center" spacing={1} style={{marginLeft:4}}>
      {/* style={{marginLeft:32}} */}
     
                 
                  <Grid style={{flex:1}}  item xs={12} sm={2}>
                  {/* <p  style={{fontSize:10,whiteSpace: "nowrap",marginLeft: 39}}></p> */}
                    <Paper className={classes.paper} style={{width: 141,
          height: 66,
          fontSize: 11,padding:3,marginLeft: 37,marginTop: 19,backgroundColor:'#F2F2F2'}}>
            {/* ,backgroundColor: '#cec8ef' */}
                      {" "}

                       Total Loss cost Savings - AI Recommendations <br /><br />{" "}
                      <div  className={classes.card}>16M $</div>{" "}
                    </Paper>
                  </Grid>
                  <Grid style={{flex:1}}  item xs={12} sm={2}>
                  {/* <p style={{fontSize:10,whiteSpace: "nowrap",marginLeft: 81
}}>AI Non Recommendations</p>    */}
                    <Paper className={classes.paper} style={{width: 145,
          height: 66,
          fontSize: 11,padding:3,marginLeft: 135,marginTop:17,backgroundColor:'#F2F2F2'}}>
                      {" "}
                      Total Loss cost Savings -  AI Non Recommendations <br /><br />{" "}
                      <div  className={classes.card}>7M $</div>{" "}
                    </Paper>
                  </Grid>
                  </Grid>
                  </div>
                  </div>
                  </div>
    );

  }

