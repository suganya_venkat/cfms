import React from 'react';
import Plot from 'react-plotly.js';
import './AIimpact.css'



export default function PreAi()  {
 
    return (
        <div>
            <p> AI Predicted</p>
      <Plot
      
    //   data={[
    //     {
    //       x: [1, 2, 3],
    //       y: [2, 6, 3],
    //       type: 'scatter',
    //       mode: 'lines+markers',
    //       marker: {color: 'red'},
    //     },
    //     {type: 'bar', x: [1, 2, 3], y: [2, 5, 3]},
    //   ]}
      
    values ={ [
        ['Salaries', 'Office', 'Merchandise', 'Legal', '<b>TOTAL</b>'],
        [1200000, 20000, 80000, 2000, 12120000],
        [1300000, 20000, 70000, 2000, 130902000],
        [1300000, 20000, 120000, 2000, 131222000],
        [1400000, 20000, 90000, 2000, 14102000]]}
  
   data = {[{
    type: 'table',
    header: {
      values: [["<b></b>"], ["<b></b>"],
                   ["<b></b>"]],
      align: ["left", "center"],
      height: 50,
      line: {width: 1, color: '#506784'},
      fill: {color: ['grey','red','green']},
      font: {family: "Arial", size: 12, color: "white"}
    },
    cells: {
      values:  [
        ['', ''],
        ['', ''],
        ['', ''],
     ],
      align: ["left", "center"],
      height: 50,
      line: {color: "#506784", width: 1},
       fill: {color: [['red','green'],'white']},
      font: {family: "Arial", size: 14, color: ["#506784"]}
    }
  }]}
  
    layout={ {margin: {l: 50,t:40}, width:405, height: 300} }
    config = {
        { displayModeBar: false, // this is the line that hides the bar.
       }
       }
        // layout= {margin : 500, width: 600, funnelmode: "stack", showlegend: 'true'} 
        // layout = {margin : {l: 130, r: 0}, width: 600, funnelmode: "stack", showlegend: 'true'}
    
      />
      <div style={{textAlign:'center'}}>
 
  <span class="legendDotred"></span>
  <span style={{fontSize:12}} > Fraud</span>
  <span style={{marginLeft:10}} class="legendDotgreen"></span>
  <span style={{marginLeft:10,fontSize:12}}>Non Fraud</span>
</div>
                  </div>
    );

  }

