import React from 'react';
import Plot from 'react-plotly.js';
import Grid from "@material-ui/core/Grid";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    border: `1px solid #EEF3F5`,
    borderRadius: 4,
    color: "#949FAF",
    minWidth: 275,
    minHeight: 70,
    marginTop: 12,
    marginLeft: 12,
    marginRight: 12,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    paddingBlock: 5,
    borderRadius: 5,
  },
  card: {
    color: "#16448F",
    fontWeight: 700,
    display: "inline-block"
  },
  cardsmall:{
    color: '#5DC9A2',
    fomtWeight: 500,
    display: "inline-block",
    fontSize: 12,
    marginInline: 4 
  }
}));


export default function PreAi()  {
    const classes = useStyles();

    return (
        <div>
      <Plot
    //   data={[
    //     {
    //       x: [1, 2, 3],
    //       y: [2, 6, 3],
    //       type: 'scatter',
    //       mode: 'lines+markers',
    //       marker: {color: 'red'},
    //     },
    //     {type: 'bar', x: [1, 2, 3], y: [2, 5, 3]},
    //   ]}
      
      var data = { [{type: 'funnel',
       y: ["Claims", "Investigated", "Fraud Captured"], 
       x: [2287 , 913, 183],marker: {color: '#8878c3'},textposition: "inside",hoverinfo:"x+percent initial",texttemplate:"%{x:,}",
        textfont: {
        size:  14,
        // fontWeight:500,
        color:"white"
      },}]}
    layout={ {margin: {l: 100,t:40}, width:400, height: 300,color:"white"} }
    
    config = {
        { displayModeBar: false, // this is the line that hides the bar.
       }
      
       }
        // layout= {margin : 500, width: 600, funnelmode: "stack", showlegend: 'true'} 
        // layout = {margin : {l: 130, r: 0}, width: 600, funnelmode: "stack", showlegend: 'true'}
    
      />
      <div style={{display:"flex"}}>
      <Grid container alignItems="center" spacing={1} style={{marginLeft:125}}>
      <Grid style={{flex:1}}  item xs={12} sm={3}>
                    <Paper className={classes.paper} style={{width: 182,
          height: 61,
          fontSize: 13,padding:10,marginLeft: -15,backgroundColor:'#F2F2F2'
        }}>
             {/* //,backgroundColor: '#8878c3' */}
                      {" "}
                      Total Loss cost Savings <br /><br />{" "}
                      <div  className={classes.card}>12M $</div>{" "}
                    </Paper>
                  </Grid>
             
                  </Grid>
                  </div>
                  </div>
    );

  }

