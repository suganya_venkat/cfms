import React, {useState}from 'react'
import Grid from "@material-ui/core/Grid";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import { Typography } from "@material-ui/core";
import NavBar from '../../Components/Navbar'
import MainNavBar from '../../Components/MainNavBar'
import { makeStyles } from "@material-ui/core/styles";
 import InvestigationKPICard from '../InvestigationOverview_v1/InvestigationKPICard'
import InvestidationClaimsByState from '../InvestigationOverview_v1/InvestigationClaimsByState'
import InvestigationComboChart from '../InvestigationOverview_v1/InvestigationComboChart'
import Investigation_filters from '../InvestigationOverview_v1/Investigation_filters'
import InvestigationByClaimType from '../InvestigationOverview_v1/InvestigationByClaimType';
import InvestigationClaimsByMonth from '../InvestigationOverview_v1/InvestigationClaimsByMonth';
import AnalyticsandReportNavBar from '../../Components/AnalyticsandReportNavBar'
import Popover from '@mui/material/Popover';
import CardMedia from "@material-ui/core/CardMedia"
import ReportingBe from '../../assets/images/ReportingBe.png'
// import AnalyticsBe from '../../assets/images/AnalyticsBe.png'
// import ProgressBarTop from '../../assets/images/ProgressBarTop.PNG'
// import NewTheme from '../../Components/newTheme.js';
import ImpactSummaryKPI from '../InvestigationOverview_v1/ImpactSummaryKPI'
// import InvestigationClaimsByClaimType from './InvestigationClaimsByclaimtype';
import AddchartIcon from '@mui/icons-material/Addchart';
import DvrIcon from '@mui/icons-material/Dvr';
import { styled } from '@mui/material/styles';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import InfoIcon from '@mui/icons-material/Info';
import '../CommonStyling/Style.css'
// import '../Invest/InvestigatedPage.css';
import FunnelChart from './FunnelChart';
import PreAi from './PreAi';
import Matrix from './Matrix';
import AIassisted from './AIassisted';
const HtmlTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#e6e6fa',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 500,
    height:100,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
    padding:"10px"
  },
}));
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    border: `1px solid #EEF3F5`,
    borderRadius: 4,
    color: "#949FAF",
    minWidth: 275,
    minHeight: 70,
    marginTop: 12,
    marginLeft: 12,
    marginRight: 12,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    paddingBlock: 5,
    borderRadius: 5,
  },
  card: {
    color: "#16448F",
    fontWeight: 700,
    display: "inline-block"
  },
  cardsmall:{
    color: '#5DC9A2',
    fomtWeight: 500,
    display: "inline-block",
    fontSize: 12,
    marginInline: 4 
  }
}));


function AIimpact() {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    console.log(event)
    setAnchorEl(event.currentTarget);
 
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;
  
  
 
    return (
        <div className='divalign'>
          
          <AnalyticsandReportNavBar/>
          {/* <CardMedia
     className='ProgressBarTopInvestigation'   
          image={ProgressBarTop}

 /> */}

          {/* <NewTheme/>
          <div className='activeLineSummary'></div><br/> */}
        <NavBar/>
      
        <div style={{padding: 8}} className="infoHeadingContainer">
        {/* <AddchartIcon style={{fontSize: 47,cursor: "pointer"}}  onClick={handleClick}  />  */}
             <h3 style={{color:"black",marginLeft:26,marginTop:15}}> AI Impact</h3><br/>
             <span className="aiImpactInfo">
       <HtmlTooltip  placement="right-start"
       
        title={
          <React.Fragment>
            <p style={{fontSize:12}}><i>
        Comparison of KPIs Pre-AI Implementation (Benchmark ) vs Post AI Implementation</i></p> <br/>
        <p style={{fontSize:12}}>Note: The Impact in highlighted green compares traditional fraud capture process vs AI driven fraud analytics​</p>
 
               </React.Fragment>
        }
      >
        <InfoIcon className="infoIcon" />
      </HtmlTooltip>
     
      </span>
      
     
     </div>
     
        <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
         <CardMedia
          className='cardMediaStylepopup'   
              image={ReportingBe}

/>
       
      </Popover>
      <div style={{marginLeft: 8}}>
              <div style={{marginLeft: 22}}>
       
        <br/>
        <ImpactSummaryKPI/>
     
       
        </div>
        
        <br/>
        <div style={{margin:20}} >
  
         {/* <h4 style={{color:"black",marginTop:-28}}> Investigation Summary - Overall</h4><br/> */}

       
      {/* <br/> */}
      
       <Grid  container spacing={1}  >
    
        <Grid item xs = {8}>
        <Paper elevation={3} className="fraudratio" style={{height:500}}>
    
        <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}><center>  Pre AI Implementation And Validation </center>
       </p>
       <div style={{display:"flex"}}>
       
       <PreAi/>
      
       <div style={{flex:1}}>
       <AIassisted/>
       {/* helo */}
       </div>
       </div>
     
        </Paper>
        </Grid>
        
        {/* <Grid item xs = {4} > 
        <Paper elevation={3} className="a" style={{height:500}} >
        
        <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}><center>  AI Assisted Impact​ </center>
          </p>
       
      

     
        
        </Paper>
        
        </Grid> */}

      
        
    
        <Grid item xs = {4} > 
        <Paper elevation={3} className="a" style={{height:500}} >
        
        <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}><center>  Post AI Implementation​ </center>
          </p>
       
     

      <FunnelChart/>
        
        </Paper>
        
        </Grid>
        
        </Grid>
        <br/>
        <InvestigationKPICard />
      <br/>
      
       <Grid  container spacing={1}  >
       {/* style={{marginLeft: 3}} */}
        <Grid item xs = {6}>
        <Paper elevation={3} className="fraudratio">
    
        <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}><center>  Open Claims by State </center>
       </p>
      
          <InvestidationClaimsByState />
        
        </Paper>
        </Grid>
        
       
        <Grid item xs = {6} > 
        <Paper elevation={3} className="a" >
        
        <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}><center> Open claims by Month​ </center>
          </p>
       
        <InvestigationClaimsByMonth />
        
        </Paper>
        </Grid>
</Grid>
<Grid  container spacing={1} style={{    marginTop: -44}} >
  {/*  style={{marginLeft: 3}} */}
        <Grid item xs = {6} > 
        <Paper elevation={3} className="a" >
       
        <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}><center> Open claims by Claim Type </center>
          </p>
     
        <InvestigationByClaimType />
      
        </Paper>
        </Grid>

        <Grid item xs = {6} > 
        <Paper elevation={3} className="a" >
        
        <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}><center> Open claims by Assigned Investigation Status​ </center>
          </p>
       
        <InvestigationComboChart />
       
        </Paper>
        </Grid>

        </Grid>

        </div>
        </div>
        </div>
    )
}

export default AIimpact
