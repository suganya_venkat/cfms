import React, { useState } from 'react'
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { Link } from 'react-router-dom';

// import CardComponent from "@material-ui/core/CardComponent"
import CardMedia from "@material-ui/core/CardMedia"
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Configuration from '../LandingPage_icon/Configuration.PNG'
import forensic from '../LandingPage_icon/forensic.PNG'
import reporting from '../LandingPage_icon/reporting.PNG'
import Alert_management from '../LandingPage_icon/Alert management.PNG'
import StructureData_insights from "../StructureData_insights/StructureData_insights";
import Rules from '../Rules/Rules';
import rules from '../../assets/images/rules.PNG'
import model from '../../assets/images/model.PNG'
import Reporting from '../../assets/images/Reporting.PNG'
import Investigation from '../../assets/images/Investigation And Alert Management.PNG'
import Business_rule from '../../assets/images/Business Rule.PNG'
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import DomainAddIcon from '@mui/icons-material/DomainAdd';
import AssessmentIcon from '@mui/icons-material/Assessment';
import AddchartIcon from '@mui/icons-material/Addchart';
//  import Capture from '../../assets/images/Capture.svg'
// import CaptureURL,{ReactComponent as Capture} from "../../assets/images/Capture.svg"
import data from './Icon.json'

import strategy from '../../assets/images/strategy.PNG'
import business_segment from '../../assets/images/business segment.PNG'
import Navbar_landingpage from '../../Components/NavBar_landingpage';
import './Configuration_icon.css'

// import Filters from './Filters';
function Adjuster() {

  const useStyles = makeStyles(theme => ({
   
    cardMediaStyle: {
      paddingTop: "84%",
      // width:'250px'
    }
  }));
  const styles = useStyles();
console.log(data.Documnents[0].BusinessRule_string)
  return (
    <div>
     
      <Navbar_landingpage/>
    <Grid container spacing={1}  alignItems='center' alignContent='center' className='gridCenter'>
    <Grid item xs = {3} style={{marginLeft:155}}>
 <Paper>
   
    <Link to={'/AlertManagement'} style={{ textDecoration: 'none' }}>
 
    {/* <CardMedia
          className={styles.cardMediaStyle}    
              image={Investigation}

/> */}
 
 <ManageSearchIcon style={{fontSize: 189,cursor: "pointer",marginLeft:81}}/>
  <p style={{fontSize: 16,marginLeft:10}}><center><b>Investigation And Alert Management</b></center></p>
 
  <br/>
    </Link>
    </Paper>
    </Grid>
    
   
    <Grid item xs = {3}> 
    <Paper   >
    
    
    <Link to={'/BusinessRule'} style={{ textDecoration: 'none' }}>
    {/* <img style={{width:"100%",height:"100%"}}
              // className="image"
              src={data.Documnents[0].BusinessRule_string}
            ></img>  */}
             <DomainAddIcon style={{fontSize: 189,cursor: "pointer",marginLeft:81}}/>
  <p style={{fontSize: 16,marginLeft:31}}><center><b>Analytics</b></center></p>
 <br/>
 
{/* <Capture/> */}
  
           {/* <CardMedia
             className={styles.cardMediaStyle} 
              image={Business_rule}

/> */}
 
    </Link>
    </Paper>
    </Grid>
    <Grid item xs = {3} width ="50px"> 
    <Paper  >
   
    <Link to={'/Summary'} style={{ textDecoration: 'none' }}>
   
    {/* <CardMedia
          className={styles.cardMediaStyle}    
              image={Reporting}

/> */}
 <AddchartIcon style={{fontSize: 189,cursor: "pointer",marginLeft:81}}/>
  <p style={{fontSize: 16,marginLeft:40}}><center><b>Reporting</b></center></p>
  <br/>

{/* <p><center>Alert Management</center></p> */}
   
    </Link>
    </Paper>
    </Grid>

    {/* <Grid item xs = {3} width ="50px"> 
    <Paper  >
    <CardActions>
      
    </CardActions>
    <CardContent>
             <CardMedia
              className={styles.cardMediaStyle}
              image={business_segment}

/>
    </CardContent>
    </Paper>
    </Grid> */}

    </Grid>

    

    </div>
    
  )
}

export default Adjuster



