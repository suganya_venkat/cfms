import React, { useState } from 'react'
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { Link } from 'react-router-dom';

// import CardComponent from "@material-ui/core/CardComponent"
import CardMedia from "@material-ui/core/CardMedia"
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Configuration from '../LandingPage_icon/Configuration.PNG'
import forensic from '../LandingPage_icon/forensic.PNG'
import reporting from '../LandingPage_icon/reporting.PNG'
import Alert_management from '../LandingPage_icon/Alert management.PNG'
import StructureData_insights from "../StructureData_insights/StructureData_insights";
import Rules from '../Rules/Rules';
import rules from '../../assets/images/rules.PNG'
import model from '../../assets/images/model.PNG'
import strategy from '../../assets/images/strategy.PNG'
import business_segment from '../../assets/images/business segment.PNG'
import Navbar_landingpage from '../../Components/NavBar_landingpage';
import './Configuration_icon.css'

// import Filters from './Filters';
function Analytics() {

  const useStyles = makeStyles(theme => ({
   
    cardMediaStyle: {
      paddingTop: "120%",
      // width:'250px'
    }
  }));
  const styles = useStyles();

  return (
    <div>
     
      <Navbar_landingpage/>
    <Grid container  alignItems='center' alignContent='center' className='gridCenter' style={{marginLeft:328}}>
    <Grid item xs = {3}>
    <Paper  >
    <CardActions>
    
    </CardActions>
    <Link to={'/Summary'} style={{ textDecoration: 'none' }}>
    <CardContent>
    <CardMedia
          className={styles.cardMediaStyle}    
              image={rules}

/>
<p><center>Reporting</center></p>
    </CardContent>
    </Link>
    </Paper>
    </Grid>
    
   
    <Grid item xs = {3}> 
    <Paper   >
    <CardActions>
      
    </CardActions>
    
    <Link to={'/BusinessRule'} style={{ textDecoration: 'none' }}>

    <CardContent>
           <CardMedia
             className={styles.cardMediaStyle} 
              image={model}

/>
<p><center>AI Rules</center></p>
    </CardContent>
    </Link>
    </Paper>
    </Grid>
    <Grid item xs = {3} width ="50px" style={{marginLeft: -134}}> 
    <Paper  >
    {/* <CardActions>
      
    </CardActions>
    <Link to={'/AlertManagement'} style={{ textDecoration: 'none' }}>
    <CardContent>
    <CardMedia
          className={styles.cardMediaStyle}    
              image={rules}

/>
    </CardContent>
    </Link> */}
    </Paper>
    </Grid>

    <Grid item xs = {3} width ="50px"> 
    <Paper  >
    {/* <CardActions>
      
    </CardActions>
    <CardContent>
             <CardMedia
              className={styles.cardMediaStyle}
              image={business_segment}

/>
    </CardContent> */}
    </Paper>
    </Grid>

    </Grid>

    

    </div>
    
  )
}

export default Analytics



