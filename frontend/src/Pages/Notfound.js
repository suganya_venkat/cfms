import React from 'react'
import './NotFound.css';

function KPI(props) {
    
    return (
//         <div className="kpi">
        <div className="kpi-description"> {props.kpi_description}
          <div className="kpi-value">{props.kpi_value}
              <div className="kpi-impact">{props.kpi_impact} </div>
           </div>  
        </div>
//         </div>
    
    )
  
}


function IconKPI(props) {
    
    return (
    <div className='icon-kpi-card'>
        <div className='row-spanned-item'> <span className='dot'> </span> </div>
        <div className='icon-kpi-description'> {props.kpi_description} </div>
        <div className='icon-kpi-value'> {props.kpi_value}</div>
    </div>
        
    )
}


function KPIGrid(props) {
    
    
    return (
    
        <div className='kpi-grid'>
        
            
                   
            <div className="kpi-row-spanned-description span-row"> Total Claims
                  <div className="kpi-value"> 16 %
                         <div className="kpi-impact"> +150% </div>
                   </div>  
            </div>
        
        
             <div>
           <KPI kpi_name="Fraud Capture Rate #2" kpi_description="Fraud Capture Rate #2" kpi_value="16 %" kpi_impact="+150%" />
            </div>
        
            <div>
           <KPI kpi_name="Fraud Capture Rate #3" kpi_description="Fraud Capture Rate #3" kpi_value="16 %" kpi_impact="+150%" />
            </div>
        
             <div>
           <KPI kpi_name="Fraud Capture Rate #4" kpi_description="Fraud Capture Rate #4" kpi_value="16 %" kpi_impact="+150%" />
            </div>
        
             <div>
           <KPI kpi_name="Fraud Capture Rate #5" kpi_description="Fraud Capture Rate #5" kpi_value="16 %" kpi_impact="+150%" />
            </div>
        
             <div className="kpi-row-spanned-description span-row"> Total Claims
                  <div className="kpi-value"> 16 %
                         <div className="kpi-impact"> +150% </div>
                   </div>  
            </div>
        
        <div>
           <KPI kpi_name="Fraud Capture Rate #7" kpi_description="Fraud Capture Rate #7" kpi_value="16 %" kpi_impact="+150%" />
            </div>
        
             <div>
           <KPI kpi_name="Fraud Capture Rate #8" kpi_description="Fraud Capture Rate #8" kpi_value="16 %" kpi_impact="+150%" />
            </div>
        
            <div>
           <KPI kpi_name="Fraud Capture Rate #9" kpi_description="Fraud Capture Rate #9" kpi_value="16 %" kpi_impact="+150%" />
            </div>
        
             <div>
           <KPI kpi_name="Fraud Capture Rate #10" kpi_description="Fraud Capture Rate #10" kpi_value="16 %" kpi_impact="+150%" />
            </div>
        
        
        </div>
    )

    

}



function AnalyticsPage(props) {
    
    return (
        <div>
            <div className='main-header'>
                Main App Header
            </div>
        
            <div className='analytics-nav-bar'> 
                <div className=''> button1 </div>
                <div className=''> button2 </div>
            </div>
        
        </div>
    )
}



function Notfound() {
    return (
                
            <div>     
                <div className="kpi">
                   <KPI kpi_name="Fraud Capture Rate #2" kpi_description="Fraud Capture Rate #2" kpi_value="16 %" kpi_impact="+150%" />
                </div>
    
                <div>
                   <KPIGrid />
                </div>
        
                
        
                <div className="header-horiz">
                      <span className='dot'> </span>
                   Claims Summary
                </div>
        
                <div className="kpi">
                   <IconKPI kpi_description="Claims Initiated" kpi_value="2000" />
                </div>
                
                <div className="app-main-header">
                   Claims Fraud Management Dashboard
                </div>
        
                <div> <br / > 
                </div>
        
                <div className="app-sub-header">
                   Subheader1
                </div>
                
        
        </div>
    )
}

export default Notfound
