import React from 'react'
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { Typography } from "@material-ui/core";
import Configuration_navbar from '../../Components/Configuration_navbar'
import MainNavBar from '../../Components/MainNavBar'
import CreateRule from './CreateRule'
import RuleBankPage from './RuleBankPage'
import NewTheme from '../../Components/newTheme.js';
import DomainAddIcon from '@mui/icons-material/DomainAdd';
import AnalyticsNavBar from '../../Components/AnalyticsNavBar'
import Popover from '@mui/material/Popover';
import CardMedia from "@material-ui/core/CardMedia"
import AnalyticsBe from '../../assets/images/AnalyticsBe.png'
import ProgressBarTop from '../../assets/images/ProgressBarTop.PNG'
import { styled } from '@mui/material/styles';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import InfoIcon from '@mui/icons-material/Info';
import '../CommonStyling/Style.css'
import '../CommonStyling/Style.css'
import './Rules.css'
import RuleSummary from './RuleSummary';

const HtmlTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#e6e6fa', //e0ffe0
    color: 'rgba(0, 0, 0, 0.87)',
    width: 400,
    height:100,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
    padding:"10px"
  },
}));

function Rules() {
  window.localStorage.setItem("id",2)
  // console.log(window.localStorage.getItem("id"))
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;



    const [getNewRule, setNewRule] = React.useState("No");
    const [getFeature,setFeature]= React.useState("")
    return (
        <div style={{margin:0}}>
            
            <AnalyticsNavBar/>
            {/* <CardMedia
     className='progressTop'   
          image={ProgressBarTop}

 /> */}

          {/* <NewTheme/>
          <div className='activeLineRules' ></div> <br/> */}
     <Configuration_navbar/>
     <div style={{margin: 10}}>
     <div  className="infoHeadingContainer">
     <div>
     {/* <DomainAddIcon style={{fontSize: 30,cursor: "pointer"}}  onClick={handleClick}  />    */}
      {/* <h3 style={{color:"black",marginLeft:50,marginTop:-37}}>Domain Intelligence</h3><br/> */}
      <h3 style={{color:"black",marginLeft:12,marginTop:31,fontSize:24}}>Domain Intelligence</h3><br/> 
     
     </div>

     <span className="info">
       <HtmlTooltip  placement="right-start"
        title={
          <React.Fragment>
             <div style={{fontSize: 12 ,height: 59,padding: 10}}>
        <p><i>Incorporating the domain knowledge to identify suspicious claims</i></p><br/>
        <p>Example: A very high Report Lag can possibly mean high chances of fraud.</p>
        </div>
          </React.Fragment>
        }
      >
        <InfoIcon className="infoIcon" />
      </HtmlTooltip>
     
      </span>
      
     
     </div> 
    
     
     {/* <p style={{fontSize:11,marginLeft: 53}}> Incorporating the domain knowledge to identify suspicious claims</p>
<br/>

      <p style={{fontSize:11,marginLeft: 53}}><i>Example: A very high Report Lag can possibly mean high chances of fraud.</i></p> */}
     
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
         <CardMedia
          className='cardMediaStylepopup'   
              image={AnalyticsBe}

/>
        {/* <Typography sx={{ p: 2 }}>The content of the Popover.</Typography> */}
      </Popover>


         <br/>
       
     <Grid  container spacing={1}>     <Grid item xs = {6}>
    <Paper style={{marginLeft:8}} >
   
      <p className='variableSummaryHeader'>
      
   Rule Analysis
      </p>

   

    <CreateRule getNewRule={getNewRule} setNewRule={setNewRule}  getFeature={getFeature} setFeature={setFeature}/>
   
    </Paper>
    </Grid>
    <Grid item xs = {6}>
    <Paper style={{marginLeft:8}} >
   
      <p className='variableSummaryHeader'>
      
   Rule Summary
      </p>

   

    <RuleSummary  getFeature={getFeature} setFeature={setFeature}/>
   
    </Paper>
    </Grid>
    </Grid>
    <br/>
    <Grid  container spacing={1}>  
    <Grid item xs = {12} > 
    <Paper  >
  
    <p className='TextStyle' >
      Business Rules Bank
            </p>
    
   
    
      <RuleBankPage getNewRule={getNewRule} setNewRule={setNewRule}/>   
    </Paper>
    </Grid>

    </Grid>
    </div>
     </div>
    )
}

export default Rules
