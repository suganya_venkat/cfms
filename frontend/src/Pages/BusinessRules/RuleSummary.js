import React, { useRef, useState, useEffect } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import MaterialTableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

import { makeStyles, withStyles } from "@material-ui/core/styles";

import './Rules.css'

const TableCell = withStyles({
  root: {
    borderBottom: "none"
 
  }
 
})(MaterialTableCell);

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto"
  },
  // table: {
  //   minWidth: 650
  // }
}));













export default function RuleSummary(props) {

  // const [getNewRule, setNewRule] = React.useState("No");
  const selectedFeature = props.getFeature;

console.log(selectedFeature)
    const [variable, setVariable] = React.useState('');
   
    // const[filterVariable,setFilterVariable]=useState([]);

      
      const [data,setData]=useState([]);
    //   const [feature,setFeature]=useState('');
     
    
    

     
  
  

  
  
  



  

 
// console.log(feature)

// useEffect(() => {
//   fetch('/unique_values').then(res => res.json()).then((data) => setFilterVariable(data));
//   // setStatus(data.States);
//   // status.push
//   },[]);

  // console.log(filterVariable)

  useEffect(() => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(  

      {"feature" : selectedFeature , 

     
    } 

     )
    };
    fetch('/var_summary', requestOptions)
    .then(res => res.json()).then((data) => setData(data)) .catch(res => {
      console.log("Issue loading data")
      setData([selectedFeature])
    });
  },[selectedFeature]);

console.log(data)
//  console.log(feature)

  return (
      <div>
           {/* <Rule_navbar/> */}
<br/>

{!data.VariableSummary? <h6><center><i>Select rule feature to see the details</i>
</center></h6>:

    <TableContainer component={Paper}  style={{maxHeight: 153}}>
      <Table sx={{ minWidth: 350 }} size="small" className="rule" aria-label="a dense table sticky table" stickyHeader>
        <TableHead>
          <TableRow>
            <TableCell style={{fontSize:12}}>Level</TableCell>
            <TableCell style={{textAlign: "center",fontSize:12}} >Total</TableCell>
            <TableCell style={{textAlign: "center",fontSize:12}}>Fraud</TableCell>
            <TableCell style={{textAlign: "center",fontSize:12}}> Fraud%</TableCell>
            <TableCell style={{WhiteSpace: "nowrap",textAlign: "center",fontSize:12}}>Overall Fraud%</TableCell>
         
          </TableRow>
        </TableHead>
        <TableBody>
            
          {
          data.VariableSummary && data.VariableName != selectedFeature ? <h6><center style={{marginLeft:250}}><i>Loading</i>
          </center></h6>: data.VariableSummary.map((row,index) => (
         
            <TableRow
              key={index}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              {/* <TableCell component="th" scope="row">
               
              </TableCell> */}
             <TableCell  style={{fontSize:12}}>{row.VariableLevel}</TableCell>
              <TableCell style={{textAlign: "center",fontSize:12}}>{row.Total.toLocaleString("en", {   
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
})}</TableCell>
              <TableCell style={{textAlign: "center",fontSize:12}}>{row.Frauds.toLocaleString("en", {   
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
})}</TableCell>
              <TableCell style={{textAlign: "center",fontSize:12}}>{(row["Fraud %"]*100).toFixed(0)}%</TableCell>
              <TableCell style={{textAlign: "center",fontSize:12}}>{(row["Overall Fraud %"]*100).toFixed(0)}%</TableCell>

            
            </TableRow>
          ))}
        </TableBody>
        
      </Table>
     
    </TableContainer>
}
   
   
        </div>
  );
}