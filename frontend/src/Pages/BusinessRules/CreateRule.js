import React, { useRef, useState, useEffect } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import MaterialTableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@mui/material/FormHelperText';
import Rule_navbar from '../../Components/Rule_navbar'
import RuleFilters from './RuleFilters'
import { makeStyles, withStyles } from "@material-ui/core/styles";
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import Alert from '@mui/material/Alert';

import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Collapse from '@mui/material/Collapse';

import RuleBank from '../RuleBank/RuleBank'
import { Link } from 'react-router-dom';
import './Rules.css'

import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
const TableCell = withStyles({
  root: {
    borderBottom: "none" 
  }
})(MaterialTableCell);

// const useStyles = makeStyles(theme => ({
//   root: {
//     width: "100%",
//     marginTop: theme.spacing(3),
//     overflowX: "auto"
//   },
 
// }));



var RuleVariable1=[]
var RuleVariable2= []
var RuleVariable3= []
var RuleOperator1=[]
var RuleOperator2=[]
var RuleOperator3=[]
var RuleValue1=[]
var RuleValue2=[]
var RuleValue3=[]










export default function CreateRule(props) {

  // const [getNewRule, setNewRule] = React.useState("No");


const [open, setOpen] = React.useState(true);

    const [variable, setVariable] = React.useState('');
    const [operator, setOperator] = React.useState('');
  
    const [operator1, setOperator1] = React.useState('');
    const [variable1, setVariable1] = React.useState('');
    const [operator2, setOperator2] = React.useState('');
    const [variable2, setVariable2] = React.useState('');
    const [value, setvalue] = React.useState('');
    const [value1, setvalue1] = React.useState('');
    const [value2, setvalue2] = React.useState('');
      const [save, setSave] =  React.useState("");
      const [FinalData, setFinalData] = React.useState([])
      const [buttonDisabled, setbuttonDisabled] = React.useState(false);
      const [isStart, setIsStart] = useState(false);
      const [rulesAdded, setRulesAdded] = useState("No");
      const [data,setData]=useState([]);
      const [feature,setFeature]=useState('');
      const[filterVariable,setFilterVariable]=useState([]);

    
    
      

      const newRuleStatus = () => {
        props.setNewRule("Yes")
        setbuttonDisabled(false)
        // setVariable("")
        // setOperator("")
        // setvalue("")
      }; 
   

  const variableSelected = (event) => {
    setVariable(event.target.value);
    RuleVariable1.push(event.target.value);
    event.preventDefault();
    // FinalData.push({"rule1":{"Variable": RuleVariable1.toString()}})
  };
  
  // console.log(variable)
  

  

  const operatorSelected = (event) => {
    setOperator(event.target.value);
    RuleOperator1.push(event.target.value);
    event.preventDefault();
    // FinalData.push({"rule1":{"Operator": RuleOperator1.toString()}})
  };

  
  

  

  
  const valueSelected = (event) => {
    setvalue(event.target.value);
    RuleValue1.push(event.target.value)
      
      ;
      event.preventDefault();
    // FinalData.push({"rule1":{"Value": RuleValue1.toString()}})

  };
 
  


  useEffect(() => {
    if(isStart)
    {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(FinalData),
    };

    fetch('/add_rules', requestOptions).then((res) =>
      setSave(res.status),
       setbuttonDisabled(true)  , 
      
                
    );}
    },[isStart],[]);

  const passingData = () =>{

    

    if(variable, operator, value){
      
    FinalData.push({"rule1":{"Operator": RuleOperator1.toString(),
    "Variable": RuleVariable1.toString(), "Value": RuleValue1[RuleValue1.length-1].toString()
    
  }
  
  }
  
  )
  RuleOperator1 = []
  RuleVariable1=[]
  RuleValue1=[]
}
  if(variable1, operator1, value1){
    FinalData.push({"rule2":{"Operator": RuleOperator2.toString(),
  "Variable": RuleVariable2.toString(), "Value": RuleValue2[RuleValue2.length-1].toString()
}} )
  
RuleOperator2=[]
RuleVariable2=[]
RuleValue2=[]
  
}

if(variable2, operator2, value2){
  FinalData.push({"rule3":{"Operator": RuleOperator3.toString(),
"Variable": RuleVariable3.toString(), "Value": RuleValue3[RuleValue3.length-1].toString()
}} )

RuleOperator3=[]
RuleVariable3=[]
RuleValue3=[]
}


setIsStart(true)
    
window.location.reload(false);
//       save === 200 ? setbuttonDisabled(true) : setbuttonDisabled(false)
 
  };
      
  
  
  



  
const variableSummary = () => {
  props.setFeature(variable)
  // setFeature(variable)
}
 
// console.log(feature)

useEffect(() => {
  fetch('/unique_values').then(res => res.json()).then((data) => setFilterVariable(data));
  // setStatus(data.States);
  // status.push
  },[]);

  // console.log(filterVariable)

  useEffect(() => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(  

      {"feature" : feature , 

     
    } 

     )
    };
    fetch('/var_summary', requestOptions)
    .then(res => res.json()).then((data) => setData(data)) .catch(res => {
      console.log("Issue loading data")
      setData([feature])
    });
  },[feature]);


  return (
      <div>
           {/* <Rule_navbar/> */}
<br/>

    <TableContainer component={Paper}>
      <Table   size="small" aria-label="a dense table">
        <TableHead>
          <TableRow  style={{
                    height: (20) 
                  }}>
         
         
          </TableRow>
        </TableHead>
        <TableBody>
         
            <TableRow
              // key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row" style={{borderBottom: "none"}}>
              <FormControl >
        <InputLabel  id="Variables">Rule Feature</InputLabel>
        <Select style={{ marginTop: 0,fontSize:10}}
          labelId="Variables"
          id="Variable"
          required
          value={variable}
          label="Variables"
          onChange={variableSelected}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          { Object.keys(filterVariable).length > 0 ?
            filterVariable.VariableSummaryFeatures.map((d) => {
             
                return ( 
            <MenuItem 
            key={d}
            value={d}
            >
              {d}
            </MenuItem>
                )
              })
              : ""
              
               
}
          {/* <MenuItem value={"Claims Amount"}>Claims Amount</MenuItem>
          <MenuItem value={"Claims Age"}>Claims Age</MenuItem> */}
          
        </Select>
        {/* <FormHelperText>With label + helper text</FormHelperText> */}
      </FormControl>
              </TableCell>
              <TableCell  style={{borderBottom: "none"}}>
              <FormControl >
        <InputLabel id="Operator">Operator</InputLabel>
        <Select style={{ marginTop: 0,fontSize:10,maxWidth: 190}}
          required
          labelId="Operator"
          id="Operator"
          value={operator}
          label="Operator"
          onChange={operatorSelected}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={">"}>Greater than</MenuItem>
          <MenuItem value={"<"}>Lesser Than</MenuItem>
          <MenuItem value={">="}>Greater than or equal to</MenuItem>
          <MenuItem value={"<="}>Lesser than or equal to</MenuItem>
          <MenuItem value={"="}>Equal to</MenuItem>

        </Select>
        {/* <FormHelperText>With label + helper text</FormHelperText> */}
      </FormControl>
      
                  
                  </TableCell>
              <TableCell align="center" style={{borderBottom: "none"}}>
              <Box
      component="form"
      sx={{
        '& > :not(style)': { m: 1, width: '25ch' },
      }}
      noValidate
      autoComplete="off"
    >
              <TextField id="Value" label="Value" style={{ borderBottom: "none", width:164}}
              variant="outlined"
              required
              name="Value"
              // label="Value"
              type="Value"
              // autoFocus
              value={value}
              // onChange={(event) => {setPassword(event.target.value)}} //whenever the text field change, you save the value in state
              onChange={valueSelected}
              />

     </Box>
                  
                  </TableCell >
           
            </TableRow>


           
      
         
                   </TableBody>
       
      </Table>
      <br/>
      {/* onClose={() => <RuleBank/>} component={Link} to='/RuleBank' */}
    {save === 200 ?  <Collapse in={open}> <Alert  action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setOpen(false);
                newRuleStatus()
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }onClose={() => newRuleStatus()}>Successfully created the Rule!</Alert> </Collapse>
    : ""}
                      {save != 200 && save ? <Alert severity="error">Rules are not creates!</Alert>
    : ""}
      <div >
        
   {/* <Button variant="outlined">Test & Save</Button>   */}

   
   <Button style={{fontSize:12,fontWeight:500,backgroundColor:"black",color:"white",marginLeft:10}}  variant="outlined"  onClick={variableSummary}>Rule Summary</Button>
<Button style={{marginLeft:10,fontSize:12,fontWeight:500,backgroundColor:"#6A359C",color:"white"}} variant="outlined" disabled = {buttonDisabled} onClick={passingData}>Move To Rule Bank</Button>

        </div>
        <br/>
    </TableContainer>

    {/* <TableContainer component={Paper} style={{height: 284}}>
      <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table sticky table" stickyHeader>
        <TableHead>
          <TableRow>
            <TableCell>Level</TableCell>
            <TableCell>Total</TableCell>
            <TableCell>Fraud</TableCell>
            <TableCell>Fraud%</TableCell>
            <TableCell>Overall Fraud%</TableCell>
         
          </TableRow>
        </TableHead>
        <TableBody>
            
          {data.VariableSummary && data.VariableName === feature ? data.VariableSummary.map((row,index) => (
         
            <TableRow
              key={index}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            > */}
              {/* <TableCell component="th" scope="row">
               
              </TableCell> */}
             {/* <TableCell>{row.VariableLevel}</TableCell>
              <TableCell>{row.Total.toLocaleString("en", {   
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
})}</TableCell>
              <TableCell>{row.Frauds.toLocaleString("en", {   
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
})}</TableCell>
              <TableCell>{(row["Fraud %"]*100).toFixed(0)}%</TableCell>
              <TableCell>{(row["Overall Fraud %"]*100).toFixed(0)}%</TableCell>

            
            </TableRow>
          )): <div><br/><h6 style={{marginLeft:85}}>Select rule feature to see the details</h6> </div>}
        </TableBody>
        
      </Table>
     
    </TableContainer>
    */}
   
   
        </div>
  );
}