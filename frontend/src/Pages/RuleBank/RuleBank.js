import React from 'react'

import Configuration_navbar from '../../Components/Configuration_navbar'
import Rule_navbar from '../../Components/Rule_navbar'
import MainNavBar from '../../Components/MainNavBar'
import RuleBankPage from './RuleBankPage'

function RuleBank() {
    return (
    <div>
        <MainNavBar/>
     <Configuration_navbar/>
     <Rule_navbar/>
     <RuleBankPage/>
     </div>
    )
}

export default RuleBank
