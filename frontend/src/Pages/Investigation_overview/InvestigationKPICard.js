import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Data from './InvestigationData_KPI.json'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    border: `1px solid #EEF3F5`,
    borderRadius: 4,
    color: "#949FAF",
    minWidth: 275,
    minHeight: 70,
    marginTop: 12,
    marginLeft: 12,
    marginRight: 12,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    paddingBlock: 20,
    borderRadius: 0.5,
  },
  card: {
    color: "#16448F",
    fontWeight: 700,
    display: "inline-block"
  },
  cardsmall:{
    color: '#5DC9A2',
    fomtWeight: 500,
    display: "inline-block",
    fontSize: 12,
    marginInline: 4 
  }
}));

const fetchURL = "http://localhost:5000/insights/tccspv_kpi";
const fetchURLcost = "http://localhost:5000/insights/cost/kpi_cost_overview";
const fetchforecast = "http://localhost:5000/insights/forecast/kpicard";

export default function InvestigationKpiCards(props) {
    const classes = useStyles();

    
  const [data,setData]=useState({});
  const selectedState = props.getState;
    const selectedToRptDate=props.getToRptDate;
    const selectedClaimType=props.getClaimType;
    const selectedClaimAmount=props.getClaimAmount;
    const selectedFromRptDate = props.getFromRptDate;
    
  //  useEffect(() => {
  // fetch('/invesigation_overview').then(res => res.json()).then((data) => setData(data));
  // },[]);
  useEffect(() => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({"reporting_view_filters" :  

      {"CLAIMREPORTDATE" : [{"from":  selectedFromRptDate,  

                          "to": selectedToRptDate}], 

      "STATE" : selectedState,  

      "CLAIM_TYPE": selectedClaimType,  

      "CLAIM_AMT_USD": selectedClaimAmount

    } 

    } )
    };
    fetch('/reporting_overview_investigation', requestOptions)
    .then(res => res.json()).then((data) => setData(data));
  },[selectedState,selectedFromRptDate,selectedToRptDate,selectedClaimType,selectedClaimAmount]);

//  console.log(data);

  // console.log(forecasts);

  return (
    <div className={classes.root}>
     {Object.keys(data).length > 0 ? (
         <div>
          <Grid container alignItems="center" spacing={1}>
            <Grid item xs={12} sm={2}>
              <Paper className={classes.paper}>
                {" "}
                Total Claim Initiated <br /><br />{" "}
                <div className={classes.card}>{data.Claims_Initiated}</div>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={10}>
              <Paper className={classes.paper}>
                {" "}
                Total Investigated<br /><br />{" "}
                {/* {datalevel.toUpperCase()} (HTD) <br /> */}
                <div className={classes.card}>
                {data.Total_Investigated}
                </div>
              </Paper>
              <Grid container alignItems="center" spacing={1}>
              <Grid item xs={12} sm={2}>
              <Paper className={classes.paper}>
                {" "}
                {/* {datalevel.toUpperCase()} (YTD) <br /> */}
                Total closed <br /><br />{" "}
                <div className={classes.card}> {data.Total_Closed}</div>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={2}>
              <Paper className={classes.paper}>
                {" "}
                {/* {datalevel.toUpperCase()} (QTG) <br /> */}
                Paid Claims  <br /><br />{" "}
                <div className={classes.card}>
                  {data.Paid_Claims}
                </div>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={2}>
              <Paper className={classes.paper}>
                {" "}
                {/* {datalevel.toUpperCase()} (HTG) <br /> */}
                Rejected Claim <br /><br />{" "}
                <div className={classes.card}>
                {data.Rejected}
                </div>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={2}>
              <Paper className={classes.paper}>
                {" "}
                {/* {datalevel.toUpperCase()} (YTG) <br /> */}
                Fraud Claims <br /><br />{" "}
                <div className={classes.card}>
                {data.Fraud_Suspected_Claims}
                </div>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={2}>
              <Paper className={classes.paper}>
                {" "}
                 {/* {datalevel.toUpperCase()} (YTG) <br />  */}
                 Investigation Rate <br /><br />{" "}
                <div className={classes.card}>
                {(data.Investigation_rate*100).toFixed(2)}%
                </div>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={2}>
              <Paper className={classes.paper}>
                {" "}
                 {/* {datalevel.toUpperCase()} (YTG) <br />  */}
                 Fraud Detection<br /><br />{" "}
                <div className={classes.card}>
                {(data.Fraud_detection*100).toFixed(2)}%
                </div>
              </Paper>
            </Grid>
            </Grid>
            </Grid>
           
           
            </Grid>
            
           
            
           
            
           
           
          
          
          
         </div> 
     ): "loading"} 
    </div>
  );
}
