import React, {useState}from 'react'
import Grid from "@material-ui/core/Grid";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import { Typography } from "@material-ui/core";
import NavBar from '../../Components/Navbar'
import MainNavBar from '../../Components/MainNavBar'
import InvestigationKPICard from './InvestigationKPICard'
import InvestidationClaimsByState from './InvestigationClaimsByState'
import InvestigationComboChart from './InvestigationComboChart'
import Investigation_filters from './Investigation_filters'
import './InvestigatedPage.css'

function Investigation_overview() {
  
  const [getState, setState] = useState(["all"]);
  const[getFromRptDate,setFromRptDate] = useState("2016-06-17 00:00:00");
  const[getToRptDate,setToRptDate] = useState("2021-12-11 00:00:00");
  const[getClaimType,setClaimType] = useState(["all"]);
  const[getClaimAmount,setClaimAmount] = useState(["all"]);
 
    return (
        <div className='divalign'>
          <MainNavBar/>
        <NavBar/>
        <br/>
        <Investigation_filters setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/>
       <InvestigationKPICard setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/>
       <Grid  container spacing={1}>
        <Grid item xs = {6}>
        <Paper elevation={3} className="fraudratio">
        <CardActions className="table">
        <Typography variant="h6" color="primary">
       Investigation by State
       </Typography>
      </CardActions>
          <InvestidationClaimsByState setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/>
          {/* <Typography variant="h6" color="primary">
          Claims By Claims Type
          </Typography> */}
        
        <CardContent>
        {/* <Filters/> */}
        
       
        </CardContent>
        </Paper>
        </Grid>
        
       
        <Grid item xs = {6} > 
        <Paper elevation={3} className="a" >
        
          <Typography variant="h6" color="primary">
         Payment Status
          </Typography>
      
        <InvestigationComboChart setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/>
       
        </Paper>
        </Grid>
        </Grid>
        </div>
    )
}

export default Investigation_overview
