import * as React from 'react';
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { Fragment, useState } from "react";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import './InvestigatedPage.css'

export default function Investigation_filters(props) {
 
  // const [Model, setModel] = React.useState('');
 
  // const stateHandleChange = (event) => {
  //   props.setState(event.target.value);
    
  // };

  const stateHandleChange = (event) => {
    const {
      target: { value },
    } = event;
    props.setState(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value,
    );
  };

  const claimTypeHandleChange = (event) => {
    const {
      target: { value },
    } = event;
    props.setClaimType(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value,
    );
  };

  
  const claimAmountHandleChange = (event) => {
    const {
      target: { value },
    } = event;
    props.setClaimAmount(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value,
    );
  };

  const [selectedFromDate, setSelectedFromDate] = useState("2016-06-17 00:00:00");
  const [selectedToDate, setSelectedToDate] = useState("2021-12-11 00:00:00");

  const handleFromDateChange = (date) => {
    console.log(date);
    props.setFromRptDate(date);
  };
  const handleToDateChange = (date) => {
    console.log(date);
    props.setToRptDate(date);
  };
  // const model_value = Model
  // console.log(model_value);
  const state_names = ['all','pahang', 'penang', 'perak', 'johor', ' malacca', 'pertis', 
'selangor', 'kedah', 'negeri sembilan', 'kelantan']

const Claim_Types= ['all','self - accident', 'Weather', 'Animal Collision', 'Theft', 'Windshield damage']
const Claim_Amount= ['all','0-1000', '1000-10000', '+10000']

const Reported_Daterange = ['2016-06-17 00:00:00', '2021-12-11 00:00:00']
  return (
    <div className='filter_div'>
    <Box sx={{ minWidth: 179 }} className='filter_flex'>
    <MuiPickersUtilsProvider utils={DateFnsUtils} >

<KeyboardDatePicker
  label="Reported Date From"
  value={props.getFromRptDate}
  onChange={handleFromDateChange}
  format="dd/MM/yyyy"
/>


</MuiPickersUtilsProvider>
</Box>
<Box sx={{ minWidth: 210 }} className='filter_flex'>
    <MuiPickersUtilsProvider utils={DateFnsUtils} >


<KeyboardDatePicker
  label="Reported Date To"
  value={props.getToRptDate}
  onChange={handleToDateChange}
  format="dd/MM/yyyy"
/>

</MuiPickersUtilsProvider>
</Box>
      <Box sx={{ minWidth: 210 }} >
      <FormControl fullWidth className='filter_flex'>
        <InputLabel id="State">State</InputLabel>
        <Select 
          labelId="State"
          id="State"
          multiple
          // displayEmpty
          value={props.getState}
          label="State"
          onChange={stateHandleChange}
        >
          {/* <MenuItem value=""><Put any default Value which you want to show/></MenuItem> */}
          {
            state_names.map((d) => {
             
                return ( 
            <MenuItem 
            key={d}
            value={d}
            >
              {d}
            </MenuItem>
                )
              })
              
               
}
        </Select>
        
      </FormControl>
      </Box>
      <Box sx={{ minWidth: 210 }} >
      <FormControl fullWidth className='filter_flex'>
        <InputLabel id="Claim Type">Claim Type</InputLabel>
        <Select 
          labelId="Claim Type"
          id="Claim Type"
          multiple
          // displayEmpty
          value={props.getClaimType}
          label="Claim Type"
          onChange={claimTypeHandleChange}
        >
          {/* <MenuItem value=""><Put any default Value which you want to show/></MenuItem> */}
          {
            Claim_Types.map((d) => {
             
                return ( 
            <MenuItem
            key={d} 
            value={d}>{d}</MenuItem>
                )
              })
              
               
}
        </Select>
        
      </FormControl>
      </Box>
      <Box sx={{ minWidth: 210 }} >
      <FormControl fullWidth className='filter_flex'>
        <InputLabel id="Claim Amount">Claim Amount</InputLabel>
        <Select 
          labelId="Claim Amount"
          id="Claim Amount"
          // displayEmpty
          multiple
          value={props.getClaimAmount}
          label="Claim Amount"
          onChange={claimAmountHandleChange}
        >
          {/* <MenuItem value=""><Put any default Value which you want to show/></MenuItem> */}
          {
            Claim_Amount.map((d) => {
             
                return ( 
            <MenuItem 
            key={d}
            value={d}
            >
              {d}
            </MenuItem>
                )
              })
              
               
}

          {/* <MenuItem value={"Less than "}>Model 3</MenuItem> */}
        </Select>
        
      </FormControl>
      </Box>
     
    </div>
  );
 
}
