import  React , {useEffect , useState,useRef} from 'react';
import claimsdata from './InvestigationData_KPI.json'
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

am4core.useTheme(am4themes_animated);

const CHART_ID = "InvestigationCombochart";

function InvestigationComboChart(props) {
    const chartRef = useRef(null);
    
     const [data,setData]=useState([]);
     const selectedState = props.getState;
    const selectedToRptDate=props.getToRptDate;
    const selectedClaimType=props.getClaimType;
    const selectedClaimAmount=props.getClaimAmount;
    const selectedFromRptDate = props.getFromRptDate;

    useEffect(() => {
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({"reporting_view_filters" :  
  
        {"CLAIMREPORTDATE" : [{"from":  selectedFromRptDate,  
  
                            "to": selectedToRptDate}], 
  
        "STATE" : selectedState,  
  
        "CLAIM_TYPE": selectedClaimType,  
  
        "CLAIM_AMT_USD": selectedClaimAmount
  
      } 
  
      } )
      };
      fetch('/reporting_overview_investigation', requestOptions)
      .then(res => res.json()).then((data) => setData(data));
    },[selectedState,selectedFromRptDate,selectedToRptDate,selectedClaimType,selectedClaimAmount]);

  //    useEffect(() => {
  // fetch('/invesigation_overview').then(res => res.json()).then((data) => setData(data));
  // },[]);
    
       var sample =  data.Investigated_Payment_Status === undefined ||  data.Investigated_Payment_Status === null ||  Object.keys(data).length <= 0
?  null : data.Investigated_Payment_Status.map((a) => {
  a.color = a.index === 'No' ? am4core.color("#00965f") :  a.index === 'Yes' ? am4core.color("#00966f") : am4core.color("#235789")
  
  
})
    
    useEffect(() => {
        var chart = am4core.create(CHART_ID, am4charts.PieChart);

        // chartRef.current = am4core.create(CHART_ID, am4charts.PieSeries);
        chart.data = Object.keys(data).length > 0 ? data.Investigated_Payment_Status : "Loading";
        

        let pieSeries = chart.series.push(new am4charts.PieSeries());
         pieSeries.dataFields.category = "index";
        pieSeries.dataFields.value = "PAID";
        pieSeries.labels.template.fontSize=12
      
     
        pieSeries.tooltip.disabled = false;
        pieSeries.ticks.template.disabled = true;
        pieSeries.slices.template.propertyFields.fill = "color";

         chart.exporting.menu = new am4core.ExportMenu();
         chart.exporting.menu.align = "right";
         chart.exporting.menu.verticalAlign = "top";
      
        //   return () => {
        //    chart &&chart.dispose();
        //   };
        
      
        // Load data into chart
    
      })
      
        return (
          <div
            id={CHART_ID}
            style={{ width: "100%", height: "200px", margin: "50px 0" }}
          >
            {" "}
          </div>
        );
  
}


export default InvestigationComboChart
