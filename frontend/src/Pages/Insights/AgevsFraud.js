import  React , {useEffect , useRef} from 'react';
import claimsdata from '../LandingPage/ClaimsData.json';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

am4core.useTheme(am4themes_animated);

const CHART_ID = "ClaimsReport_chart";

function AgevsFraud(){

    const chartRef = useRef(null);
    useEffect(() => {
        chartRef.current = am4core.create(CHART_ID, am4charts.XYChart);
        chartRef.current.data = claimsdata;
    
        chartRef.current.colors.list = [
          am4core.color("#52C1C8"),
          am4core.color("#A0CCEC"),
        ];

        // let dateAxis = chartRef.current.xAxes.push(new am4charts.DateAxis());
        // dateAxis.dataFields.category = "date";
        // dateAxis.dateFormats.setKey("month", "yyyy-MM");
        // dateAxis.periodChangeDateFormats.setKey("month", "yyyy-MM");
        // dateAxis.renderer.grid.template.disabled = true;
        // dateAxis.renderer.labels.template.fill = am4core.color("#949FAF");
        // dateAxis.tooltip.disabled = true;
        // dateAxis.gridIntervals.setAll([
        //     { timeUnit: "month", count: 1 },
        //     { timeUnit: "month", count: 3 },
        //   ]);

                // Create axes
        var categoryAxis = chartRef.current.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "claim_counts";
        categoryAxis.title.text = "claim_counts";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 20;

        var  valueAxis = chartRef.current.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = "";

        // Create series
        var series = chartRef.current.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = "fraud_reported_n";
        series.dataFields.categoryX = "claim_counts";
        series.name = "fraud_reported_n";
        series.tooltipText = "{name}: [bold]{valueY}[/]";
        series.stacked = true;

      
        //   let valueAxis = chartRef.current.yAxes.push(new am4charts.ValueAxis());
        //   valueAxis.title.text = "Case Vol.";
        //   valueAxis.tooltip.disabled = true;
        //   valueAxis.renderer.labels.template.fill = am4core.color("#BCC3CE");
        //   valueAxis.renderer.grid.template.stroke = am4core.color("#e7eff3");
        //   valueAxis.renderer.grid.template.strokeOpacity = 1;
        //   // valueAxis.min = 0;
        //   // valueAxis.extraMin = 0.2;
        //   // valueAxis.extraMax = 0.2;
      
        //   let series = chartRef.current.series.push(new am4charts.LineSeries());
        //   series.dataFields.dateX = "incident_date";
        //   series.dataFields.valueY = "age";
        //   series.strokeWidth = 3;
        //   series.name = "Actual";
        //   series.tooltipText = "{dateX}{valueY}";
        //   series.propertyFields.strokeDasharray = "lineDash";
        //   series.propertyFields.stroke = "lineColor";
        //   // series.propertyFields.fill = "lineColor";
      
        //   let bullet = series.bullets.push(new am4charts.CircleBullet());
        //   bullet.circle.radius = 4;
        //   bullet.circle.strokeWidth = 2;
      
          // bullet.adapter.add("fill", function (fill, target) {
          //   if (!target.dataItem) {
          //     return fill;
          //   }
          //   var values = target.dataItem.properties;
      
          //   return values.strokeDasharray === ""
          //     ? am4core.color("#52C1C8")
          //     : am4core.color("#FC9D5A");
          // });
      
          // bullet.adapter.add("stroke", function (stroke, target) {
          //   if (!target.dataItem) {
          //     return stroke;
          //   }
          //   var values = target.dataItem.properties;
      
          //   return values.strokeDasharray === ""
          //     ? am4core.color("#52C1C8")
          //     : am4core.color("#FC9D5A");
          // });
      
      
          // Add cursor
          chartRef.current.cursor = new am4charts.XYCursor();
        //   chartRef.current.cursor.xAxis = dateAxis;
      
         
      
          // Add scrollbar
        //   chartRef.current.scrollbarX = new am4core.Scrollbar();
        //   chartRef.current.scrollbarX.thumb.minWidth = 30;
        //   chartRef.current.scrollbarX.parent = chartRef.current.bottomAxesContainer;
        //   chartRef.current.scrollbarX.startGrip.disabled = false;
        //   chartRef.current.scrollbarX.endGrip.disabled = false;
      
        //   chartRef.current.exporting.menu = new am4core.ExportMenu();
        //   chartRef.current.exporting.menu.align = "right";
        //   chartRef.current.exporting.menu.verticalAlign = "top";
      
        //   return () => {
        //     chartRef.current && chartRef.current.dispose();
        //   };
        // }, [timeframe]);
      
        // Load data into chart
      
      })
      
        return (
          <div
            id={CHART_ID}
            style={{ width: "100%", height: "400px", margin: "50px 0" }}
          >
            {" "}
          </div>
        );
  
}
export default AgevsFraud


