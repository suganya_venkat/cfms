import React from 'react';
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { Typography } from "@material-ui/core";
import FraudRatio from './FraudRatio';
import AgevsFraud from './AgevsFraud';
import IncidentBasedClaims from './IncidentBasedClaims';
import "./Alertmanage.css";

function Alertmanage() {
    return (
      <div>
        <Grid container>
        <Grid item xs = {6}>
        <Paper elevation={3} className="fraudratio">
        <CardActions>
          <Typography variant="h6" color="primary">
           Total fraud claims ratio
          </Typography>
        </CardActions>
        <CardContent>
        <FraudRatio />
        </CardContent>
        </Paper>
        </Grid>
        
       
        <Grid item xs = {6} width ="50px"> 
        <Paper elevation={3} className="a" >
        <CardActions>
          <Typography variant="h6" color="primary">
           Claims based on incident type
          </Typography>
        </CardActions>
        <CardContent>
        <IncidentBasedClaims />
        </CardContent>
        </Paper>
        </Grid>
        </Grid>
        <Paper elevation={3}>
        <CardActions>
          <Typography variant="h6" color="primary">
          Fraud causes vs age distribution
          </Typography>
        </CardActions>
        <CardContent>
        <AgevsFraud/>
        </CardContent>
        </Paper>

        </div>
        
    )
}

export default Alertmanage
