import React, { useRef, useState, useEffect } from "react";
import claimsdata from '../LandingPage/FraudRatio.json';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

am4core.useTheme(am4themes_animated);

const CHART_ID = "fraudratio_chart";

function FraudRatio(props) {
    const chartRef = useRef(null);
    
    const [data,setData]=useState([]);

    const selectedState = props.getState;
    const selectedToRptDate=props.getToRptDate;
    const selectedClaimType=props.getClaimType;
    const selectedClaimAmount=props.getClaimAmount;
    const selectedFromRptDate = props.getFromRptDate;
    
  //    useEffect(() => {
  // fetch('/kpicards').then(res => res.json()).then((data) => setData(data));
  // },[]);

  useEffect(() => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({"reporting_view_filters" :  

      {"CLAIMREPORTDATE" : [{"from":  selectedFromRptDate,  

                          "to": selectedToRptDate}], 

      "STATE" : selectedState,  

      "CLAIM_TYPE": selectedClaimType,  

      "CLAIM_AMT_USD": selectedClaimAmount

    } 

    } )
    };
    fetch('/reporting_overview_claims', requestOptions)
    .then(res => res.json()).then((data) => setData(data));
  },[selectedState,selectedFromRptDate,selectedToRptDate,selectedClaimType,selectedClaimAmount]);
    
    var sample =  data.Fraud_Ratio === undefined ||  data.Fraud_Ratio === null ||  Object.keys(data).length <= 0
?  null : data.Fraud_Ratio.map((a) => {
  a.color = a.index === 'Normal' ? am4core.color("#235789") : am4core.color("#00965f")
  
  
})
    
    useEffect(() => {
        var chart = am4core.create(CHART_ID, am4charts.PieChart);

        // chartRef.current = am4core.create(CHART_ID, am4charts.PieSeries);
    chart.data = Object.keys(data).length > 0 ? data.Fraud_Ratio : "Loading";
       

        let pieSeries = chart.series.push(new am4charts.PieSeries());
         pieSeries.dataFields.category = "index";
        pieSeries.dataFields.value = "target";
      
        pieSeries.tooltip.disabled = false;
        pieSeries.ticks.template.disabled = true;
      
         chart.exporting.menu = new am4core.ExportMenu();
         chart.exporting.menu.align = "right";
         chart.exporting.menu.verticalAlign = "top";
        pieSeries.slices.template.propertyFields.fill = "color";

//         pieSeries.slices.template.stroke = am4core.color("#4a2abb");

//           pieSeries.slices.template.fill = am4core.color("#00965f");
//           return () => {
//            chart &&chart.dispose();
//           };
        
      
        // Load data into chart
    
      })
      
        return (
          <div
            id={CHART_ID}
            style={{ width: "100%", height: "200px", margin: "50px 0" }}
          >
            {" "}
          </div>
        );
  
}


export default FraudRatio
