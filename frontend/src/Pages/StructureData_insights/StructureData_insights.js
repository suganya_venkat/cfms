import React, { useState } from 'react'
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { Typography } from "@material-ui/core";
import ShapeMean from './ShapeMean';
import Lift_positive from './Lift_positive';
import ModelDetailsjs from './ModelDetails';
import Filters from '../LandingPage/Filters';
import './ModelDetails.css';
import NavBar from '../../Components/Navbar_configuration'
import AnalyticsNavBar from '../../Components/AnalyticsNavBar'
import { styled } from '@mui/material/styles';

// import Configuration_navbar from '../../Components/Configuration_navbar'

// import Rule_navbar from '../../Components/Rule_navbar'
import Configuration_navbar from '../../Components/Configuration_navbar';
import NewTheme from '../../Components/newTheme.js';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import InfoIcon from '@mui/icons-material/Info';
import FeatureChart from './FeatureChart';
import '../CommonStyling/Style.css'


// import Filters from './Filters';

const HtmlTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#e6e6fa',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 400,
    height:100,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
    padding:"10px"
  },
}));

const HtmlTooltipshap = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#e6e6fa',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 300,
    height:60,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
    padding:"10px"
  },
}));
function StructureData_insights() {
  const [model_value, setmodel_value] = useState('Model1');

 

  return (
    <div>

      

    
      <AnalyticsNavBar/>
      {/* <CardMedia
     className='progressTopStructured'   
          image={ProgressBarTop}

 /> */}
      {/* <NewTheme/>
      <div className='activeLineRules'></div> <br/> */}
      <Configuration_navbar/>
     <NavBar/>
     <br/>
     {/* <Configuration_navbar/> */}
     <div style={{margin:8}}>
      <p className='st-box'><ModelDetailsjs setmodel_value={setmodel_value} model_value={model_value}/> </p>
      <p className='nd-box'><Filters setmodel_value={setmodel_value} model_value={model_value}/> </p>
      <Grid  container spacing={1}>     <Grid item xs = {6}>
    <Paper elevation={3} className="fraudratio">
    <div  className="infoContainer">
      <p  style={{padding: 10,color:"black",fontWeight:'bold',fontSize:16,marginLeft: 244}}>
      
      Model Lift Chart
      
     
      </p>
      
      <span className="aiRulesInfo">
      <HtmlTooltip  placement="right-start"
        title={
          <React.Fragment>
            <div style={{fontSize: 12 ,height: 59,padding: 10}}>
        <p><i> Lift is the ratio of the number of fraudulent observations in a given Quantile to 
    the expected number of fraudulent observations based on random selection​ </i></p>
     
        </div>
          </React.Fragment>
        }
      >
        <InfoIcon style={{fontSize:10}} className="infoIcon" />
      </HtmlTooltip>
     
     
      </span>
      </div>
  
    {/* <h6 className='noteWrap'>
    Lift is the ratio of the number of fraudulent observations in a given Quantile to 
    the expected number of fraudulent observations based on random selection​      </h6> */}
    <Lift_positive setmodel_value={setmodel_value} model_value={model_value}/>
   
    </Paper>
    </Grid>
    <Grid item xs = {6} > 
    <Paper elevation={3} className="a" >
    <div  className="infoContainer">
    <p  style={{padding: 10,color:"black",fontWeight:'bold',fontSize:16,marginLeft: 244}}>
     Key Fraud Indicators
            </p>
            <span className="aiRulesInfo">
      <HtmlTooltipshap placement="right-start"
        title={
          <React.Fragment>
            <div style={{fontSize: 12 ,height: 59,padding: 10}}>
        <p><i>Shows the relative importance of features in model predictions​</i></p>
     
        </div>
          </React.Fragment>
        }
      >
        <InfoIcon className="infoIcon" />
      </HtmlTooltipshap>
     
     
      </span>
      </div>
  
    
    {/* <h6 className='noteWrap'>
      Shows the relative importance of features in model predictions​      </h6> */}
    <ShapeMean setmodel_value={setmodel_value} model_value={model_value}/>
    
    </Paper>
    </Grid>
    </Grid>
    <br/> 
     {/* <Grid  container spacing={1}>     <Grid item xs = {12}>
    <Paper elevation={3}>
    <div  className="infoContainer">
    <p  style={{padding: 10,color:"black",fontWeight:'bold',fontSize:16,marginLeft: 467}}>
  <center>
Inbuilt AI Model Features - Distribution

</center>
            </p>
  
    </div>
    <FeatureChart/>
    
    </Paper>
    </Grid>
    </Grid> */}
    {/* <Paper elevation={3}>
    <CardActions>
      <Typography variant="h6" color="primary">
      Model Lift Chart with claims data
      </Typography>
    </CardActions>
    <CardContent>
    <Lift setmodel_value={setmodel_value} model_value={model_value}/>
    </CardContent>
    </Paper> */}
</div>
    </div>
    
  )
}

export default StructureData_insights



