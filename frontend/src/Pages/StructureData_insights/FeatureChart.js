import React, { useRef, useState, useEffect } from "react";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import FeatureChartData from './FeatureChart.json';


// import "./linechart_paper.css";

am4core.useTheme(am4themes_animated);

const CHART_ID = "FeatureChart";

// const fetchURL = "http://localhost:5000/insights/actualforecast_chart";

function FeatureChart() {
  const chartRef = useRef(null);
  
  useEffect(() => {
    chartRef.current = am4core.create(CHART_ID, am4charts.XYChart);
    chartRef.current.data = FeatureChartData

    // chartRef.current.padding(60, 0, 60, 0);
    // chartRef.current.numberFormatter.numberFormat = "#.##";

    var categoryAxis = chartRef.current.yAxes.push(new am4charts.CategoryAxis());
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.dataFields.category = "ft_name";
    // categoryAxis.renderer.minGridDistance = 1;
    categoryAxis.renderer.inversed = true;
    categoryAxis.renderer.labels.template.fontSize = 12;
    categoryAxis.renderer.minGridDistance = 20;

categoryAxis.renderer.cellStartLocation = 0;
categoryAxis.renderer.cellEndLocation = 1;
categoryAxis.title.text = "Features";
categoryAxis.title.fontSize = 12;


    
    var valueAxis = chartRef.current.xAxes.push(new am4charts.ValueAxis());
    valueAxis.min = 0;
     valueAxis.renderer.grid.template.disabled = true;
   valueAxis.renderer.labels.template.fontSize = 12;
   valueAxis.title.text = "Distribution of 1 ratio";
   valueAxis.title.fontSize = 12;


    var series = chartRef.current.series.push(new am4charts.ColumnSeries());
    series.dataFields.categoryY = "ft_name";
    series.dataFields.valueX = "count_1_per";
    
    series.columns.template.strokeOpacity = 0;
    series.columns.template.fill = am4core.color("#0a54c3"); // fill #9400d3 00965f
    series.columns.template.tooltipText = "[#fff font-size: 15px] {categoryY}:\n[/][#fff font-size: 15px]{valueX}%[/] [#fff]{additional}[/]";
    // series.columns.template.width = am4core.percent(100);
    // series.calculatePercent = true;
    // series.dataFields.valueYShow = "changePercent";

    // series.columns.template.tooltipText = "[font-size:14px] {valueX}";

    
    // var labelBullet = series.bullets.push(new am4charts.LabelBullet())
     // Add label
     var labelBullet = series.bullets.push(new am4charts.LabelBullet())
     labelBullet.label.horizontalCenter = "right";
     labelBullet.label.text = "{valueX}%";
     labelBullet.label.textAlign = "end";
     labelBullet.label.dx = -10;
     labelBullet.label.fontSize=10
     labelBullet.label.fill=am4core.color("white")
     labelBullet.label.hideOversized = true;

     chartRef.current.zoomOutButton.disabled = true;
   
    
   categoryAxis.sortBySeries = series;
    chartRef.current.exporting.menu = new am4core.ExportMenu();
          chartRef.current.exporting.menu.align = "right";
          chartRef.current.exporting.menu.verticalAlign = "top";

})
  return (
    <div
      id={CHART_ID}
      style={{ width: "800px", height: "1000px",marginLeft:30 }}
    >
      {" "}
    </div>
  );
}

export default FeatureChart;
