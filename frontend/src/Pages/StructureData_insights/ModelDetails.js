import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import ProductionModelDetails from './ProductionModelDetails.json';
import Model2Details from './Model2Details.json';
import Model3Details from './Model3Details.json';
import  './ModelDetails.css'
import Filters from '../LandingPage/Filters';



const bull = (
  <Box
    component="span"
    sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
  >
    
  </Box>
);

export default function BasicCard(props) {
    // console.log(ProductionModelDetails[0].model_type);
    const model_type = props.model_value;

  return (
     
//     <Card sx={{ minWidth: 275,height: 93 }}>
//     <CardContent>
//       <ul style={{marginLeft:12,fontSize:11}}>
// <li>Claimant Name - {value.claimant_name}</li>
// <li>  { value.document === "Hospital Admission - Inpatient" ? "Admission Date" : value.document === "Police Report" ? "Report Date" : value.document === "Driver License" ?  "Expiry Date" : ""}- {dateFormat(value.date, "yyyy-mm-dd")}</li>
// { value.document === "Hospital Admission - Inpatient" ?
// <li>  Discharge Date - {dateFormat(value.discharge_date, "yyyy-mm-dd")}</li> : ""}
// <li> { value.document === "Hospital Admission - Inpatient" ? "Provider Address" : value.document === "Police Report" ? "Police Report Address" : value.document === "Driver License" ?  "License Address" : ""} - {value.address}</li>
// </ul>
// </CardContent>
// </Card>

    <Card sx={{ Width: 15}}>
      <CardContent>
      <Typography sx={{ fontSize: 14, fontWeight:"bold" }} color="text.secondary" gutterBottom>  
      Model Type: {model_type === "Model1" ? ProductionModelDetails[0].model_type :
       model_type === "Model2" ? Model2Details[0].model_type : Model3Details[0].model_type  }
      {/* {model_type === "Model1" ? ProductionModelDetails[0].model_number :
       model_type === "Model2" ? Model2Details[0].model_number : Model3Details[0].model_number  }  */}
       {/* Model Type: {model_type === "Model1" ? ProductionModelDetails[0].model_type :
       model_type === "Model2" ? Model2Details[0].model_type : Model3Details[0].model_type  }

       <br/>Tree Depth: {model_type === "Model1" ? ProductionModelDetails[0].max_depth :
       model_type === "Model2" ? Model2Details[0].max_depth : Model3Details[0].max_depth  }  Learning Rate: {model_type === "Model1" ? ProductionModelDetails[0].learning_rate:
       model_type === "Model2" ? Model2Details[0].learning_rate: Model3Details[0].learning_rate } n_estimators: {model_type === "Model1" ? ProductionModelDetails[0].n_estimates:
       model_type === "Model2" ? Model2Details[0].n_estimates: Model3Details[0].n_estimates } Model Lift: {model_type === "Model1" ? ProductionModelDetails[0].model_lift:
       model_type === "Model2" ? Model2Details[0].model_lift: Model3Details[0].model_lift }   */}
       
        </Typography>
      <ul style={{marginLeft:12,fontSize:11}}>

<li>Tree Depth:  <b>{model_type === "Model1" ? ProductionModelDetails[0].max_depth :
       model_type === "Model2" ? Model2Details[0].max_depth : Model3Details[0].max_depth  } </b></li>

<li> Learning Rate: <b>{model_type === "Model1" ? ProductionModelDetails[0].learning_rate:
       model_type === "Model2" ? Model2Details[0].learning_rate: Model3Details[0].learning_rate }</b></li> 
<li>n_estimators: <b> {model_type === "Model1" ? ProductionModelDetails[0].n_estimates:
       model_type === "Model2" ? Model2Details[0].n_estimates: Model3Details[0].n_estimates } </b></li>
       <li> Model Lift:<b> {model_type === "Model1" ? ProductionModelDetails[0].model_lift:
       model_type === "Model2" ? Model2Details[0].model_lift: Model3Details[0].model_lift } </b> </li>
</ul>
        
       
      </CardContent>
      
    </Card>
    
  );
}
