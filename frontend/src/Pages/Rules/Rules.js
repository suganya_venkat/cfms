import React from 'react'

import Configuration_navbar from '../../Components/Configuration_navbar'
import MainNavBar from '../../Components/MainNavBar'
import CreateRule from './CreateRule'
import './Rules.css'

function Rules() {
    return (
        <div>
            <MainNavBar/>
     <Configuration_navbar/>
     <CreateRule/>
     </div>
    )
}

export default Rules
