import React, { useRef, useState, useEffect } from "react";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import Paper from "@material-ui/core/Paper";
import data from "./AnamolyPageData.json"

// import "./linechart_paper.css";

am4core.useTheme(am4themes_animated);

const CHART_ID = "lift_positive_anamoly";

// const fetchURL = "http://localhost:5000/insights/actualforecast_chart";

function LiftChart(props) {
  const chartRef = useRef(null);
  const [items, setItems] = useState([]);

//   const model_type = props.model_value;
//   console.log(model_type)

  useEffect(() => {
    chartRef.current = am4core.create(CHART_ID, am4charts.XYChart);
    chartRef.current.data = data["Lift Chart"]

    chartRef.current.colors.step = 2;
    chartRef.current.legend = new am4charts.Legend()
// chartRef.current.legend.position = 'top'
// chartRef.current.legend.paddingBottom = 20
 chartRef.current.legend.fontSize = 12
    //* Create axes */
var categoryAxis = chartRef.current.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "index";
categoryAxis.renderer.minGridDistance = 10;
categoryAxis.renderer.labels.template.fontSize = 12;
categoryAxis.title.text = "QT";
categoryAxis.title.fontSize = 12;


/* Create value axis */
var valueAxis = chartRef.current.yAxes.push(new am4charts.ValueAxis());
valueAxis.renderer.labels.template.fontSize = 12;
valueAxis.title.text = "Suspicious/Claim count";
valueAxis.title.fontSize = 12;

/* Create series */
var columnSeries = chartRef.current.series.push(new am4charts.ColumnSeries());
columnSeries.name = "Suspicious";
columnSeries.dataFields.valueY = "Positives";
columnSeries.dataFields.categoryX = "index";

// var label = chartRef.current.createChild(am4core.Label);
// label.text = "Hello world!";
// label.fontSize = 20;
// label.align = "center";
// label.isMeasured = false;
// label.x = 70;
// label.y = 20;

columnSeries.columns.template.tooltipText = "[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]"
columnSeries.columns.template.propertyFields.fillOpacity = "fillOpacity";
columnSeries.columns.template.propertyFields.stroke = "stroke";
columnSeries.columns.template.propertyFields.strokeWidth = "strokeWidth";
columnSeries.columns.template.propertyFields.strokeDasharray = "columnDash";
columnSeries.tooltip.label.textAlign = "middle";
columnSeries.columns.template.fill = am4core.color("#0a54c3"); // fill

var lineSeries = chartRef.current.series.push(new am4charts.LineSeries());
lineSeries.name = "lift";
lineSeries.dataFields.valueY = "lift";
lineSeries.dataFields.categoryX = "index";

lineSeries.stroke = am4core.color("#7e50ae");
lineSeries.strokeWidth = 3;
lineSeries.propertyFields.strokeDasharray = "lineDash";
lineSeries.tooltip.label.textAlign = "middle";

var bullet = lineSeries.bullets.push(new am4charts.Bullet());
bullet.fill = am4core.color("#7e50ae"); // tooltips grab fill from parent by default
bullet.tooltipText = "[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]"
var circle = bullet.createChild(am4core.Circle);
circle.radius = 4;
circle.fill = am4core.color("#fff");
circle.strokeWidth = 3;

// chartRef.current.data = data;

    chartRef.current.exporting.menu = new am4core.ExportMenu();
    chartRef.current.exporting.menu.align = "right";
    chartRef.current.exporting.menu.verticalAlign = "top";
    return () => {
      chartRef.current && chartRef.current.dispose();
    };
  });

  
  return (
    <div
      id={CHART_ID}
      style={{ width: "100%", height: "300px" }}
    >
      {" "}
    </div>
  );
}

export default LiftChart;
