import React, { useState } from 'react';
import { makeStyles } from "@material-ui/core/styles";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import { Typography } from "@material-ui/core";
import ShapeFeatureImportance from './ShapeFeatureImportance';
import LiftChart from './LiftChart';
import AnamolyCharts from '../../assets/images/AnamolyCharts.png'
import CardMedia from "@material-ui/core/CardMedia"
import "./AnamolyPage.css"
import Grid from "@material-ui/core/Grid";
import NewTheme from '../../Components/newTheme.js';
import ProgressBarTop from '../../assets/images/ProgressBarTop.PNG'
import AnalyticsNavBar from '../../Components/AnalyticsNavBar'
import NavBar from '../../Components/Navbar_configuration'
import MainNavBar from '../../Components/MainNavBar';
import Configuration_navbar from '../../Components/Configuration_navbar';
import { styled } from '@mui/material/styles';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import InfoIcon from '@mui/icons-material/Info';
import '../CommonStyling/Style.css'

const HtmlTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#e6e6fa',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 400,
    height:100,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
    padding:"10px"
  },
}));

const HtmlTooltipshap = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#e6e6fa',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 300,
    height:60,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
    padding:"10px"
  },
}));

function AnamolyPage() {

  
  const useStyles = makeStyles(theme => ({
   
    cardMediaStyle: {
      paddingTop: "40%",
      // width:'250px'
    }
  }));
  const styles = useStyles();

  return (
  
    <div>
     {/* className='divalign' */}
         <AnalyticsNavBar/>
         {/* <CardMedia
     className='progressTopStructured'   
          image={ProgressBarTop}

 /> */}
      {/* <NewTheme/>
      <div className='activeLineRules'></div> <br/> */}
      <Configuration_navbar/>
  
            <NavBar/>
            <br/>
    
      {/* <div>
        <br/>
      <Filters/>
      </div>
      <br/> */}
      <div style={{margin:8}}>
      <Grid  container spacing={1}>
      <Grid item xs = {6} > 
        <Paper elevation={3} className="a" >
     
        <div  className="infoContainer">
         <p  style={{padding: 10,color:"black",fontWeight:'bold',fontSize:16,marginLeft: 244}}>
         Model Lift Chart
          </p>
          <span className="anamolyInfo">
      <HtmlTooltip  placement="right-start"
        title={
          <React.Fragment>
            <div style={{fontSize: 12 ,height: 59,padding: 10}}>
        <p><i> Lift is the ratio of the number of fraudulent observations in a given Quantile to 
    the expected number of fraudulent observations based on random selection​ </i></p>
     
        </div>
          </React.Fragment>
        }
      >
        <InfoIcon className="infoIcon" />
      </HtmlTooltip>
     
     
      </span>
      </div>
       
        <LiftChart/>
       
        </Paper>
        </Grid>
       
      
       
        <Grid item xs = {6} > 
        <Paper elevation={3} className="a" >
        <div  className="infoContainer">
        <p  style={{padding: 10,color:"black",fontWeight:'bold',fontSize:16,marginLeft: 244}}>
          Key Fraud Indicators
          </p>
          <span className="anamolyInfo">
      <HtmlTooltipshap  placement="right-start"
        title={
          <React.Fragment>
            <div style={{fontSize: 12 ,height: 59,padding: 10}}>
        <p><i>   Shows the relative importance of features in model predictions​ </i></p>
     
        </div>
          </React.Fragment>
        }
      >
        <InfoIcon className="infoIcon" />
      </HtmlTooltipshap>
     
     
      </span>
      </div>
   
        {/* <h6 className='noteWrap'>
      Shows the relative importance of features in model predictions​      </h6> */}
        <ShapeFeatureImportance/>
       
        </Paper>
        </Grid>
       
        </Grid>

        {/* <Grid  > 
        <Paper elevation={3} className="a" >
        <CardActions>
          <Typography variant="h6" color="primary">
        
          </Typography>
        </CardActions>
        <CardContent>
        <CardMedia
           className={styles.cardMediaStyle}    
              image={AnamolyCharts}

/>
        </CardContent>
        </Paper>
        </Grid> */}
      </div>
      </div>
    
  )
}

export default AnamolyPage
