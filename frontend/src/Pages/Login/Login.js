import React, { useState } from "react"
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import { InputAdornment } from "@material-ui/core";
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import LockIcon from '@mui/icons-material/Lock';
import Navbar_landingpage from '../../Components/NavBar_landingpage';
// import InputAdornment from '@mui/icons-material/Lock';

// import loginBackground from '../assets/images/loginBackground.png'
// import nokiaBlue from '../assets/images/nokiaBlue.png'

import './Login.css'

function Login() {
    const [validated, setValidated] = useState(false);

    const handleSubmit = (event) => {
        console.log("Handling onSubmit Event with-->>", event)
        setValidated(true);
    };

const login = () =>{
    window.location.href = '/Summary';
}


    return(
      <div>
       <Navbar_landingpage/>
        <div className="cardcenter">
        <Paper className="papercontent">
        <CardActions className="cardactions" >
        <div style={{ display : 'flex', flexDirection : 'column',
        minWidth : 265,
        maxWidth : 300,
        marginBottom: 50,
        marginLeft:10
    }}>
          <TextField id="Value" label="Enter Value"
              variant="standard"
             
            autoComplete="off"
              label="Username"
              inputProps={{ startAdornment: <InputAdornment position="start"><AccountCircleIcon/></InputAdornment>}}

            fullWidth
              // autoFocus
            //   value={value}
              // onChange={(event) => {setPassword(event.target.value)}} //whenever the text field change, you save the value in state
            //   onChange={valueSelected}
              />
<br/>
<TextField id="Value" label="Enter Value" 
              variant="standard"
              autoComplete="off"

              inputProps={{ startAdornment: <InputAdornment position="start"><LockIcon/></InputAdornment>}}
              label="Password"
              type = "Password"
             fullWidth
              // autoFocus
            //   value={value}
              // onChange={(event) => {setPassword(event.target.value)}} //whenever the text field change, you save the value in state
            //   onChange={valueSelected}
              />

<br/><br/><br/>
<Button   style={{
      
        backgroundColor: "#00965f",
        color:"white"
    }}  onClick={login}>Login</Button>

       </div>
        </CardActions>
      
        </Paper>
        </div>
        </div>
    );
}

export default Login;