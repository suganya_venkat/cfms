
import React, { useRef, useState, useEffect } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import data from './ClaimsDetails.json';
import Button from '@mui/material/Button';
import Alert from '@mui/material/Alert';
import NexusView from './NexusView'
import dateFormat from "dateformat";
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';


import { makeStyles, TextField } from "@material-ui/core";

import { Divider, Avatar, Grid } from "@material-ui/core";

import './ClaimsDetails.css'
import Model_inferance from "./Model_inferance";
import Documents from "./Document";

const imgLink =
  "https://images.pexels.com/photos/1681010/pexels-photo-1681010.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260";

  var claim_number_selected = {};
var newComment = {};

 const useStyles = makeStyles(() => ({
    // textField: {
    //   width: "90%",
    //   marginLeft: "auto",
    //   marginRight: "auto",
    //   paddingBottom: 0,
    //   marginTop: 0,
    //   fontWeight: 500
    // },
    input: {
      color: "white"
    }
  }));



// console.log(data['claim specific details'])

export default function ClaimsDetailsPage(props) {

  const claim_data = props.claimDetails;
  const valueSel = props.value;
  const[claimDetails,setclaimDetails] = React.useState(claim_data)
  const[valueSelected,setValueSelected] = React.useState(valueSel)

  // console.log(valueSel)
     const [newdata,newsetData]=useState([]);
     const [Comment, setComment] = useState("");


claim_number_selected = {"claim_number" : claim_data}

  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(claim_number_selected),
  };

  useEffect(() => {
  fetch('/claim_level_data', requestOptions).then((res) => res.json()).then((newdata) => newsetData(newdata)
                                
  )},[Comment]);
//  console.log(claimDetails)       
  //    useEffect(() => {
  // fetch('/claim_level_data').then(res => res.json()).then((newdata) => newsetData(newdata));
  // },[]);
    
    //  console.log(newdata['comments'])
    
     const classes = useStyles();

  const [save, setSave] =  React.useState("");

  



const getComment = () =>{
  // console.log(Comment)
  newComment = {"user_id": 1, "claim_number": claim_data, "comment": Comment}
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(newComment),
  };

  fetch('/add_comments', requestOptions).then((res) =>
    setSave(res.status),
    //  setbuttonDisabled(true)   
                                     
  );
  setComment("")
}

// console.log(newComment)
// console.log(save)
    
  return (
      <div style={{ marginLeft: -24, marginRight: -46}}>
        {/* style={{ marginLeft: -24, marginRight: -46 }} */}
    <TableContainer component={Paper} style={{marginTop:-38}}>
      <Table sx={{ minWidth: 1124 }} aria-label="simple table">
      {/* {data['claim specific details'].map((row) => ( */}
      {newdata === undefined || newdata === null || Object.keys(newdata).length <= 0 ? "loading":
        <TableHead>
     
          <TableRow>
            <TableCell  align="center"><b>Claim Number</b><br/> <div>{newdata['claim specific details'].claim_number}</div></TableCell>
            <TableCell align="center"><b>Reported Date</b> <div>{dateFormat(newdata['claim specific details'].CLAIMREPORTDATE, "dd-mm-yyyy")}</div></TableCell>
            <TableCell align="center"><b>Date Of Loss</b> <div>{dateFormat(newdata['claim specific details'].DATEOFLOSS, "dd-mm-yyyy")}</div></TableCell>
            
          </TableRow>
          <TableRow>
            <TableCell align="center"><b>Claim Type Description</b> <div>{newdata['claim specific details'].CLAIMTYPEDESC}</div></TableCell>
            <TableCell align="center" style={{inlineSize: 393,overflowWrap:"brealWrap"}}><b>Claimant Address</b> <div>{newdata['claim specific details'].address}</div></TableCell>
            <TableCell  align="center"><b>Pin Code</b> <br/> <div>{newdata['claim specific details'].pin_code}</div></TableCell>
            
          </TableRow>

        </TableHead>
}
          {/* ))} */}
        {/* <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.calories}</TableCell>
              <TableCell align="right">{row.fat}</TableCell>
              <TableCell align="right">{row.carbs}</TableCell>
              <TableCell align="right">{row.protein}</TableCell>
            </TableRow>
          ))}
        </TableBody> */}
      </Table>
    </TableContainer>
    <br/>
    {valueSel === 0 ?
     <div>
  
      <Grid container alignItems="center" spacing={1}>
            <Grid item sm={3} style= {{ marginLeft: 10 }}>
            
              <Paper style={{width: 391}} >
              <div style={{display:"flex"}}>
            <p style={{margin:14}}>Select Investigation Action</p>
                {/* style={{backgroundColor: "rgb(195 244 210)"}} */}
    <Box sx style= {{ marginLeft: 3,height: 61,flex:1 ,marginTop: -20}}>
      <FormControl>
      
        <InputLabel style={{fontSize:12,fontWeight:"bold"}} id="Investigation Status"></InputLabel>
        <Select style={{fontsize:10}}
          labelId="Investigation Status"
          id="Investigation Status"
          displayEmpty
          value={props.model_value}
          label="Investigation Status"
          // onChange={handleChange}
        >
        
          {/* <MenuItem value=""><Put any default Value which you want to show/></MenuItem> */}
          <MenuItem value={"Assigned Internally"}>Assigned Internally</MenuItem>
          <MenuItem value={"Not Taken up for Investigation"}>Not Taken up for Investigation</MenuItem>
          <MenuItem value={"Assigned Externally"}>Assigned Externally</MenuItem>
          <MenuItem value={"Already Identified by SIU"}>Already Identified by SIU</MenuItem>

        </Select>
      </FormControl>
    </Box>
    </div>
    </Paper>
    
    </Grid>
    </Grid>
    
    
    <br/>
    <div  className="scroll">
     
     {newdata['comments'] === null || newdata['comments'] === undefined || newdata['comments'].length <=0 ? <center> no comments</center> :
     newdata['comments'].map((row) => (
     <Paper style={{ padding: "40px 20px" }}>
       <Grid container wrap="nowrap" spacing={2}>
         <Grid item>
           <Avatar />
         </Grid>
         <Grid justifyContent="left" item xs zeroMinWidth>
           <h4 style={{ margin: 0, textAlign: "left" }}>{row.user_name}</h4>
           <p style={{ textAlign: "left" }}>
           {row.comment}.{" "}
           </p>
           <p style={{ textAlign: "left", color: "gray" }}>
           {row.insert_ts}
           </p>
         </Grid>
       </Grid>
       {/* <Divider variant="fullWidth" style={{ margin: "30px 0" }} />
       <Grid container wrap="nowrap" spacing={2}>
         <Grid item>
           <Avatar alt="Remy Sharp" src={imgLink} />
         </Grid>
         <Grid justifyContent="left" item xs zeroMinWidth>
           <h4 style={{ margin: 0, textAlign: "left" }}>Michel Michel</h4>
           <p style={{ textAlign: "left" }}>
             Lorem ipsum dolor sit amet, consectetur adipiscing elit. .{" "}
           </p>
           <p style={{ textAlign: "left", color: "gray" }}>
             posted 1 minute ago
           </p>
         </Grid>
       </Grid> */}
     </Paper>
     ))}
     </div>

  
  <h2 style={{marginLeft: 13}}>Add Comments</h2>
  <div style={{display:"flex"}}>
  
  <Box
    
      style={{marginLeft: 10}}
    >
  <TextField
  // fullWidth
  className="d"
  style={{ width: "100%" }}
         id="filled-multiline-static"
          // label="Add Comment"
         multiline
           rows={1}
        //  defaultValue="Default Value"
        //  variant="filled"
        // id="Comment"
        // label="Add Comments"
         variant = "outlined"
        // placeholder="Add Comment"
        // multiline
        // className={classes.textField}
        value={Comment}
        // rows = {3}
        // maxRows={10}
        onChange={(e) => setComment(e.target.value)}
        
        
        ></TextField>
        
        </Box>
      
        <div style={{flex:1,
    marginLeft: 21,marginTop:18,borderRadius:2}}>
        <Button onClick={getComment} className="commentbutton" variant="outlined">Add </Button>
        {save === 200 ? <Alert>Comment Inserted</Alert>
    : ""}
                      {save != 200 && save ? <Alert severity="error">Something Wrong!</Alert>
    : ""}
    </div>

  </div>
      
   <br/>
    
      
        </div>
: valueSel === 2 ?
<NexusView setclaimDetails={setclaimDetails} claimDetails={claimDetails} setValueSelected={setValueSelected} valueSelected={valueSelected}/>
: valueSel === 1 ?
<Model_inferance setclaimDetails={setclaimDetails} claimDetails={claimDetails} setValueSelected={setValueSelected} valueSelected={valueSelected}/>
: valueSel === 3 ?
<Documents setclaimDetails={setclaimDetails} claimDetails={claimDetails} setValueSelected={setValueSelected} valueSelected={valueSelected}/>
:""

}
       </div>
  );
}
