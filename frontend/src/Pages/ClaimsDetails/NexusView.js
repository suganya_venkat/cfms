import React, { useRef, useState, useEffect } from "react";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import * as am4plugins_forceDirected from "@amcharts/amcharts4/plugins/forceDirected"; 
import CircleIcon from '@mui/icons-material/Circle';
import Grid from "@material-ui/core/Grid";
import RadioButtonCheckedIcon from '@mui/icons-material/RadioButtonChecked';
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import Card from '@mui/material/Card';

// import claimsdata from './KpiCards_histogramData.json';

// import "./linechart_paper.css";

am4core.useTheme(am4themes_animated);

const CHART_ID = "claimsChart2";
var claim_number_selected = {};

// const fetchURL = "http://localhost:5000/insights/actualforecast_chart";

export default function NexusView(props) {
    const chartRef = useRef(null);
    const claim_data = props.claimDetails;
    const [newdata,newsetData]=useState([]);

    claim_number_selected = {"claim_number" : claim_data}

    useEffect(() => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(claim_number_selected),
  };

  
  fetch('/graph_view', requestOptions).then((res) => res.json()).then((newdata) => newsetData(newdata)
                                
  )},[]);
 
    //  console.log(newdata);
  useEffect(() => {
    chartRef.current = am4core.create(CHART_ID, am4plugins_forceDirected.ForceDirectedTree);
    // var chart = am4core.create("chartdiv", am4plugins_forceDirected.ForceDirectedTree);
    // chartRef.current.legend = new am4charts.Legend();
    // chartRef.current.data = Object.keys(data).length > 0 ? data.Claims_by_state_Histogram : "Loading";
    var networkSeries = chartRef.current.series.push(new am4plugins_forceDirected.ForceDirectedSeries())
    // networkSeries.data = [{"id": "V4302239", "name": "claim_number", "value": "10", "children": [{"id": "06d316b628d393212d6eff66474f6061", "name": "Claimant", "value": "1"}, {"id": "da86d620ba7ec3e0378984af83bfb10e", "name": "Witness", "value": "1"}, {"id": "77c55c7858a5fb12193101df6f39cffd", "name": "Lawyer", "value": "1"}, {"id": "b6ade9841df601711c0e9099c2273241", "name": "Doctor", "value": "1"}, {"id": "9c250dcdbb40351c91835c8a8c15dfd3", "name": "Hospital", "value": "1"}]}, {"id": "V4302030", "name": "claim_number", "value": "10", "link": ["9c250dcdbb40351c91835c8a8c15dfd3"], "children": [{"id": "34b8287f608326e6595673c4c5c824c4", "name": "Claimant", "value": "1"}, {"id": "a302920e503d387d93f3641ffb83cfd2", "name": "Doctor", "value": "1"}, {"id": "c88622260fbd3981dd3ac90258a875c0", "name": "Witness", "value": "1"}, {"id": "631f64f3df6b88b2e9346b5d19f4932e", "name": "Lawyer", "value": "1"}, {"id": "9c250dcdbb40351c91835c8a8c15dfd3", "name": "Hospital", "value": "1"}]}, {"id": "V4296635", "name": "claim_number", "value": "10", "link": ["06d316b628d393212d6eff66474f6061"], "children": [{"id": "4e9ae53059cd70692e8e85d1a17abef0", "name": "Hospital", "value": "1"}, {"id": "22c383cfbba21a8a9b89d72218635bbe", "name": "Lawyer", "value": "1"}, {"id": "dbcbd2b4b21aef48e83cc649da327853", "name": "Witness", "value": "1"}, {"id": "fd38f60194468776e44811a651c83edd", "name": "Doctor", "value": "1"}]}, {"id": "V4304839", "name": "claim_number", "value": "10", "link": ["06d316b628d393212d6eff66474f6061", "9c250dcdbb40351c91835c8a8c15dfd3"], "children": [{"id": "b45b28bea954c9d5f901f6da3a165c1b", "name": "Witness", "value": "1"}, {"id": "bed23720b7155c5e26cd7a9247b4949a", "name": "Doctor", "value": "1"}, {"id": "21de472bca804b0915b8a94a8b200d98", "name": "Lawyer", "value": "1"}]}];
    networkSeries.data = newdata
// [{
//       name: "claim_number",
//       collapsed:true,
//       id: "V4305874",
// info_color: "blue",
// info_frd_rate: "",
// name: "claim_number",
// value: "10",
//       children: [{
//         id: 'b18636488fd952f0f0a4aeb61ff39bd4', name: "Claimant",value:1,info_name: "Azmi Tang",collapsed: true
//       }, {
//         id: 'b18636488fd952f0f0a4aeb61ff39bd4', name: "Claimant",value:1,info_name: "Azmi Tang",collapsed: true
//       },
//       {
//         id: 'b18636488fd952f0f0a4aeb61ff39bd4', name: "Claimant",value:1,info_name: "Azmi Tang",collapsed: true
//       }
//     ]
    
//     }];
// networkSeries.dataFields.linkWith = "link";
// networkSeries.dataFields.name = "name";
// networkSeries.dataFields.id = "id";
// networkSeries.dataFields.value = "value";

networkSeries.dataFields.value = "value";
networkSeries.dataFields.name = "name";
networkSeries.dataFields.children = "children";
networkSeries.dataFields.id = "id";
// networkSeries.dataFields.linkWith = "link";
networkSeries.dataFields.color = "info_color"
networkSeries.dataFields.collapsed = "collapsed"
// networkSeries.dataFields.children = "children";

// networkSeries.nodes.template.tooltipText = "{name}";
networkSeries.nodes.template.fillOpacity = 1;

networkSeries.nodes.template.label.text = "{name}"
networkSeries.fontSize = 8;
networkSeries.maxRadius = am4core.percent(6);
networkSeries.manyBodyStrength = -16;
networkSeries.nodes.template.label.hideOversized = true;
networkSeries.nodes.template.label.truncate = true;

networkSeries.nodes.template.label.text = "{name}"; 
// chartRef.current.legend = new am4charts.Legend();

// networkSeries.nodes.template.tooltipText = "{name}: {id}\n info_name: {info_name}\n Fraud Rate: {info_frd_rate}" ; 

networkSeries.nodes.template.adapter.add("tooltipText", function(text, target) {
  if (target.dataItem) {
    switch(target.dataItem.level) {
      case 0:
        return target.dataItem.color === 'grey' ? "id: {id}" :"{id}";
      case 1:
        return  target.dataItem.name  === 'claim_number'? "Claim Number : {id}\n   Report Date : {info_report_dt} \n Loss Date: {info_loss_dt} \n Claimed amount : {info_claimed_amt}\n Claim Type: {info_clm_type}" 
         :  "Entity Name: {info_name}\n Fraud/Suspicious Score: {info_frd_rate}";
      case 2:
        return target.dataItem.name  === 'claim_number'? "Claim Number : {id}\n   Report Date : {info_report_dt} \n Loss Date: {info_loss_dt} \n Claimed amount : {info_claimed_amt}\n Claim Type: {info_clm_type}"
         :  "Entity Name: {info_name}\n Fraud/Suspicious Score: {info_frd_rate}"
        case 3:
        return target.dataItem.name  === 'claim_number'? "Claim Number : {id}\n   Report Date : {info_report_dt} \n Loss Date: {info_loss_dt} \n Claimed amount : {info_claimed_amt}\n Claim Type: {info_clm_type}"
         :  "Entity Name: {info_name}\n Fraud/Suspicious Score: {info_frd_rate}"
        case 4:
        return target.dataItem.name  === 'claim_number'? "Claim Number : {id}\n   Report Date : {info_report_dt} \n Loss Date: {info_loss_dt} \n Claimed amount : {info_claimed_amt}\n Claim Type: {info_clm_type}" 
        : "Entity Name: {info_name}\n Fraud/Suspicious Score: {info_frd_rate}"
        case 5:
        return target.dataItem.name  === 'claim_number'? "Claim Number : {id}\n   Report Date : {info_report_dt} \n Loss Date: {info_loss_dt} \n Claimed amount : {info_claimed_amt}\n Claim Type: {info_clm_type}" 
        : "Entity Name: {info_name}\n Fraud/Suspicious Score: {info_frd_rate}"
    }
  }
  return text;
});

networkSeries.fontSize = 10; 

networkSeries.minRadius = 22; 

networkSeries.maxRadius = 45; 

networkSeries.centerStrength = 0.5; 


// Set link width
networkSeries.links.template.propertyFields.strokeWidth = "linkWidth";
 

// Start collapsed
networkSeries.maxLevels = 1;

// Expand single level only
networkSeries.nodes.template.expandAll = false;
networkSeries.manyBodyStrength = -30;
networkSeries.links.template.distance = 0.5;
networkSeries.links.template.strength = 1;


// Close other nodes when one is opened
networkSeries.nodes.template.events.on("hit", function(ev) {
    var targetNode = ev.target;
    if (targetNode.isActive) {
      networkSeries.nodes.each(function(node) {
        if (targetNode !== node && node.isActive && targetNode.dataItem.level == node.dataItem.level) {
          node.isActive = false;
        }
      });
    }
  });
 
});
   

  return (
    <div>
      <div >
        <Grid container alignItems="center" style={{fontSize:14,marginLeft:8}} >
        <Grid item xs={12} sm={2}>
            <RadioButtonCheckedIcon  style={{fontSize:12}}/>  <span>Drill Down Node</span> 
            </Grid>
            {/* <Grid item xs={12} sm={2} style={{marginLeft: 35}}>
            <CircleIcon className="circle" style={{color:"white"}}/>  <span>No Connectives</span> 
            </Grid> */}
            <Grid item xs={12} sm={2}>
            <CircleIcon className="circle" style={{color:"#1e90ff"}}/> <span>Selected Claim</span> 
            </Grid>
            <Grid item xs={12} sm={2}>
            <CircleIcon className="circle" style={{color:"Orange"}}/>  <span>Suspicious</span> 
            </Grid>
            <Grid item xs={12} sm={2} style={{marginLeft: -10}}>
            <CircleIcon className="circle" style={{color:"#ff4040"}}/>  <span>Fraud</span> 
            </Grid>
            <Grid item xs={12} sm={2} style={{marginLeft: -62}}>
            <CircleIcon className="circle" style={{color:"#00965F"}}/>  <span>Non Fraud</span> 
            </Grid>
          
            </Grid>
 
  {/* <ul style={{listStyleType: "none", fontSize:14,   lineHeight: "2.5em",float:"left"}}>
    <li>
    <CircleIcon className="circle" style={{color:"#1e90ff"}}/> <span>Selected Claim</span> 
    </li>
    <li>
    <CircleIcon className="circle" style={{color:"#ff4040"}}/>  <span>Fraud</span>
    </li>
    <li>
    <CircleIcon className="circle" style={{color:"#00965F"}}/>  <span>Non Fraud</span> 
    </li>
  </ul> */}
    
    
    
    </div>
    <div style={{display:"flex"}}>
    <div  style={{flex:1,marginLeft:10,marginTop:70}}>
    
 
      
      
 
 
<br/>
  <h4 style={{backgroundColor:"Gainsboro",padding: 4,
    height: 26}}><center>Optimal path for investigation</center></h4>
 
  {/* <Box sx={{ width: '100%' }}> */}
      {/* <Stack>
        <Item> */}
     
        <Card >
      <CardContent>
        <ul className="c" style={{fontSize:12,lineHeight:1.5,marginLeft:10}}>
      
        <li> Investigate the Bodyshop Claiming Patterns, historically its linked to fradulent claims </li>
        
  <li>Investigate the Surveyor, historically have links to fradulent claims</li> 
  
  
  
  
 
        <li>  Investigate prior claim, its past & current patterns are suspicious</li> 
  
</ul>
</CardContent>
</Card>

        
       
            
           
          
 
       
      
      
       
    </div>
    <div
      id={CHART_ID}
      style={{  height: "350px",flex:"1 1 45%" }}
    >
      {" "}
    </div>
   
    </div>
   
    </div>
  );
}

