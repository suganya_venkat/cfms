import React, { useRef, useState, useEffect } from "react";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import MuiTableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Grid from "@material-ui/core/Grid";
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';
import { Typography } from "@material-ui/core";

const TableCell = withStyles({
    root: {
      borderBottom: "none"
    }
  })(MuiTableCell);

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      border: `1px solid #EEF3F5`,
      borderRadius: 4,
      color: "#949FAF",
      minWidth: 275,
      minHeight: 70,
      marginTop: 12,
      marginLeft: 12,
      marginRight: 12,
    },
    paper: {
      padding: theme.spacing(1),
      textAlign: "center",
      color: theme.palette.text.secondary,
      paddingBlock: 5,
      borderRadius: 5,
    },
    card: {
      color: "#16448F",
      fontWeight: 700,
      display: "inline-block"
    },
    cardsmall:{
      color: '#5DC9A2',
      fomtWeight: 500,
      display: "inline-block",
      fontSize: 12,
      marginInline: 4 
    }
  }));
  
  
// import claimsdata from './KpiCards_histogramData.json';

// import "./linechart_paper.css";

am4core.useTheme(am4themes_animated);

const CHART_ID = "claimsChart23"
var claim_number_selected = {};


export default function Model_inferance(props) {
    const classes = useStyles();

    const chartRef = useRef(null);
    const claim_data = props.claimDetails;
    const [newdata,newsetData]=useState([]);

    claim_number_selected = {"claim_number" : claim_data,
    "run_id": "RUN-bde682dc-b413-11ec-930a-062fcc6cb972", 
    "model_id": "XGB-8dd62108-b407-11ec-930a-062fcc6cb972"}
    useEffect(() => {
        const requestOptions = {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(claim_number_selected),
        };
      
        
        fetch('/model_infer_output', requestOptions).then((res) => res.json()).then((newdata) => newsetData(newdata)
                                      
        )},[]);

        const c ="red"

    return (
      
        <div className={classes.root}>
            {}
          {}
    {Object.keys(newdata).length > 0 ? (
             <div>
              <Grid container alignItems="center" spacing={1}>
                <Grid item xs={12} sm={3}>
                  <Paper className={classes.paper}>
                    {" "}
                 Score Date<br /><br />{" "}
                    <div className={classes.card}>{newdata["ModelScoreDate"]}</div>{" "}
                  </Paper>
                </Grid>
                
                <Grid item xs={12} sm={3}>
                  <Paper className={classes.paper}>
                    {" "}
                   
                    Fraud Score   <br /><br />{" "}
                    <div className={classes.card}> {newdata["ModelScaledScore"]}</div>
                  </Paper>
                </Grid>
                <Grid item xs={12} sm={3}>
                  <Paper className={classes.paper}>
                    {" "}
                   Fraud Score Group <br /><br />{" "}
                   
                  
          {newdata["ModelScoreGroup"] === "SIGNIFICANTLY HIGH" || newdata["ModelScoreGroup"] === "HIGH"
                  ? 
                  <div style={{color:"red"}}  className={classes.card} >
                  {newdata["ModelScoreGroup"]} </div>
                  : <div style={{color:"green"}}  className={classes.card} >
                  {newdata["ModelScoreGroup"]} </div>}
                         
                  
                    {/* <span>{newdata["ModelScoreGroup"] === "HIGH"? <ArrowDropUpIcon style={{color: "orange"}}/> :
            newdata["ModelScoreGroup"] === "SIGNIFICANTLY HIGH" ?        <ArrowDropUpIcon style={{color: "red"}}/> :
            <ArrowDropUpIcon style={{color: "green"}}/>}
            </span> */}
                    
                  </Paper>
                </Grid>
              
                </Grid>
<br/>
<div style={{backgroundColor: "Gainsboro", width:450,height:32 }}  >
           <p  style={{fontSize: 14,marginLeft: 161,fontWeight:"Bold",color:"Black",padding:5}}>Fraud Indicator </p>
           </div>
                <TableContainer  style={{width: 450}} component={Paper}>
      <Table   size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
           
          </TableRow>
          <br/>
        </TableHead>
        <TableBody>
          {newdata.model_indicators.sort((a, b) => b.ModelFeatureImportance - a.ModelFeatureImportance).map((row) => (
            <TableRow
              key={row.index}
              style={{width: 100}}
            >
              <TableCell  component="th" scope="row">
                {row.index+1}
              </TableCell>
              <TableCell>{row.FeatureBusinessName}</TableCell>
                  {/* <TableCell>{row.FeatureBusinessName}</TableCell> */}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
                
                            </div> 
         
       
    ): "loading"} 
     </div>
      );
}

