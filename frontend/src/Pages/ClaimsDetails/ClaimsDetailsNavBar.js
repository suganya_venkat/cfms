import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import ClaimsDetailsPage from './ClaimsDetailsPage';
import NexusView from './NexusView'
import './ClaimsDetails.css'

// var claim_number_selected = {};

function TabPanel(props) {
   const { children, value, index, ...other } = props;


  return (
    <div
      // role="tabpanel"
      // hidden={value !== index}
      // id={`vertical-tabpanel-${index}`}
      // aria-labelledby={`vertical-tab-${index}`}
      // {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

// TabPanel.propTypes = {
//   children: PropTypes.node,
//   index: PropTypes.number.isRequired,
//   value: PropTypes.number.isRequired,
// };

// function a11yProps(index) {
//   return {
//     id: `vertical-tab-${index}`,
//     'aria-controls': `vertical-tabpanel-${index}`,
//   };
// }

export default function ClaimsDetailsNavBar(props) {
  const [value, setValue] = React.useState(0);
  
  // const[valueSelected,setvalueSelected] = React.useState(0)

//  console.log(value)
  const claim_data = props.claimData;

  // console.log(claim_data)
  const[claimDetails,setclaimDetails] = React.useState(claim_data)
  // claim_number_selected = {"claim_number" : claim_data}

  // const requestOptions = {
  //   method: "POST",
  //   headers: { "Content-Type": "application/json" },
  //   body: JSON.stringify(claim_number_selected),
  // };

  // fetch('/claim_level_data', requestOptions).then((res) => res.json()).then((claimDetails) => setclaimDetails(claimDetails),
  // // console.log(claimDetails)                                     
  // );

  const handleChange = (event, newValue) => {
    setValue(newValue);
    // setvalueSelected(value)
  };

  return (
    <Box
      sx={{ flexGrow: 1, bgcolor: 'background.paper', display: 'flex', height: 550}}
    >
      <Tabs
        orientation="vertical"
        // variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        sx={{ borderRight: 1, borderColor: 'divider' }}
        style={{backgroundColor:"rgb(245 245 255)",marginTop:-15,
       
        marginRight: 0,
        height: 600,
        marginLeft: -16}}
      >
        <Tab label="Claim Info"/>
      
        {/* <Tab label="Rules"  /> */}
        <Tab label="AI insights" />
        <Tab label="Forensic Analysis"  />
         <Tab label="Document Review"  />
        
      </Tabs>
      <TabPanel  value={value} index={0} >
      <ClaimsDetailsPage setclaimDetails={setclaimDetails} claimDetails={claimDetails} setValue={setValue} value={value}/>
{/*     
      </TabPanel>
      <TabPanel value={value} index={1}> */}
      
      </TabPanel>
      <TabPanel value={value} index={1}>
      <ClaimsDetailsPage setclaimDetails={setclaimDetails} claimDetails={claimDetails} setValue={setValue} value={value}/>

      </TabPanel>
      <TabPanel value={value} index={2}>
      <ClaimsDetailsPage setclaimDetails={setclaimDetails} claimDetails={claimDetails} setValue={setValue} value={value}/>
      </TabPanel>

      <TabPanel value={value} index={3}>
      <ClaimsDetailsPage setclaimDetails={setclaimDetails} claimDetails={claimDetails} setValue={setValue} value={value}/>
      </TabPanel>
     
     
    </Box>
  );
}
