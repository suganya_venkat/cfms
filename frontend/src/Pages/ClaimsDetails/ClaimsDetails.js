import React, { useState } from 'react'
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Configuration_navbar from '../../Components/Configuration_navbar'
import ClaimsDetailsPage from './ClaimsDetailsPage'
import ClaimsDetailsNavBar from './ClaimsDetailsNavBar'
import {useLocation} from 'react-router-dom';
import MainNavBar from '../../Components/MainNavBar';
import ProgressBarTop from '../../assets/images/ProgressBarTop.PNG'
import NewTheme from '../../Components/newTheme.js';
import CardMedia from "@material-ui/core/CardMedia"
import AlertsNavBar from '../../Components/AlertsNavBar.js';

import './ClaimsDetails'

// constructor(props){
//     super(props);
//     this.state={
//         value:this.props.location.state,
//     }

// }

function AlertManagement(props) {

    
    const  state  = props.location.state
    const [claimData, setclaimData] = useState(state);
//    console.log(state)




    return (
        <div className='divalign'>
  <MainNavBar />
  {/* <CardMedia
     className='ProgressBarTopClaimsDetails'   
          image={ProgressBarTop}

 /> */}

          {/* <NewTheme/>
          <div className='activeLine' ></div><br/> */}
          <AlertsNavBar/>
        <Grid >
        <Paper>
       
          
     
       
       
        <CardContent className='cardcontent'>
     
        <ClaimsDetailsNavBar setclaimData={setclaimData} claimData={claimData}/>
       
        </CardContent>
        </Paper>
        </Grid>
        
       
      
   
     </div>
    )
}

export default AlertManagement
