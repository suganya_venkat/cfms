import React, { useState }  from 'react'
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import CardMedia from "@material-ui/core/CardMedia"
import Card from '@mui/material/Card';
import List from '@material-ui/core/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@mui/material/IconButton';
import DoneIcon from '@mui/icons-material/Done';
import data from './Document.json'
import FlagIcon from '@mui/icons-material/Flag';
import './ClaimsDetails.css'
import dateFormat from "dateformat";


// Import Worker
// import { Worker } from '@react-pdf-viewer/core';
// Import the main Viewer component
// import { Viewer } from '@react-pdf-viewer/core';
// Import the styles
// import '@react-pdf-viewer/core/lib/styles/index.css';
// import '@react-pdf-viewer/default-layout/lib/styles/index.css';



import { styled } from '@mui/material/styles';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

function Documents(props) {
  //  console.log(data['Rules'][0]['document'])
  // const [pdfFile, setPdfFile]=useState(null);
 
  // const defaultLayoutPluginInstance = defaultLayoutPlugin();

  // const documents = ['Drivers License','Police Report', 'Hospital admission - Inpatient', 'Hospital admission - Outpatient']
  //, 'Hospital admission - Inpatient', 'Hospital admission - Outpatient',Claim Form
  // const ReportAnalysis = 
  // ['Name in Driver License matches with police report​',
  // 'Driver\'s License expired before Claim report date​', 'Claimant address in Driver\'s license different from Address in police report​', 
  // 'Police report date succeeding Incident data​']
 
  const claim_data = props.claimDetails;
  // console.log(claim_data)
  const useStyles = makeStyles(theme => ({
   
        cardMediaStyle: {
         paddingTop: "70%",
          // width:'250px'
        }
      }));
      const styles = useStyles();

      const [checked, setChecked] = React.useState([0]);

      const handleToggle = (value) => () => {
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];
    
        if (currentIndex === -1) {
          newChecked.push(value);
        } else {
          newChecked.splice(currentIndex, 1);
        }
    
        setChecked(newChecked);
      
      };
      //  console.log(data.Documnents[0].document_string)

    //  console.log(data.Documnents[0].document_string)
    return (
    
        <div style={{margin:4}}>
              {claim_data === "V3882" || claim_data === "V3621" || claim_data === "V3300" ?
      <div>
            <Grid  container spacing={1}>
     
       
      
       
        <Grid item xs = {6} > 
        <Paper elevation={3} className="a"style={{height:202}} >
       <h3 style={{backgroundColor:"Gainsboro"}}><center>View Document</center></h3>
        <List style={{marginLeft: 17}} sx={{ width: '100%',  maxWidth: 500, maxHeight: 175,overflow: "auto", bgcolor: 'background.paper' }}>
      {data['View Document'].map((value) => {
        const labelId = `${value}`;

        return (
          <ListItem
            key={value}
            // secondaryAction={
            //   <IconButton edge="end" aria-label="comments">
            //     <CommentIcon />
            //   </IconButton>
            // }
            disablePadding
          >
            <ListItemButton role={undefined} onClick={handleToggle(value)} dense>
              <ListItemIcon>
                <Checkbox
                  edge="start"
                  checked={checked.indexOf(value) !== -1}
                  tabIndex={-1}
                  disableRipple
                  inputProps={{ 'aria-labelledby': labelId }}
                />
              </ListItemIcon>
           
              <ListItemText id={labelId} primary={labelId} />
               
            </ListItemButton>
          </ListItem>
        );
      })}
    </List>
<br/>
</Paper>
</Grid>
<Grid item xs = {6} > 
<Paper elevation={3} className="a" style={{height:202}} >
    <h3 style={{backgroundColor:"Gainsboro"}}><center>Identified Risk Flags</center></h3>
      {/* { data['Rules'].map((d) => { */}
        {/* // if(d.document === "Driver License"){ */}
        <List style={{marginLeft: 13}} sx={{ width: '100%', maxWidth: 500, maxHeight: 175,overflow: "auto",bgcolor: 'background.paper' ,marginLeft:2}}>
      {data['Rules'].map((value) => {
        const labelId = `${value.rules}`;
       
        return (
          <ListItem
            key={value.rules}
            secondaryAction={
              <IconButton edge="end" aria-label="comments">
             {value.flag.startsWith("N")   ? <FlagIcon style={{color:"red"}}/> 
             :    <DoneIcon style={{color:"green"}} />
            }   
              

              </IconButton>
            }
            disablePadding
          >
            {/* <ListItemButton role={undefined} onClick={handleToggle(value)} dense> */}
              {/* <ListItemIcon>
                <Checkbox
                  edge="start"
                  checked={checked.indexOf(value) !== -1}
                  tabIndex={-1}
                  disableRipple
                  inputProps={{ 'aria-labelledby': labelId }}
                />
              </ListItemIcon> */}
           
              <ListItemText id={labelId} primary={labelId} />
               
            {/* </ListItemButton> */}
          </ListItem>
        );
      })}
     
    </List>
        {/* // }
        // else{
        // console.log("Its not driver license")
        // }
    // })} */}
    
        {/* <ShapeFeatureImportance/> */}
     <p style={{marginLeft:410,
    marginTop: 60,fontSize:11}}><i>*Median Severity = $1219</i> </p>  
       </Paper>
        </Grid>
     
       </Grid>
       <br/>
       <Grid  container >
        <Grid item xs = {12} > 
        <Paper>
       <div style={{fontSize:14}}><center>Select document for preview</center></div><br/>
        <div   class="row">
      
 {data.Documnents.map((value)=> <div>
 {checked.find(el=>el === value.document) ?
 <div class="column">
  <h3><center>{value.document}</center></h3><br/>
  <h4>Entity Identified</h4>
 
  {/* <Box sx={{ width: '100%' }}> */}
      {/* <Stack>
        <Item> */}
     
        <Card sx={{ minWidth: 275,height: 93 }}>
      <CardContent>
        <ul style={{marginLeft:12,fontSize:11}}>
        { value.document === "Bodyshop Repair Invoice" ?
        <li>  Bodyshop Name-  {value["Bodyshop Name"]}</li> : ""}
         { value.document != "Bodyshop Repair Invoice" ?
  <li>Claimant Name - {value.claimant_name}</li> : "" }
   { value.document != "Bodyshop Repair Invoice" ?
  <li>  {  value.document === "Police Report" ? "Report Date" : value.document === "Driver License" ?  "Expiry Date" : ""}- {dateFormat(value.date, "dd-mm-yyyy")}
  </li>
  :""}
  {/* { value.document === "Hospital Admission - Inpatient" ?
  <li>  Discharge Date - {dateFormat(value.discharge_date, "yyyy-mm-dd")}</li> : ""} */}
  <li> { value.document === "Bodyshop Repair Invoice" ? "Bodyshop Address" : value.document === "Police Report" ? "Keywords Extracted" : value.document === "Driver License" ?  "License Address" : ""} - {value.address}</li>
  { value.document === "Bodyshop Repair Invoice" ?
        <li>  Total Cost-  {value["Labour Cost"]}</li> : ""}
  {/* <li> { value.document === "Bodyshop Repair Invoice" ? "Labour Cost-" : ""} {value["Labour Cost"]}</li> */}

</ul>
</CardContent>
</Card>

        
        {/* </Item> */}
        {/* <Item>Item 2</Item>
        <Item>Item 3</Item> */}
      {/* </Stack> */}
    {/* </Box> */}
    <br/>
            <img style={{width:"100%",height:"100%"}}
              // className="image"
              src={value.document_string}
            ></img> 
            
            </div>: " " }
          
 </div>
         )}
      </div>
        {/* {data.Documnents.map((value) => {
        //  { value === 0 ?
        {console.log(value.document_string)}
        <img
              className="image"
              src={value.document_string}
            ></img>
            // : " "   } 
         } ) } */}
        {/* <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.12.313/build/pdf.worker.min.js">
        <Viewer fileUrl={data.Rules[0].document_string}    
            plugins={[defaultLayoutPluginInstance]}></Viewer>
    </Worker> */}
    {/* {Base64ToImage()} */}
        {/* <CardMedia
           className={styles.cardMediaStyle}    
              image={Documents_pic}

/> */}
      
        </Paper>
        </Grid>
        </Grid>
        </div>
:<div><h2><center style={{marginTop:145,marginRight:0}}> No Documents Available For This Claim Type</center></h2></div>}
  {/* <h6>{ <ReportProblemIcon style={{color:"yellow"}}/>}</h6> */}
     </div>
    )
}

export default Documents
