import  React, { useEffect} from 'react';
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Paper from '@mui/material/Paper';
import Grid from "@material-ui/core/Grid";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Button from '@mui/material/Button';
import FilterData from './FilterData.json'

import { Fragment, useState } from "react";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import './InvestigatedPage.css'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    border: `1px solid #EEF3F5`,
    borderRadius: 4,
    color: "#949FAF",
    minWidth: 275,
    minHeight: 70,
    marginTop: 12,
    marginLeft: 12,
    marginRight: 12,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    paddingBlock: 5,
    fontSize: 13,

    borderRadius: 5,
  },
  card: {
    color: "#16448F",
    fontWeight: 500,
    fontSize: 13,
    display: "inline-block"
  },
  cardsmall:{
    color: '#5DC9A2',
    fomtWeight: 500,
    display: "inline-block",
    fontSize: 12,
    marginInline: 4 
  }
}));

export default function Investigation_filters(props) {
  const classes = useStyles();

  // const [Model, setModel] = React.useState('');
 
  // const stateHandleChange = (event) => {
  //   props.setState(event.target.value);
    
  // };
  const [data,setData]=useState(FilterData);

  const[getFilterState,setFilterState]=useState(['all'])
  const[getFilterClaimType,setFilterClaimType]=useState(['all'])
  const[getFilterClaimAmt,setFilterClaimAmt]=useState(['all'])
  const[getFilterFromRptDate,setFilterFromRptDate] =useState("2020-01-01 00:00:00");
  const[getFilterToRptDate,setFilterToRptDate] =useState("2021-12-31 00:00:00");

  useEffect(() => {
    fetch('/unique_values').then(res => res.json()).then((data) => setData(data));
    // setStatus(data.States);
    // status.push
    },[]);

  const stateHandleChange = (event) => {
    const {
      target: { value },
    } = event;
    setFilterState(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value,
    );
  };

  const claimTypeHandleChange = (event) => {
    const {
      target: { value },
    } = event;
    setFilterClaimType(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value,
    );
  };

  
  const claimAmountHandleChange = (event) => {
    const {
      target: { value },
    } = event;
    setFilterClaimAmt(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value,
    );
  };
  const handleFromDateChange = (date) => {
    setFilterFromRptDate(date)
   
  };
  const handleToDateChange = (date) => {
    setFilterToRptDate(date)
   
  };
  const passFilterValues = () =>{
    props.setState(getFilterState)
    props.setClaimType(getFilterClaimType)
    props.setClaimAmount(getFilterClaimAmt)
    props.setFromRptDate(getFilterFromRptDate);
    props.setToRptDate(getFilterToRptDate);
  }

  

  
  // const model_value = Model
  // console.log(model_value);

const Claim_Amount= ['all','0-1000', '1000-10,000', '+10,000']


  return (
    <div style={{marginLeft:-8}}>
     <Grid container alignItems="center" spacing={1}  style={{marginLeft: 3}}>
                <Grid item xs={12} sm={3}>
                  <Paper className={classes.paper}>
                    {" "}
                Modelling Timeframe<br /><br />{" "}
                    <div className={classes.card}>Jan 01, 2020 to Dec 31, 2020</div>{" "}
                  </Paper>
                </Grid>
                
                <Grid item xs={12} sm={3}>
                  <Paper className={classes.paper}>
                    {" "}
                   
                   Referrals Start Date   <br /><br />{" "}
                    <div className={classes.card}> Jan 01, 2021</div>
                  </Paper>
                </Grid>
                {/* <Grid item xs={12} sm={6}>
                  
                    <div className={classes.card}>
                      <h5>
                      Referrals are outputs from predictive modelling, which are continuously updated & uploaded to tool, Investigation Unit then review these referrals and acts on whether it should be picked up for further investigation or not
                      </h5>
                    </div>
                  
                </Grid>
               */}
                </Grid>
                <br/>
    <div style={{marginLeft: 12}} className='filter_div'>
    <Box sx={{ minWidth: 179 }} className='filter_flex'>
    <MuiPickersUtilsProvider utils={DateFnsUtils} >

<KeyboardDatePicker
  label="Reported Date From"
  value={getFilterFromRptDate}
  onChange={handleFromDateChange}
  format="dd/MM/yyyy"
/>


</MuiPickersUtilsProvider>
</Box>
<Box sx={{ minWidth: 210 }} className='filter_flex'>
    <MuiPickersUtilsProvider utils={DateFnsUtils} >


<KeyboardDatePicker
  label="Reported Date To"
  value={getFilterToRptDate}
  onChange={handleToDateChange}
  format="dd/MM/yyyy"
/>

</MuiPickersUtilsProvider>
</Box>
      <Box sx={{ minWidth: 210 }} >
      <FormControl fullWidth className='filter_flex'>
        <InputLabel id="State">State</InputLabel>
        <Select 
          labelId="State"
          id="State"
          multiple
          // displayEmpty
          value={getFilterState}
          label="State"
          onChange={stateHandleChange}
        >
          {/* <MenuItem value=""><Put any default Value which you want to show/></MenuItem> */}
          {Object.keys(data).length > 0 ?
           data.States.map((d) => {
             
                return ( 
            <MenuItem 
            key={d}
            value={d}
            >
              {d}
            </MenuItem>
                )
              })
              : ""  
               
}
        </Select>
        
      </FormControl>
      </Box>
      <Box sx={{ minWidth: 210 }} >
      <FormControl fullWidth className='filter_flex'>
        <InputLabel id="Claim Type">Claim Type</InputLabel>
        <Select 
          labelId="Claim Type"
          id="Claim Type"
          multiple
          // displayEmpty
          value={getFilterClaimType}
          label="Claim Type"
          onChange={claimTypeHandleChange}
        >
          {/* <MenuItem value=""><Put any default Value which you want to show/></MenuItem> */}
          {  Object.keys(data).length > 0 ?
             data.Claim_Types.map((d) => {
             
                return ( 
            <MenuItem
            key={d} 
            value={d}>{d}</MenuItem>
                )
              })
              : ""  
               
}
        </Select>
        
      </FormControl>
      </Box>
      <Box sx={{ minWidth: 210 }} >
      <FormControl fullWidth className='filter_flex'>
        <InputLabel id="claim_amt">Claim Amount</InputLabel>
        <Select 
          labelId="claim_amt"
          id="claim_amt"
          // displayEmpty
          multiple
          value={getFilterClaimAmt}
          label="Claim Amount"
          onChange={claimAmountHandleChange}
        >
          {/* <MenuItem value=""><Put any default Value which you want to show/></MenuItem> */}
          {
            Claim_Amount.map((d) => {
             
                return ( 
            <MenuItem 
            key={d}
            value={d}
            >
              {d}
            </MenuItem>
                )
              })
              
               
}

          {/* <MenuItem value={"Less than "}>Model 3</MenuItem> */}
        </Select>
        
      </FormControl>
      </Box>
      {/* onClick={passFilterValues} */}
      <Button  onClick={passFilterValues} style={{
      
      backgroundColor: "Black",
      color:"white",
      height: 32,
    fontSize: 12
    }}   > Filter</Button>
    </div>
    </div>
  );
 
}
