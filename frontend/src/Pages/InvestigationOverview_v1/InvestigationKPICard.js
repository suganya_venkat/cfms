import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Data from './InvestigationData_KPI.json'
import Investigation_Data from './investigation_summary_dict.json'
import DvrIcon from '@mui/icons-material/Dvr';

import SpinnerPage from '../../utils/loader';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    border: `1px dotted  #B0BEC5`,
    borderRadius: 4,
    color: "#949FAF",
    minWidth: 275,
    minHeight: 70,
    marginTop: 12,
    marginLeft: 12,
    marginRight: 12,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    paddingBlock: 20,
    fontSize: 12,
    borderRadius: 10.5,
    backgroundColor: "#dcd0ff" // #BBDEFB
  },
  card: {
    color: "#16448F",
    fontWeight: 700,
    fontSize: 13,
    display: "inline-block"

  },
  cardsmall:{
    color: '#5DC9A2',
    fomtWeight: 500,
    display: "inline-block",
    fontSize: 13,
    marginInline: 4 
  }
}));

// const fetchURL = "http://localhost:5000/insights/tccspv_kpi";
// const fetchURLcost = "http://localhost:5000/insights/cost/kpi_cost_overview";
// const fetchforecast = "http://localhost:5000/insights/forecast/kpicard";

export default function InvestigationKpiCards(props) {
    const classes = useStyles();

    
  const [data,setData]=useState(Investigation_Data);
  const selectedState = props.getState;
    const selectedToRptDate=props.getToRptDate;
    const selectedClaimType=props.getClaimType;
    const selectedClaimAmount=props.getClaimAmount;
    const selectedFromRptDate = props.getFromRptDate;
    
     console.log(data)
  //  useEffect(() => {
  // fetch('/invesigation_overview').then(res => res.json()).then((data) => setData(data));
  // },[]);
  useEffect(() => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({"reporting_view_filters" :  

      {"CLAIMREPORTDATE" : [{"from":  selectedFromRptDate,  

                          "to": selectedToRptDate}], 

      "STATE" : selectedState,  

      "CLAIM_TYPE": selectedClaimType,  

      "CLAIM_AMT_USD": selectedClaimAmount

    } 

    } )
    };
    fetch('/reporting_overview_investigation_v2', requestOptions)
    .then(res => res.json()).then((data) => setData(data));
  },[selectedState,selectedFromRptDate,selectedToRptDate,selectedClaimType,selectedClaimAmount]);

// console.log(data.under_investigation_summary.charts.open_claims_by_investigation_status);

  // console.log(forecasts);

  return (
    <div>
   
      {Object.keys(data).length > 0 ? ( 
        <div >
       <div >
         
    
          {/* <Grid container alignItems="center" spacing={3}  className='filter_flex'>
         
         <Grid item xs={12} sm={2} style={{ marginLeft: 27}} >
              <Paper style={{width:123, height:146, 
    textAlign: "center",
    marginTop: 7,
    marginLeft: 7,
    paddingBlock: 20,
    fontSize: 12,
    borderRadius: 10.5}}>
                {" "}
                Total Claim (Since Referral Start Date) <br /><br/><br/>{" "}
                <div className={classes.card}>{data.total_claims_since_ref_start_date}</div>{" "}
              </Paper>
            </Grid>
            </Grid> */}
            {/* <Grid container alignItems="center" spacing={1}  >
            <Grid item xs={12} sm={1} >
              </Grid>
            <Grid item xs={12} sm={2} >
            
              <Paper className={classes.paper}>
                {" "}
                AI Recommendations​<br />{" "}
                <div className={classes.card}>{data.referral_summary.cur_total_referred}</div>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={3} className="CardWidth">
              <Paper className={classes.paper} > */}
                {/* style={{backgroundColor: "#BBDEFB"}} */}
                {/* {" "}
                Total Investigated​   <br />{" "}
                <div className={classes.card}>{data.referral_summary.cur_referrals_assigned_str}</div>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Paper className={classes.paper} > */}
                {/* style={{backgroundColor: "#BBDEFB"}} */}
                {/* {" "}
               Total Frauds Captured <br />{" "}
                <div className={classes.card}>{data.referral_summary.cur_referrals_fraud_captured_str}</div>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={2}>
              <Paper className={classes.paper} > */}
                {/* style={{backgroundColor: "#BBDEFB"}} */}
                {" "}
                {/* Total Loss Cost Savings​($) <br />{" "}
                <div className={classes.card}>{(data.referral_summary.cur_referrals_total_claimed_amt/100000).toFixed(0)}M</div>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={1} >
              </Grid> */}
            {/* </div>
            <div className='filter_div'> */}
          
            {/* <Grid item xs={12} sm={2}>
              <Paper style={{width:123, height:146, 
    textAlign: "center",
    
    paddingBlock: 20,
    fontSize: 12,
    borderRadius: 10.5}}>
                {" "}
              Overall Fraud Captured Rate <br/><br /><br/>{" "}
                <div className={classes.card}>{data.overall_fraud_capture_str}</div>{" "}
              </Paper>
         
              </Grid> */}
            {/* </Grid>
        
            <br/>
             <div className='filter_div_kpi'>
             <Grid container alignItems="center" spacing={3}  className='filter_flex'>
             
         <Grid item xs={12} sm={2} style={{marginLeft: 58,
    marginTop: -66}} >
              <Paper className="TotalClaimbgc" style={{width:96, height: 122,
    textAlign: "center",
     marginTop: -22,
    backgroundColor: "#e6e6fa",
    marginLeft:-56,
    paddingBlock: 20,
    fontSize: 12,
    borderRadius: 10.5}}>
                {" "}
                Total Claims  <br /><br/><br/>{" "}
                <div className={classes.card}>{data.total_claims_since_ref_start_date.toLocaleString("en", {   
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
})}</div>{" "}
              </Paper>
            </Grid>
            </Grid>
         
            <Grid container alignItems="center" spacing={1} style={{marginLeft: 21}}>
           
            <Grid item xs={12} sm={2}>
              <Paper className={classes.paper} style={{backgroundColor: "#e6e6fa",marginLeft:-20}}>
                {" "}
                AI Non-Recommendations​<br />{" "}
                <div className={classes.card}>{data.nonreferral_summary.cur_total_nonreferred}</div>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={3} style={{ marginLeft: 26}}>
              <Paper className={classes.paper} style={{backgroundColor: "#e6e6fa",marginLeft: -25}}>
                {" "}
                Total Investigated​   <br />{" "}
                <div className={classes.card}>{data.nonreferral_summary.cur_nonreferrals_assigned_str}</div>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={3} style={{ marginLeft: 32}}>
              <Paper className={classes.paper} style={{backgroundColor: "#e6e6fa", marginLeft: -31}}>
                {" "}
               Total Frauds Captured <br />{" "}
                <div className={classes.card}>{data.nonreferral_summary.cur_nonreferrals_fraud_captured_str}</div>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={2} style={{ marginLeft: 18}}>
              <Paper className={classes.paper} style={{backgroundColor: "#e6e6fa",marginLeft: -18}}>
                {" "}
                Total Loss Cost Savings​($) <br />{" "}
                <div className={classes.card}>{(data.nonreferral_summary.cur_nonreferrals_total_claimed_amt/100000).toFixed(0)}M</div>{" "}
              </Paper>
            </Grid> */}
            {/* <Grid item xs={12} sm={2}>
              <Paper style={{width:123, height:146, 
    textAlign: "center",
    
    paddingBlock: 20,
    fontSize: 12,
    borderRadius: 10.5}}>
                {" "}
              Overall Fraud Captured Rate <br/><br /><br/>{" "}
                <div className={classes.card}>{data.overall_fraud_capture_str}</div>{" "}
              </Paper>
         
              </Grid> */}
            {/* </Grid>

            <Grid container alignItems="center" spacing={1}  className='filter_flex'>
           
            <Grid item xs={12} sm={2} style={{marginTop:-65, marginLeft:-139}}>
            <Paper style={{width:96, 
    textAlign: "center",
    marginTop: -22,
    // marginTop: 7,
    height:126,
     marginLeft: 34,
    backgroundColor: "#d891ef",
    paddingBlock: 20,
    fontSize: 12,
    borderRadius: 10.5}}>
                {" "}
              Overall Fraud Captured Rate <br/><br /><br/>{" "}
                <div className={classes.card}>{data.overall_fraud_capture_str}</div>{" "}
              </Paper>
              </Grid>
         </Grid>
         </div>
         </div>
         <br/>
         <h4 style={{color:"black",fontSize:14}}> Pre-AI Implementation</h4><br/>
         
            <div>
       
            
            <Grid container alignItems="center" spacing={1}>
            <Grid item xs={12} sm={3}>
              <Paper className={classes.paper} style={{backgroundColor: "#ccccff"}}>
                {" "}
                Total Claims<br />{" "}
                <div className={classes.card}>{data.hist_summary.hist_total_data.toLocaleString("en", {   
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
})}</div>{" "}
              </Paper>
            </Grid> */}
            {/* <Grid item xs={12} sm={2}>
              <Paper className={classes.paper} style={{backgroundColor: "#ccccff"}}>
                {" "}
               Total Claims Used For Modelling  <br />{" "}
                <div className={classes.card}>{data.hist_summary.hist_total_claims_for_modelling.toLocaleString("en", {   
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
})}</div>{" "}
              </Paper>
            </Grid> */}
            {/* <Grid item xs={12} sm={2}>
              <Paper className={classes.paper} style={{backgroundColor: "#ccccff"}}>
                {" "}
                Total Investigated​ <br />{" "}
                <div className={classes.card}>{data.hist_summary.hist_total_investigated_str}</div>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={2}>
              <Paper className={classes.paper} style={{backgroundColor: "#ccccff"}}>
                {" "}
                Total Fraud Captured<br />{" "}
                <div className={classes.card}>{data.hist_summary.hist_total_fraud_found_str}</div>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={2}>
              <Paper className={classes.paper} style={{backgroundColor: "#ccccff"}}>
                {" "}
                Total Lost Cost Savings($) <br />{" "}
                <div className={classes.card}>{(data.hist_summary.hist_total_claimed_amt/100000).toFixed(0)}M</div>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Paper className={classes.paper} style={{backgroundColor: "#ccccff"}}>  */}
              {/* #e6e6fa */}
                {/* {" "}
               Overall Fraud Capture Rate <br />{" "}
                <div className={classes.card}>{data.hist_summary.hist_overall_fraud_capture_rate}</div>{" "}
              </Paper>
            </Grid>
          
            </Grid>
            <br/> */}
            {/* <DvrIcon style={{fontSize: 30,color:"black"}} />     */}
            <br/><br/>
             <h4 style={{color:"black",marginTop:-28}}>  Investigation Summary - Open Claims</h4><br/>
      
            <Grid container alignItems="center" spacing={1}>
            <Grid item xs={12} sm={2}>
              <Paper className={classes.paper} >
                {/* style={{backgroundColor: "#BBDEFB"}} */}
                {" "}
                Total Open Claims<br />{" "}
                <div className={classes.card}>{data.under_investigation_summary.total_open_claims}</div>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={2}>
              <Paper className={classes.paper} >
                {/* style={{backgroundColor: "#BBDEFB"}} */}
                {" "}
                AI Recommendations​ <br />{" "}
                <div className={classes.card}>{data.under_investigation_summary.total_open_reffered_claims}</div>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Paper className={classes.paper} >
                {/* style={{backgroundColor: "#BBDEFB"}} */}
                {" "}
                AI Recommendations Assigned​ <br />{" "}
                <div className={classes.card}>{data.under_investigation_summary.total_referrals_assigned_under_investigation_str}</div>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Paper className={classes.paper} >
                {/* style={{backgroundColor: "#BBDEFB"}} */}
                {" "}
                AI Non-Recommendations Assigned​<br />{" "}
                <div className={classes.card}>{data.under_investigation_summary.total_nonreferrals_under_investigation}</div>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={2}>
              <Paper className={classes.paper} >
                {/* style={{backgroundColor: "#BBDEFB"}} */}
                {" "}
                Claims Unassigned  <br />{" "}
                <div className={classes.card}>{data.under_investigation_summary.total_unassigned_under_investigation_str}</div>{" "}
              </Paper>
            </Grid>

            </Grid>
        
            
           
            
           
            
           
           
          </div>
          </div>
         
        //  </div> 
    ): "Loading"} 
    </div>
  
  );
}
