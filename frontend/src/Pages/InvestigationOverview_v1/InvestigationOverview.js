import React, {useState}from 'react'
import Grid from "@material-ui/core/Grid";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Paper from "@material-ui/core/Paper";
import { Typography } from "@material-ui/core";
import NavBar from '../../Components/Navbar'
import MainNavBar from '../../Components/MainNavBar'
import InvestigationKPICard from './InvestigationKPICard'
import InvestidationClaimsByState from './InvestigationClaimsByState'
import InvestigationComboChart from './InvestigationComboChart'
import Investigation_filters from './Investigation_filters'
import InvestigationByClaimType from './InvestigationByClaimType';
import InvestigationClaimsByMonth from './InvestigationClaimsByMonth';
import AnalyticsandReportNavBar from '../../Components/AnalyticsandReportNavBar'
import Popover from '@mui/material/Popover';
import CardMedia from "@material-ui/core/CardMedia"
import ReportingBe from '../../assets/images/ReportingBe.png'
import AnalyticsBe from '../../assets/images/AnalyticsBe.png'
import ProgressBarTop from '../../assets/images/ProgressBarTop.PNG'
import NewTheme from '../../Components/newTheme.js';
import ImpactSummaryKPI from '../InvestigationOverview_v1/ImpactSummaryKPI'
// import InvestigationClaimsByClaimType from './InvestigationClaimsByclaimtype';
import AddchartIcon from '@mui/icons-material/Addchart';
import DvrIcon from '@mui/icons-material/Dvr';
import { styled } from '@mui/material/styles';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import InfoIcon from '@mui/icons-material/Info';
import FunnelChart from '../AI_impact/FunnelChart'
import '../CommonStyling/Style.css'
import './InvestigatedPage.css';

const HtmlTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#e6e6fa',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 500,
    height:100,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
    padding:"10px"
  },
}));
function InvestigationOverview() {

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    console.log(event)
    setAnchorEl(event.currentTarget);
 
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;
  
  const [getState, setState] = useState(["all"]);
  const[getFromRptDate,setFromRptDate] = useState("2020-01-01 00:00:00");
  const[getToRptDate,setToRptDate] = useState("2021-12-31 00:00:00");
  const[getClaimType,setClaimType] = useState(["all"]);
  const[getClaimAmount,setClaimAmount] = useState(["all"]);
 
    return (
        <div className='divalign'>
          
          <AnalyticsandReportNavBar/>
          {/* <CardMedia
     className='ProgressBarTopInvestigation'   
          image={ProgressBarTop}

 /> */}

          {/* <NewTheme/>
          <div className='activeLineSummary'></div><br/> */}
        <NavBar/>
      
        <div style={{padding: 8}} className="infoHeadingContainer">
        {/* <AddchartIcon style={{fontSize: 47,cursor: "pointer"}}  onClick={handleClick}  />  */}
             <h3 style={{color:"black",marginLeft:26,marginTop:15}}> AI Impact</h3><br/>
             <span className="aiImpactInfo">
       <HtmlTooltip  placement="right-start"
       
        title={
          <React.Fragment>
            <p style={{fontSize:12}}><i>
        Comparison of KPIs Pre-AI Implementation (Benchmark ) vs Post AI Implementation</i></p> <br/>
        <p style={{fontSize:12}}>Note: The Impact in highlighted green compares traditional fraud capture process vs AI driven fraud analytics​</p>
 
               </React.Fragment>
        }
      >
        <InfoIcon className="infoIcon" />
      </HtmlTooltip>
     
      </span>
      
     
     </div>
     
        <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
         <CardMedia
          className='cardMediaStylepopup'   
              image={ReportingBe}

/>
        {/* <Typography sx={{ p: 2 }}>The content of the Popover.</Typography> */}
      </Popover>
      <div style={{marginLeft: 8}}>
              <div style={{marginLeft: 22}}>
        {/* <h5 >Total Investigations</h5><br/> */}
        <br/>
        <ImpactSummaryKPI/>
        {/* <p style={{fontSize:12}}>1. Overall Fraud capture rate  <b style={{color: "green",
    fontSize: 13}}><i>increased by 1.5X</i></b></p>
    <p style={{fontSize:12,marginTop: 10}}>2. Investigation efforts <b style={{color: "green",
    fontSize: 13}}><i>Reduced 17%</i></b></p> */}
       
        </div>
        
        <br/><br/><br/>
        <div style={{margin:20}} >
        {/* <DvrIcon style={{fontSize: 30,marginLeft:8}} />     */}
         <h4 style={{color:"black",marginTop:-28}}> Investigation Summary - Overall</h4><br/>

        <Investigation_filters setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/><br/>
    
          <h4 style={{color:"black",fontSize:14}}> Post AI Implementation</h4><br/>
       <InvestigationKPICard setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/>
      <br/>
      
       <Grid  container spacing={1}  >
       {/* style={{marginLeft: 3}} */}
        <Grid item xs = {6}>
        <Paper elevation={3} className="fraudratio">
    
        <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}><center>  Open Claims by State </center>
       </p>
      
          <InvestidationClaimsByState setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/>
        
        </Paper>
        </Grid>
        
       
        <Grid item xs = {6} > 
        <Paper elevation={3} className="a" >
        
        <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}><center> Open claims by Month​ </center>
          </p>
       
        <InvestigationClaimsByMonth setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/>
        
        </Paper>
        </Grid>
</Grid>
<Grid  container spacing={1} style={{    marginTop: -44}} >
  {/*  style={{marginLeft: 3}} */}
        <Grid item xs = {6} > 
        <Paper elevation={3} className="a" >
       
        <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}><center> Open claims by Claim Type </center>
          </p>
     
        <InvestigationByClaimType setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/>
      
        </Paper>
        </Grid>

        <Grid item xs = {6} > 
        <Paper elevation={3} className="a" >
        
        <p  style={{color:"black",fontWeight:'bold',fontSize:16,backgroundColor:"Gainsboro",
    height: 31,
    padding: 6}}><center> Open claims by Assigned Investigation Status​ </center>
          </p>
       
        <InvestigationComboChart setState={setState} getState={getState} setClaimType={setClaimType} getClaimType={getClaimType}
      setClaimAmount={setClaimAmount} getClaimAmount={getClaimAmount} setFromRptDate={setFromRptDate} 
      getFromRptDate={getFromRptDate} setToRptDate={setToRptDate} getToRptDate={getToRptDate}/>
       
        </Paper>
        </Grid>

        </Grid>
        <FunnelChart/>
        </div>
        </div>
        </div>
    )
}

export default InvestigationOverview
