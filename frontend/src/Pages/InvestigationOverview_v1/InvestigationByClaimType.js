import  React , {useEffect , useState,useRef} from 'react';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import Investigation_Data from './investigation_summary_dict.json'


am4core.useTheme(am4themes_animated);
const CHART_ID = "InvestigationClaimsByClaimStatus";
 function InvestigationByClaimType(props) {
    const chartRef = useRef(null);
 

     const [data,setData]=useState(Investigation_Data);
     const selectedState = props.getState;
    const selectedToRptDate=props.getToRptDate;
    const selectedClaimType=props.getClaimType;
    const selectedClaimAmount=props.getClaimAmount;
    const selectedFromRptDate = props.getFromRptDate;

    useEffect(() => {
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({"reporting_view_filters" :  
  
        {"CLAIMREPORTDATE" : [{"from":  selectedFromRptDate,  
  
                            "to": selectedToRptDate}], 
  
        "STATE" : selectedState,  
  
        "CLAIM_TYPE": selectedClaimType,  
  
        "CLAIM_AMT_USD": selectedClaimAmount
  
      } 
  
      } )
      };
      fetch('/reporting_overview_investigation_v2', requestOptions)
      .then(res => res.json()).then((data) => setData(data));
    },[selectedState,selectedFromRptDate,selectedToRptDate,selectedClaimType,selectedClaimAmount]);

  //    useEffect(() => {
  // fetch('/invesigation_overview').then(res => res.json()).then((data) => setData(data));
  // },[]);
    
       var sample =  data === undefined ||  data === null ||  Object.keys(data).length <= 0
?  null : data.under_investigation_summary.charts.open_claims_by_clm_type.map((a) => {
  a.color = a["Claim Type"] === 'Vandalism' ? am4core.color("#8878c3") :  a["Claim Type"] === 'Theft'
   ? am4core.color("#6a5acd") : a["Claim Type"] === "Fire" ? am4core.color("#663399")
   :  a["Claim Type"] === "Natural Calamities" ? am4core.color("#9370db")
   :  a["Claim Type"] === "TPBI" ? am4core.color("#e0b0ff")
  
  : am4core.color("#235789")
  
})

console.log(data)
    
    useEffect(() => {
        var chart = am4core.create(CHART_ID, am4charts.PieChart);

        // chartRef.current = am4core.create(CHART_ID, am4charts.PieSeries);
        chart.data = Object.keys(data).length > 0 ? data.under_investigation_summary.charts.open_claims_by_clm_type : "Loading";
        

        let pieSeries = chart.series.push(new am4charts.PieSeries());
         pieSeries.dataFields.category = "Claim Type";
        pieSeries.dataFields.value = "Claims";
        pieSeries.labels.template.fontSize=8.8
        pieSeries.labels.template.disabled = true;

      
     
        pieSeries.tooltip.disabled = false;
        pieSeries.ticks.template.disabled = true;
        pieSeries.slices.template.propertyFields.fill = "color";
        chart.legend = new am4charts.Legend();
        chart.legend.position = "right";
        chart.legend.fontSize="10"
        //  chart.exporting.menu = new am4core.ExportMenu();
        //  chart.exporting.menu.align = "right";
        //  chart.exporting.menu.verticalAlign = "top";
      
        //   return () => {
        //    chart &&chart.dispose();
        //   };
        
      
        // Load data into chart
    
      })
      
        return (
          <div
            id={CHART_ID}
            style={{ width: "100%", height: "200px", margin: "20px 0" }}
          >
            {" "}
          </div>
        );
  
        }
export default InvestigationByClaimType;
