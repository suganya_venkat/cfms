import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { styled } from '@mui/material/styles';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import InfoIcon from '@mui/icons-material/Info';
import './InvestigatedPage.css'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    border: `1px dotted  #B0BEC5`,
    borderRadius: 4,
    color: "#949FAF",
    minWidth: 275,
    minHeight: 70,
    marginTop: 12,
    marginLeft: 12,
    marginRight: 12,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    paddingBlock: 20,
    fontSize: 14,
    borderRadius: 10.5,
  },
  card: {
    color: "#16448F",
    fontWeight: 700,
    fontSize: 14,
    display: "inline-block"

  },
  cardsmall:{
    color: '#5DC9A2',
    fomtWeight: 500,
    display: "inline-block",
    fontSize: 13,
    marginInline: 4 
  }
}));


const HtmlTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#e6e6fa',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 400,
    height:60,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
    padding:"10px"
  },
}));
const ToolTipInvestigation = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#e6e6fa',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 400,
    height:80,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
    padding:"10px"
  },
}));
// const fetchURL = "http://localhost:5000/insights/tccspv_kpi";
// const fetchURLcost = "http://localhost:5000/insights/cost/kpi_cost_overview";
// const fetchforecast = "http://localhost:5000/insights/forecast/kpicard";

export default function InvestigationKpiCards(props) {
    const classes = useStyles();

    
  
  
// console.log(data.under_investigation_summary.charts.open_claims_by_investigation_status);

  // console.log(forecasts);

  return (
    <div>
         <Grid container alignItems="center" spacing={1}>
            <Grid item xs={12} sm={3}>
              <Paper className={classes.paper}>
                {" "}
                <div  className="impactSummaryInfoContainer">
            <div className="impactSummaryHeadingFraudRate">   Overall Fraud Capture Rate</div><br /><br />{" "}
            <span className="impactSummaryHeadingFraudRateInfo">
       <HtmlTooltip  placement="top-start"
        title={
          <React.Fragment>
            <p style={{fontSize:12}}><i>
         
            ​Fraud Capture Rate  = Total Frauds Identified/ Total Claims​</i></p> <br/>
 
               </React.Fragment>
        }
      >
        <InfoIcon className="impactSummaryInfoIcon" />
      </HtmlTooltip>
     
      </span>
      
     
     </div>
           
                <div className={classes.card} >16%</div>
                <span className="impactSummaryKPIStyle">+1.5x</span>{" "}
              </Paper>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Paper className={classes.paper}>
                {" "}
                <div  className="impactSummaryInfoContainer">
                <div className="impactSummaryHeading">    Total Investigations </div><br /><br />{" "}
                <span className="impactSummaryInfo">
       <ToolTipInvestigation  placement="top-start"
        title={
          <React.Fragment>
            <p style={{fontSize:12}}><i>
          <b>  Overall Fraud Capture Rate: </b><br/>
          Total Investigations is total of Claims Investigated from Model recommendations and Non-Recommendations​

​​</i></p> <br/>
 
               </React.Fragment>
        }
      >
        <InfoIcon className="impactSummaryInfoIcon" />
      </ToolTipInvestigation>
     
      </span>
      
     
     </div>
           
                <div className={classes.card} >
               40%
                </div>
                <span className="impactSummaryKPIStyle">-10%</span>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Paper className={classes.paper}>
                {" "}
                <div  className="impactSummaryInfoContainer">
                <div className="impactSummaryHeading">     Loss Cost Savings​   </div><br /><br />{" "}
                <span className="impactSummaryInfo">
       <HtmlTooltip  placement="top-start"
        title={
          <React.Fragment>
            <p style={{fontSize:12}}><i>
        
            Total of Claimed amount saved due to identification of claims as fraudulent​

​
​​</i></p> <br/>
 
               </React.Fragment>
        }
      >
        <InfoIcon className="impactSummaryInfoIcon" />
      </HtmlTooltip>
     
      </span>
      
     
     </div>
                <div className={classes.card} > $23M</div>
                <span className="impactSummaryKPIStyle">+92%</span>
              </Paper>
            </Grid>
         </Grid>
   
   
          </div>
         
        //  </div> 
   
  );
}
