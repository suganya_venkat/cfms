import * as React from 'react';
import { useState } from "react";
import {createContext} from "react";
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import claimsInfoData from './claimsInfo.json'
import Grid from "@material-ui/core/Grid";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import './UploadDocument.css';
import DocumentProcessed_Table from './DocumentProcessed_Table';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';

const ClaimNumberFilter = createContext();

export default function UnstructureModelPageFilter(props) {
 
 
  
 
  const [Model, setModel] = React.useState('');
  const handleChange = (event) => {
    props.setmodel_value(event.target.value);
    
  };

const [claimNumber, setClaimNumber] = React.useState('')
  const selectedClaimNumber = (event) =>{
    setClaimNumber(event.target.value)
  }
  console.log(claimNumber)
  // const model_value = Model
  // console.log(model_value);
  const cn = ["c002","c003","c004"]
  const state_names = ['all','pahang', 'penang', 'perak', 'johor', ' malacca', 'pertis', 
'selangor', 'kedah', 'negeri sembilan', 'kelantan']
// const Reported_Daterange = ['2016-06-17 00:00:00', '2021-12-11 00:00:00']
const [selectedFromDate, setSelectedFromDate] = useState("2016-06-17 00:00:00");
  const [selectedToDate, setSelectedToDate] = useState("2021-12-11 00:00:00");

  return (
    <div>
    <div className='pagefilter_div'>
    {/* <Grid container>
    <Grid item xs = {4}> */}
    <Box  className='filter_flex'>
    <MuiPickersUtilsProvider utils={DateFnsUtils} >

<KeyboardDatePicker
  label="Reported Date From"
  value={selectedFromDate}
  // onChange={handleFromDateChange}
/>


</MuiPickersUtilsProvider>
</Box>

<Box className='filter_flex'>
    <MuiPickersUtilsProvider utils={DateFnsUtils} >


<KeyboardDatePicker
  label="Reported Date To"
  value={selectedToDate}
  // onChange={handleToDateChange}
/>

</MuiPickersUtilsProvider>
</Box>
      {/* </Grid> */}
      {/* <Grid item xs = {4}> */}
     <Box >
      <FormControl  className='filter_flex'>
        <InputLabel id="State">State</InputLabel>
        <Select 
          labelId="State"
          id="State"
          displayEmpty
          value={props.model_value}
          label="State"
          onChange={handleChange}
          className="filter_length"
        >
          {/* <MenuItem value=""></MenuItem>
          <MenuItem value={"Model1"}>Model 1</MenuItem>
          <MenuItem value={"Model2"}>Model 2</MenuItem>
          <MenuItem value={"Model3"}>Model 3</MenuItem> */}
           {
            state_names.map((d) => {
             
                return ( 
            <MenuItem value={d}>{d}</MenuItem>
                )
              })
              
               
}
        </Select>
        
      </FormControl>
      </Box> 
      {/* </Grid>
      <Grid item xs = {4}> */}
       <Box >
      <FormControl  className='filter_flex'>
        <InputLabel id="Severity Type">Severity Type</InputLabel>
        <Select 
          labelId="Severity Type"
          id="Severity Type"
          displayEmpty
          value={props.model_value}
          label="Severity Type"
          onChange={handleChange}
          className="filter_length"
        >
          <MenuItem value=""></MenuItem>
          <MenuItem value={"Minor Damage"}>Minor Damage</MenuItem>
          <MenuItem value={"Major Damage"}>Major Damage</MenuItem>
          <MenuItem value={"Total Loss"}>Total Loss</MenuItem>
        </Select>
        
      </FormControl>
    </Box>
    {/* </Grid>
   </Grid> */}
            
     
    </div>
    <br/>
    <div className='pagefilter_div'>
    <Box    >
      <FormControl  className='filter_flex'>
        <InputLabel id="Claim Number">Claim Number</InputLabel>
        <Select 
          labelId="Claim Number"
          id="Claim Number"
          displayEmpty
          value={props.model_value}
          label="Claim Number"
          onChange={selectedClaimNumber}
          className="filter_length"
        >
          {/* <MenuItem value=""></MenuItem> */}
          {
            cn.map((d) => {
             
                return ( 
            <MenuItem value={d}>{d}</MenuItem>
                )
              })
              
               
}
          
        </Select>
        
      </FormControl>
      </Box>
     

      {claimNumber ? 
    <Card sx={{ Width: 20 }} className="claims_card">
                <CardContent>
                  <Typography sx={{ fontSize: 14, fontWeight:"bold" }} color="text.secondary" gutterBottom>  
                Claims Information
                <br/>
               
                
                {claimNumber ? "Reported Date :" : "" }     
                  {
            claimsInfoData.map((d) => {
                    if( d.Claims_no === claimNumber)
              return ( 
                
        d.claims_reported_date
                 
                )
              })
  }
  <br/>
  {claimNumber ? "Claim Loss Date :" : "" }
 
{
            claimsInfoData.map((d) => {
                    if( d.Claims_no === claimNumber)
              return ( 
                
        d.claims_loss_date
                 
                )
              })
  }
   <br/>
   {claimNumber ? " Claim Amount : " : "" }
  
  {
            claimsInfoData.map((d) => {
                    if( d.Claims_no === claimNumber)
              return ( 
                
        d.Claim_amount
                 
                )
              })
  }
                  </Typography>
                 
                </CardContent>
                
              </Card>
              :  <Card sx={{ Width: 20 }}>
              <CardContent>
                <Typography sx={{ fontSize: 14, fontWeight:"bold" }} color="text.secondary" gutterBottom>  
              Claims Information : 
              <br/>
              No Claim Number selected
              </Typography>
              </CardContent>
              </Card>
               } 
          </div>
          <ClaimNumberFilter.Provider value={claimNumber}>
        <DocumentProcessed_Table/>
        </ClaimNumberFilter.Provider>
    </div>
  );
 
}

export {ClaimNumberFilter};
