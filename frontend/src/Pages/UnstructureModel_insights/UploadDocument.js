import React from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import  './UploadDocument.css'
import { styled } from '@mui/material/styles';
import { blue } from '@mui/material/colors';
import { createTheme, ThemeProvider } from '@mui/material/styles';

import Upload_Document from './Upload_Document';


// import PhotoCamera from '@material-ui/icons/PhotoCamera';
// import IconButton from '@material-ui/core/IconButton';

function UploadDocument() {

    // const ColorButton = styled(Button)(({ theme }) => ({
    //     color: theme.palette.getContrastText(blue[500]),
    //     backgroundColor: blue[500],
    //     '&:hover': {
    //       backgroundColor: blue[700],
    //     },
    //   }));
      
    //   function myFunction() {
    //     window.location.href="http://programminghead.com";  
    //   }

	// const theme = createTheme({
	// 	palette: {
	// 	  primary: {
	// 		// Purple and green play nicely together.
	// 		main: blue[500],
	// 	  },
	// 	  secondary: {
	// 		// This is green.A700 as hex.
	// 		main: '#11cb5f',
	// 	  },
	// 	},
	//   });

return (
	// <div style={{
	// display: 'flex',
	// margin: 'auto',
	// width: 400,
	// flexWrap: 'wrap',
	// }}>
    
        <div className='upload'>
	{/* <div style={{ width: '100%', float: 'left' }}>
		<h3>How to use create button to choose file in ReactJS?</h3> <br />
	</div> */}
	{/* <input
		type="file"
		accept="image/*"
		style={{ display: 'none' }}
		id="contained-button-file"
	/> */}
	<label htmlFor="contained-button-file">
	
		<Button onClick={() => <Upload_Document/>} component={Link} to='/Upload_Document'  style={{
      
	  backgroundColor: "#00965f",
	  color:"white"
  }}   >
		Upload Document
		</Button>
		
	</label>
	{/* <h3> OR </h3> */}
	{/* <input accept="image/*" id="icon-button-file"
		type="file" style={{ display: 'none' }} />
	 */}
    
		{/* <IconButton color="primary" aria-label="upload picture"
		component="span">
		<PhotoCamera />
		</IconButton> */}
	
	</div>
);
}

export default UploadDocument;
