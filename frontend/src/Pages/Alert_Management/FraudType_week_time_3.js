//week_time_3

import React, { useRef, useState, useEffect } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import MaterialTableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Data from './Alert_Management.json';
import TablePagination from '@mui/material/TablePagination';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import RemoveRedEyeOutlinedIcon from '@mui/icons-material/RemoveRedEyeOutlined';
import { makeStyles, withStyles } from "@material-ui/core/styles";
import {Link} from 'react-router-dom';
import dateFormat from "dateformat";
import AlertManagementData from './alert_management_dict.json'
import './Alert_Management.css'
import { styled } from '@mui/material/styles';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import InfoIcon from '@mui/icons-material/Info';
import Crop169SharpIcon from '@mui/icons-material/Crop169Sharp';
const HtmlTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#e6e6fa',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 220,
    height:90,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
    padding:"10px"
  },
}));

const TableCell = withStyles({
  root: {
    borderBottom: "none" ,
    borderRightStyle: "solid",
    borderRightColor:"azure",
    borderRightWidth: "thin",

  }
})(MaterialTableCell);

export default function FraudType_week_time_3() {
    
 const [newdata,newsetData]=useState(AlertManagementData);
    
     useEffect(() => {
  fetch('/alert_management_frdtyp').then(res => res.json()).then((newdata) => newsetData(newdata));
  },[]);

  // console.log(newdata);

  var sample =  newdata.week_time_3 === undefined ||  newdata.week_time_3 === null || newdata.week_time_3.length <=0 
  ?  null : newdata.week_time_3.map((a) => {
    a.FraudTpe = a.rc_bucket === 'week_time_3' ? 'Accident occurred on weekends' :
    
    a.rc_bucket
  })


const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [dense, setDense] = React.useState(false);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleChangeDense = (event) => {
    setDense(event.target.checked);
  };
  const gettingIndex  = (index) =>{
    window.location.href = '/ClaimsDetails';
console.log(index)
  }
// console.log(Data.expire_loss_cr_Low)
  
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="a dense table sticky table"
      size= 'small'
      >
        <TableHead>
          <TableRow style={{ backgroundColor: "Gainsboro" }}className="alertsCell"> 
          {/* Gainsboro #E8E8E8 */}
          <TableCell className="alertsCell">Fraud Type</TableCell>
            <TableCell align="center" style={{borderBottom: "none"}} className="alertsCell">Claim Number</TableCell>
            <TableCell align="center" style={{borderBottom: "none"}} className="alertsCell">Severity
            <HtmlTooltip  placement="right-start"
        title={
          <React.Fragment>
           
           <Crop169SharpIcon style={{fontSize:10, width:150, backgroundImage: "linear-gradient(to right,red, yellow)",color:"transparent"}}/>
           <div ><b>High</b></div> 
           <div  style={{ textAlign:"end",width:150,marginTop:-12}}><b>Low</b></div> <br/>
       <div  style={{ whiteSpace: "nowrap",fontSize:10}}><b>Severity signifies likelihood of fraud</b></div> 


          </React.Fragment>
        }
      >
        <InfoIcon style={{fontSize:13,color:"#8d848d"}}/>
      </HtmlTooltip>
            </TableCell>
            <TableCell align="center" style={{borderBottom: "none"}}className="alertsCell">Report Date</TableCell>
            <TableCell align="center" style={{borderBottom: "none"}} className="alertsCell">Claimed Amount ($)</TableCell>
           
            <TableCell align="center"style={{borderBottom: "none"}} className="alertsCell">Assigned Resource</TableCell>
          </TableRow>
        </TableHead >
        <TableBody>
        {newdata.week_time_3 === undefined ||  newdata.week_time_3 === null || newdata.week_time_3.length <=0 ? "Loading" :
                newdata.week_time_3.sort((a, b) => b.severity - a.severity).slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row,index) => ( 
  
            <TableRow
            key={index}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell style={{borderBottom: "none",width: 221,color:"purple",fontWeight:"bold",fontSize:14}} component="th" scope="row" >
              {index === 0 ? row.FraudTpe: ""}
              </TableCell>
              {/* <TableCell align="center"><RemoveRedEyeOutlinedIcon className='icon' onClick={(e) => {gettingIndex(row.claim_number); }}fontSize='small'/>{row.claim_number}</TableCell> */}
              <TableCell align="center" style={{borderBottom: "none"}}> 
              <Link to={{
  pathname: '/ClaimsDetails',
  state:  row.claim_number
}} ><RemoveRedEyeOutlinedIcon className='viewicon' fontSize='small'/></Link>  {row.claim_number}  
              </TableCell>
        {row.severity === 1 ?  <TableCell align="center" style={{borderBottom: "none"}}><span className='Severitydot1'></span></TableCell> :  
      row.severity === 2 ? <TableCell align="center" style={{borderBottom: "none"}}><span className='Severitydot2'></span></TableCell>:
      row.severity === 3 ? <TableCell align="center" style={{borderBottom: "none"}}><span className='Severitydot3'></span></TableCell>:
      row.severity === 4 ? <TableCell align="center" style={{borderBottom: "none"}}><span className='Severitydot4'></span></TableCell>:
      row.severity === 5 ? <TableCell align="center" style={{borderBottom: "none"}}><span className='Severitydot5'></span></TableCell>:
      row.severity === 6 ? <TableCell align="center" style={{borderBottom: "none"}}><span className='Severitydot6'></span></TableCell>:
      row.severity === 7 ? <TableCell align="center" style={{borderBottom: "none"}}><span className='Severitydot7'></span></TableCell>:
      row.severity === 8 ? <TableCell align="center"style={{borderBottom: "none"}}><span className='Severitydot8'></span></TableCell>:
      row.severity === 9 ? <TableCell align="center"style={{borderBottom: "none"}}><span className='Severitydot9'></span></TableCell>:
      row.severity === 10 ? <TableCell align="center"style={{borderBottom: "none"}}><span className='Severitydot10'></span></TableCell>:
    
      <TableCell align="center"style={{borderBottom: "none"}}>{row.severity}</TableCell>}
              <TableCell align="center"style={{borderBottom: "none"}}>{dateFormat(row.claim_report_date, "yyyy-mm-dd")}</TableCell>
              <TableCell align="center"style={{borderBottom: "none"}}>{row.claim_amt_usd.toLocaleString("en", {   
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
})}</TableCell>
              <TableCell align="center"style={{borderBottom: "none"}}>{row.assigned_resource}</TableCell>

            </TableRow>
         

          ))}
        </TableBody>
      </Table>
      {/* <FormControlLabel
        control={<Switch checked={dense} onChange={handleChangeDense} />}
        label="Dense padding"
      /> */}
      <TablePagination
        rowsPerPageOptions={[5,10, 25, 100]}
        component="div"
        count={newdata.week_time_3 ? newdata.week_time_3.length : ""}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
     
    </TableContainer>
    
  );
}
