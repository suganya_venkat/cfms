import React from 'react'
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Stack from '@mui/material/Stack';
import Configuration_navbar from '../../Components/Configuration_navbar'
import MainNavBar from '../../Components/MainNavBar';
import NewTheme from '../../Components/newTheme.js';
import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import Alert_Management from './Alert_Management_Page'
import FraudType_is_self_accident from './FraudType_is_self_accident'
import FraudType_expire_loss_cr_Low from './FraudType_expire_loss_cr_Low'
import FraudType_week_time_3 from './FraudType_week_time_3'
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import SeverityGrad from '../../assets/images/SeverityGrad.png'
import AlertManagementBe from '../../assets/images/AlertManagementBe.png'
import AlertManagementProcessBar from '../../assets/images/AlertManagementProcessBar.png'
import ProgressBarTop from '../../assets/images/ProgressBarTop.PNG'
import CardMedia from "@material-ui/core/CardMedia"
import { makeStyles } from "@material-ui/core/styles";
import Popover from '@mui/material/Popover';
import Typography from '@mui/material/Typography';
import './Alert_Management.json'
import './Alert_Management.css'
import { styled } from '@mui/material/styles';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import InfoIcon from '@mui/icons-material/Info';
import '../CommonStyling/Style.css'

const HtmlTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#e6e6fa',
    color: 'rgba(0, 0, 0, 0.87)',
    width: 300,
    height:60,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
    padding:"10px"
  },
}));

function AlertManagement() {

  // window.localStorage.setItem("id",3)
  // console.log(window.localStorage)
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;


  const useStyles = makeStyles(theme => ({
   
    cardMediaStyle: {
    // paddingTop: "5%",
      // width:'50px'
      width: '123px',
    height: '31px',
    marginLeft: '940px'
    }
  }));
  const styles = useStyles();

    return (
        <div className='divalign'>
          
  <MainNavBar/>
  {/* <CardMedia
     className='progressTopAlert'   
          image={ProgressBarTop}

 /> */}
{/* <NewTheme/> */}
 {/* <div className='activeLine' ></div> */}
<br/>
<div  className="infoHeadingContainer">
<div style={{margin:8}}> 
{/* <ManageSearchIcon style={{fontSize: 47,cursor: "pointer"}}  onClick={handleClick} /> */}
<h3 style={{color:"black",marginLeft:8,marginTop:27}} >Investigation and Alerts</h3></div>
<span className="Alertinfo">
       <HtmlTooltip  placement="right-start"
        title={
          <React.Fragment>
              <p style={{color:"black",marginLeft:10}}><i>Suspicious claims identified through fraud detection engine</i></p>
          </React.Fragment>
        }
      >
        <InfoIcon className="infoIcon" />
      </HtmlTooltip>
     
      </span>
      
     
     </div>

 <br/>
<i style={{fontSize:12,marginLeft:16}}> Click on claim number to start claims evaluations </i><br/>
        {/* <h6 style={{marginLeft:10}}>User id : USER123</h6> */}
        
        <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
         <CardMedia
          className='cardMediaStylepopup'   
              image={AlertManagementBe}

/>
        {/* <Typography sx={{ p: 2 }}>The content of the Popover.</Typography> */}
      </Popover>
    {/* <div  >  <CardMedia
          className={styles.cardMediaStyle}    
              image={SeverityGrad}

/>
      </div> <div className= "gradData" style={{ whiteSpace: "nowrap"}}>Severity signifies likelihood of fraud</div>  */}
    {/* <Stack direction="row" spacing={2}>
  <p>  <p className='dot1'>low</p>  </p>
  <p className='dot2'>     <p>High</p></p>
  <p className='dot3'>      <p>High</p></p>
</Stack>
   */}
      
      <CardContent>
    <Alert_Management/>
    </CardContent>
    <CardContent>
     <FraudType_is_self_accident/>
     </CardContent>
     <CardContent>
     <FraudType_expire_loss_cr_Low/>
     </CardContent>
     <CardContent>
     <FraudType_week_time_3/>
     </CardContent>
   
     </div>
    )
}

export default AlertManagement
