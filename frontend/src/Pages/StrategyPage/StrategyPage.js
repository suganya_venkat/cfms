import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import MaterialTableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles, withStyles } from "@material-ui/core/styles";
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@mui/material/Button';
import Box from '@material-ui/core/Box';
import TextField from '@mui/material/TextField';
import Alert from '@mui/material/Alert';
import AlertManagement from '../Alert_Management/AlertManagement'
import { Link } from 'react-router-dom';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import Configuration_navbar from '../../Components/Configuration_navbar'
import MainNavBar from '../../Components/MainNavBar';
import './StrategyPage.css';

const TableCell = withStyles({
  root: {
    borderBottom: "none",
    padding: '4px 8px'
  }
})(MaterialTableCell);

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto"
  },
  table: {
    minWidth: 650,
    // borderCollapse: 'separate',
    // borderSpacing: '0px 4px'
  }
}));



var RuleVariable1=[]
var RuleVariable2= []
var RuleModel3=[]
var RuleOperator1=[]
var RuleOperator2=[]

var RuleValue1=[]
var RuleValue2=[]

var FinalData =[]
var FinalDataToBePassed1=[]
var FinalDataToBePassed
var error_message = "Please Enter All values"
var SumOfWeights = 1
export default function StrategyPage() {

   

    const [variable, setVariable] = React.useState('');
    const [operator, setOperator] = React.useState('');
    const [Relation, setRelation] = React.useState('');
    const [operator1, setOperator1] = React.useState('');
    const [variable1, setVariable1] = React.useState('');
    // const [operator2, setOperator2] = React.useState('');
     const [model3, setModel3] = React.useState('');
    const [value, setvalue] = React.useState('');
    const [value1, setvalue1] = React.useState('');
    // const [value2, setvalue2] = React.useState('');
      const [save, setSave] =  React.useState("");
      const [validate, setValidate] =  React.useState(false);

      const [buttonDisabled, setbuttonDisabled] = React.useState(false);






  const variableSelected = (event) => {
    setVariable(event.target.value);
    RuleVariable1.push(event.target.value);
    // FinalData.push({"rule1":{"Variable": RuleVariable1.toString()}})
  };
  
 

  const variableSelected1 = (event) => {
    setVariable1(event.target.value);
    RuleVariable2.push(event.target.value);
    // FinalData.push({"rule2":{"Variable": RuleVariable2.toString()}})
  };

  const modelSelected3 = (event) => {
    setModel3(event.target.value);
    RuleModel3.push(event.target.value);
    // FinalData.push({"rule2":{"Variable": RuleVariable2.toString()}})
  };

  const valueSelected = (event) => {
    setvalue(event.target.value);
    RuleValue1.push(event.target.value)
      
      ;
      event.preventDefault();
    // FinalData.push({"rule1":{"Value": RuleValue1.toString()}})

  };
 
  const valueSelected1 = (event) => {
    setvalue1(event.target.value);
    RuleValue2.push(event.target.value)
    event.preventDefault();
  };

//   const valueSelected2 = (event) => {
//     setvalue2(event.target.value);
//     RuleValue3.push(event.target.value)
//     event.preventDefault();
//   };
  // console.log(RuleValue1[RuleValue1.length -1])

const actionDelete = ()=>{
 setVariable("")
 setvalue("")
 setVariable1("") 
 setvalue1("")
 setModel3("")
}

console.log(RuleVariable1)



  const passingData = () =>{


     
if(SumOfWeights === Number(value)+Number(value1))

{
  setValidate(false)

    if(variable, value){
      
    FinalData.push({"Type": RuleVariable1.toString(),
     "Weight": RuleValue1[RuleValue1.length-1].toString()
    
  
  
  })

  RuleVariable1=[]
  RuleValue1=[]
 
}
  if(variable1, value1){
    FinalData.push({
  "Type": RuleVariable2.toString(), "Weight": RuleValue2[RuleValue2.length-1].toString()
} )
  
RuleVariable2=[]
RuleValue2=[]

  
}

if(model3){
  FinalData.push({
"Type": RuleModel3.toString(), "Weight": " "
} )

RuleModel3=[]



}
const requestOptions = {
  method: "POST",
  headers: { "Content-Type": "application/json" },
  body: JSON.stringify(FinalData),
};

fetch('/apply_strategy ', requestOptions).then((res) =>
  setSave(res.status),
   setbuttonDisabled(true)                                        
);

FinalData = ['']
// useEffect(() => {
//   fetch('/claim_level_data', requestOptions).then((res) => res.json()).then((newdata) => newsetData(newdata)
                                
//   )},[Comment]);


}

else{
  setValidate(true)
}



console.log(FinalData)
// fetch('/add_rules', {  // Enter your IP address here

//   method: 'POST', 
//   headers: { "Content-Type": "application/json" },
// //   mode: 'cors', 
//   body: JSON.stringify(FinalData) // body data type must match "Content-Type" header

// })

  
    
//       save === 200 ? setbuttonDisabled(true) : setbuttonDisabled(false)
 



 
  };
      
  
  
  


 
  return (
      <div>
        <MainNavBar/>
          <Configuration_navbar/>
           {/* <Rule_navbar/> */}
<br/>

    <TableContainer component={Paper}>
      <Table sx={{ Width: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow  style={{
                    height: (20) 
                  }}>
            
            <TableCell align="center" >Type</TableCell>
            <TableCell align="center" >Weight</TableCell>
            <TableCell align="center" >Action</TableCell>

        
         
          </TableRow>
        </TableHead>
        <TableBody>
         
            <TableRow
              // key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row" align="center" style={{borderBottom: "none"}}>
              <FormControl sx={{ m: 1 }}>
        <InputLabel id="Variables">Model</InputLabel>
        <Select
          labelId="Variables"
          id="Variable"
          required
          value={variable}
        //   sx={{ Width: 500 }}
          label="Model"
          onChange={variableSelected}
          style={{ 
        minWidth : 265,
        maxWidth : 300,
       
    }}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={"Supervised"}>Supervised</MenuItem>
          <MenuItem value={"anomaly"}>anomaly</MenuItem>
          <MenuItem value={"Business Rules"}>Business Rules</MenuItem>
          
        </Select>
        {/* <FormHelperText>With label + helper text</FormHelperText> */}
       </FormControl>
                  
                  </TableCell>
              <TableCell align="center" style={{borderBottom: "none"}} >
              <Box
      component="form"
      sx={{
        '& > :not(style)': { m: 1 },
      }}
      noValidate
      autoComplete="off"
    >
              <TextField id="Weights" label="Enter Weights"
              variant="outlined"
              required
              name="Weights"
              label="Weights"
              type="Weights"
              style={{ 
                minWidth : 265,
                maxWidth : 300,
              }}
              // autoFocus
              value={value}
              // onChange={(event) => {setPassword(event.target.value)}} //whenever the text field change, you save the value in state
              onChange={valueSelected}
              />

     </Box>
                  
                  </TableCell >
             
<TableCell align="center" style={{borderBottom: "none"}} rowSpan={2}>
              <DeleteOutlineIcon onClick={actionDelete} style={{ cursor: "pointer"}}/>
                  </TableCell>
            </TableRow>


           
      
         
            <TableRow
              // key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row" align="center" style={{borderBottom: "none"}}>
              <FormControl sx={{ m: 1 }}>
        <InputLabel id="Model">Model</InputLabel>
        <Select
          required
          labelId="Model"
          id="Model"
          value={variable1}
          label="Model"
          style={{ 
            minWidth : 265,
            maxWidth : 300,
          }}
          onChange={variableSelected1}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={"Supervised"}>Supervised</MenuItem>
          <MenuItem value={"anomaly"}>anomaly</MenuItem>
          <MenuItem value={"Business Rules"}>Business Rules</MenuItem>
          
        </Select>
        {/* <FormHelperText>With label + helper text</FormHelperText> */}
      </FormControl>
              </TableCell>
              
              <TableCell align="center" style={{borderBottom: "none"}}  >
              <Box
      component="form"
      sx={{
        '& > :not(style)': { m: 1 },
      }}
      noValidate
      autoComplete="off"
    >
              <TextField id="weights" label="Enter weights"
              variant="outlined"
              required
              name="weights"
              label="weights"
              type="weights"
              // autoFocus
              style={{ 
                minWidth : 265,
                maxWidth : 300,
              }}
              value={value1}
              // onChange={(event) => {setPassword(event.target.value)}} //whenever the text field change, you save the value in state
              onChange={valueSelected1}
              // error={value1 === ""}
              // helperText={value1 === "" ? 'Required!' : ' '}
              />

     </Box>
           </TableCell>
            
            </TableRow>
            <TableRow
              // key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row" align="center" style={{borderBottom: "none"}}>
              <FormControl sx={{ m: 1 }}>
        <InputLabel id="Model">Model</InputLabel>
        <Select
          required
          labelId="Model"
          id="Model"
          value={model3}
          label="Model"
          style={{ 
            minWidth : 265,
            maxWidth : 300,
          }}
          onChange={modelSelected3}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={"Supervised"}>Supervised</MenuItem>
          <MenuItem value={"anomaly"}>anomaly</MenuItem>
          <MenuItem value={"Business Rules"}>Business Rules</MenuItem>
        </Select>
        {/* <FormHelperText>With label + helper text</FormHelperText> */}
      </FormControl>
              </TableCell>
              </TableRow>
        </TableBody>
       
      </Table>
      <br/>
    {save === 200 ? <Alert onClose={() => <AlertManagement/>} component={Link} to='/AlertManagement' >Successfully created the Weights!</Alert>
    : ""}
                      {save != 200 && save ? <Alert severity="error">Weights are not created!</Alert>
    : ""}
    {validate  ? <Alert severity="error">Added weights are not 1.Please check the model weights</Alert>
    : ""}
      <div className='button'>
        
   {/* <Button variant="outlined">Test & Save</Button> */}
<Button variant="outlined" disabled = {buttonDisabled}  onClick={passingData}>Generate Claim Alerts</Button>
{/*  */}
        </div>
        <br/>
    </TableContainer>
   
        </div>
  );
}
