import './Navbar.css'
import React, {useState} from 'react'

import bi2ilogo from '../Components/bi2i.png'
import {NavLink} from 'react-router-dom'
import {FaBars, FaTimes} from 'react-icons/fa'
import HomeIcon from '@mui/icons-material/Home';
// import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
function AlertsNavBar() {
  
    return (
       <div>
       <nav className='rule_navbar'>

      
           
           
           <ul className=  'rule_nav-menu'>
              <li>
                <NavLink to = '/AlertManagement' className='Alert_nav-links'  >
              <div style={{display:"flex"}}>
              <ArrowBackIcon  style={{fontSize:18}}/> 
              <span style={{ flex: 1}}>
            <p style={{fontSize:18,marginTop:-2}}><b>Alerts</b></p></span>
                </div> 
                </NavLink>
            </li>
            {/* <li>
                <NavLink to = '/RuleBank' className='rule_nav-links' activeClassName="rule_is-active" >
                   Rule Bank
                </NavLink>
            </li> */}
           
            {/* <li>
                <NavLink to = ' ' className='nav-links' activeClassName="is-active" onClick={closeSideDrawer}>
                <HomeIcon/>
                </NavLink>
            </li> */}
            
           {/* <li>
                <NavLink to = '/Alertmanage' onClick={closeSideDrawer} className='nav-links' activeClassName="is-active" >
                    Insights
                </NavLink>
            </li> */}
            {/* <li>
                <NavLink to = '/StructureData_insights' onClick={closeSideDrawer} className='nav-links' activeClassName="is-active" >
                    Structured Data Model Insights
                </NavLink>
           </li>
           
           <li>
                <NavLink to = '/UnstructureModel_insights' onClick={closeSideDrawer} className='nav-links' activeClassName="is-active" >
                Unstructured(OCR) Data Insights
                </NavLink>
           </li>
           <li>
                <NavLink to = '/SignIn' onClick={closeSideDrawer} className='nav-links' activeClassName="is-active">
                    Sign in
                </NavLink>
           </li> */}
           </ul>
          
       </nav>
       </div>
    )
}

export default AlertsNavBar
