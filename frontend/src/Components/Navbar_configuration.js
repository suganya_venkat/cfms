import './Navbar.css'
import React, {useState} from 'react'

import bi2ilogo from '../Components/bi2i.png'
import {NavLink} from 'react-router-dom'
import {FaBars, FaTimes} from 'react-icons/fa'
import HomeIcon from '@mui/icons-material/Home';
import Configuration_icon from '../Pages/Configuration/Adjuster'

function Navbar_configuration() {
    // const [icon , setIcon] =useState(false)
    // const handleClick = () =>{
    //     setIcon (!icon)
    // }
    // const closeSideDrawer = () =>{
    //     setIcon(false)
    // }
    return (
       <>
       <nav className='rule_subnavbar'>
       {/* <NavLink to = '/Configuration_icon' className='nav-links' >
                <HomeIcon/>
                </NavLink> */}
           {/* <NavLink to = '/' className = 'nav-logo'>
           <img src={bi2ilogo} alt="bi2i" width="500" height="600"/> 
           Logo
           </NavLink>
            */}
           {/* <div className='menu-icon' onClick={handleClick}>
                {
                    icon ? <FaTimes className = 'fa-times' ></FaTimes> :
                    <FaBars className = 'fa-bars'></FaBars>
                }
           </div> */}
           <ul className= 
                'rule_nav-menu'>
            
            <li>
                <NavLink to = '/StructureData_insights' className='rule_nav-links' activeClassName="rulesubbar_is-active" >
                    Structured Data Model Insights
                </NavLink>
           </li>
{/*            
           <li>
                <NavLink to = '/UnstructureModel_insights' className='rule_nav-links' activeClassName="rule_is-active" >
                Unstructured(OCR) Data Insights
                </NavLink>
           </li> */}
           <li>
                <NavLink to = '/AnamolyPage' className='rule_nav-links' activeClassName="rulesubbar_is-active" >
                Anomaly Model
                </NavLink>
           </li>
           
           </ul>
          
       </nav>
       </>
    )
}

export default Navbar_configuration
