import './Navbar.css'
import React, {useState} from 'react'
import { useLocation } from 'react-router-dom';

import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import {NavLink} from 'react-router-dom'
import Stack from '@mui/material/Stack';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';

import HomeIcon from '@mui/icons-material/Home';
import Configuration_icon from '../Pages/Configuration/Adjuster'



function AnalyticsandReportNavBar() {
    // const [icon , setIcon] =useState(false)
    // const handleClick = () =>{
    //     setIcon (!icon)
    // }
    // const closeSideDrawer = () =>{
    //     setIcon(false)
    // }

    const { pathname } = useLocation();
    return (
       <>
       <nav className='navbar'>
       <NavLink to = '/Adjuster' className='nav-links' >
                <HomeIcon/>
                </NavLink>
       <p  style={{whiteSpace: "nowrap",fontWeight:700,color:"white",marginLeft:430}}  >
              Claims Fraud Management Desktop
                </p>
         
     
           {/* <NavLink to = '/' className = 'nav-logo-landing'>
          
           Logo
           </NavLink> */}
           
           {/* <div className='menu-icon' onClick={handleClick}>
                {
                    icon ? <FaTimes className = 'fa-times' ></FaTimes> :
                    <FaBars className = 'fa-bars'></FaBars>
                }
           </div> */}
           <ul className= 
                'nav-menu'>
            
            {/* <li>
                <NavLink to = '/AlertManagement'  className='nav-links' activeClassName="is-active" isActive={() => ['/ClaimsDetails','/AlertManagement',"/"].includes(pathname)} >
               Alert Management
                </NavLink>
           </li>
           <li>
                <NavLink to = '/BusinessRule'   className='nav-links' activeClassName="is-active" isActive={() => ['/Rules', '/StructureData_insights','/Strategy','/RuleBank','/StructureData_insights', '/UnstructureModel_insights','/AnamolyPage','/BusinessRule'].includes(pathname)} >
                   Analytics
                </NavLink>
           </li>
           
           <li>
                <NavLink to = '/Summary'   className='nav-links' activeClassName="is-active" isActive={() => ['/Summary', '/Investigation_overview',"/Claims_Forecast","/InvestigationOverview"].includes(pathname)} > 
                Reporting
                </NavLink>
           </li> */}
          

          
           {/* <li>
                <NavLink to = '/Notfound'  className='nav-links' activeClassName="is-active" >
               Forensic
                </NavLink>
           </li>
            */}
           </ul>
           <Stack spacing={1} style={{marginBottom: 15,marginRight: 13}}>
        <p className='iconalign1' style={{marginleft: 40}}><AccountCircleIcon style={{color:"white",marginTop: 15,marginRight: 20,fontSize: 28,textAlign:"center"}}/></p>
        <p style={{color:"white",fontSize: 14,whiteSpace: "nowrap",marginTop: -5}}> Paul </p>
        {/* Derek (Claims Manager) */}
        {/* <Item>Item 3</Item> */}
      </Stack>
           {/* <AccountCircleIcon style={{color:"grey",width:100}}/>
               
              
               Anirban, Admin */}
       </nav>
     
       
       </>
    )
}

export default AnalyticsandReportNavBar
