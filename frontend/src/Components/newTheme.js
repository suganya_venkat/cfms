
import React, { Component, useEffect } from "react";
// import { useNavigate } from "react-router";
import { useState } from "react";
import Popover from '@mui/material/Popover';
import CardMedia from "@material-ui/core/CardMedia"
import DataAggregationImage from '../assets/images/DataAggregationImage.png'
// ../../assets/images/ReportingBe.png
import "./newTheme.css";
import ProgressBarTop from '../assets/images/ProgressBarTop.PNG'

// import StepContent from '@mui/material/StepContent';
// import Themedata from  "./newTheme.json";
// import Themed from  "../assets/data/example.json";
// import Box from '@mui/material/Box';
// import Stepper from '@mui/material/Stepper';
// import Step from '@mui/material/Step';
// import appsdata from "../assets/data/example.json";
// import { ThemeProvider, createTheme, StepLabel } from "@mui/material";
// import newExampledata from "./newExample.json";
// import { id } from "date-fns/locale";
// import recommender from "../assets/recommender.png";
// import "../carousel/Carousel.css";
// import Underwriting from "./browse/underwriting/underwriting";
// import Carousel from "../carousel/Carousel";
//importing styles
// const themes = createTheme({
//   components: {
//     // Name of the component
//     MuiStepIcon: {
//       styleOverrides: {
//         // Name of the slot
//         "root": {
//           "cursor":"pointer",
//           '&:hover': {
//             " transform": "scale(1.14)"
//           },
//           "&.Mui-active": {
//             "color": "green"
//           },
//           "&.Mui-active:hover":{
//             " transform": "scale(1.14)"
   
//           } 
//         },
//         "text":{
//           "display":"none"
//         }
//       },
//     },
//   },
// });

// const theme = (image) => ({
//   liveDiv: {
   
//     "background-size": "25% 25%",
//   },
//   commingSoon: {
   
   
//     "background-color": "rgb(236, 236, 236)",
//   },
// });

 

const NewTheme = () => {
const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    console.log(event)
    setAnchorEl(event.currentTarget);
   
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;
    // const {name,notShow}=props;
    const processname = ["Upstream Data","Data Aggregation/Transformation","Analytics","Investigation & Alert Management","Insights & Reporting","Downstream Data"]
  //  const [name2,setName2]=useState("");
   const [active,setActive]=useState();
  //  const navigate = useNavigate();
  const navLink =["/BusinessRule","/AlertManagement","/Summary"]
  // console.log(window.localStorage)
  const changeStyle=(id)=>{
    setActive(id);
    window.location.pathname= navLink[id-2]
    window.localStorage.setItem("id",id)
 }
//  const dataTransform =(id)=>{
  
//  if(id == 1)
//  console.log(id)
// //  ocument.getElementById("demo").innerHTML = "Hello World";
//     {
   
//       // handleClick()

// }
//  }
  return (
    <>
    {/* <CardMedia
     className='progressTopAlert'   
          image={ProgressBarTop}

 /> */}
     <Popover
  id={id}
  open={open}
  anchorEl={anchorEl}
  onClose={handleClose}
  anchorOrigin={{
    vertical: 'bottom',
    horizontal: 'right',
  }}
  transformOrigin={{
    vertical: 'top',
    horizontal: 'left',
  }}
>
   <CardMedia
    className='cardMediaStylepopup'   
        image={DataAggregationImage}

/>
</Popover>
   
    <div style={{justifyContent:"center",alignItems:"center",height:99,padding:15,backgroundColor: "#e6e6fa"}}>
   {processname.length < 0 ? null:
      <div style={{display:"flex",width:"100%",height:"auto",padding:"10px",fontSize: "11px",wordWrap: "break-word",justifyContent:"center",alignItems:"center",zIndex:"1000",marginLeft: 19}} >
      {(() => {
        const k = [];

        for (let i = 0; i < processname.length; i++) {
          if(i <= window.localStorage.getItem("id") ){
            k.push(
         
              <>
              
           <div style={{display:"block"}}>
              <div style={{display:"flex",justifyContent:"center",alignItems:"center",width:"210px"}}>
        
        <div  className="circle" style={{backgroundColor:active==i ||
         window.localStorage.getItem("id") == 0 ||  window.localStorage.getItem("id") == 1 || 
         window.localStorage.getItem("id") == 2 ||  window.localStorage.getItem("id") == 3 || 
         window.localStorage.getItem("id") == 4  
         ?"#552586":"grey",pointerEvents:   i == 0  ? "none" : "auto",
            transform:active==i?"scale(1.2)":null}} id={"id-"+processname[i]}  onClick={(event)=>{  i === 1 ? handleClick(event) :changeStyle(i)}
        } >
        </div>
        
        <div style={{width:"190px",height:"1px",backgroundColor:"grey",borderRadius:"0%"}}></div>
        </div>
        <div style={{height:"9px",margin:"0",padding:"0"}}></div>
        {processname[i] === "Analytics" ?
        <span style={{position:"relative",textAlign:"center",left:-14}}>{processname[i]}</span>

:processname[i] === "Investigation & Alert Management"
?
<span style={{position:"relative",textAlign:"center",left:-60}}>{processname[i]}</span>

:processname[i] === "Insights & Reporting"
?
<span style={{position:"relative",textAlign:"center",left:-35}}>{processname[i]}</span>

:processname[i] === "Upstream Data"?
<span style={{position:"relative",textAlign:"center",left:-28}}>{processname[i]}</span>
          :
          processname[i] === "Data Aggregation/Transformation"?
          <span style={{position:"relative",textAlign:"center",left:-62}}>{processname[i]}</span>
                   
         : <span style={{position:"relative",textAlign:"center",left:-35}}>{processname[i]}</span>
       
          }

        </div>
        
          {/* style={{marginLeft:"14vw",marginTop:"0px",marginBottom:"0px"}} */}
            {/* <div style={{marginLeft:"0px",marginTop:"0px",marginBottom:"0px"}}>{constructCarousel(data)}</div> */}
          
            </>
            // </Step>
          );}
          
          else if(i!=processname.length-1){
          k.push(
         
            <>
         <div style={{display:"block"}}>
            <div style={{display:"flex",justifyContent:"center",alignItems:"center",width:"210px"}}>
      
      <div  className="circle" style={{backgroundColor:active==i   ?"#552586":"grey",
      pointerEvents: i == 0 || i == 1 ? "none": "auto"
      ,transform:active==i?"scale(1.2)":null}} id={"id-"+processname[i]} onClick={()=>{i == 1 ?  handleClick() :changeStyle(i)}
      } >
      </div>
      
      <div style={{width:"190px",height:"1px",backgroundColor:"grey",borderRadius:"0%"}}></div>
      </div>
      <div style={{height:"9px",margin:"0",padding:"0"}}></div>
      {processname[i] === "Analytics" ?
        <span style={{position:"relative",textAlign:"center",left:-14}}>{processname[i]}</span>

:processname[i] === "Investigation & Alert Management"
?
<span style={{position:"relative",textAlign:"center",left:-60}}>{processname[i]}</span>

:processname[i] === "Insights & Reporting"
?
<span style={{position:"relative",textAlign:"center",left:-35}}>{processname[i]}</span>

:processname[i] === "Upstream Data"?
<span style={{position:"relative",textAlign:"center",left:-28}}>{processname[i]}</span>
          :
          processname[i] === "Data Aggregation/Transformation"?
          <span style={{position:"relative",textAlign:"center",left:-62}}>{processname[i]}</span>
                   
         : <span style={{position:"relative",textAlign:"center",left:-35}}>{processname[i]}</span>
       
          }
      {/* <span style={{position:"relative"}}>{processname[i]}</span> */}
      </div>
      
        {/* style={{marginLeft:"14vw",marginTop:"0px",marginBottom:"0px"}} */}
          {/* <div style={{marginLeft:"0px",marginTop:"0px",marginBottom:"0px"}}>{constructCarousel(data)}</div> */}
        
          </>
          // </Step>
        );}
        
        else{
          k.push(
<div style={{display:"block",    marginTop: "10px"}}>
<div style={{display:"flex",justifyContent:"center",alignItems:"center",width:"20px",marginTop: "-9px",
    }}>
      
      <div className="circle" style={{backgroundColor:"grey",transform:active==i?"scale(1.2)":null,}}id={"id-"+processname[i]}  >
      </div>
      
      
      </div>
      <div style={{height:"9px",margin:"0",padding:"0"}}></div>
      <span style={{position:"relative",left:"-35%"}}>{processname[i]}</span>
</div>
  )
        }
      }
      
      return k;
    })()} 
      {/* </Stepper>
       */}
       </div>}
    
      
     
    {/* <Carousel data={appsdata[name].data}/> */}
    {/* <div style={{display:"flex",gap:"3vw",justifyContent:"center",alignItems:"center",marginTop:"40px"}}>
   {appsdata[name].data.map((item)=> <a
          className="flexible-card-live"
          // style={theme(watchtower).liveDiv}
          
          href={item.link}
          target="_blank"
        >
          <div className="content">
            <img className="image" src={item.image}></img>
            <br></br>
            <div className="appname" style={{fontFamily:"sans-serif"}}>{item.app}</div>
          </div>
        </a>)}
        </div> */}
      {/* {notShow?
      <div style={{display:"flex",width:"100%",height:"auto",padding:"10px",justifyContent:"center",alignItems:"center",zIndex:"10000"}} >
      {(() => {
        const k = [];
        
           k.push(
            <div style={{display:"flex",gap:"3vw",justifyContent:"flex-start",alignItems:"center",marginTop:"5%",marginLeft:"5%"}}>
 
 {newExampledata[name]["nothing"].map((item)=>  <div className="flexible-card-notlive">
            <img
              className="image"
              src={item.image}
            ></img>
            <div style={{ paddingTop: "25px" }}>{item.app}</div> */}
            {/* <div>(Coming Soon)</div> */}
          {/* </div>)} */}
      {/* </div>
           )
         */}
        {/* return k;
        })()}  */}
        {/* </div>:  <div style={{display:"flex",width:"100%",height:"auto",padding:"10px",justifyContent:"flex-start",alignItems:"center",zIndex:"1000"}} >
        {(() => {
          const k = [];
          if(name2){
             k.push(
              <div style={{display:"flex",gap:"3vw",justifyContent:"flex-start",alignItems:"center",marginTop:"5%",marginLeft:"5%"}}>
   {newExampledata[name][name2].map((item)=> <a
          className="flexible-card-live" */}
          {/* // style={theme(watchtower).liveDiv} */}
{/*           
          href={item.link}
          target="_blank"
        >
          <div className="content">
            <img className="image" src={item.image}></img>
            <br></br>
            <div className="appname" style={{fontFamily:"sans-serif"}}>{item.app}</div>
          </div>
        </a>)}
        </div>
             )
          }
          return k;
          })()}  */}
          {/* </div>} */}
        </div>
    </>
  );
};
export default NewTheme;