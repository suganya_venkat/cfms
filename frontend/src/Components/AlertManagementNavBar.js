import './Navbar.css'
import React, {useState} from 'react'
import { useLocation } from 'react-router-dom';


import {NavLink} from 'react-router-dom'

import HomeIcon from '@mui/icons-material/Home';
import Configuration_icon from '../Pages/Configuration/Adjuster'

function AlertManagementNavBar() {
    // const [icon , setIcon] =useState(false)
    // const handleClick = () =>{
    //     setIcon (!icon)
    // }
    // const closeSideDrawer = () =>{
    //     setIcon(false)
    // }

    const { pathname } = useLocation();
    return (
       <>
       <nav className='navbar'>
       <p  style={{whiteSpace: "nowrap",fontWeight:700}} className='nav-links' >
              Claims Fraud Management
                </p>
         
       <NavLink to = '/Configuration_icon' className='nav-links' >
                <HomeIcon/>
                </NavLink>
           {/* <NavLink to = '/' className = 'nav-logo-landing'>
          
           Logo
           </NavLink> */}
           
           {/* <div className='menu-icon' onClick={handleClick}>
                {
                    icon ? <FaTimes className = 'fa-times' ></FaTimes> :
                    <FaBars className = 'fa-bars'></FaBars>
                }
           </div> */}
           <ul className= 
                'nav-menu'>
            
            <li>
                <NavLink to = '/AlertManagement'  className='nav-links' activeClassName="is-active" isActive={() => ['/ClaimsDetails','/AlertManagement'].includes(pathname)} >
               Alert Management
                </NavLink>
           </li>
{/*            
           <li>
                <NavLink to = '/Summary'   className='nav-links' activeClassName="is-active" isActive={() => ['/Summary', '/Investigation_overview',"/","/Claims_Forecast","/InvestigationOverview"].includes(pathname)} > 
                Reporting
                </NavLink>
           </li>
           <li>
                <NavLink to = '/BusinessRule'   className='nav-links' activeClassName="is-active" isActive={() => ['/Rules', '/StructureData_insights','/Strategy','/RuleBank','/StructureData_insights', '/UnstructureModel_insights','/AnamolyPage','/BusinessRule'].includes(pathname)} >
                   Analytics
                </NavLink>
           </li> */}
         
           {/* <li>
                <NavLink to = '/Notfound'  className='nav-links' activeClassName="is-active" >
               Forensic
                </NavLink>
           </li>
            */}
           </ul>
          
       </nav>
       </>
    )
}

export default AlertManagementNavBar
