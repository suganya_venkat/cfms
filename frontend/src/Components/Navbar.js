import './Navbar.css'
import React, {useState} from 'react'
import { useLocation } from 'react-router-dom';


import {NavLink} from 'react-router-dom'


function Navbar() {
    // const [icon , setIcon] =useState(false)
    // const handleClick = () =>{
    //     setIcon (!icon)
    // }
    // const closeSideDrawer = () =>{
    //     setIcon(false)
    // }
    const { pathname } = useLocation();

    return (
       <div>
       <nav className='rule_navbar'>
       {/* <NavLink to = '/' className='nav-links'  onClick={closeSideDrawer}>
                <HomeIcon/>
                </NavLink>
           <NavLink to = '/' className = 'nav-logo'> */}
           {/* <img src={bi2ilogo} alt="bi2i" width="500" height="600"/>  */}
           {/* Logo
           </NavLink> */}
           {/* <div className='menu-icon' onClick={handleClick}>
                {
                    icon ? <FaTimes className = 'fa-times' ></FaTimes> :
                    <FaBars className = 'fa-bars'></FaBars>
                }
           </div> */}
           <ul className= 'rulebank_nav-menu'>
              <li>
                <NavLink to = '/Summary' className='rule_nav-links' activeClassName="rule_is-active" isActive={() => ['/Summary',"/"].includes(pathname)}>
                    Claims Overview
                </NavLink>
            </li>
            {/* <li>
                <NavLink to = '/Investigation_overview' className='rule_nav-links' activeClassName="rule_is-active">
                   Investigation Overview
                </NavLink>
            </li> */}
            <li>
                <NavLink to = '/AIimpact' className='rule_nav-links' activeClassName="rule_is-active">
                   Investigation Overview
                </NavLink>
                {/* <NavLink to = '/InvestigationOverview' className='rule_nav-links' activeClassName="rule_is-active">
                   Investigation Overview
                </NavLink> */}
            </li>
            <li>
                <NavLink to = '/Claims_Forecast' className='rule_nav-links' activeClassName="rule_is-active">
                   Claims Forecast
                </NavLink>
            </li>
            {/* <li>
                <NavLink to = ' ' className='nav-links' activeClassName="is-active" onClick={closeSideDrawer}>
                <HomeIcon/>
                </NavLink>
            </li> */}
            
           {/* <li>
                <NavLink to = '/Alertmanage' onClick={closeSideDrawer} className='nav-links' activeClassName="is-active" >
                    Insights
                </NavLink>
            </li> */}
            {/* <li>
                <NavLink to = '/StructureData_insights' onClick={closeSideDrawer} className='nav-links' activeClassName="is-active" >
                    Structured Data Model Insights
                </NavLink>
           </li>
           
           <li>
                <NavLink to = '/UnstructureModel_insights' onClick={closeSideDrawer} className='nav-links' activeClassName="is-active" >
                Unstructured(OCR) Data Insights
                </NavLink>
           </li>
           <li>
                <NavLink to = '/SignIn' onClick={closeSideDrawer} className='nav-links' activeClassName="is-active">
                    Sign in
                </NavLink>
           </li> */}
           </ul>
          
       </nav>
       </div>
    )
}

export default Navbar
