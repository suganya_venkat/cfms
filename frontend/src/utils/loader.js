/*import React from "react";
import Spinner from 'react-bootstrap/Spinner'

const SpinnerPage = () => {
  return (
    <>
    <div>
      <Spinner animation="border" variant="secondary" />
      </div>
    </>
  );
}

export default SpinnerPage;*/


/*import React from 'react';
import ReactLoading from 'react-loading';
 
const SpinnerPage = () => (
    <ReactLoading type="balls" color="#ffffff" height={'20%'} width={'20%'} />
);
 
export default SpinnerPage; */

import Spinner from 'react-spinner-material';
import React, { Component } from 'react';
// import "./linechart_paper.css";


export default class SpinnerPage extends Component {
  render() {
  return (
    <div className ="load">
        <center>
        <Spinner radius={35} color = "#808080" stroke={2} visible={true}  />
        </center>
        </div>
    );
  }
}